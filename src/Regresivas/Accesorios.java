
package Regresivas;

import Core.DeviceActions;
import Core.WebAction;
import Core.WebDriv;
import Core.Report;
import Operaciones.CargaVariables;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class Accesorios {
    
    public Accesorios(){
        
    }
    
    public void AccesoriosFlap(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, DeviceActions da,String Caso) throws InterruptedException, IOException 
    {
        String Accesorio = CargaVariables.LeerCSV(0, "Accesorios", Caso);
        String Nombre = CargaVariables.LeerCSV(1, "Accesorios", Caso);
        String ApellPa = CargaVariables.LeerCSV(2, "Accesorios", Caso);
        String ApellMa = CargaVariables.LeerCSV(3, "Accesorios", Caso);
        String Correo = CargaVariables.LeerCSV(4, "Accesorios", Caso);
        String Telefono = CargaVariables.LeerCSV(5, "Accesorios", Caso);
        String TelefonoAdic = CargaVariables.LeerCSV(6, "Accesorios", Caso);
        String Rfc = CargaVariables.LeerCSV(7, "Accesorios", Caso);
        String Cp = CargaVariables.LeerCSV(8, "Accesorios", Caso);
        String Calle = CargaVariables.LeerCSV(9, "Accesorios", Caso);
        String NumExt = CargaVariables.LeerCSV(10, "Accesorios", Caso);
        String NumInt = CargaVariables.LeerCSV(11, "Accesorios", Caso);
        String Colonia = CargaVariables.LeerCSV(12, "Accesorios", Caso);
        String CpEnvio = CargaVariables.LeerCSV(13, "Accesorios", Caso);
        String CalleEnvio = CargaVariables.LeerCSV(14, "Accesorios", Caso);
        String NumExtEnvio = CargaVariables.LeerCSV(15, "Accesorios", Caso);
        String NumIntEnvio = CargaVariables.LeerCSV(16, "Accesorios", Caso);
        String ColoniaEnvio = CargaVariables.LeerCSV(17, "Accesorios", Caso);
        String Referencia = CargaVariables.LeerCSV(18, "Accesorios", Caso);
        String NumTarjeta = CargaVariables.LeerCSV(19, "Accesorios", Caso);
        String MesTarjeta = CargaVariables.LeerCSV(20, "Accesorios", Caso);
        String AñoTarjeta = CargaVariables.LeerCSV(21, "Accesorios", Caso);
        String Cvv = CargaVariables.LeerCSV(22, "Accesorios", Caso);
        
        WebDrive.WebDriver.navigate().to(Url + "accesorios.html");
        reporte.AddImageToReport("Se ingresa a la pagina principal", OPG.SS());
        SelenElem.focus(By.xpath("//img[@alt='" + Accesorio + "']"));
        reporte.AddImageToReport("Seleccionar accesorio", OPG.SS());
        SelenElem.Click(By.xpath("//img[@alt='" + Accesorio + "']"));
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Ver detalle del accesorio", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.xpath("//p[.='Comprar']"));
        
        SelenElem.WaitForLoad(By.id("total-mobile"), 15);
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        SelenElem.Click(By.id("total-mobile"));
        Thread.sleep(2000);
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 15);
        SelenElem.TypeText(By.id("valid-nombre"), Nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), ApellPa);
        SelenElem.TypeText(By.id("valid-appelido-m"), ApellMa);
        SelenElem.TypeText(By.id("valid-email"), Correo);
        SelenElem.TypeText(By.id("telefono-contacto"), Telefono);
        SelenElem.TypeText(By.id("additionalContactPhone"), TelefonoAdic);
        da.hideKeyBoard();
        SelenElem.Click(By.id("recibeFactura"));
        reporte.AddImageToReport("Sección tus datos del checkout", OPG.SS());
        
        SelenElem.TypeText(By.id("valida-rfc"), Rfc);
        SelenElem.TypeText(By.id("codigo-postal-valida"), Cp);
        SelenElem.TypeText(By.id("calle-domicilio"), Calle);
        SelenElem.TypeText(By.id("numero-domicilio"), NumExt);
        SelenElem.TypeText(By.id("numero-interior"), NumInt);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("colonia"), Colonia);
        da.hideKeyBoard();
        SelenElem.Click(By.id("checkPrivacyOne"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("btn_continuar"));
        
        SelenElem.WaitForLoad(By.id("codigoPostal"), 15);
        SelenElem.Click(By.id("billingAddress"));
        SelenElem.TypeText(By.id("codigoPostal"), CpEnvio);
        SelenElem.TypeText(By.id("street"), CalleEnvio);
        SelenElem.TypeText(By.id("outer_number"), NumExtEnvio);
        SelenElem.TypeText(By.id("inner_number"), NumIntEnvio);
        da.hideKeyBoard();
        Thread.sleep(2000);
        reporte.AddImageToReport("Sección opciones de envío del checkout", OPG.SS());
        SelenElem.SelectOption(By.id("coloniaTwo"), ColoniaEnvio);
        SelenElem.TypeText(By.id("valida-additionalReference"), Referencia);
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        
        SelenElem.WaitForLoad(By.id("continuar-checkout-out"), 15);
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        
        SelenElem.Click(By.id("continuar-checkout-out"));
        
        SelenElem.WaitForLoad(By.id("imagen-cliente"), 15);
        reporte.AddImageToReport("Medio de pago Flap", OPG.SS());
        da.slide("Down", 2);
        SelenElem.focus(By.id("1"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.id("1"));
        SelenElem.WaitForLoad(By.id("contrato_108553_1"), 10);
        SelenElem.focus(By.id("contrato_108553_1"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.id("contrato_108553_1"));
        
        SelenElem.WaitForLoad(By.id("numTarjeta"), 0);
        SelenElem.TypeText(By.id("numTarjeta"), NumTarjeta);
        SelenElem.SelectOption(By.id("vigenciames"), MesTarjeta);
        SelenElem.SelectOption(By.id("vigenciaanio"), AñoTarjeta);
        SelenElem.TypeText(By.id("cvv2"), Cvv);
        da.hideKeyBoard();
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("continuar"));
        
        SelenElem.WaitForLoad(By.id("btnFinalizar"), 10);
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        
       SelenElem.WaitForLoad(By.id("numero_portar"), 20);
        reporte.AddImageToReport("Pagina de pago realizada con exito", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        
        
        
    }
    
}
