      
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class ErrorCPPortabilidadPrepagoTerminalSIMConRecarga {
    
    public ErrorCPPortabilidadPrepagoTerminalSIMConRecarga(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
        
        String dn = CargaVariables.LeerCSV(0, "Error CP Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Error CP Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Error CP Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Error CP Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Error CP Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String correo = CargaVariables.LeerCSV(5, "Error CP Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String tel = CargaVariables.LeerCSV(6, "Error CP Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String calle = CargaVariables.LeerCSV(8, "Error CP Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Error CP Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Error CP Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String cp = CargaVariables.LeerCSV(7, "Error CP Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Error CP Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Error CP Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String ine = CargaVariables.LeerCSV(14, "Error CP Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String año = CargaVariables.LeerCSV(16, "Error CP Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String mes = CargaVariables.LeerCSV(17, "Error CP Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String  dia= CargaVariables.LeerCSV(18, "Error CP Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String curp= CargaVariables.LeerCSV(19, "Error CP Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        //SelenElem.Click(By.linkText("Cámbiate a Movistar"));
        SelenElem.WaitForLoad(By.id("menu_list"), 15);
        OPG.SelectMenu("Cámbiate a Movistar");
        
        SelenElem.WaitFor(By.id("sc2-dn"));
       
        SelenElem.Click(By.id("sc2-dn"));
        SelenElem.TypeText(By.id("sc2-dn"),dn);
        reporte.AddImageToReport("Seleccionamos el flujo de Cámbiate a Movistar", OPG.SS());
        
        SelenElem.Click(By.id("sc2-goPortaStep2"));
        SelenElem.waitForLoadPage();
       
        SelenElem.WaitForLoad(By.id("option_prepaid"),10);
        SelenElem.Click(By.id("option_prepaid"));
       
        Thread.sleep(350);
        reporte.AddImageToReport("Seleccionamos Quiero comprar un prepago", OPG.ScreenShotElement());
        SelenElem.Click(By.id("sc2-goToPortability"));
       
        SelenElem.focus(By.id("plan2"));
        reporte.AddImageToReport("Seleccionar con Smartphone y continuar", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[5]/div/div[3]/a"));
        reporte.AddImageToReport("Dar clic en ver mas equipos", OPG.SS());

        WebDrive.WebDriver.navigate().to(Url + "telefonos.html/");
        
        reporte.AddImageToReport("Seleccionar un smartphone", OPG.ScreenShotElementFocus(By.xpath("//a[@href='" + Url + "iphone-xr-lte-amarillo-64gb-apple.html']")));
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + CargaVariables.LeerCSV(20,"Error CP Portabilidad Prepago Terminal + SIM Con Recarga",Caso) + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[2]/div/div[3]/div/div[4]/div[2]/div/div[2]/div[2]/div/a"));
        reporte.AddImageToReport("Ver detalle del Smartphone ", OPG.SS());
        SelenElem.focus(By.id("portabilidad"));
        reporte.AddImageToReport(" ", OPG.SS());
        SelenElem.focus(By.id("js_submit_miniparrilla_portabilidad_prepago"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("js_submit_miniparrilla_portabilidad_prepago"));
        SelenElem.WaitForLoad(By.id("portability_number"), 20);
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        SelenElem.focus(By.id("coupon_code"));
        reporte.AddImageToReport(" ", OPG.SS());
        SelenElem.focus(By.id("continueToCheckout"));
        reporte.AddImageToReport(" ", OPG.SS());
        
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 5);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.Click(By.id("checkPrivacyOne"));
        
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        SelenElem.focus(By.id("botonTusDatos"));
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        SelenElem.Click(By.id("botonTusDatos"));
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 15);
        
        SelenElem.Click(By.id("inv-mail2"));
        //OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("js_codigo_postal"), "066");
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        Thread.sleep(500);
        reporte.AddImageToReport("Validacion Error en codigo postal", OPG.ScreenShotElementInstant());
       
        
    }
}
