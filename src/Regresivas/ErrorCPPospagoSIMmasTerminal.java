/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class ErrorCPPospagoSIMmasTerminal {

    public ErrorCPPospagoSIMmasTerminal(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{

                 
        String nombre = CargaVariables.LeerCSV(2, "Error en Código Postal Pospago SIM + Terminal", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Error en Código Postal Pospago SIM + Terminal", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Error en Código Postal Pospago SIM + Terminal", Caso);
        String correo = CargaVariables.LeerCSV(5, "Error en Código Postal Pospago SIM + Terminal", Caso);
        String tel = CargaVariables.LeerCSV(6, "Error en Código Postal Pospago SIM + Terminal", Caso);
        String calle = CargaVariables.LeerCSV(8, "Error en Código Postal Pospago SIM + Terminal", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Error en Código Postal Pospago SIM + Terminal", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Error en Código Postal Pospago SIM + Terminal", Caso);
        String cp = CargaVariables.LeerCSV(7, "Error en Código Postal Pospago SIM + Terminal", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Error en Código Postal Pospago SIM + Terminal", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Error en Código Postal Pospago SIM + Terminal", Caso);
        String ine = CargaVariables.LeerCSV(14, "Error en Código Postal Pospago SIM + Terminal", Caso);
        
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Vemos los telefonos  ", OPG.SS());
        reporte.AddImageToReport("Se selecciona el telefono", OPG.ScreenShotElementFocus(By.xpath("//a[@href='" + Url + "iphone-xr-lte-amarillo-64gb-apple.html']")));
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + CargaVariables.LeerCSV(16,"Error en Código Postal Pospago SIM + Terminal",Caso) + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        
        SelenElem.waitForLoadPage();
        
        
        SelenElem.Click(By.id("soy-nuevo"));
        OPG.cargaAjax2();
        
        SelenElem.Click(By.id("js_next_mp_pospago"));
        
        Thread.sleep(1000);
        reporte.AddImageToReport("Ver detalle de Smartphone y seleccionar plan", OPG.SS());
        SelenElem.focus(By.id("js_submit_miniparrilla_pospago"));
        reporte.AddImageToReport("", OPG.SS());
       
        SelenElem.Click(By.className("js_mp-pospago__table-cell"));
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Ver detalle del Plan seleccionado", OPG.SS());
        SelenElem.Click(By.xpath("//div[.='Quiero este plan']"));
        
        SelenElem.Click(By.id("js_submit_miniparrilla_pospago"));
        
        SelenElem.WaitForLoad(By.id("continueToCheckout"), 10);
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElementFocus(By.id("continueToCheckout")));
        SelenElem.WaitForLoad(By.id("continueToCheckout"), 4);
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-email"), 10);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
         
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.focus(By.id("telefono-contacto"));
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), "1994");
        SelenElem.SelectOption(By.className("ui-datepicker-month"), "Mar");
        SelenElem.Click(By.linkText("3"));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("numero-domicilio-interior"), numInterior);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.focus(By.id("couponCode"));
        SelenElem.TypeText(By.id("couponCode"), "de43de4");
        SelenElem.Click(By.id("btn_continuar"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("cod"), 4);
        reporte.AddImageToReport("Sección consulta tu crédito del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("cod"), "1111");
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        SelenElem.Click(By.id("checkout-step2-radio-04"));
        SelenElem.Click(By.id("checkout-step2-radio-06"));
        SelenElem.Click(By.id("checkout-step2"));
        reporte.AddImageToReport("Sección consulta tu crédito del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("btn_continuar_2"));
        OPG.cargaAjax2();
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 2);
        SelenElem.Click(By.id("inv-mail2"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("js_codigo_postal"), "0660");
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        Thread.sleep(500);
        reporte.AddImageToReport("Error en codigo postal", OPG.ScreenShotElementInstant());
        
        

     }
    
    
}
