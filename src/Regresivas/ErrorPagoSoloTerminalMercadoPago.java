
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class ErrorPagoSoloTerminalMercadoPago {

     public ErrorPagoSoloTerminalMercadoPago (Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
       
       String dn = CargaVariables.LeerCSV(0, "Error en Forma de Pago Solo Terminal - Mercado Pago", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Error en Forma de Pago Solo Terminal - Mercado Pago", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Error en Forma de Pago Solo Terminal - Mercado Pago", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Error en Forma de Pago Solo Terminal - Mercado Pago", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Error en Forma de Pago Solo Terminal - Mercado Pago", Caso);
        String correo = CargaVariables.LeerCSV(5, "Error en Forma de Pago Solo Terminal - Mercado Pago", Caso);
        String tel = CargaVariables.LeerCSV(6, "Error en Forma de Pago Solo Terminal - Mercado Pago", Caso);
        String calle = CargaVariables.LeerCSV(8, "Error en Forma de Pago Solo Terminal - Mercado Pago", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Error en Forma de Pago Solo Terminal - Mercado Pago", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Error en Forma de Pago Solo Terminal - Mercado Pago", Caso);
        String cp = CargaVariables.LeerCSV(7, "Error en Forma de Pago Solo Terminal - Mercado Pago", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Error en Forma de Pago Solo Terminal - Mercado Pago", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Error en Forma de Pago Solo Terminal - Mercado Pago", Caso);
        String ine = CargaVariables.LeerCSV(14, "Error en Forma de Pago Solo Terminal - Mercado Pago", Caso);
        String rfcc = CargaVariables.LeerCSV(15, "Error en Forma de Pago Solo Terminal - Mercado Pago", Caso);
        String tarjeta = CargaVariables.LeerCSV(17, "Error en Forma de Pago Solo Terminal - Mercado Pago", Caso);
        String vencimiento = CargaVariables.LeerCSV(18, "Error en Forma de Pago Solo Terminal - Mercado Pago", Caso);
        String cvv = CargaVariables.LeerCSV(19, "Error en Forma de Pago Solo Terminal - Mercado Pago", Caso);

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        
        reporte.AddImageToReport("Se selecciona el telefono", OPG.ScreenShotElementFocus(By.xpath("//a[@href='" + Url + "iphone-xr-lte-amarillo-64gb-apple.html']")));
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + CargaVariables.LeerCSV(16,"Error en Forma de Pago Solo Terminal - Mercado Pago",Caso) + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        
        Thread.sleep(3000);
        SelenElem.focus(By.xpath("//*[@id=\"solo-smartphone__content\"]/form/button/p"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//*[@id=\"solo-smartphone__content\"]/form/button/p"));
        reporte.AddImageToReport("Resumen de compra ",OPG.SS() );
        SelenElem.focus(By.xpath("//*[@id=\"section-equipo\"]/div[2]/div[3]/a"));
        reporte.AddImageToReport("",OPG.SS() );
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 5);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        SelenElem.Click(By.id("checkPrivacyOne"));
        
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("recibeFactura"));
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.focus(By.id("btn_continuar"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("btn_continuar"));
        OPG.cargaAjax2();
        
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        OPG.cargaAjax2();
        
        SelenElem.TypeText(By.id("form-checkout__cardNumber"), "4075595716483764");
        SelenElem.Click(By.id("form-checkout__expirationDate"));
        SelenElem.TypeText(By.id("form-checkout__expirationDate"), vencimiento);
        SelenElem.Click(By.id("form-checkout__securityCode"));
        //SelenElem.TypeText(By.id("securityCode_mask"), cvv);
        SelenElem.Click(By.id("form-checkout__expirationDate"));
        SelenElem.TypeText(By.id("form-checkout__expirationDate"), vencimiento);
        SelenElem.Click(By.id("labelSameBillingAddress"));
        reporte.AddImageToReport("Sección “Medios de pago” pagar con Mercado Pago", OPG.SS());
        
        SelenElem.SelectOption(By.id("form-checkout__installments"), "12 mensualidades de $ 666.67 ($ 8,000.00)");
        Thread.sleep(1000);
        reporte.AddImageToReport("Sección “Medios de pago” pagar con Mercado Pago", OPG.SS());
        //reporte.AddImageToReport("Sección “Medios de pago” pagar con Mercado Pago", OPG.SS());
        
        //SelenElem.focus(By.id("continuar-checkout-out-validation-mp"));
        SelenElem.Click(By.id("form-checkout__submit"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Error forma de pago falta codigo cvv", OPG.SS());
        
        //////////////////////
              
              
        //SelenElem.TypeText(By.id("cardNumber"), "64");
        SelenElem.FindElementByID("form-checkout__cardNumber").clear();
        SelenElem.TypeText(By.id("form-checkout__cardNumber"), "40755957164837");
        SelenElem.TypeText(By.id("form-checkout__securityCode"), cvv);
        //SelenElem.FindElementByID("full-name").clear();
        SelenElem.Click(By.id("form-checkout__expirationDate"));
        SelenElem.TypeText(By.id("form-checkout__expirationDate"), vencimiento);
        SelenElem.Click(By.id("form-checkout__securityCode"));
        SelenElem.TypeText(By.id("form-checkout__securityCode"), cvv);
        SelenElem.Click(By.id("form-checkout__expirationDate"));
        SelenElem.TypeText(By.id("form-checkout__expirationDate"), vencimiento);
        //SelenElem.Click(By.id("labelSameBillingAddress"));
        //reporte.AddImageToReport("Sección “Medios de pago” pagar con Mercado Pago", OPG.SS());
        
        SelenElem.SelectOption(By.id("form-checkout__installments"), "12 mensualidades de $ 666.67 ($ 8,000.00)");
        Thread.sleep(1000);
        
        SelenElem.Click(By.id("form-checkout__submit"));
        Thread.sleep(500);
        reporte.AddImageToReport("Error forma de pago tarjeta no valida", OPG.SS());
        //reporte.AddImageToReport("Error forma de pago tarjeta sin mensualidades", OPG.SS());
        
        /*
        SelenElem.FindElementByID("cardNumber").clear();
        SelenElem.TypeText(By.id("cardNumber"), "40755957164837");
        //SelenElem.SelectOption(By.id("installments"), "9 mensualidades de $ 888.89 ($ 8,000.00)");
        reporte.AddImageToReport("Error forma de pago tarjeta no valida", OPG.SS());
        */
        
        SelenElem.FindElementByID("form-checkout__cardNumber").clear();
        SelenElem.TypeText(By.id("form-checkout__cardNumber"), tarjeta);
        SelenElem.Click(By.id("form-checkout__expirationDate"));
        SelenElem.TypeText(By.id("form-checkout__expirationDate"), vencimiento);
        SelenElem.Click(By.id("form-checkout__securityCode"));
        SelenElem.TypeText(By.id("form-checkout__securityCode"), cvv);
        SelenElem.Click(By.id("form-checkout__expirationDate"));
        SelenElem.TypeText(By.id("form-checkout__expirationDate"), vencimiento);
        
        SelenElem.Click(By.id("form-checkout__submit"));
        Thread.sleep(500);
        reporte.AddImageToReport("Error forma de pago sin parcialidades", OPG.SS());
        
        SelenElem.FindElementByID("form-checkout__cardholderName").clear();
        SelenElem.SelectOption(By.id("form-checkout__installments"), "12 mensualidades de $ 666.67 ($ 8,000.00)");
        Thread.sleep(1000);
        
        SelenElem.Click(By.id("form-checkout__submit"));
        Thread.sleep(500);
        reporte.AddImageToReport("Error forma de pago tarjeta sin nombre del titular", OPG.SS());
        
        Thread.sleep(1500);
        SelenElem.TypeText(By.id("form-checkout__cardholderName"), "Vianey Macias Nieves");
        SelenElem.FindElementByID("form-checkout__expirationDate").clear();
        SelenElem.TypeText(By.id("form-checkout__expirationDate"), "01/22");
        
        SelenElem.Click(By.id("form-checkout__submit"));
        Thread.sleep(500);
        reporte.AddImageToReport("Error forma de pago Fecha de tarjeta expirada", OPG.SS());
        
        Thread.sleep(1500);
        SelenElem.FindElementByID("form-checkout__expirationDate").clear();
        SelenElem.TypeText(By.id("form-checkout__expirationDate"), vencimiento);
        SelenElem.Click(By.id("labelSameBillingAddress"));
        SelenElem.Click(By.id("form-checkout__submit"));
        Thread.sleep(500);
        reporte.AddImageToReport("Error forma de pago Sin direccion", OPG.SS());
        
        //validar vencimiento 
        Thread.sleep(1500);
        SelenElem.FindElementByID("form-checkout__expirationDate").clear();
        SelenElem.Click(By.id("labelSameBillingAddress"));
        SelenElem.Click(By.id("form-checkout__submit"));
        Thread.sleep(500);
        reporte.AddImageToReport("Error forma de pago sin fecha de vencimiento", OPG.SS());
        //OPG.cargaAjax2();
        /*
        SelenElem.WaitForLoad(By.xpath("//*[@id=\"contenedor-card-desktop\"]/div[2]/div[1]/div[1]"), 30);
        reporte.AddImageToReport("Página de pago realizado con éxito", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"contenedor-card-desktop\"]/div[2]/div[2]/div[1]/p"));
        reporte.AddImageToReport("Página de pago realizado con éxito", OPG.SS());
       
        
        //*/

     }
}
