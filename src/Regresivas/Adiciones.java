
package Regresivas;

import Core.DeviceActions;
import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.FlapError;
import Operaciones.OpcionesEnvio;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class Adiciones {
    
    public Adiciones(){
    
    }

    public void AdicionesLogin(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
      
        String dn = CargaVariables.LeerCSV(0, "Adiciones", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Adiciones", Caso);
        String email = CargaVariables.LeerCSV(2, "Adiciones", Caso);
        
        WebDrive.WebDriver.navigate().to(Url + "adiciones");
        SelenElem.focus(By.id("dn_movistar"));
        reporte.AddImageToReport("Iniciamos en la pagina de adiciones", OPG.SS());
        
        SelenElem.TypeText(By.id("dn_movistar"), dn);
        Thread.sleep(4000);
        reporte.AddImageToReport("Ingresar el DN", OPG.SS());
        SelenElem.Click(By.id("btn_additional"));
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.TypeText(By.id("valida-rfc"), "1");
        reporte.AddImageToReport("Ingresar el rfc", OPG.SS());
        SelenElem.Click(By.id("btn_additional"));
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        reporte.AddImageToReport("Validar identidad", OPG.SS());
        
        SelenElem.ClickById(WebDrive.WebDriver,"continue-btn2");
        SelenElem.Click(By.xpath("//*[@id=\"continue-btn2\"]/div"));
        
        
        SelenElem.focus(By.xpath("//*[@id=\"js_setTabIndex0\"]/div/div[5]/div[1]/img"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//button[.='Contrata ahora']"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//button[.='Contrata ahora']"));
        SelenElem.focus(By.id("submitplanes"));
        reporte.AddImageToReport("Sin SVA", OPG.SS());
        SelenElem.Click(By.id("submitplanes"));
        reporte.AddImageToReport("Ver resumen de compra", OPG.SS());
        SelenElem.focus(By.id("continueToCheckout"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("continueToCheckout"));
        SelenElem.TypeText(By.id("valid-email"), email);
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        reporte.AddImageToReport("Sección tus datos del checkout", OPG.SS());
        SelenElem.Click(By.id("btn_continuar"));
        OPG.cargaAjax2();
        /*SelenElem.Click(By.id("inv-mail2"));
        
        SelenElem.WaitForLoad(By.id("cp"),15);
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        SelenElem.TypeText(By.id("cp"), "06600");
        SelenElem.Click(By.id("longlat"));
        OPG.cargaAjax2();
        SelenElem.Click(By.id("99090073cac"));
        SelenElem.focus(By.id("99090005cac"));
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());*/
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        SelenElem.focus(By.id("99090003cac"));
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        SelenElem.focus(By.id("99090028cac"));
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        SelenElem.focus(By.id("btn_continuar_2"));
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        
        SelenElem.Click(By.id("btn_continuar_2"));
        OPG.cargaAjax2();
        
        SelenElem.WaitForLoad(By.xpath("//*[@id=\"msmx-pospago\"]/aside/section[1]/h4"),10);
        SelenElem.focus(By.xpath("//*[@id=\"msmx-pospago\"]/aside/section[1]/h4"));
        reporte.AddImageToReport("Sección contrato del checkout", OPG.SS());
        SelenElem.Click(By.id("checkConsentimiento"));
        SelenElem.Click(By.id("checkTerms"));
        reporte.AddImageToReport("Sección contrato del checkout", OPG.SS());
        
        SelenElem.Click(By.id("end-step"));
        SelenElem.WaitForLoad(By.id("pay"), 10);
        SelenElem.focus(By.id("changetwo"));
        
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        SelenElem.focus(By.id("pay"));
        reporte.AddImageToReport("", OPG.SS());
        //SelenElem.Click(By.id("changeone"));
        
        SelenElem.ClickById(WebDrive.WebDriver,"pay");
        
        new Flap( reporte, OPG, SelenElem, WebDrive);
        SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"init-additions\"]/button"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[2]/div[2]/div[2]/div[3]/div/div/div/div/ul/li[2]/a"));
        reporte.AddImageToReport("", OPG.SS());
        
    }

    public void ErrorCP(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
      
        String dn = CargaVariables.LeerCSV(0, "Adiciones", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Adiciones", Caso);
        String email = CargaVariables.LeerCSV(2, "Adiciones", Caso);

        WebDrive.WebDriver.navigate().to(Url + "adiciones");
        SelenElem.focus(By.id("dn_movistar"));
        reporte.AddImageToReport("Iniciamos en la pagina de adiciones", OPG.SS());
        
        SelenElem.TypeText(By.id("dn_movistar"), dn);
        Thread.sleep(4000);
        reporte.AddImageToReport("Ingresar el DN", OPG.SS());
        SelenElem.Click(By.id("btn_additional"));
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.TypeText(By.id("valida-rfc"), "1");
        reporte.AddImageToReport("Ingresar el rfc", OPG.SS());
        SelenElem.Click(By.id("btn_additional"));
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        reporte.AddImageToReport("Validar identidad", OPG.SS());
        
        SelenElem.ClickById(WebDrive.WebDriver,"continue-btn2");
        SelenElem.Click(By.xpath("//*[@id=\"continue-btn2\"]/div"));
        
        
        SelenElem.focus(By.xpath("//*[@id=\"js_setTabIndex0\"]/div/div[5]/div[1]/img"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//button[.='Contrata ahora']"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//button[.='Contrata ahora']"));
        SelenElem.focus(By.id("submitplanes"));
        reporte.AddImageToReport("Sin SVA", OPG.SS());
        SelenElem.Click(By.id("submitplanes"));
        reporte.AddImageToReport("Ver resumen de compra", OPG.SS());
        SelenElem.focus(By.id("continueToCheckout"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("continueToCheckout"));
        SelenElem.TypeText(By.id("valid-email"), email);
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        reporte.AddImageToReport("Sección tus datos del checkout", OPG.SS());
        SelenElem.Click(By.id("btn_continuar"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("inv-mail2"), 15);
        
        SelenElem.Click(By.id("inv-mail2"));
        //OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("js_codigo_postal"), "066");
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        Thread.sleep(500);
        reporte.AddImageToReport("Validacion Error en codigo postal", OPG.ScreenShotElementInstant());
    }

     public void ErrorPago(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
       
        String dn = CargaVariables.LeerCSV(0, "Adiciones", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Adiciones", Caso);
        String email = CargaVariables.LeerCSV(2, "Adiciones", Caso);

        WebDrive.WebDriver.navigate().to(Url + "adiciones");
        SelenElem.focus(By.id("dn_movistar"));
        reporte.AddImageToReport("Iniciamos en la pagina de adiciones", OPG.SS());
        
        SelenElem.TypeText(By.id("dn_movistar"), dn);
        Thread.sleep(4000);
        reporte.AddImageToReport("Ingresar el DN", OPG.SS());
        SelenElem.Click(By.id("btn_additional"));
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.TypeText(By.id("valida-rfc"), "1");
        reporte.AddImageToReport("Ingresar el rfc", OPG.SS());
        SelenElem.Click(By.id("btn_additional"));
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        reporte.AddImageToReport("Validar identidad", OPG.SS());
        
        SelenElem.ClickById(WebDrive.WebDriver,"continue-btn2");
        SelenElem.Click(By.xpath("//*[@id=\"continue-btn2\"]/div"));
        
        
        SelenElem.focus(By.xpath("//*[@id=\"js_setTabIndex0\"]/div/div[5]/div[1]/img"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//button[.='Contrata ahora']"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//button[.='Contrata ahora']"));
        SelenElem.focus(By.id("submitplanes"));
        reporte.AddImageToReport("Sin SVA", OPG.SS());
        SelenElem.Click(By.id("submitplanes"));
        reporte.AddImageToReport("Ver resumen de compra", OPG.SS());
        SelenElem.focus(By.id("continueToCheckout"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("continueToCheckout"));
        SelenElem.TypeText(By.id("valid-email"), email);
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        reporte.AddImageToReport("Sección tus datos del checkout", OPG.SS());
        SelenElem.Click(By.id("btn_continuar"));
        OPG.cargaAjax2();
        /*SelenElem.Click(By.id("inv-mail2"));
        
        SelenElem.WaitForLoad(By.id("cp"),15);
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        SelenElem.TypeText(By.id("cp"), "06600");
        SelenElem.Click(By.id("longlat"));
        OPG.cargaAjax2();
        SelenElem.Click(By.id("99090073cac"));
        SelenElem.focus(By.id("99090005cac"));
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());*/
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        SelenElem.focus(By.id("99090003cac"));
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        SelenElem.focus(By.id("99090028cac"));
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        SelenElem.focus(By.id("btn_continuar_2"));
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        
        SelenElem.Click(By.id("btn_continuar_2"));
        OPG.cargaAjax2();
        
        SelenElem.WaitForLoad(By.xpath("//*[@id=\"msmx-pospago\"]/aside/section[1]/h4"),10);
        SelenElem.focus(By.xpath("//*[@id=\"msmx-pospago\"]/aside/section[1]/h4"));
        reporte.AddImageToReport("Sección contrato del checkout", OPG.SS());
        SelenElem.Click(By.id("checkConsentimiento"));
        SelenElem.Click(By.id("checkTerms"));
        reporte.AddImageToReport("Sección contrato del checkout", OPG.SS());
        
        SelenElem.Click(By.id("end-step"));
        SelenElem.WaitForLoad(By.id("pay"), 10);
        SelenElem.focus(By.id("changetwo"));
        
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        SelenElem.focus(By.id("pay"));
        reporte.AddImageToReport("", OPG.SS());
        //SelenElem.Click(By.id("changeone"));
        
        SelenElem.ClickById(WebDrive.WebDriver,"pay");
        
        new FlapError( reporte, OPG, SelenElem, WebDrive);
        
    }

     public void AdicionesFianza200(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, DeviceActions da, String Caso) throws InterruptedException, IOException
    {
        
        String DN = CargaVariables.LeerCSV(0, "Adiciones", Caso);
        String SkuPlan = CargaVariables.LeerCSV(1, "Adiciones", Caso);
        String Nombre = CargaVariables.LeerCSV(2, "Adiciones", Caso);
        String ApellPa = CargaVariables.LeerCSV(3, "Adiciones", Caso);
        String ApellMa = CargaVariables.LeerCSV(4, "Adiciones", Caso);
        String Telefono = CargaVariables.LeerCSV(5, "Adiciones", Caso);
        String Rfc = CargaVariables.LeerCSV(6, "Adiciones", Caso);
        String Correo = CargaVariables.LeerCSV(7, "Adiciones", Caso);
        String CodPostal = CargaVariables.LeerCSV(8, "Adiciones", Caso);
        String Domicilio = CargaVariables.LeerCSV(9, "Adiciones", Caso);
        String NumExt = CargaVariables.LeerCSV(10, "Adiciones", Caso);
        String Colonia = CargaVariables.LeerCSV(11, "Adiciones", Caso);
        String Tarjeta = CargaVariables.LeerCSV(12, "Adiciones", Caso);
        String Mes = CargaVariables.LeerCSV(13, "Adiciones", Caso);
        String Año = CargaVariables.LeerCSV(14, "Adiciones", Caso);
        String Cvv = CargaVariables.LeerCSV(15, "Adiciones", Caso);
 
        WebDrive.WebDriver.navigate().to(Url + "liferay.php");
        Thread.sleep(3000);
        
        SelenElem.SelectOption(By.id("flowCode"), "adiciones");
        SelenElem.Find(By.id("skuPlan")).clear();
        SelenElem.TypeText(By.id("skuPlan"), SkuPlan);
        SelenElem.Find(By.id("namePlan")).clear();
        SelenElem.Find(By.id("skuRecarga")).clear();
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.Find(By.id("skuDn")).clear();
        SelenElem.TypeText(By.id("skuDn"), DN);
        SelenElem.Find(By.id("monto")).clear();
        SelenElem.Find(By.id("rfc")).clear();
        
        reporte.AddImageToReport("Continuamos con la pagina principal", OPG.SS());
        Thread.sleep(3000);
        SelenElem.Click(By.name("Comprar"));
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        SelenElem.focus(By.id("firstCharacter"));
        da.hideKeyBoard();
        Thread.sleep(8000);
        reporte.AddImageToReport("Valida tu indentidad", OPG.SS());
        SelenElem.Click(By.id("continue-btn"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 15);
        SelenElem.TypeText(By.id("valid-nombre"), Nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), ApellPa);
        SelenElem.TypeText(By.id("valid-appelido-m"), ApellMa);
        SelenElem.TypeText(By.id("valid-email"), Correo);
        SelenElem.TypeText(By.id("telefono-contacto"), Telefono);
        //SelenElem.TypeText(By.id("valida-rfc"), Rfc);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del checkout", OPG.SS());
        
        SelenElem.Click(By.id("btn_continuar"));
        
        SelenElem.WaitForLoad(By.id("inv-mail3"), 15);
        Thread.sleep(5000);
        SelenElem.Click(By.id("inv-mail2"));
        SelenElem.Click(By.id("js_codigo_postal"));
        SelenElem.TypeText(By.id("js_codigo_postal"), CodPostal);
        da.hideKeyBoard();
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        Thread.sleep(3000);
        da.slide("Down", 1);
        SelenElem.Click(By.id("99090073cac"));
        reporte.AddImageToReport("Sección opciones de envío del checkout", OPG.ScreenShotElementInstant());
        SelenElem.focus(By.id("99090102cac"));
        reporte.AddImageToReport("", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("btn_continuar_2"));
        
        SelenElem.Click(By.id("checkConsentimiento"));
        SelenElem.Click(By.id("checkTerms"));
        //SelenElem.focus(By.id("titleStep-2"));
        da.slide("Up", 1);
        reporte.AddImageToReport("Contrato del checkout", OPG.SS());
        SelenElem.Click(By.id("end-step"));
        
        SelenElem.Click(By.id("total-mobile"));
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        SelenElem.Click(By.id("pay"));
        
        SelenElem.WaitForLoad(By.id("ui-accordion-itemsSale-header-0"), 15);
        SelenElem.focus(By.id("1"));
        da.slide("Down", 1);
        reporte.AddImageToReport("Medio de pago Flap", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("1"));
        
        SelenElem.WaitForLoad(By.id("numTarjeta"), 10);
        SelenElem.TypeText(By.id("numTarjeta"), Tarjeta);
        SelenElem.SelectOption(By.id("vigenciames"), Mes);
        SelenElem.SelectOption(By.id("vigenciaanio"), Año);
        SelenElem.TypeText(By.id("cvv2"), Cvv);
        da.hideKeyBoard();
        reporte.AddImageToReport("", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("continuar"));
        
        Thread.sleep(2000);
        da.slide("Down", 2);
        reporte.AddImageToReport("Información acerca del pago Flap", OPG.ScreenShotElementInstant());
        
        SelenElem.WaitForLoad(By.id("numero_portar"), 20);
        reporte.AddImageToReport("Página de pago realizada con exito ", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.ScreenShotElementInstant());
        
    }

     public void AdicionesFianza0(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, DeviceActions da, String Caso) throws InterruptedException, IOException
    {
        String DN = CargaVariables.LeerCSV(0, "Adiciones", Caso);
        String SkuPlan = CargaVariables.LeerCSV(1, "Adiciones", Caso);
        String Correo = CargaVariables.LeerCSV(2, "Adiciones", Caso);
        String CodPostal = CargaVariables.LeerCSV(3, "Adiciones", Caso);

        WebDrive.WebDriver.navigate().to(Url + "liferay.php");
        Thread.sleep(3000);
        
        SelenElem.SelectOption(By.id("flowCode"), "adiciones");
        SelenElem.Find(By.id("skuPlan")).clear();
        SelenElem.TypeText(By.id("skuPlan"), SkuPlan);
        SelenElem.Find(By.id("skuRecarga")).clear();
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.Find(By.id("skuDn")).clear();
        SelenElem.TypeText(By.id("skuDn"), DN);
        SelenElem.Find(By.id("monto")).clear();
        SelenElem.Find(By.id("rfc")).clear();
        reporte.AddImageToReport("Continuamos con la pagina principal", OPG.SS());
        Thread.sleep(3000);
        SelenElem.Click(By.name("Comprar"));
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
        Thread.sleep(3000);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        Thread.sleep(1000);
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        Thread.sleep(1000);
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        Thread.sleep(1000);
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        Thread.sleep(1000);
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        Thread.sleep(1000);
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        SelenElem.focus(By.id("firstCharacter"));
        Thread.sleep(3000);
        reporte.AddImageToReport("Valida tu indentidad", OPG.SS());
        SelenElem.Click(By.id("continue-btn"));
        
        SelenElem.WaitForLoad(By.id("valid-email"), 10);
        SelenElem.Click(By.id("valid-email"));
        SelenElem.TypeText(By.id("valid-email"), Correo);
        reporte.AddImageToReport("Sección datos del checkout", OPG.SS());
        SelenElem.Click(By.id("btn_continuar"));
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 15);
        SelenElem.Click(By.id("inv-mail2"));
        SelenElem.Click(By.id("js_codigo_postal"));
        SelenElem.TypeText(By.id("js_codigo_postal"), CodPostal);
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        
        SelenElem.WaitForLoad(By.id("js_geo_option_cards"), 15);
        SelenElem.ClickButton(WebDrive.WebDriver, SelenElem, "ClassName", "geo-cac-elocker-option__place-title");
        reporte.AddImageToReport("Secciones envío del checkout", OPG.SS());
        SelenElem.Click(By.id("btn_continuar_2"));
        
        SelenElem.Click(By.id("checkConsentimiento"));
        SelenElem.Click(By.id("checkTerms"));
        reporte.AddImageToReport("Seccion contrato del checkout", OPG.SS());
        SelenElem.Click(By.id("end-step"));
        
        SelenElem.Click(By.id("pay"));
        Thread.sleep(5000);
        
        reporte.AddImageToReport("Pago realizado con exito", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        
    }
}
