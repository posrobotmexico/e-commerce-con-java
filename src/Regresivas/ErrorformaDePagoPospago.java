
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.FlapError;
import Operaciones.FlapErrorPospago;
import Operaciones.OpcionesEnvio;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class ErrorformaDePagoPospago {

    public ErrorformaDePagoPospago(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
         
        String dn = CargaVariables.LeerCSV(0, "Error en la forma de pago Pospago", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Error en la forma de pago Pospago", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Error en la forma de pago Pospago", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Error en la forma de pago Pospago", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Error en la forma de pago Pospago", Caso);
        String correo = CargaVariables.LeerCSV(5, "Error en la forma de pago Pospago", Caso);
        String tel = CargaVariables.LeerCSV(6, "Error en la forma de pago Pospago", Caso);
        String calle = CargaVariables.LeerCSV(8, "Error en la forma de pago Pospago", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Error en la forma de pago Pospago", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Error en la forma de pago Pospago", Caso);
        String cp = CargaVariables.LeerCSV(7, "Error en la forma de pago Pospago", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Error en la forma de pago Pospago", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Error en la forma de pago Pospago", Caso);
        String ine = CargaVariables.LeerCSV(14, "Error en la forma de pago Pospago", Caso);
        String rfcc = CargaVariables.LeerCSV(15, "Error en la forma de pago Pospago", Caso);

       WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Vemos los telefonos  ", OPG.SS());
        reporte.AddImageToReport("Se selecciona el telefono", OPG.ScreenShotElementFocus(By.xpath("//a[@href='" + Url + "iphone-xr-lte-amarillo-64gb-apple.html']")));
        Thread.sleep(1000);
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + CargaVariables.LeerCSV(16,"Error en la forma de pago Pospago",Caso) + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        
       reporte.AddImageToReport("Ver detalle de Smartphone y seleccionar plan", OPG.SS());
        SelenElem.waitForLoadPage();
        
        /////
        SelenElem.Click(By.id("soy-nuevo"));
        OPG.cargaAjax2();
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//*[@id=\"js_slider_mp_pospago\"]/div/div/div[5]/div/div/div/div"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//div[.='Quiero este plan']"));
        
        Thread.sleep(2000);
        SelenElem.focus(By.id("js_submit_miniparrilla_pospago"));
        Thread.sleep(1000);
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("js_submit_miniparrilla_pospago"));
        SelenElem.WaitForLoad(By.id("continueToCheckout"), 10);
        
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElementFocus(By.id("continueToCheckout")));
        SelenElem.WaitForLoad(By.id("continueToCheckout"), 4);
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.waitForLoadPage();
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        
         reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.focus(By.id("telefono-contacto"));
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), "1993");
        SelenElem.SelectOption(By.className("ui-datepicker-month"), "Mar");
        SelenElem.Click(By.linkText("3"));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("numero-domicilio-interior"), numInterior);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.focus(By.id("couponCode"));
        SelenElem.TypeText(By.id("couponCode"), "de43de4");
        SelenElem.Click(By.id("btn_continuar"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("cod"), 14);
        SelenElem.TypeText(By.id("cod"), "1111");
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        SelenElem.Click(By.id("checkout-step2-radio-04"));
        SelenElem.Click(By.id("checkout-step2-radio-06"));
        SelenElem.Click(By.id("checkout-step2"));
        reporte.AddImageToReport("Sección consulta tu crédito del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("btn_continuar_2"));
        OPG.cargaAjax2();
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        /*
        SelenElem.Click(By.id("inv-mail2"));
        SelenElem.focus(By.id("cp"));
        SelenElem.WaitForLoad(By.id("cp"), 4);
        OPG.cargaAjax2();
        
        SelenElem.TypeText(By.id("cp"), "06600");
        SelenElem.Click(By.id("longlat"));
        OPG.cargaAjax2(); 
        
        SelenElem.Click(By.id("99090005cac"));
        SelenElem.focus(By.id("99090005cac"));
        
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        */
        SelenElem.Click(By.id("btn_continuar_3"));
        OPG.cargaAjax2();
        SelenElem.Click(By.id("checkout-step4-check-01"));
        SelenElem.Click(By.id("checkTerms"));
        reporte.AddImageToReport("Sección contrato del checkout", OPG.SS());
        SelenElem.Click(By.id("end-step"));
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        
        //SelenElem.Click(By.id("continuar-checkout-out"));
        SelenElem.ClickById(WebDrive.WebDriver, "continuar-checkout-out");
        
        OPG.cargaAjax2();
        SelenElem.ClickById(WebDrive.WebDriver, "continuar-checkout-out");
        
        new FlapErrorPospago( reporte, OPG, SelenElem, WebDrive);
       
     }
    
}
