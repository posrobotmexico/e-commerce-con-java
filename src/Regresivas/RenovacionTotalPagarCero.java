
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.OperacionesGenerales;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class RenovacionTotalPagarCero {

     public RenovacionTotalPagarCero(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException{
         
        String dn = CargaVariables.LeerCSV(0, "Renovación Total a Pagar $0", Caso);
        String phone = CargaVariables.LeerCSV(1,"Renovación Total a Pagar $0",Caso);
        String correo = CargaVariables.LeerCSV(2, "Renovación Total a Pagar $0", Caso);
        String telopcional = CargaVariables.LeerCSV(4, "Renovación Total a Pagar $0", Caso);
        String identificacion = CargaVariables.LeerCSV(5,"Renovación Total a Pagar $0",Caso);
        String nombre = CargaVariables.LeerCSV(6,"Renovación Total a Pagar $0",Caso);
        String tarjeta = CargaVariables.LeerCSV(7,"Renovación Total a Pagar $0",Caso);
        String expiracion = CargaVariables.LeerCSV(8,"Renovación Total a Pagar $0",Caso);
        String cvv = CargaVariables.LeerCSV(9,"Renovación Total a Pagar $0",Caso);
        

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal ", OPG.SS());
        SelenElem.Click(By.id("boton_smartphone"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar el flujo Smartphones > Todas las marcas", OPG.SS());

        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        reporte.AddImageToReport("Una vez en esta seccion se selecciona la opcion Renovacion", OPG.SS());
        
        SelenElem.Click(By.xpath("//span[.='Renovación']"));
        
        reporte.AddImageToReport("Seleccionar Categoría Renovación", OPG.SS());
        
        reporte.AddImageToReport("Seleccionar Smartphone, ver detalle y continuar", OPG.SS());
        SelenElem.WaitForLoad(By.xpath("//img[contains(@alt, '" + phone + "')]"),10);
        SelenElem.focus(By.xpath("//img[contains(@alt, '" + phone + "')]"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + phone + "')]"));
        
        
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        reporte.AddImageToReport("Ver detalle del Smartphone", OPG.SS());
        SelenElem.focus(By.id("js_dn_input_soy_cliente"));
        SelenElem.Click(By.id("js_dn_input_soy_cliente"));
        SelenElem.TypeText(By.id("js_dn_input_soy_cliente"), dn);
        reporte.AddImageToReport("Ver detalle de Smartphone e Ingresar DN", OPG.SS());
        
        SelenElem.Click(By.id("js_input_dn_button_soy_cliente"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.xpath("//span[.='Continuar']"), 20);
        reporte.AddImageToReport("Elegir plan Movistar", OPG.SS());
        SelenElem.Click(By.xpath("//span[.='Continuar']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Elegir plan Movistar", OPG.SS());
        SelenElem.focus(By.id("js_submit_miniparrilla_renovacion"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.id("js_submit_miniparrilla_renovacion"));
        
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 6);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        
        reporte.AddImageToReport("Validación de identidad", OPG.SS());
        Thread.sleep(15000);
        //SelenElem.Click(By.id("continue-btn"));
        SelenElem.focus(By.id("continue-btn"));
        SelenElem.ClickById(WebDrive.WebDriver,"continue-btn");
        
        Thread.sleep(4000);
        //SelenElem.Find(By.id("continue-btn")).click();
       
        
        SelenElem.waitForLoadPage();
        
        SelenElem.WaitForLoad(By.id("valid-email"), 20);
        //SelenElem.TypeText(By.id("valid-nombre"), CargaVariables.LeerCSV(1,"Renovaciones",caso));
        SelenElem.Click(By.id("valid-email"));
        SelenElem.TypeText(By.id("valid-email"), correo);
        
        SelenElem.Click(By.id("valid-phone"));
        SelenElem.TypeText(By.id("valid-phone"), dn);
        
        SelenElem.Click(By.id("INE-IFE"));
        SelenElem.TypeText(By.id("INE-IFE"), identificacion);
        reporte.AddImageToReport("Sección de datos del checkout", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("input-privacidad"));
        SelenElem.Click(By.id("input-terminos"));
        
        SelenElem.focus(By.id("btnStep1Principal"));
        reporte.AddImageToReport("Sección de datos del checkout", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("btnStep1Principal"));
        
        OPG.cargaAjax2();
        Thread.sleep(30000);
        
        SelenElem.WaitForLoad(By.id("inv-mail1"), 30);
        Thread.sleep(1000);
        SelenElem.focus(By.id("inv-mail1"));
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.ScreenShotElement());
        SelenElem.Click(By.id("checkout-paso3-renovaciones"));
        
        Thread.sleep(5000);
        SelenElem.WaitForLoad(By.id("input-terminos"), 30);
        //SelenElem.focus(By.id("input-terminos"));
        OPG.cargaAjax2();
        Thread.sleep(5000);
        SelenElem.Click(By.id("input-terminos"));
        SelenElem.Click(By.id("input-serie"));
        reporte.AddImageToReport("Sección Contrato del checkout", OPG.ScreenShotElement());
        SelenElem.Click(By.id("btnContract"));
        
        Thread.sleep(5000);
        SelenElem.WaitForLoad(By.id("profile-container"), 30);
        SelenElem.focus(By.id("profile-container"));
        Thread.sleep(1000);
        
        SelenElem.WaitForLoad(By.id("valid-card"), 30);
        Thread.sleep(1000);
        //SelenElem.Click(By.id("cardEfectivo"));
        Thread.sleep(1000);
        SelenElem.Click(By.id("valid-card"));
        SelenElem.TypeText(By.id("valid-card"),tarjeta);
        SelenElem.Click(By.id("valid-nombre-card"));
        SelenElem.TypeText(By.id("valid-nombre-card"),nombre);
        SelenElem.Click(By.id("valid-vence"));
        SelenElem.TypeText(By.id("valid-vence"),expiracion);
        SelenElem.Click(By.id("valid-cvc"));
        SelenElem.TypeText(By.id("valid-cvc"),cvv);
        reporte.AddImageToReport("Sección Pago del checkout, Ingresar Datos del Pago", OPG.ScreenShotElement());
        
        SelenElem.focus(By.id("btnPay"));
        reporte.AddImageToReport("Sección Pago del checkout, Ingresar Datos del Pago", OPG.ScreenShotElement());
        SelenElem.Click(By.id("btnPay"));
        
        
        OPG.cargaAjax2();
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        SelenElem.WaitForLoad(By.id("btnPayEfecCard"), 20);
        reporte.AddImageToReport("Confirmación de Pago ", OPG.ScreenShotElement());
        
        SelenElem.Click(By.id("btnPayEfecCard"));
        
        OPG.cargaAjax2();
        Thread.sleep(2000);
        SelenElem.WaitForLoad(By.id("continuar-checkout-out"), 20);
        Thread.sleep(3000);
        SelenElem.focus(By.xpath("//*[@id=\"requestCont\"]/div/div[3]/div[1]"));
        reporte.AddImageToReport("Detalle de la Renovación", OPG.ScreenShotElement());
        SelenElem.focus(By.id("continuar-checkout-out"));
        reporte.AddImageToReport("Detalle de la Renovación", OPG.ScreenShotElement());
        
        SelenElem.Click(By.id("continuar-checkout-out"));
        Thread.sleep(10000);
        SelenElem.WaitForLoad(By.id("OrderOnx"), 20);
        reporte.AddImageToReport("Success ", OPG.ScreenShotElement());
        
     }
    
}
