
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.FlapMeses;
import Operaciones.OpcionesEnvio;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class PrepagoSoloSIMTerminalRecarga {

     public PrepagoSoloSIMTerminalRecarga (Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
        
        String dn = CargaVariables.LeerCSV(0, "Prepago Solo SIM + Terminal + Recargaa", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Prepago Solo SIM + Terminal + Recarga", Caso);
        
        String nombre =         CargaVariables.LeerCSV(2, "Prepago Solo SIM + Terminal + Recarga", Caso);
        String apellidop =      CargaVariables.LeerCSV(3, "Prepago Solo SIM + Terminal + Recarga", Caso);
        String apellidom =      CargaVariables.LeerCSV(4, "Prepago Solo SIM + Terminal + Recarga", Caso);
        String correo =         CargaVariables.LeerCSV(5, "Prepago Solo SIM + Terminal + Recarga", Caso);
        String tel =            CargaVariables.LeerCSV(6, "Prepago Solo SIM + Terminal + Recarga", Caso);
        String calle =          CargaVariables.LeerCSV(8, "Prepago Solo SIM + Terminal + Recarga", Caso);
        String numdomicilio =   CargaVariables.LeerCSV(9, "Prepago Solo SIM + Terminal + Recarga", Caso);
        String numInterior =    CargaVariables.LeerCSV(10, "Prepago Solo SIM + Terminal + Recarga", Caso);
        String cp =             CargaVariables.LeerCSV(7, "Prepago Solo SIM + Terminal + Recarga", Caso);
        String ciudad =         CargaVariables.LeerCSV(13, "Prepago Solo SIM + Terminal + Recarga", Caso);
        String colonia =        CargaVariables.LeerCSV(12, "Prepago Solo SIM + Terminal + Recarga", Caso);
        String ine =            CargaVariables.LeerCSV(14, "Prepago Solo SIM + Terminal + Recarga", Caso);
        String rfcc =           CargaVariables.LeerCSV(15, "Prepago Solo SIM + Terminal + Recarga", Caso);
        String modTel=          CargaVariables.LeerCSV(16,"Prepago Solo SIM + Terminal + Recarga",Caso);

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "prepago/oferta");
        Thread.sleep(3000);
        reporte.AddImageToReport("Seleccionamos el flujo de Prepago", OPG.SS());
        
        SelenElem.Click(By.id("sin_numero"));
        reporte.AddImageToReport("Seleccionar quiero un número nuevo", OPG.SS());
        SelenElem.Click(By.id("btnArt"));
        
        SelenElem.Click(By.id("plan2"));
        reporte.AddImageToReport("Seleccionar con Smartphone y continuar", OPG.SS());
        Thread.sleep(2000);
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html/");
        
        reporte.AddImageToReport("Se selecciona el telefono", OPG.ScreenShotElementFocus(By.xpath("//a[@href='" + Url + "iphone-xr-lte-amarillo-64gb-apple.html']")));
        Thread.sleep(2000);
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + modTel + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        
        
        SelenElem.Click(By.id("soy-nuevo"));
        reporte.AddImageToReport("Ver detalle, seleccionar llévatelo con prepago y comprar ", OPG.SS());
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("levatelo_con_prepago_soy_nuevo"), 10);
        SelenElem.Click(By.xpath("//*[@id=\"soy-nuevo__content\"]/div[2]/div[2]/label"));
        Thread.sleep(1500);
        SelenElem.Click(By.xpath("//*[@id=\"soy-nuevo__content\"]/div[2]/div[2]/label"));
        
        Thread.sleep(3000);
        reporte.AddImageToReport("Ver detalle, seleccionar llévatelo con prepago y comprar ", OPG.SS());
        
        
        SelenElem.focus(By.id("js_submit_miniparrilla_prepago"));
        reporte.AddImageToReport("Ver detalle, seleccionar llévatelo con prepago y comprar ", OPG.SS());
        SelenElem.Click(By.id("js_submit_miniparrilla_prepago"));
        SelenElem.waitForLoadPage();
        /*
        SelenElem.focus(By.xpath("//*[@id=\"parrilla_planes_form_sim\"]/div[1]/div/div[1]/div[10]/button"));
        reporte.AddImageToReport("Elegir plan de recarga", OPG.SS());
        SelenElem.Click(By.xpath("//button[.='Lo quiero']"));*/
        Thread.sleep(3000);
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        SelenElem.WaitForLoad(By.id("continueToCheckout"), 4);
        
        SelenElem.focus(By.id("continueToCheckout"));
        reporte.AddImageToReport("", OPG.SS());
        
       Thread.sleep(3000);
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 5);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        SelenElem.Click(By.id("checkPrivacyOne"));
        
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        SelenElem.focus(By.id("btn_continuar"));
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        SelenElem.Click(By.id("btn_continuar"));
        
        OPG.cargaAjax2();
        
        /*SelenElem.WaitForLoad(By.id("inv-mail2"), 4);
        SelenElem.Click(By.id("inv-mail2"));
        SelenElem.WaitForLoad(By.id("cp"), 4);
        SelenElem.Click(By.id("cp"));
        SelenElem.TypeText(By.id("cp"), "06600");
        SelenElem.Click(By.id("longlat"));
        OPG.cargaAjax2(); 
        
        SelenElem.Click(By.id("99090073cac"));
        SelenElem.focus(By.id("99090005cac"));
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());*/
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        SelenElem.focus(By.id("99090003cac"));
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());
        
        SelenElem.focus(By.id("end-step"));
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("end-step"));
        //OPG.cargaAjax2(); 
        
        SelenElem.WaitForLoad(By.id("continuar-checkout-out"), 10);
        Thread.sleep(5000);
        SelenElem.focus(By.xpath("//*[@id=\"msmx-prepago\"]/div[2]/div/div[1]/div/h2"));
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        
        Thread.sleep(3000);
        SelenElem.focus(By.id("continuar-checkout-out"));
        
        //SelenElem.Click(By.id("continuar-checkout-out"));
        SelenElem.ClickById(WebDrive.WebDriver, "continuar-checkout-out");
        //SelenElem.Click(By.xpath("//*[@id=\"continuar-checkout-out\"]"));
        //Thread.sleep(3000);
        //OPG.cargaAjax2();
        
        new FlapMeses(reporte,OPG,SelenElem,WebDrive);
        
        SelenElem.WaitForLoad(By.id("numeroOrden"), 50);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/h2"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/section[3]"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/footer/a"));
        reporte.AddImageToReport("", OPG.SS());

    }
}
