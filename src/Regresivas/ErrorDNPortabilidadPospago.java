
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class ErrorDNPortabilidadPospago {

     public ErrorDNPortabilidadPospago(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
         
        String dn = CargaVariables.LeerCSV(0, "Error en el DN Portabilidad Pospago", Caso);
        
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        SelenElem.waitForLoadPage();
        Thread.sleep(3000);

        WebDrive.WebDriver.navigate().to(Url + "cambiate-a-movistar");
        reporte.AddImageToReport("Se ingresa a el flujo de Cámbiate a Movistar", OPG.SS());
        SelenElem.focus(By.xpath("//p[.='8 GB ']"));
        reporte.AddImageToReport("Se ingresa a el flujo de Cámbiate a Movistar", OPG.SS());
        SelenElem.Click(By.id("DN"));
        SelenElem.TypeText(By.id("DN"),dn);
        reporte.AddImageToReport(" Ingresar DN", OPG.SS());
        
        SelenElem.Click(By.id("portar_DN"));
        Thread.sleep(500);
        reporte.AddImageToReport("Error en el DN", OPG.ScreenShotElement());
     }
    
}
