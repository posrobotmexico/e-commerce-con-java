
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class MigracionSoloSIM {

     public MigracionSoloSIM(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
         
        String dn = CargaVariables.LeerCSV(0, "Migración Solo SIM", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Migración Solo SIM", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Migración Solo SIM", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Migración Solo SIM", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Migración Solo SIM", Caso);
        String correo = CargaVariables.LeerCSV(5, "Migración Solo SIM", Caso);
        String tel = CargaVariables.LeerCSV(6, "Migración Solo SIM", Caso);
        String calle = CargaVariables.LeerCSV(8, "Migración Solo SIM", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Migración Solo SIM", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Migración Solo SIM", Caso);
        String cp = CargaVariables.LeerCSV(7, "Migración Solo SIM", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Migración Solo SIM", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Migración Solo SIM", Caso);
        String ine = CargaVariables.LeerCSV(14, "Migración Solo SIM", Caso);
        String año = CargaVariables.LeerCSV(16, "Migración Solo SIM", Caso);
        String mes = CargaVariables.LeerCSV(17, "Migración Solo SIM", Caso);
        String  dia= CargaVariables.LeerCSV(18, "Migración Solo SIM", Caso);
       

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        SelenElem.waitForLoadPage();
        Thread.sleep(3000);
        //SelenElem.Click(By.linkText("Cámbiate a un plan"));
        //SelenElem.Click(By.xpath("//a[@title='Cámbiate a Movistar']"));
        SelenElem.WaitForLoad(By.id("menu_list"), 15);
        OPG.SelectMenu("Cámbiate a un plan");
        
        SelenElem.WaitFor(By.id("sc3-dn"));
       
        SelenElem.Click(By.id("sc3-dn"));
        SelenElem.TypeText(By.id("sc3-dn"),dn);
         reporte.AddImageToReport("Se selecciona el flujo de Cámbiate a un plan", OPG.SS());
        
        SelenElem.Click(By.id("sc3-goMigraStep2"));
        SelenElem.waitForLoadPage();
        SelenElem.WaitFor(By.id("sc3-dn"));
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        SelenElem.focus(By.id("firstCharacter"));
        reporte.AddImageToReport("Valida tu identidad", OPG.ScreenShotElement());
        SelenElem.Click(By.id("continue-btn"));
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("plan1"),10);
        SelenElem.Click(By.id("plan1"));
        SelenElem.focus(By.id("plan1"));
        reporte.AddImageToReport("Seleccionamos sin smartphone y continuar.", OPG.ScreenShotElement());
        SelenElem.focus(By.id("gtm-plans-continue-button"));
        reporte.AddImageToReport("Seleccionamos sin smartphone y continuar.", OPG.ScreenShotElement());
        
        SelenElem.Click(By.id("gtm-plans-continue-button"));
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Seleccionamos un plan", OPG.SS());
        SelenElem.Click(By.id("Plan-ilimitado-Plus"));
        reporte.AddImageToReport("", OPG.SS());
        Thread.sleep(1000);
        reporte.AddImageToReport("Sin SVA", OPG.SS());
        SelenElem.Click(By.id("submitplanes"));
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        SelenElem.focus(By.id("btn_continue_check"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("btn_continue_check"));
        
         SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-nombre"), 6);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), año);
        SelenElem.SelectOption(By.className("ui-datepicker-month"), mes);
        SelenElem.Click(By.linkText(dia));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("calleNumeroInterior"), numInterior);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.focus(By.id("paso1-pospago-porta"));
        reporte.AddImageToReport("", OPG.SS()); 
        
        SelenElem.Click(By.id("paso1-pospago-porta"));
        OPG.cargaAjax2();
        SelenElem.Click(By.id("checkTeminosCondiciones"));
        reporte.AddImageToReport("Sección Contrato del checkout", OPG.SS()); 
        SelenElem.Click(By.id("checkConsentimiento"));
        SelenElem.Click(By.id("checkConsentimientoRecarga"));

        SelenElem.Click(By.id("realizar-pago-2"));
        
        new Flap(reporte,OPG,SelenElem,WebDrive);
        
        SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/section[2]/dl/dt"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/section[2]/footer/a[1]"));
        reporte.AddImageToReport("", OPG.SS());

    }
}
