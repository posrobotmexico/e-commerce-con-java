/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.OperacionesArmatuPlan;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class ArmatuPlan {
    private String resultado;
    
    public ArmatuPlan()
    {
        
    }
    
    public void MovistarPersonalizado(Report reporte,OperacionesGenerales OPG,OperacionesArmatuPlan armatuplan,String DN,String Url,WebAction SelenElem,WebDriv WebDrive) throws InterruptedException, IOException
    {
        resultado = "Prueba fallida";
        
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos desde la pagina principal y seleccionamos planes", OPG.ScreenShotElement());
        //armatuplan.SeleccionarPlanes();
        SelenElem.WaitForLoad(By.id("menu_list"), 15);
        OPG.SelectMenu("Planes");
        
        armatuplan.IngresaNumero(DN);
        reporte.AddImageToReport("Ingresamos el numero y verificamos", OPG.ScreenShotElement());
        armatuplan.VerificarNumero();
        armatuplan.IngresaCodigo123();
        reporte.AddImageToReport("Ingresamos el código y damos click en verificar", OPG.ScreenShotElement());
        armatuplan.VerificarCodigo123();
        armatuplan.EligeCaracteristicas();
        reporte.AddImageToReport("Elegimos las caracteristicas", OPG.ScreenShotElement());
        armatuplan.ModulosAdicionales();
        reporte.AddImageToReport("Agregamos las caracteristicas adicionales", OPG.ScreenShotElement());
        armatuplan.VerResumendePlanes();
        reporte.AddImageToReport("Vemos el resumen de la compra", OPG.ScreenShotElement());
        armatuplan.VerResumendeCosto();
        reporte.AddImageToReport("Vemos el total a pagar y damos click en lo quiero!", OPG.ScreenShotElement());
        armatuplan.GenerarCompraPlan();
        reporte.AddImageToReport("Procedemos al checkout", OPG.ScreenShotElement());
        armatuplan.CompletarCheckout();
        OPG.cargaAjax();
        reporte.AddImageToReport("Llenamos el campo de beneficios", OPG.ScreenShotElement());
        armatuplan.PushButtonContinuar();
        OPG.cargaAjax();
        reporte.AddImageToReport("Aceptamos el contrato", OPG.ScreenShotElement());
        armatuplan.PushButtonContinuar2();
        OPG.cargaAjax();
        reporte.AddImageToReport("Llenamos los datos de la tarjeta y damos click en procesar orden", OPG.ScreenShotElement());
        armatuplan.ProcesarOrden();
        //OPG.cargaAjax2();
        reporte.AddImageToReport("Success", OPG.ScreenShotElementFocus(By.className("content_info")));
                
        resultado = "Prueba exitosa";
    }
    
    public void MovistarPersonalizado2(Report reporte,OperacionesGenerales OPG,OperacionesArmatuPlan armatuplan,String DN,String Url,WebAction SelenElem,WebDriv WebDrive) throws InterruptedException, IOException
    {
        resultado = "Prueba fallida";
        
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos desde la pagina principal y seleccionamos planes", OPG.ScreenShotElement());
        armatuplan.SeleccionarPlanes();
        armatuplan.IngresaNumero(DN);
        reporte.AddImageToReport("Ingresamos el numero y verificamos", OPG.ScreenShotElement());
        armatuplan.VerificarNumero();
        armatuplan.IngresaCodigo123();
        reporte.AddImageToReport("Ingresamos el código y damos click en verificar", OPG.ScreenShotElement());
        armatuplan.VerificarCodigo123();
        armatuplan.EligeCaracteristicas2();
        reporte.AddImageToReport("Elegimos las caracteristicas", OPG.ScreenShotElement());
        armatuplan.ModulosAdicionales();
        reporte.AddImageToReport("Agregamos las caracteristicas adicionales", OPG.ScreenShotElement());
        armatuplan.VerResumendePlanes();
        reporte.AddImageToReport("Vemos el resumen de la compra", OPG.ScreenShotElement());
        armatuplan.VerResumendeCosto();
        reporte.AddImageToReport("Vemos el total a pagar y damos click en lo quiero!", OPG.ScreenShotElement());
        armatuplan.GenerarCompraPlan();
        reporte.AddImageToReport("Procedemos al checkout", OPG.ScreenShotElement());
        armatuplan.CompletarCheckout();
        OPG.cargaAjax();
        reporte.AddImageToReport("Llenamos el campo de beneficios", OPG.ScreenShotElement());
        armatuplan.PushButtonContinuar();
        OPG.cargaAjax();
        reporte.AddImageToReport("Aceptamos el contrato", OPG.ScreenShotElement());
        armatuplan.PushButtonContinuar2();
        OPG.cargaAjax();
        reporte.AddImageToReport("Llenamos los datos de la tarjeta y damos click en procesar orden", OPG.ScreenShotElement());
        armatuplan.ProcesarOrden();
        //OPG.cargaAjax2();
        reporte.AddImageToReport("Success", OPG.ScreenShotElementFocus(By.className("content_info")));
                
        resultado = "Prueba exitosa";
    }
    
    public void CambioPlanDatosIlimitados(Report reporte,OperacionesGenerales OPG,OperacionesArmatuPlan armatuplan,String DN,String Url,WebAction SelenElem,WebDriv WebDrive) throws InterruptedException, IOException
    {
        resultado = "Prueba fallida";
        
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos desde la pagina principal y seleccionamos planes (Inferior)", OPG.ScreenShotElementFocus(By.id("change-planpost")));
        armatuplan.SeleccionarPlanesInferior();
        reporte.AddImageToReport("Seleccionamos el plan de gigas ilimitados ($415)", OPG.ScreenShotElementFocus(By.id("changeplan_pos_form")));
        armatuplan.SeleccionaPlanGigasIlimitados();
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementFocus(By.id("customplanHead")));
        armatuplan.IngresaNumero(DN);
        armatuplan.AceptarTerminosyCondiciones();
        reporte.AddImageToReport("Ingresamos el DN y damos click en ¡lo quiero!", OPG.ScreenShotElement());
        armatuplan.VerificarNumero();
        armatuplan.IngresaCodigo123();
        reporte.AddImageToReport("Ingresamos el código y damos click en verificar", OPG.ScreenShotElementFocus(By.id("otp-input")));
        armatuplan.VerificarCodigo123();
        reporte.AddImageToReport("Procedemos al checkout", OPG.ScreenShotElement());
        armatuplan.CompletarCheckout();
        OPG.cargaAjax();
        reporte.AddImageToReport("Llenamos el campo de beneficios", OPG.ScreenShotElement());
        armatuplan.PushButtonContinuar();
        OPG.cargaAjax();
        reporte.AddImageToReport("Aceptamos el contrato", OPG.ScreenShotElement());
        armatuplan.PushButtonContinuar2();
        OPG.cargaAjax();
        reporte.AddImageToReport("Llenamos los datos de la tarjeta y damos click en procesar orden", OPG.ScreenShotElement());
        armatuplan.ProcesarOrden();
        reporte.AddImageToReport("Success", OPG.ScreenShotElementFocus(By.className("content_info")));
        
        
        
        resultado = "Prueba exitosa";
    }

    public String getResultado() {
        return resultado;
    }
}