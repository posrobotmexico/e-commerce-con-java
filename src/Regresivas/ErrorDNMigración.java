/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class ErrorDNMigración {

     public ErrorDNMigración(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
       
        String dn = CargaVariables.LeerCSV(0, "Error en el DN Migración", Caso);
        

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        SelenElem.waitForLoadPage();
        Thread.sleep(3000);
        //SelenElem.Click(By.linkText("Cámbiate a un plan"));
        //SelenElem.Click(By.xpath("//a[@title='Cámbiate a Movistar']"));
        SelenElem.WaitForLoad(By.id("menu_list"), 15);
        OPG.SelectMenu("Cámbiate a un plan");
        
        SelenElem.WaitFor(By.id("sc3-dn"));
       
        SelenElem.Click(By.id("sc3-dn"));
        SelenElem.TypeText(By.id("sc3-dn"),dn);
         reporte.AddImageToReport("Se selecciona el flujo de Cámbiate a un plan", OPG.SS());
        
        SelenElem.Click(By.id("sc3-goMigraStep2"));
        Thread.sleep(500);
        reporte.AddImageToReport("Error en el DN ", OPG.SS());
        
     }
}
