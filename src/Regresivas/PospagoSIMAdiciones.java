
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class PospagoSIMAdiciones {

     public PospagoSIMAdiciones(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException
     {
          String dn = CargaVariables.LeerCSV(0, "Pospago solo SIM Adiciones", Caso);
          String rfc = CargaVariables.LeerCSV(1, "Pospago solo SIM Adiciones", Caso);
          String correo = CargaVariables.LeerCSV(2, "Pospago solo SIM Adicione", Caso);
          
          WebDrive.WebDriver.navigate().to(Url);
          reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
     
          WebDrive.WebDriver.navigate().to(Url + "adiciones");
          reporte.AddImageToReport("Ingresamos al flujo adiciones", OPG.SS());
          
          SelenElem.TypeText(By.id("dn_movistar"), dn);
          reporte.AddImageToReport("Ingresa DN", OPG.SS());
          OPG.cargaAjax2();
          
          SelenElem.Click(By.id("btn_additional"));
          SelenElem.TypeText(By.id("valida-rfc"), rfc);
          SelenElem.TypeText(By.id("valida-rfc"), "1");
          reporte.AddImageToReport("Ingresar RFC", OPG.SS());
          SelenElem.Click(By.id("btn_additional"));
          
          //SelenElem.TypeText(By.id("otp-input"), "123456");
          SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
          SelenElem.TypeText(By.id("firstCharacter"), "1");
          SelenElem.TypeText(By.id("secondCharacter"), "2");
          SelenElem.TypeText(By.id("thirdCharacter"), "3");
          SelenElem.TypeText(By.id("fourthCharacter"), "4");
          SelenElem.TypeText(By.id("fifthCharacter"), "5");
          SelenElem.TypeText(By.id("sixthCharacter"), "6");
          Thread.sleep(4000);
          reporte.AddImageToReport("Validar identidad", OPG.SS());
          SelenElem.Click(By.id("continue-btn2"));
          
          SelenElem.Click(By.id("Plan-ilimitado-Plus"));
          reporte.AddImageToReport("Elegir plan", OPG.SS());
          SelenElem.focus(By.id("submitplanes"));
          reporte.AddImageToReport("Sin SVA", OPG.SS());
          SelenElem.Click(By.id("submitplanes"));
          reporte.AddImageToReport("Resumen de compra", OPG.SS());
     
          SelenElem.Click(By.id("continueToCheckout"));
          SelenElem.waitForLoadPage();
          SelenElem.focus(By.id("valid-email"));
          SelenElem.Click(By.id("valid-email"));
          SelenElem.TypeText(By.id("valid-email"), "vmacias@practia.global");
          reporte.AddImageToReport("Sección tus datos del checkout", OPG.SS());
          
          SelenElem.Click(By.id("btn_continuar"));
          OPG.cargaAjax2();
          SelenElem.Click(By.id("inv-mail2"));
          SelenElem.TypeText(By.id("js_codigo_postal"), "06600");
          SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
          OPG.cargaAjax2();   
          SelenElem.Click(By.id("99090073cac"));
          SelenElem.focus(By.id("99090003cac"));
          reporte.AddImageToReport("Sección opciones de envio del checkout", OPG.SS());
          
          SelenElem.Click(By.id("btn_continuar_2"));
          OPG.cargaAjax2();   
          SelenElem.Click(By.id("checkConsentimiento"));
          SelenElem.Click(By.id("checkTerms"));
          SelenElem.focus(By.id("end-step"));
          reporte.AddImageToReport("Sección Contrato del checkout", OPG.SS());
          
          SelenElem.Click(By.id("end-step"));
          OPG.cargaAjax2();
          SelenElem.focus(By.xpath("//*[@id=\"content_collapse\"]/div[3]/p[1]/span"));
          reporte.AddImageToReport("Resumen de compra", OPG.SS());
          
          
          SelenElem.Click(By.id("pay"));
          OPG.cargaAjax2();
          new Flap( reporte, OPG, SelenElem, WebDrive);
          SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
          reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());

     }
}
