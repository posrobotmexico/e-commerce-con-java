
package Regresivas;

import Core.DeviceActions;
import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.FlapReno;
import Operaciones.OpcionesEnvio;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

/**
 *
 * @author LAPTOP-JORGE
 */
public class Renovaciones {
    
    public Renovaciones(){
    
    }

    public void ErrorINERenovacionLiferay(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
        String dn = CargaVariables.LeerCSV(0, "Renovaciones", Caso);
        String phone = CargaVariables.LeerCSV(1,"Renovaciones",Caso);
        String correo = CargaVariables.LeerCSV(2, "Renovaciones", Caso);
        String telopcional = CargaVariables.LeerCSV(4, "Renovaciones", Caso);
        String identificacion = CargaVariables.LeerCSV(5,"Renovaciones",Caso);
        String nombre = CargaVariables.LeerCSV(6,"Renovaciones",Caso);
        String tarjeta = CargaVariables.LeerCSV(7,"Renovaciones",Caso);
        String expiracion = CargaVariables.LeerCSV(8,"Renovaciones",Caso);
        String cvv = CargaVariables.LeerCSV(9,"Renovaciones",Caso);
        String skuterminal = CargaVariables.LeerCSV(10,"Renovaciones",Caso);
        

        WebDrive.WebDriver.navigate().to(Url + "liferay.php");
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        Thread.sleep(2000);
        SelenElem.SelectOption(By.id("flowCode"), "renovacion");
        SelenElem.Find(By.id("skuDn")).clear();
        SelenElem.TypeText(By.id("skuDn"), dn);
        SelenElem.Find(By.id("skuPlan")).clear();
        SelenElem.TypeText(By.id("skuPlan"), "0005_OB");
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.TypeText(By.id("skuTerminal"), skuterminal);
        SelenElem.Find(By.id("skuRecarga")).clear();
        SelenElem.Find(By.id("monto")).clear();
        reporte.AddImageToReport("Ingresamos los datos y damos clic en enviar", OPG.SS());
        SelenElem.Click(By.name("Comprar"));
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 30);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        da.hideKeyBoard();
        reporte.AddImageToReport("Validación de identidad", OPG.SS());
        Thread.sleep(5000);
        //SelenElem.Click(By.id("continue-btn"));
        
        SelenElem.ClickById(WebDrive.WebDriver,"continue-btn");
        
        Thread.sleep(4000);
        //SelenElem.Find(By.id("continue-btn")).click();
       
        SelenElem.WaitForLoad(By.xpath("//div[.='Resumen de mi compra']"), 30);
        Thread.sleep(2000);
        SelenElem.Click(By.xpath("//div[.='Resumen de mi compra']"));
        Thread.sleep(2000);
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección de datos del checkout", OPG.ScreenShotElementInstant());
        
        
         SelenElem.WaitForLoad(By.id("valid-email"), 20);
        //SelenElem.TypeText(By.id("valid-nombre"), CargaVariables.LeerCSV(1,"Renovaciones",caso));
        SelenElem.Click(By.id("valid-email"));
        SelenElem.TypeText(By.id("valid-email"), correo);
        
        SelenElem.Click(By.id("valid-phone"));
        SelenElem.TypeText(By.id("valid-phone"), dn);
        
        SelenElem.Find(By.id("INE-IFE")).clear();
        SelenElem.Click(By.id("INE-IFE"));
        SelenElem.TypeText(By.id("INE-IFE"), "23332a");
        SelenElem.Click(By.id("input-privacidad"));
        SelenElem.Click(By.id("input-terminos"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección de datos del checkout", OPG.ScreenShotElementInstant());
        
        SelenElem.focus(By.id("btnStep1Principal"));
        
        SelenElem.Click(By.id("btnStep1Principal"));
        Thread.sleep(700);
        reporte.AddImageToReport("Error INE 18 dígitos", OPG.ScreenShotElementFocus(By.id("INE-IFE")));
        
        
        
    }
     public void ErrorCPRenovacionLiferay(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
        String dn = CargaVariables.LeerCSV(0, "Renovaciones", Caso);
        String phone = CargaVariables.LeerCSV(1,"Renovaciones",Caso);
        String correo = CargaVariables.LeerCSV(2, "Renovaciones", Caso);
        String telopcional = CargaVariables.LeerCSV(4, "Renovaciones", Caso);
        String identificacion = CargaVariables.LeerCSV(5,"Renovaciones",Caso);
        String nombre = CargaVariables.LeerCSV(6,"Renovaciones",Caso);
        String tarjeta = CargaVariables.LeerCSV(7,"Renovaciones",Caso);
        String expiracion = CargaVariables.LeerCSV(8,"Renovaciones",Caso);
        String cvv = CargaVariables.LeerCSV(9,"Renovaciones",Caso);
        String skuterminal = CargaVariables.LeerCSV(10,"Renovaciones",Caso);
        

        WebDrive.WebDriver.navigate().to(Url + "liferay.php");
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        Thread.sleep(2000);
        SelenElem.SelectOption(By.id("flowCode"), "renovacion");
        SelenElem.Find(By.id("skuDn")).clear();
        SelenElem.TypeText(By.id("skuDn"), dn);
        SelenElem.Find(By.id("skuPlan")).clear();
        SelenElem.TypeText(By.id("skuPlan"), "0005_OB");
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.TypeText(By.id("skuTerminal"), skuterminal);
        SelenElem.Find(By.id("skuRecarga")).clear();
        SelenElem.Find(By.id("monto")).clear();
        reporte.AddImageToReport("Ingresamos los datos y damos clic en enviar", OPG.SS());
        SelenElem.Click(By.name("Comprar"));
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 30);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        da.hideKeyBoard();
        reporte.AddImageToReport("Validación de identidad", OPG.SS());
        Thread.sleep(5000);
        //SelenElem.Click(By.id("continue-btn"));
        
        SelenElem.ClickById(WebDrive.WebDriver,"continue-btn");
        
        Thread.sleep(4000);
        //SelenElem.Find(By.id("continue-btn")).click();
       
        SelenElem.WaitForLoad(By.xpath("//div[.='Resumen de mi compra']"), 30);
        Thread.sleep(2000);
        SelenElem.Click(By.xpath("//div[.='Resumen de mi compra']"));
        Thread.sleep(2000);
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección de datos del checkout", OPG.ScreenShotElementInstant());
        
        
         SelenElem.WaitForLoad(By.id("valid-email"), 20);
        //SelenElem.TypeText(By.id("valid-nombre"), CargaVariables.LeerCSV(1,"Renovaciones",caso));
        SelenElem.Click(By.id("valid-email"));
        SelenElem.TypeText(By.id("valid-email"), correo);
        
        SelenElem.Click(By.id("valid-phone"));
        SelenElem.TypeText(By.id("valid-phone"), dn);
        
        SelenElem.Click(By.id("INE-IFE"));
        SelenElem.TypeText(By.id("INE-IFE"), identificacion);
        SelenElem.Click(By.id("input-privacidad"));
        SelenElem.Click(By.id("input-terminos"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección de datos del checkout", OPG.ScreenShotElementInstant());
        
        SelenElem.focus(By.id("btnStep1Principal"));
        
        SelenElem.Click(By.id("btnStep1Principal"));
        
        SelenElem.WaitForLoad(By.id("checkout-step2-radio-01"), 30);
        da.slide("Up", 1);
        SelenElem.focus(By.id("checkout-step2-radio-01"));
        SelenElem.Click(By.id("checkout-step2-radio-01"));
        Thread.sleep(1500);
        SelenElem.Click(By.id("cod"));
        SelenElem.TypeText(By.id("cod"), "1111");
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        da.slide("Up", 1);
        reporte.AddImageToReport("Sección Consulta de crédito", OPG.SS());
        
        SelenElem.Click(By.id("input-buro"));
        da.slide("Down", 1);
        reporte.AddImageToReport("Sección Consulta de crédito", OPG.ScreenShotElement());
        SelenElem.Click(By.id("btnConsultaBuro"));
        
        Thread.sleep(1000);
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("inv-mail1"), 30);
        SelenElem.focus(By.id("inv-mail1"));
        Thread.sleep(5000);
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.ScreenShotElement());
        
        /*SelenElem.Click(By.id("inv-mail2"));
        SelenElem.WaitForLoad(By.id("cp"), 4);
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("cp"), "066");
        da.hideKeyBoard();
        SelenElem.Click(By.id("longlat"));
        Thread.sleep(800);
        reporte.AddImageToReport("Error CP", OPG.ScreenShotElementInstant());*/
        SelenElem.WaitForLoad(By.id("inv-mail2"), 2);
        SelenElem.Click(By.id("inv-mail2"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("js_codigo_postal"), "066");
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        Thread.sleep(500);
        reporte.AddImageToReport("Validacion Error en codigo postal", OPG.ScreenShotElementInstant());
        
        
        Thread.sleep(1000);
        
    }

    public void RenovacionLiferay(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
        String dn = CargaVariables.LeerCSV(0, "Renovaciones", Caso);
        String phone = CargaVariables.LeerCSV(1,"Renovaciones",Caso);
        String correo = CargaVariables.LeerCSV(2, "Renovaciones", Caso);
        String telopcional = CargaVariables.LeerCSV(4, "Renovaciones", Caso);
        String identificacion = CargaVariables.LeerCSV(5,"Renovaciones",Caso);
        String nombre = CargaVariables.LeerCSV(6,"Renovaciones",Caso);
        String tarjeta = CargaVariables.LeerCSV(7,"Renovaciones",Caso);
        String expiracion = CargaVariables.LeerCSV(8,"Renovaciones",Caso);
        String cvv = CargaVariables.LeerCSV(9,"Renovaciones",Caso);
        String skuterminal = CargaVariables.LeerCSV(10,"Renovaciones",Caso);
        

        WebDrive.WebDriver.navigate().to(Url + "liferay.php");
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        Thread.sleep(2000);
        SelenElem.SelectOption(By.id("flowCode"), "renovacion");
        SelenElem.Find(By.id("skuDn")).clear();
        SelenElem.TypeText(By.id("skuDn"), dn);
        SelenElem.Find(By.id("skuPlan")).clear();
        SelenElem.TypeText(By.id("skuPlan"), "0005_OB");
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.TypeText(By.id("skuTerminal"), skuterminal);
        SelenElem.Find(By.id("skuRecarga")).clear();
        SelenElem.Find(By.id("monto")).clear();
        reporte.AddImageToReport("Ingresamos los datos y damos clic en enviar", OPG.SS());
        SelenElem.Click(By.name("Comprar"));
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 30);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        da.hideKeyBoard();
        reporte.AddImageToReport("Validación de identidad", OPG.SS());
        Thread.sleep(5000);
        //SelenElem.Click(By.id("continue-btn"));
        
        SelenElem.ClickById(WebDrive.WebDriver,"continue-btn");
        
        Thread.sleep(4000);
        //SelenElem.Find(By.id("continue-btn")).click();
       
        SelenElem.WaitForLoad(By.xpath("//div[.='Resumen de mi compra']"), 30);
        Thread.sleep(2000);
        SelenElem.Click(By.xpath("//div[.='Resumen de mi compra']"));
        Thread.sleep(2000);
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección de datos del checkout", OPG.ScreenShotElementInstant());
        
        
         SelenElem.WaitForLoad(By.id("valid-email"), 20);
        //SelenElem.TypeText(By.id("valid-nombre"), CargaVariables.LeerCSV(1,"Renovaciones",caso));
        SelenElem.Click(By.id("valid-email"));
        SelenElem.TypeText(By.id("valid-email"), correo);
        
        SelenElem.Click(By.id("valid-phone"));
        SelenElem.TypeText(By.id("valid-phone"), dn);
        
        SelenElem.Click(By.id("INE-IFE"));
        SelenElem.TypeText(By.id("INE-IFE"), identificacion);
        SelenElem.Click(By.id("input-privacidad"));
        SelenElem.Click(By.id("input-terminos"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección de datos del checkout", OPG.ScreenShotElementInstant());
        
        SelenElem.focus(By.id("btnStep1Principal"));
        
        SelenElem.Click(By.id("btnStep1Principal"));
        
        SelenElem.WaitForLoad(By.id("checkout-step2-radio-01"), 30);
        da.slide("Up", 1);
        SelenElem.focus(By.id("checkout-step2-radio-01"));
        SelenElem.Click(By.id("checkout-step2-radio-01"));
        Thread.sleep(1500);
        SelenElem.Click(By.id("cod"));
        SelenElem.TypeText(By.id("cod"), "1111");
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        da.slide("Up", 1);
        reporte.AddImageToReport("Sección Consulta de crédito", OPG.SS());
        
        SelenElem.Click(By.id("input-buro"));
        da.slide("Down", 1);
        reporte.AddImageToReport("Sección Consulta de crédito", OPG.ScreenShotElement());
        SelenElem.Click(By.id("btnConsultaBuro"));
        
        Thread.sleep(1000);
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("inv-mail1"), 30);
        SelenElem.focus(By.id("inv-mail1"));
        Thread.sleep(5000);
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.ScreenShotElement());
        
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.ScreenShotElement());
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.ScreenShotElement());
        SelenElem.focus(By.id("checkout-paso3-renovaciones"));
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.ScreenShotElement());
        SelenElem.Click(By.id("checkout-paso3-renovaciones"));
        
        SelenElem.WaitForLoad(By.id("input-terminos"), 30);
        OPG.cargaAjax2();
        Thread.sleep(20000);
        da.slide("Up",5);
        reporte.AddImageToReport("Sección Contrato del checkout", OPG.SS());
        da.slide("Down", 3);
        
        
        SelenElem.Click(By.id("input-terminos"));
        SelenElem.Click(By.id("input-serie"));
        reporte.AddImageToReport("Sección Contrato del checkout", OPG.ScreenShotElement());
        SelenElem.Click(By.id("btnContract"));
        
        Thread.sleep(5000);
        SelenElem.WaitForLoad(By.id("profile-container"), 30);
        SelenElem.focus(By.id("profile-container"));
        Thread.sleep(1000);
        
        reporte.AddImageToReport("Sección Pago ", OPG.ScreenShotElement());
        Thread.sleep(1500);
        da.slide("Up",2);
        reporte.AddImageToReport("Sección Pago ", OPG.SS());
        
        SelenElem.Click(By.id("btnPay"));
        
        SelenElem.waitForLoadPage();
        Thread.sleep(5000);
        da.slide("Up",2);
        reporte.AddImageToReport("Confirmar pedido ", OPG.ScreenShotElement());
        SelenElem.focus(By.id("continuar-checkout-out"));
        reporte.AddImageToReport("Confirmar pedido ", OPG.ScreenShotElement());
        SelenElem.Click(By.id("continuar-checkout-out"));
        
        Thread.sleep(1000);
        
        
        new Flap(reporte,OPG,SelenElem,WebDrive);
        
        SelenElem.WaitForLoad(By.className("descripcion-orden-terminal-card-texto"),30);
        SelenElem.waitForLoadPage();                
               
        reporte.AddImageToReport("Página de pago realizado con éxito", OPG.ScreenShotElement());
        
        
    }

    public void RenovacionCeroLiferay(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
        String dn = CargaVariables.LeerCSV(0, "Renovaciones", Caso);
        String phone = CargaVariables.LeerCSV(1,"Renovaciones",Caso);
        String correo = CargaVariables.LeerCSV(2, "Renovaciones", Caso);
        String telopcional = CargaVariables.LeerCSV(4, "Renovaciones", Caso);
        String identificacion = CargaVariables.LeerCSV(5,"Renovaciones",Caso);
        String nombre = CargaVariables.LeerCSV(6,"Renovaciones",Caso);
        String tarjeta = CargaVariables.LeerCSV(7,"Renovaciones",Caso);
        String expiracion = CargaVariables.LeerCSV(8,"Renovaciones",Caso);
        String cvv = CargaVariables.LeerCSV(9,"Renovaciones",Caso);
        String skuterminal = CargaVariables.LeerCSV(10,"Renovaciones",Caso);
        

        WebDrive.WebDriver.navigate().to(Url + "liferay.php");
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        Thread.sleep(2000);
        SelenElem.SelectOption(By.id("flowCode"), "renovacion");
        SelenElem.Find(By.id("skuDn")).clear();
        SelenElem.TypeText(By.id("skuDn"), dn);
        SelenElem.Find(By.id("skuPlan")).clear();
        SelenElem.TypeText(By.id("skuPlan"), "0001_PP");
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.TypeText(By.id("skuTerminal"), skuterminal);
        SelenElem.Find(By.id("skuRecarga")).clear();
        SelenElem.Find(By.id("monto")).clear();
        reporte.AddImageToReport("Ingresamos los datos y damos clic en enviar", OPG.SS());
        SelenElem.Click(By.name("Comprar"));
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 30);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        da.hideKeyBoard();
        reporte.AddImageToReport("Validación de identidad", OPG.SS());
        Thread.sleep(5000);
        //SelenElem.Click(By.id("continue-btn"));
        
        SelenElem.ClickById(WebDrive.WebDriver,"continue-btn");
        
        Thread.sleep(4000);
        //SelenElem.Find(By.id("continue-btn")).click();
       
        SelenElem.WaitForLoad(By.xpath("//div[.='Resumen de mi compra']"), 30);
        Thread.sleep(2000);
        SelenElem.Click(By.xpath("//div[.='Resumen de mi compra']"));
        Thread.sleep(2000);
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección de datos del checkout", OPG.ScreenShotElementInstant());
        
        
         SelenElem.WaitForLoad(By.id("valid-email"), 20);
        //SelenElem.TypeText(By.id("valid-nombre"), CargaVariables.LeerCSV(1,"Renovaciones",caso));
        SelenElem.Click(By.id("valid-email"));
        SelenElem.TypeText(By.id("valid-email"), correo);
        
        SelenElem.Click(By.id("valid-phone"));
        SelenElem.TypeText(By.id("valid-phone"), dn);
        
        SelenElem.Click(By.id("INE-IFE"));
        SelenElem.TypeText(By.id("INE-IFE"), identificacion);
        SelenElem.Click(By.id("input-privacidad"));
        SelenElem.Click(By.id("input-terminos"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección de datos del checkout", OPG.ScreenShotElementInstant());
        
        SelenElem.focus(By.id("btnStep1Principal"));
        
        SelenElem.Click(By.id("btnStep1Principal"));
        
        Thread.sleep(2000);
        OPG.cargaAjax2();
        Thread.sleep(20000);
        SelenElem.WaitForLoad(By.id("inv-mail1"), 30);
        SelenElem.focus(By.id("inv-mail1"));
        Thread.sleep(5000);
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.ScreenShotElement());
        
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.ScreenShotElement());
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.ScreenShotElement());
        SelenElem.focus(By.id("checkout-paso3-renovaciones"));
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.ScreenShotElement());
        SelenElem.Click(By.id("checkout-paso3-renovaciones"));
        
        SelenElem.WaitForLoad(By.id("input-terminos"), 30);
        OPG.cargaAjax2();
        Thread.sleep(20000);
        da.slide("Up",5);
        reporte.AddImageToReport("Sección Contrato del checkout", OPG.SS());
        da.slide("Down", 3);
        
        
        SelenElem.Click(By.id("input-terminos"));
        SelenElem.Click(By.id("input-serie"));
        reporte.AddImageToReport("Sección Contrato del checkout", OPG.ScreenShotElement());
        SelenElem.Click(By.id("btnContract"));
        
        Thread.sleep(5000);
        SelenElem.WaitForLoad(By.id("profile-container"), 30);
        SelenElem.focus(By.id("profile-container"));
        Thread.sleep(1000);
        
        SelenElem.WaitForLoad(By.id("valid-card"), 30);
        Thread.sleep(1000);
        
        //SelenElem.Click(By.id("cardEfectivo"));
        Thread.sleep(1000);
        SelenElem.Click(By.id("valid-card"));
        SelenElem.TypeText(By.id("valid-card"),tarjeta);
        SelenElem.Click(By.id("valid-nombre-card"));
        SelenElem.TypeText(By.id("valid-nombre-card"),nombre);
        SelenElem.Click(By.id("valid-vence"));
        SelenElem.TypeText(By.id("valid-vence"),expiracion);
        SelenElem.Click(By.id("valid-cvc"));
        SelenElem.TypeText(By.id("valid-cvc"),cvv);
        da.hideKeyBoard();
        
        da.slide("Up", 4);
        reporte.AddImageToReport("Sección Pago del checkout, Ingresar Datos del Pago", OPG.ScreenShotElement());
        
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección Pago del checkout, Ingresar Datos del Pago", OPG.ScreenShotElement());
        
        da.slide("Down", 1);
        reporte.AddImageToReport("Sección Pago del checkout, Ingresar Datos del Pago", OPG.ScreenShotElement());
        
        SelenElem.Click(By.id("btnPay"));
        
        
        OPG.cargaAjax2();
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        SelenElem.WaitForLoad(By.id("btnPayEfecCard"), 20);
        Thread.sleep(6000);
        da.slide("Up", 3);
        reporte.AddImageToReport("Confirmación de Pago ", OPG.ScreenShotElement());
        da.slide("Down", 3);
        reporte.AddImageToReport("Confirmación de Pago ", OPG.ScreenShotElement());
        Thread.sleep(2000);
        
        SelenElem.Click(By.id("continuar-checkout-out"));
        Thread.sleep(10000);
        SelenElem.WaitForLoad(By.id("OrderOnx"), 20);
        reporte.AddImageToReport("Success ", OPG.ScreenShotElement());
        /*SelenElem.WaitForLoad(By.id("checkout-step2-radio-01"), 30);
        da.slide("Up", 1);
        SelenElem.focus(By.id("checkout-step2-radio-01"));
        SelenElem.Click(By.id("checkout-step2-radio-01"));
        Thread.sleep(1500);
        SelenElem.Click(By.id("cod"));
        SelenElem.TypeText(By.id("cod"), "1111");
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        da.slide("Up", 1);
        reporte.AddImageToReport("Sección Consulta de crédito", OPG.SS());
        
        SelenElem.Click(By.id("input-buro"));
        da.slide("Down", 1);
        reporte.AddImageToReport("Sección Consulta de crédito", OPG.ScreenShotElement());
        SelenElem.Click(By.id("btnConsultaBuro"));
        
        Thread.sleep(1000);
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("inv-mail1"), 30);
        SelenElem.focus(By.id("inv-mail1"));
        Thread.sleep(5000);
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.ScreenShotElement());
        
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.ScreenShotElement());
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.ScreenShotElement());
        SelenElem.focus(By.id("checkout-paso3-renovaciones"));
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.ScreenShotElement());
        SelenElem.Click(By.id("checkout-paso3-renovaciones"));
        
        SelenElem.WaitForLoad(By.id("input-terminos"), 30);
        OPG.cargaAjax2();
        Thread.sleep(20000);
        da.slide("Up",5);
        reporte.AddImageToReport("Sección Contrato del checkout", OPG.SS());
        da.slide("Down", 3);
        
        
        SelenElem.Click(By.id("input-terminos"));
        SelenElem.Click(By.id("input-serie"));
        reporte.AddImageToReport("Sección Contrato del checkout", OPG.ScreenShotElement());
        SelenElem.Click(By.id("btnContract"));
        
        Thread.sleep(5000);
        SelenElem.WaitForLoad(By.id("profile-container"), 30);
        SelenElem.focus(By.id("profile-container"));
        Thread.sleep(1000);
        
        reporte.AddImageToReport("Sección Pago ", OPG.ScreenShotElement());
        Thread.sleep(1500);
        da.slide("Up",2);
        reporte.AddImageToReport("Sección Pago ", OPG.SS());
        
        SelenElem.Click(By.id("btnPay"));
        
        SelenElem.waitForLoadPage();
        Thread.sleep(5000);
        da.slide("Up",2);
        reporte.AddImageToReport("Confirmar pedido ", OPG.ScreenShotElement());
        SelenElem.focus(By.id("continuar-checkout-out"));
        reporte.AddImageToReport("Confirmar pedido ", OPG.ScreenShotElement());
        SelenElem.Click(By.id("continuar-checkout-out"));
        
        Thread.sleep(1000);
        
        
        new Flap(reporte,OPG,SelenElem,WebDrive);
        
        SelenElem.WaitForLoad(By.className("descripcion-orden-terminal-card-texto"),30);
        SelenElem.waitForLoadPage();                
               
        reporte.AddImageToReport("Página de pago realizado con éxito", OPG.ScreenShotElement());
        */
        
    }

    public void RenovaciónTotalPagarCero(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
        String dn = CargaVariables.LeerCSV(0, "Renovaciones", Caso);
        String phone = CargaVariables.LeerCSV(1,"Renovaciones",Caso);
        String correo = CargaVariables.LeerCSV(2, "Renovaciones", Caso);
        String telopcional = CargaVariables.LeerCSV(4, "Renovaciones", Caso);
        String identificacion = CargaVariables.LeerCSV(5,"Renovaciones",Caso);
        String nombre = CargaVariables.LeerCSV(6,"Renovaciones",Caso);
        String tarjeta = CargaVariables.LeerCSV(7,"Renovaciones",Caso);
        String expiracion = CargaVariables.LeerCSV(8,"Renovaciones",Caso);
        String cvv = CargaVariables.LeerCSV(9,"Renovaciones",Caso);

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal ", OPG.SS());
        Thread.sleep(1000);
        SelenElem.Click(By.id("checkbox"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar el menu principal la opcion Smartphones", OPG.SS());
        
        SelenElem.Click(By.xpath("//input[@value='Smartphones']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar el flujo Smartphones > Todas las marcas", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        reporte.AddImageToReport("Una vez en esta seccion se selecciona la opcion Renovacion", OPG.SS());
        
        SelenElem.Click(By.xpath("//span[.='Renovación']"));
        
        reporte.AddImageToReport("Seleccionar Categoría Renovación", OPG.SS());
        
        reporte.AddImageToReport("Seleccionar Smartphone, ver detalle y continuar", OPG.SS());
        SelenElem.WaitForLoad(By.xpath("//img[contains(@alt, '" + phone + "')]"),10);
        SelenElem.focus(By.xpath("//img[contains(@alt, '" + phone + "')]"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + phone + "')]"));
        
        
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        reporte.AddImageToReport("Ver detalle del Smartphone", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Ver detalle del Smartphone", OPG.SS());
        SelenElem.focus(By.id("js_dn_input_soy_cliente"));
        SelenElem.Click(By.id("js_dn_input_soy_cliente"));
        SelenElem.TypeText(By.id("js_dn_input_soy_cliente"), dn);
        reporte.AddImageToReport("Ver detalle de Smartphone e Ingresar DN", OPG.SS());
        
        SelenElem.Click(By.id("js_input_dn_button_soy_cliente"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.xpath("//span[.='Continuar']"), 20);
        reporte.AddImageToReport("Elegir plan Movistar", OPG.SS());
        SelenElem.Click(By.xpath("//span[.='Continuar']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Elegir plan Movistar", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Elegir plan Movistar", OPG.SS());
        SelenElem.focus(By.id("js_submit_miniparrilla_renovacion"));
        //reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.id("js_submit_miniparrilla_renovacion"));
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 20);
        Thread.sleep(5000);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        
        da.hideKeyBoard();
        reporte.AddImageToReport("Validación de identidad", OPG.SS());
        Thread.sleep(15000);
        //SelenElem.Click(By.id("continue-btn"));
        SelenElem.focus(By.id("continue-btn"));
        SelenElem.ClickById(WebDrive.WebDriver,"continue-btn");
        
        Thread.sleep(4000);
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-email"), 10);
        
        SelenElem.Click(By.xpath("//*[@id=\"principalContainerReno\"]/div[2]/div[1]/div/div[2]"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Sección de datos del checkout", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección de datos del checkout", OPG.ScreenShotElementInstant());
        
        
        SelenElem.WaitForLoad(By.id("valid-email"), 10);
        //SelenElem.TypeText(By.id("valid-nombre"), CargaVariables.LeerCSV(1,"Renovaciones",caso));
        SelenElem.Click(By.id("valid-email"));
        SelenElem.TypeText(By.id("valid-email"), correo);
        
        SelenElem.Click(By.id("valid-phone"));
        SelenElem.TypeText(By.id("valid-phone"), dn);
        
        SelenElem.Click(By.id("INE-IFE"));
        SelenElem.TypeText(By.id("INE-IFE"), identificacion);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección de datos del checkout", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("input-privacidad"));
        SelenElem.Click(By.id("input-terminos"));
        
        SelenElem.focus(By.id("btnStep1Principal"));
        reporte.AddImageToReport("Sección de datos del checkout", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("btnStep1Principal"));
        
        OPG.cargaAjax2();
        Thread.sleep(30000);
        
        SelenElem.WaitForLoad(By.id("inv-mail1"), 30);
        SelenElem.focus(By.id("inv-mail1"));
        Thread.sleep(15000);
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.ScreenShotElement());
        //SelenElem.Click(By.id("checkout-paso3-renovaciones"));
        SelenElem.WaitForLoad(By.id("inv-mail2"), 2);
          SelenElem.Click(By.id("inv-mail2"));
          //OPG.cargaAjax2();
          SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
          Thread.sleep(2000);
          SelenElem.TypeText(By.id("js_codigo_postal"), "06600");
          SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
          SelenElem.ClickById(WebDrive.WebDriver,"js_search_cacs_by_postal_code");
          OPG.cargaAjax2();   
          SelenElem.Click(By.id("99090073cac"));
          reporte.AddImageToReport("Sección opciones de envio del checkout", OPG.SS());
          da.slide("Down", 2);
          reporte.AddImageToReport("Sección opciones de envio del checkout", OPG.SS());
        
        SelenElem.Click(By.id("checkout-paso3-renovaciones"));
        
        Thread.sleep(5000);
        SelenElem.WaitForLoad(By.id("input-terminos"), 30);
        //SelenElem.focus(By.id("input-terminos"));
        OPG.cargaAjax2();
        Thread.sleep(5000);
        SelenElem.Click(By.id("input-terminos"));
        SelenElem.Click(By.id("input-serie"));
        da.slide("Up", 4);
        reporte.AddImageToReport("Sección Contrato del checkout", OPG.ScreenShotElement());
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección Contrato del checkout", OPG.ScreenShotElement());
        SelenElem.Click(By.id("btnContract"));
        
        Thread.sleep(5000);
        SelenElem.WaitForLoad(By.id("profile-container"), 30);
        SelenElem.focus(By.id("profile-container"));
        Thread.sleep(1000);
        
        SelenElem.WaitForLoad(By.id("valid-card"), 30);
        Thread.sleep(1000);
        
        //SelenElem.Click(By.id("cardEfectivo"));
        Thread.sleep(1000);
        SelenElem.Click(By.id("valid-card"));
        SelenElem.TypeText(By.id("valid-card"),tarjeta);
        SelenElem.Click(By.id("valid-nombre-card"));
        SelenElem.TypeText(By.id("valid-nombre-card"),nombre);
        SelenElem.Click(By.id("valid-vence"));
        SelenElem.TypeText(By.id("valid-vence"),expiracion);
        SelenElem.Click(By.id("valid-cvc"));
        SelenElem.TypeText(By.id("valid-cvc"),cvv);
        da.hideKeyBoard();
        
        da.slide("Up", 4);
        reporte.AddImageToReport("Sección Pago del checkout, Ingresar Datos del Pago", OPG.ScreenShotElement());
        
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección Pago del checkout, Ingresar Datos del Pago", OPG.ScreenShotElement());
        
        da.slide("Down", 1);
        reporte.AddImageToReport("Sección Pago del checkout, Ingresar Datos del Pago", OPG.ScreenShotElement());
        
        SelenElem.Click(By.id("btnPay"));
        
        
        OPG.cargaAjax2();
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        SelenElem.WaitForLoad(By.id("btnPayEfecCard"), 20);
        Thread.sleep(6000);
        da.slide("Up", 3);
        reporte.AddImageToReport("Confirmación de Pago ", OPG.ScreenShotElement());
        da.slide("Down", 3);
        reporte.AddImageToReport("Confirmación de Pago ", OPG.ScreenShotElement());
        Thread.sleep(2000);
        
        SelenElem.Click(By.id("continuar-checkout-out"));
        Thread.sleep(10000);
        SelenElem.WaitForLoad(By.id("OrderOnx"), 20);
        reporte.AddImageToReport("Success ", OPG.ScreenShotElement());
        
        
        
       
    }
    
    public void Renovaciones1(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String caso) throws InterruptedException, IOException
    {
        String dn =           CargaVariables.LeerCSV(0,"Renovaciones",caso);
        String email =           CargaVariables.LeerCSV(1,"Renovaciones",caso);
        String modTel=          CargaVariables.LeerCSV(2,"Renovaciones",caso);
        String ine=          CargaVariables.LeerCSV(3,"Renovaciones",caso);
        
        
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal ", OPG.SS());
        Thread.sleep(1000);
        SelenElem.Click(By.id("checkbox"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar el menu principal la opcion Smartphones", OPG.SS());
        
        SelenElem.Click(By.xpath("//input[@value='Smartphones']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar el flujo Smartphones > Renueva tu Smartphone", OPG.SS());
        SelenElem.Click(By.xpath("//a[.='Renueva tu Smartphone']"));
        
        SelenElem.WaitForLoad(By.id("telefono-contacto"), 20);
        SelenElem.TypeText(By.id("telefono-contacto"), dn);
        reporte.AddImageToReport("Se ingresa dn", OPG.SS());
        
        SelenElem.Click(By.id("gtm-plans-continue-button"));

        SelenElem.WaitForLoad(By.id("firstCharacter"), 20);
        Thread.sleep(5000);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        
        da.hideKeyBoard();
        reporte.AddImageToReport("Validación de identidad", OPG.SS());
        Thread.sleep(10000);
        SelenElem.focus(By.id("continue-btn"));
        SelenElem.ClickById(WebDrive.WebDriver,"continue-btn");
        
        reporte.AddImageToReport("Seleccionar Smartphone, ver detalle y continuar", OPG.SS());
        SelenElem.WaitForLoad(By.xpath("//img[contains(@alt, '" + modTel + "')]"),10);
        SelenElem.focus(By.xpath("//img[contains(@alt, '" + modTel + "')]"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + modTel + "')]"));
        
        SelenElem.Click(By.id("js_submit_miniparrilla_renovacion"));
        
        Thread.sleep(4000);
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-email"), 10);
  
        SelenElem.Click(By.id("valid-email"));
        SelenElem.TypeText(By.id("valid-email"), email);
        
        SelenElem.Click(By.id("valid-phone"));
        SelenElem.TypeText(By.id("valid-phone"), dn);
        
        SelenElem.Find(By.id("INE-IFE")).clear();
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        da.hideKeyBoard();
        SelenElem.Click(By.id("input-privacidad"));
        //SelenElem.Click(By.id("input-terminos"));
        reporte.AddImageToReport("Sección de datos del checkout", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("btnStep1Principal"));
        
        SelenElem.WaitForLoad(By.id("checkout-step2-radio-01"), 30);
        SelenElem.focus(By.id("checkout-step2-radio-01"));
        reporte.AddImageToReport("Sección Consulta de crédito", OPG.SS());
        SelenElem.Click(By.id("checkout-step2-radio-01"));
        Thread.sleep(1500);
        SelenElem.Find(By.id("cod")).clear();
        SelenElem.TypeText(By.id("cod"), "1111");
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        reporte.AddImageToReport("Sección Consulta de crédito", OPG.SS());
        
        SelenElem.Click(By.id("input-buro"));
        SelenElem.focus(By.id("btnConsultaBuro"));
        reporte.AddImageToReport("Sección Consulta de crédito", OPG.ScreenShotElement());
        SelenElem.Click(By.id("btnConsultaBuro"));
        
        Thread.sleep(1000);
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("inv-mail1"), 30);
        SelenElem.focus(By.id("inv-mail1"));
        Thread.sleep(7000);
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.ScreenShotElement());
        //SelenElem.Click(By.id("checkout-paso3-renovaciones"));
        SelenElem.WaitForLoad(By.id("inv-mail2"), 2);
          SelenElem.Click(By.id("inv-mail2"));
          //OPG.cargaAjax2();
          SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
          Thread.sleep(2000);
          SelenElem.TypeText(By.id("js_codigo_postal"), "06600");
          SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
          SelenElem.ClickById(WebDrive.WebDriver,"js_search_cacs_by_postal_code");
          OPG.cargaAjax2();   
          SelenElem.Click(By.id("99090073cac"));
          reporte.AddImageToReport("Sección opciones de envio del checkout", OPG.SS());
          da.slide("Down", 2);
          reporte.AddImageToReport("Sección opciones de envio del checkout", OPG.SS());
          SelenElem.Click(By.id("checkout-paso3-renovaciones"));
        
        Thread.sleep(5000);
        SelenElem.WaitForLoad(By.id("input-terminos"), 30);
        OPG.cargaAjax2();
        Thread.sleep(5000);
        //reporte.AddImageToReport("Sección Contrato del checkout", OPG.ScreenShotElement());
        SelenElem.Click(By.id("input-terminos"));
        SelenElem.Click(By.id("input-serie"));
        da.slide("Up", 2);
        reporte.AddImageToReport("Sección Contrato del checkout", OPG.ScreenShotElement());
        SelenElem.Click(By.id("btnContract"));
        
        Thread.sleep(5000);
        SelenElem.WaitForLoad(By.id("profile-container"), 30);
        SelenElem.focus(By.id("profile-container"));
        Thread.sleep(1000);
        
        reporte.AddImageToReport("Sección Pago ", OPG.ScreenShotElement());
        SelenElem.Click(By.id("btnPay"));
        
        SelenElem.waitForLoadPage();
        Thread.sleep(5000);
        reporte.AddImageToReport("Confirmar pedido ", OPG.ScreenShotElement());
        SelenElem.focus(By.id("continuar-checkout-out"));
        reporte.AddImageToReport("Confirmar pedido ", OPG.ScreenShotElement());
        SelenElem.Click(By.id("continuar-checkout-out"));
        
        Thread.sleep(1000);
        
        SelenElem.WaitForLoad(By.id("imagen-cliente"),20);
        reporte.AddImageToReport("Ingresamos a la pagina principal de FLAP", OPG.ScreenShotElement());
        Thread.sleep(3000);
        SelenElem.focus(By.id("1"));
        reporte.AddImageToReport("Medio de pago Flap", OPG.ScreenShotElement());
        SelenElem.Click(By.id("1"));
        SelenElem.focus(By.id("numTarjeta"));          
        SelenElem.TypeText(By.id("numTarjeta"), CargaVariables.LeerCSV(2,"Config","Tarjeta_Flap"));
        SelenElem.TypeText(By.id("cvv2"), CargaVariables.LeerCSV(4,"Config","Tarjeta_Flap"));   
        SelenElem.TypeText(By.id("vigenciames"), CargaVariables.LeerCSV(5,"Config","Tarjeta_Flap"));
        SelenElem.TypeText(By.id("vigenciaanio"), CargaVariables.LeerCSV(6,"Config","Tarjeta_Flap"));
        reporte.AddImageToReport("Llenamos los datos de pago y damos click en pagar", OPG.ScreenShotElement());
        SelenElem.Click(By.id("continuar"));
        
        Thread.sleep(2000);
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.ScreenShotElement());
        
        Thread.sleep(7000);
        
        SelenElem.WaitForLoad(By.className("descripcion-orden-terminal-card-texto"),30);
        SelenElem.waitForLoadPage();                
               
        reporte.AddImageToReport("Página de pago realizado con éxito", OPG.ScreenShotElement());
        
        
    }

    public void RenovacionWeb(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
       
        String dn =           CargaVariables.LeerCSV(0,"Renovaciones",Caso);
        String email =           CargaVariables.LeerCSV(1,"Renovaciones",Caso);
        String modTel=          CargaVariables.LeerCSV(2,"Renovaciones",Caso);
        String ine=          CargaVariables.LeerCSV(3,"Renovaciones",Caso);
        String codigo=          CargaVariables.LeerCSV(4,"Renovaciones",Caso);
        
        reporte.AddImageToReport("Iniciamos desde la pagina principal", OPG.ScreenShotElement());
        Thread.sleep(1000);
        //WebDrive.WebDriver.navigate().to(Url + "renovaciones/acceso/index");
        SelenElem.Click(By.id("boton_smartphone"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar el flujo Smartphones > Todas las marcas", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        reporte.AddImageToReport("Una vez en esta seccion se selecciona la opcion Renovacion", OPG.SS());
        
        SelenElem.Click(By.xpath("//span[.='Renovación']"));
        
        reporte.AddImageToReport("Seleccionar Categoría Renovación", OPG.SS());
        
        reporte.AddImageToReport("Seleccionar Smartphone, ver detalle y continuar", OPG.SS());
        SelenElem.WaitForLoad(By.xpath("//img[contains(@alt, '" + modTel + "')]"),10);
        SelenElem.focus(By.xpath("//img[contains(@alt, '" + modTel + "')]"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + modTel + "')]"));
        
        
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        reporte.AddImageToReport("Ver detalle del Smartphone", OPG.SS());
        SelenElem.focus(By.id("js_dn_input_soy_cliente"));
        SelenElem.Click(By.id("js_dn_input_soy_cliente"));
        SelenElem.TypeText(By.id("js_dn_input_soy_cliente"), dn);
        reporte.AddImageToReport("Ver detalle de Smartphone e Ingresar DN", OPG.SS());
        
        SelenElem.Click(By.id("js_input_dn_button_soy_cliente"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.xpath("//span[.='Continuar']"), 20);
        reporte.AddImageToReport("Elegir plan Movistar", OPG.SS());
        SelenElem.Click(By.xpath("//span[.='Continuar']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Elegir plan Movistar", OPG.SS());
        SelenElem.focus(By.id("js_submit_miniparrilla_renovacion"));
        reporte.AddImageToReport("", OPG.SS());
        Thread.sleep(3000);
        SelenElem.Click(By.id("js_submit_miniparrilla_renovacion"));
        
      /*SelenElem.WaitForLoad(By.id("firstCharacter"), 6);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        
        reporte.AddImageToReport("Validación de identidad", OPG.SS());
        Thread.sleep(15000);
        //SelenElem.Click(By.id("continue-btn"));
        SelenElem.focus(By.id("continue-btn"));
        SelenElem.ClickById(WebDrive.WebDriver,"continue-btn");
        
        Thread.sleep(4000);*/
          
        SelenElem.waitForLoadPage();
        
        SelenElem.WaitForLoad(By.id("valid-email"), 10);
        //SelenElem.TypeText(By.id("valid-nombre"), CargaVariables.LeerCSV(1,"Renovaciones",caso));
        SelenElem.Click(By.id("valid-email"));
        SelenElem.TypeText(By.id("valid-email"), email);
        
        SelenElem.Click(By.id("valid-phone"));
        SelenElem.TypeText(By.id("valid-phone"), dn);
        
        SelenElem.Click(By.id("INE-IFE"));
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        reporte.AddImageToReport("Sección de datos del checkout", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("input-privacidad"));
        SelenElem.Click(By.id("input-terminos"));
        
        SelenElem.focus(By.id("btnStep1Principal"));
        reporte.AddImageToReport("Sección de datos del checkout", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("btnStep1Principal"));
        
        SelenElem.WaitForLoad(By.id("checkout-step2-radio-01"), 30);
        SelenElem.focus(By.id("checkout-step2-radio-01"));
        SelenElem.Click(By.id("checkout-step2-radio-01"));
        Thread.sleep(1500);
        SelenElem.Click(By.id("cod"));
        SelenElem.TypeText(By.id("cod"), "1111");
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        reporte.AddImageToReport("Sección de datos del checkout", OPG.SS());
        
        SelenElem.Click(By.id("input-buro"));
        SelenElem.focus(By.id("btnConsultaBuro"));
        reporte.AddImageToReport("Sección Consulta de crédito", OPG.ScreenShotElement());
        SelenElem.Click(By.id("btnConsultaBuro"));
        
        Thread.sleep(5000);
        SelenElem.WaitForLoad(By.id("inv-mail1"), 30);
        SelenElem.focus(By.id("inv-mail1"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.ScreenShotElement());
        SelenElem.Click(By.id("checkout-paso3-renovaciones"));
        
        Thread.sleep(5000);
        SelenElem.WaitForLoad(By.id("input-terminos"), 30);
        //SelenElem.focus(By.id("input-terminos"));
        OPG.cargaAjax2();
        Thread.sleep(5000);
        SelenElem.Click(By.id("input-terminos"));
        SelenElem.Click(By.id("input-serie"));
        reporte.AddImageToReport("Sección Contrato del checkout", OPG.ScreenShotElement());
        SelenElem.Click(By.id("btnContract"));
        
        Thread.sleep(5000);
        SelenElem.WaitForLoad(By.id("profile-container"), 30);
        SelenElem.focus(By.id("profile-container"));
        Thread.sleep(1000);
        
        reporte.AddImageToReport("Sección Pago ", OPG.ScreenShotElement());
        SelenElem.Click(By.id("btnPay"));
        
        SelenElem.waitForLoadPage();
        Thread.sleep(5000);
        reporte.AddImageToReport("Confirmar pedido ", OPG.ScreenShotElement());
        SelenElem.focus(By.id("continuar-checkout-out"));
        reporte.AddImageToReport("Confirmar pedido ", OPG.ScreenShotElement());
        SelenElem.Click(By.id("continuar-checkout-out"));
        
        Thread.sleep(1000);
        
        
        new Flap(reporte,OPG,SelenElem,WebDrive);
        
        SelenElem.WaitForLoad(By.className("descripcion-orden-terminal-card-texto"),30);
        SelenElem.waitForLoadPage();                
               
        reporte.AddImageToReport("Página de pago realizado con éxito", OPG.ScreenShotElement());
        
    }

    public void RenoR3Efectivo(Report reporte, WebAction WB, OperacionesGenerales OPG, WebDriv Driver, String Url, String Caso) throws InterruptedException, IOException
    {
        WebElement Input,InputINE,BtnPago;
        String TxtPago;
        String DN =            CargaVariables.LeerCSV(0,"Renovaciones R3",Caso);
        String modTel=         CargaVariables.LeerCSV(1,"Renovaciones R3",Caso);
        String Email=          CargaVariables.LeerCSV(2,"Renovaciones R3",Caso);
        String INE=            CargaVariables.LeerCSV(3,"Renovaciones R3",Caso);
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement());
        Thread.sleep(1000);
        Driver.WebDriver.navigate().to(Url + "renovaciones/acceso/");
        reporte.AddImageToReport("Una vez en esta seccion se selecciona la opcion Renovacion", OPG.SS());
        
        Input = WB.FindElementByID("telefono-contacto");
        WB.TypeText(Input, DN);
        reporte.AddImageToReport(" ", OPG.ScreenShotElement());
        WB.ClickById(Driver.WebDriver, "gtm-plans-continue-button");
       // WB.CargaAjax2(Driver.WebDriver);
        
        WB.WaitForLoad(By.id("firstCharacter"),10);
        WB.TypeText(By.id("firstCharacter"), "1");
        WB.TypeText(By.id("secondCharacter"), "2");
        WB.TypeText(By.id("thirdCharacter"), "3");
        WB.TypeText(By.id("fourthCharacter"), "4");
        WB.TypeText(By.id("fifthCharacter"), "5");
        WB.TypeText(By.id("sixthCharacter"), "6");
        reporte.AddImageToReport(" ", OPG.ScreenShotElement());
        WB.ClickById(Driver.WebDriver, "continue-btn");
        Thread.sleep(1000);
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport(" ", OPG.ScreenShotElement());
        //Samsung Galaxy Note 9 128 GB Azul 
        WB.focus(By.xpath("//img[contains(@alt, '" + modTel + "')]"));
        reporte.AddImageToReport("", OPG.SS());
        WB.Click(By.xpath("//img[contains(@alt, '" + modTel + "')]"));

        WB.waitForLoadPage();
        //Thread.sleep(5000);
        
        WB.Click(By.id("js_input_dn_button_soy_cliente"));
        OPG.cargaAjax2();
        WB.WaitForLoad(By.xpath("//span[.='Continuar']"), 20);
        reporte.AddImageToReport("Elegir plan Movistar", OPG.SS());
        WB.Click(By.xpath("//span[.='Continuar']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Elegir plan Movistar", OPG.SS());
        WB.focus(By.id("js_submit_miniparrilla_renovacion"));
        reporte.AddImageToReport("", OPG.SS());
        Thread.sleep(3000);
        WB.Click(By.id("js_submit_miniparrilla_renovacion"));
        
        WB.waitForLoadPage();
        
        WB.WaitForLoad(By.id("valid-email"), 10);
        //SelenElem.TypeText(By.id("valid-nombre"), CargaVariables.LeerCSV(1,"Renovaciones",caso));
        WB.Click(By.id("valid-email"));
        WB.TypeText(By.id("valid-email"), Email);
        
        WB.Click(By.id("valid-phone"));
        WB.TypeText(By.id("valid-phone"), DN);
        
        WB.Click(By.id("INE-IFE"));
        InputINE = WB.FindElementByID("INE-IFE");
        InputINE.clear();
        WB.TypeText(By.id("INE-IFE"), INE);
        reporte.AddImageToReport("Sección de datos del checkout", OPG.ScreenShotElement());
        WB.Click(By.id("input-privacidad"));
        WB.Click(By.id("input-terminos"));
        
        WB.focus(By.id("btnStep1Principal"));
        reporte.AddImageToReport("Sección de datos del checkout", OPG.ScreenShotElement());
        
        WB.Click(By.id("btnStep1Principal"));
        OPG.cargaAjax2();
        
        WB.Click(By.id("inv-mail2"));          
        WB.WaitForLoad(By.id("js_codigo_postal"), 10);
        Thread.sleep(3000);
        WB.TypeText(By.id("js_codigo_postal"), "06600");
        WB.Click(By.id("js_search_cacs_by_postal_code"));
        WB.ClickById(Driver.WebDriver,"js_search_cacs_by_postal_code");
        OPG.cargaAjax2();   
        WB.Click(By.id("99090073cac"));
        reporte.AddImageToReport("Sección opciones de envio del checkout", OPG.ScreenShotElement());
        WB.focus(By.id("checkout-paso3-renovaciones"));
        reporte.AddImageToReport("Sección opciones de envio del checkout", OPG.ScreenShotElement());
        
        WB.Click(By.id("checkout-paso3-renovaciones"));
        
        Thread.sleep(5000);
        WB.WaitForLoad(By.id("input-terminos"), 30);
        //SelenElem.focus(By.id("input-terminos"));
        OPG.cargaAjax2();
        Thread.sleep(5000);
        WB.Click(By.id("input-terminos"));
        WB.Click(By.id("input-serie"));
        reporte.AddImageToReport("Sección Contrato del checkout", OPG.ScreenShotElement());
        WB.Click(By.id("btnContract"));
        OPG.cargaAjax2();
        Thread.sleep(5000);
        BtnPago = WB.FindElementByID("btnPay");
        TxtPago = BtnPago.getText();
        if(TxtPago.equals("Continuar con pago en efectivo"))
        {
            reporte.AddImageToReport("", OPG.ScreenShotElement());
            WB.Click(By.id("btnPay"));
        }
        else
        {
            reporte.AddImageToReport("La forma de Pago no es efectivo", OPG.ScreenShotElement());
            throw new NullPointerException( "El metiodo de pago no es efectivo" );
        }
        
        OPG.cargaAjax2();
        WB.waitForLoadPage();
        Thread.sleep(2000);
        reporte.AddImageToReport("Confirmación de Pago ", OPG.ScreenShotElement());
        Thread.sleep(2000);
        
        WB.Click(By.id("continuar-checkout-out"));
        Thread.sleep(10000);
        WB.WaitForLoad(By.id("OrderOnx"), 20);
        reporte.AddImageToReport("Success ", OPG.ScreenShotElement());
 
    }

    public void RenoR3Tarjeta(Report reporte, WebAction WB, OperacionesGenerales OPG, WebDriv Driver, String Url, String Caso) throws InterruptedException, IOException
    {
        WebElement Input,InputINE,BtnPago;
        String TxtPago;
        
        String DN =            CargaVariables.LeerCSV(0,"Renovaciones R3",Caso);
        String modTel=         CargaVariables.LeerCSV(1,"Renovaciones R3",Caso);
        String Email=          CargaVariables.LeerCSV(2,"Renovaciones R3",Caso);
        String INE=            CargaVariables.LeerCSV(3,"Renovaciones R3",Caso);
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement());
        Thread.sleep(1000);
        Driver.WebDriver.navigate().to(Url + "renovaciones/acceso/");
        reporte.AddImageToReport("Una vez en esta seccion se selecciona la opcion Renovacion", OPG.SS());
        
        Input = WB.FindElementByID("telefono-contacto");
        WB.TypeText(Input, DN);
        reporte.AddImageToReport(" ", OPG.ScreenShotElement());
        WB.ClickById(Driver.WebDriver, "gtm-plans-continue-button");
       // WB.CargaAjax2(Driver.WebDriver);
        
        WB.WaitForLoad(By.id("firstCharacter"),10);
        WB.TypeText(By.id("firstCharacter"), "1");
        WB.TypeText(By.id("secondCharacter"), "2");
        WB.TypeText(By.id("thirdCharacter"), "3");
        WB.TypeText(By.id("fourthCharacter"), "4");
        WB.TypeText(By.id("fifthCharacter"), "5");
        WB.TypeText(By.id("sixthCharacter"), "6");
        reporte.AddImageToReport(" ", OPG.ScreenShotElement());
        Thread.sleep(3000);
        WB.ClickById(Driver.WebDriver, "continue-btn");
        Thread.sleep(1000);
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport(" ", OPG.ScreenShotElement());
        //Samsung Galaxy Note 9 128 GB Azul
        WB.focus(By.xpath("//img[contains(@alt, '" + modTel + "')]"));
        reporte.AddImageToReport("", OPG.SS());
        WB.Click(By.xpath("//img[contains(@alt, '" + modTel + "')]"));

        WB.waitForLoadPage();
        //Thread.sleep(5000);
        
        WB.Click(By.id("js_input_dn_button_soy_cliente"));
        OPG.cargaAjax2();
        WB.WaitForLoad(By.xpath("//span[.='Continuar']"), 20);
        reporte.AddImageToReport("Elegir plan Movistar", OPG.SS());
        WB.Click(By.xpath("//span[.='Continuar']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Elegir plan Movistar", OPG.SS());
        WB.focus(By.id("js_submit_miniparrilla_renovacion"));
        reporte.AddImageToReport("", OPG.SS());
        Thread.sleep(3000);
        WB.Click(By.id("js_submit_miniparrilla_renovacion"));
        
        WB.waitForLoadPage();
        
        WB.WaitForLoad(By.id("valid-email"), 10);
        //SelenElem.TypeText(By.id("valid-nombre"), CargaVariables.LeerCSV(1,"Renovaciones",caso));
        WB.Click(By.id("valid-email"));
        WB.TypeText(By.id("valid-email"), Email);
        
        WB.Click(By.id("valid-phone"));
        WB.TypeText(By.id("valid-phone"), DN);
        
        WB.Click(By.id("INE-IFE"));
        WB.Click(By.id("INE-IFE"));
        InputINE = WB.FindElementByID("INE-IFE");
        InputINE.clear();
        WB.TypeText(By.id("INE-IFE"), INE);
        reporte.AddImageToReport("Sección de datos del checkout", OPG.ScreenShotElement());
        WB.Click(By.id("input-privacidad"));
        //WB.Click(By.id("input-terminos"));
        
        WB.focus(By.id("btnStep1Principal"));
        reporte.AddImageToReport("Sección de datos del checkout", OPG.ScreenShotElement());
        
        WB.Click(By.id("btnStep1Principal"));
        OPG.cargaAjax2();
        
        WB.WaitForLoad(By.id("inv-mail2"), 30);
        WB.Click(By.id("inv-mail2"));          
        WB.WaitForLoad(By.id("js_codigo_postal"), 10);
        Thread.sleep(3000);
        WB.TypeText(By.id("js_codigo_postal"), "06600");
        WB.Click(By.id("js_search_cacs_by_postal_code"));
        WB.ClickById(Driver.WebDriver,"js_search_cacs_by_postal_code");
        OPG.cargaAjax2();   
        WB.Click(By.id("99090073cac"));
        reporte.AddImageToReport("Sección opciones de envio del checkout", OPG.ScreenShotElement());
        WB.focus(By.id("checkout-paso3-renovaciones"));
        reporte.AddImageToReport("Sección opciones de envio del checkout", OPG.ScreenShotElement());
        
        WB.Click(By.id("checkout-paso3-renovaciones"));
        
        Thread.sleep(5000);
        WB.WaitForLoad(By.id("input-terminos"), 30);
        //SelenElem.focus(By.id("input-terminos"));
        OPG.cargaAjax2();
        Thread.sleep(5000);
        WB.Click(By.id("input-terminos"));
        WB.Click(By.id("input-serie"));
        reporte.AddImageToReport("Sección Contrato del checkout", OPG.ScreenShotElement());
        WB.Click(By.id("btnContract"));
        OPG.cargaAjax2();
        Thread.sleep(5000);
        BtnPago = WB.FindElementByID("btnPay");
        TxtPago = BtnPago.getText();
        if(TxtPago.equals("Confirma método de pago"))
        {
            reporte.AddImageToReport("", OPG.ScreenShotElement());
            WB.Click(By.id("btnPay"));
        }
        else
        {
            reporte.AddImageToReport("La forma de Pago no es efectivo", OPG.ScreenShotElement());
            throw new NullPointerException( "El metiodo de pago no es efectivo" );
        }
                
        OPG.cargaAjax2();
        WB.waitForLoadPage();
        Thread.sleep(1000);
        WB.WaitForLoad(By.id("btnPayEfecCard"), 20);
        Thread.sleep(6000);
        reporte.AddImageToReport("Confirmación de Pago ", OPG.ScreenShotElement());
        Thread.sleep(2000);
        
        WB.Click(By.id("continuar-checkout-out"));
        Thread.sleep(10000);
        WB.WaitForLoad(By.id("OrderOnx"), 20);
        reporte.AddImageToReport("Success ", OPG.ScreenShotElement());
 
    }
    

    public void ErrorDN(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
        
        String dn =             CargaVariables.LeerCSV(0,"Renovaciones",Caso);
        String modTel=          CargaVariables.LeerCSV(2,"Renovaciones",Caso);
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement());
        Thread.sleep(1000);
        
        SelenElem.Click(By.id("boton_smartphone"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar el flujo Smartphones > Todas las marcas", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        reporte.AddImageToReport("Una vez en esta seccion se selecciona la opcion Renovacion", OPG.SS());
        
        SelenElem.Click(By.xpath("//span[.='Renovación']"));
        
        reporte.AddImageToReport("Seleccionar Categoría Renovación", OPG.SS());
        
        reporte.AddImageToReport("Seleccionar Smartphone, ver detalle y continuar", OPG.SS());
        SelenElem.WaitForLoad(By.xpath("//img[contains(@alt, '" + modTel + "')]"),10);
        SelenElem.focus(By.xpath("//img[contains(@alt, '" + modTel + "')]"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + modTel + "')]"));
        
        
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        reporte.AddImageToReport("Ver detalle del Smartphone", OPG.SS());
        SelenElem.focus(By.id("js_dn_input_soy_cliente"));
        SelenElem.Click(By.id("js_dn_input_soy_cliente"));
        SelenElem.TypeText(By.id("js_dn_input_soy_cliente"), dn);
        reporte.AddImageToReport("Ver detalle de Smartphone e Ingresar DN", OPG.SS());
        
        SelenElem.Click(By.id("js_input_dn_button_soy_cliente"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.xpath("//span[.='Continuar']"), 20);
        reporte.AddImageToReport("Error en DN", OPG.SS()); 
        
    }

    public void ErrorINE(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
       
        String dn =           CargaVariables.LeerCSV(0,"Renovaciones",Caso);
        String email =           CargaVariables.LeerCSV(1,"Renovaciones",Caso);
        String modTel=          CargaVariables.LeerCSV(2,"Renovaciones",Caso);
        String ine=          CargaVariables.LeerCSV(3,"Renovaciones",Caso);
        
        SelenElem.Click(By.id("boton_smartphone"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar el flujo Smartphones > Todas las marcas", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        reporte.AddImageToReport("Una vez en esta seccion se selecciona la opcion Renovacion", OPG.SS());
        
        SelenElem.Click(By.xpath("//span[.='Renovación']"));
        
        reporte.AddImageToReport("Seleccionar Categoría Renovación", OPG.SS());
        
        reporte.AddImageToReport("Seleccionar Smartphone, ver detalle y continuar", OPG.SS());
        SelenElem.WaitForLoad(By.xpath("//img[contains(@alt, '" + modTel + "')]"),10);
        SelenElem.focus(By.xpath("//img[contains(@alt, '" + modTel + "')]"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + modTel + "')]"));
        
        
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        reporte.AddImageToReport("Ver detalle del Smartphone", OPG.SS());
        SelenElem.focus(By.id("js_dn_input_soy_cliente"));
        SelenElem.Click(By.id("js_dn_input_soy_cliente"));
        SelenElem.TypeText(By.id("js_dn_input_soy_cliente"), dn);
        reporte.AddImageToReport("Ver detalle de Smartphone e Ingresar DN", OPG.SS());
        
        SelenElem.Click(By.id("js_input_dn_button_soy_cliente"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.xpath("//span[.='Continuar']"), 20);
        reporte.AddImageToReport("Elegir plan Movistar", OPG.SS());
        SelenElem.Click(By.xpath("//span[.='Continuar']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Elegir plan Movistar", OPG.SS());
        SelenElem.focus(By.id("js_submit_miniparrilla_renovacion"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.id("js_submit_miniparrilla_renovacion"));
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        SelenElem.focus(By.id("firstCharacter"));
        reporte.AddImageToReport("Valida tu identidad", OPG.ScreenShotElement());
        
        
        SelenElem.focus(By.id("continue-btn"));
        SelenElem.ClickById(WebDrive.WebDriver,"continue-btn");
        
        Thread.sleep(4000);
        
        SelenElem.waitForLoadPage();
        
        SelenElem.WaitForLoad(By.id("valid-email"), 10);
        //SelenElem.TypeText(By.id("valid-nombre"), CargaVariables.LeerCSV(1,"Renovaciones",caso));
        SelenElem.Click(By.id("valid-email"));
        SelenElem.TypeText(By.id("valid-email"), email);
        
        SelenElem.Click(By.id("valid-phone"));
        SelenElem.TypeText(By.id("valid-phone"), dn);
        
        SelenElem.Click(By.id("INE-IFE"));
        SelenElem.Find(By.id("INE-IFE")).clear();
        SelenElem.TypeText(By.id("INE-IFE"), "1234");
        reporte.AddImageToReport("Sección de datos del checkout", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("input-privacidad"));
        SelenElem.Click(By.id("input-terminos"));
        
        SelenElem.focus(By.id("btnStep1Principal"));
        reporte.AddImageToReport("Sección de datos del checkout", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("btnStep1Principal"));
        Thread.sleep(300);
        reporte.AddImageToReport("Error en INE", OPG.ScreenShotElementInstant());
        
        
}

    public void RenovacionR3TarjetaMovil(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, DeviceActions da, String Caso) throws InterruptedException, IOException
    {
        String Dn = CargaVariables.LeerCSV(0, "Renovaciones R3", Caso);
        String ModTel = CargaVariables.LeerCSV(1, "Renovaciones R3", Caso);
        String Correo = CargaVariables.LeerCSV(2, "Renovaciones R3", Caso);
        String Telefono = CargaVariables.LeerCSV(3, "Renovaciones R3", Caso);
        String INE = CargaVariables.LeerCSV(4, "Renovaciones R3", Caso);
        String CodPostal = CargaVariables.LeerCSV(5, "Renovaciones R3", Caso);
        String Tarjeta = CargaVariables.LeerCSV(6, "Renovaciones R3", Caso);
        WebElement Input;

        WebDrive.WebDriver.navigate().to(Url);
        Thread.sleep(3000);
        reporte.AddImageToReport("Continuamos con la pagina principal", OPG.SS());
        SelenElem.Click(By.id("checkbox"));
        SelenElem.ClickButton(WebDrive.WebDriver, SelenElem, "ClassName", "dropdown_menu_smart");
        SelenElem.Click(By.xpath("//a[@title='Renueva tu Smartphone']"));
        
        SelenElem.WaitForLoad(By.id("gtm-plans-continue-button"), 10);
        SelenElem.Click(By.id("telefono-contacto"));
        SelenElem.TypeText(By.id("telefono-contacto"), Dn);
        da.hideKeyBoard();
        reporte.AddImageToReport("Seleccionar el flujo de Renueva tu plan", OPG.SS());
        SelenElem.Click(By.id("gtm-plans-continue-button"));
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 10);
        SelenElem.Click(By.id("firstCharacter"));
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.Click(By.id("secondCharacter"));
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.Click(By.id("thirdCharacter"));
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.Click(By.id("fourthCharacter"));
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.Click(By.id("fifthCharacter"));
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.Click(By.id("sixthCharacter"));
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        da.hideKeyBoard();
        reporte.AddImageToReport("Validacion de identidad", OPG.SS());
        
        SelenElem.Click(By.id("continue-btn"));
       
        Thread.sleep(5000);
        reporte.AddImageToReport("Seleccionar Smartphone y continuar", OPG.SS());
        
        SelenElem.focus(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        reporte.AddImageToReport("Se procede a seleccionar el Smartphone", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        
        reporte.AddImageToReport("Ver detalle del Smartphone", OPG.SS());
        SelenElem.Click(By.id("soy-cliente"));
        SelenElem.Click(By.id("js_dn_input_soy_cliente"));
        SelenElem.TypeText(By.id("js_dn_input_soy_cliente"), Dn);
        reporte.AddImageToReport("", OPG.SS());
        //SelenElem.Click(By.id("js_input_dn_button_soy_cliente"));
        
        Thread.sleep(5000);
        SelenElem.ClickButton(WebDrive.WebDriver, SelenElem, "ClassName", "temm-2022-modal__accept-button");
        //SelenElem.Click(By.xpath("//button[@class='temm-2022-modal__accept-button']"));
        reporte.AddImageToReport("Elegir Plan Movistar", OPG.SS());
        //SelenElem.ClickButton(WebDrive.WebDriver, SelenElem, "ClassName", "texto-boton-comprar");
        SelenElem.Click(By.id("js_submit_miniparrilla_renovacion"));
        
        SelenElem.WaitForLoad(By.id("valid-email"), 10);
        SelenElem.Click(By.id("valid-email"));
        SelenElem.TypeText(By.id("valid-email"), Correo);
        SelenElem.Click(By.id("valid-phone"));
        SelenElem.TypeText(By.id("valid-phone"), Telefono);
        SelenElem.Click(By.id("INE-IFE"));
        
        Input = SelenElem.FindElementByID(WebDrive.WebDriver, "INE-IFE");
        Input.clear();
        SelenElem.TypeText(By.id("INE-IFE"), INE);
        da.hideKeyBoard();
        SelenElem.Click(By.id("input-privacidad"));
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.Click(By.id("btnStep1Principal"));
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 10);
        SelenElem.Click(By.id("inv-mail2"));
        SelenElem.ClickButton(WebDrive.WebDriver, SelenElem, "ClassName", "form__radio_label mb-3");
        SelenElem.Click(By.id("js_codigo_postal"));
        SelenElem.TypeText(By.id("js_codigo_postal"), CodPostal);
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        
        SelenElem.WaitForLoad(By.id("js_geo_option_cards"), 10);
        SelenElem.ClickButton(WebDrive.WebDriver, SelenElem, "ClassName", "geo-cac-elocker-option__place-title");
        reporte.AddImageToReport("Sección opciones de envío del checkout", OPG.SS());
        SelenElem.Click(By.id("checkout-paso3-renovaciones"));
        
        SelenElem.WaitForLoad(By.id("input-terminos"), 10);
        SelenElem.Click(By.id("input-terminos"));
        SelenElem.Click(By.id("input-serie"));
        reporte.AddImageToReport("Sección contrato del checkout", OPG.SS());
        SelenElem.Click(By.id("btnContract"));
        
        SelenElem.WaitForLoad(By.id("creditCard"), 10);
        SelenElem.Click(By.id("creditCard"));
        SelenElem.TypeText(By.id("creditCard"), Tarjeta);
        reporte.AddImageToReport("Sección de pago del checkout", OPG.SS());
        SelenElem.Click(By.id("btnPay"));
        
        Thread.sleep(3000);
        reporte.AddImageToReport("Sección confirmación del checkout", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.id("continuar-checkout-out"));
        
        Thread.sleep(5000);
        reporte.AddImageToReport("Pago realizado con exito", OPG.SS());
        
     
        
        
     
    }        

    public void RenovacionR3TokenMovil(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, DeviceActions da, String Caso) throws InterruptedException, IOException
    {
        
        String DN = CargaVariables.LeerCSV(0, "Renovaciones R3", Caso);
        String SkuPlan = CargaVariables.LeerCSV(1, "Renovaciones R3", Caso);
        String SkuTerminal = CargaVariables.LeerCSV(2, "Renovaciones R3", Caso);
        String Correo = CargaVariables.LeerCSV(3, "Renovaciones R3", Caso);
        String Telefono = CargaVariables.LeerCSV(4, "Renovaciones R3", Caso);
        String INE = CargaVariables.LeerCSV(5, "Renovaciones R3", Caso);
        String CodPostal = CargaVariables.LeerCSV(6, "Renovaciones R3", Caso);
        String Tarjeta = CargaVariables.LeerCSV(7, "Renovaciones R3", Caso);
        String Nombre = CargaVariables.LeerCSV(8, "Renovaciones R3", Caso);
        String Vence = CargaVariables.LeerCSV(9, "Renovaciones R3", Caso);
        String CVV = CargaVariables.LeerCSV(10, "Renovaciones R3", Caso);

        WebDrive.WebDriver.navigate().to(Url + "liferay.php");
        Thread.sleep(3000);
       
        reporte.AddImageToReport("Pagina principal", OPG.SS());
        SelenElem.SelectOption(By.id("flowCode"), "renovacion");
        SelenElem.Find(By.id("skuPlan")).clear();
        SelenElem.TypeText(By.id("skuPlan"), SkuPlan);
        SelenElem.Find(By.id("skuRecarga")).clear();
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.TypeText(By.id("skuTerminal"), SkuTerminal);
        SelenElem.Find(By.id("skuDn")).clear();
        SelenElem.TypeText(By.id("skuDn"), DN);
        SelenElem.Find(By.id("monto")).clear();
        SelenElem.Find(By.id("rfc")).clear();
        
        reporte.AddImageToReport("Ingresar los datos correspondientes", OPG.SS());
        Thread.sleep(3000);
        SelenElem.Click(By.name("Comprar"));
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
        Thread.sleep(5000);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        SelenElem.focus(By.id("firstCharacter"));
        Thread.sleep(5000);
        reporte.AddImageToReport("Valida tu indentidad", OPG.SS());
        SelenElem.Click(By.id("continue-btn"));
        
        SelenElem.WaitForLoad(By.id("valid-email"), 10);
        SelenElem.TypeText(By.id("valid-email"), Correo);
        SelenElem.TypeText(By.id("valid-phone"), Telefono);
        SelenElem.Find(By.id("INE-IFE")).clear();
        SelenElem.TypeText(By.id("INE-IFE"), INE);
        SelenElem.Click(By.id("input-privacidad"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección  tus datos del checkout", OPG.SS());
        SelenElem.Click(By.id("btnStep1Principal"));
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 10);
        SelenElem.Click(By.id("inv-mail2"));
        SelenElem.Click(By.id("js_codigo_postal"));
        SelenElem.TypeText(By.id("js_codigo_postal"), CodPostal);
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        
        SelenElem.WaitForLoad(By.id("js_geo_option_cards"), 10);
        SelenElem.ClickButton(WebDrive.WebDriver, SelenElem, "ClassName", "geo-cac-elocker-option__place-title");
        reporte.AddImageToReport("Sección opciones de envío del checkout", OPG.SS());
        SelenElem.Click(By.id("checkout-paso3-renovaciones"));
        
        //SelenElem.WaitForLoad(By.id("ver_contrato_portabilidad_pdf"), 15);
        //SelenElem.Click(By.id("ver_contrato_portabilidad_pdf"));
        
        SelenElem.Click(By.id("input-terminos"));
        SelenElem.Click(By.id("input-serie"));
        SelenElem.Click(By.id("btnContract"));
        
        SelenElem.WaitForLoad(By.id("valid-card"), 15);
        SelenElem.Click(By.id("valid-card"));
        SelenElem.TypeText(By.id("valid-card"), Tarjeta);
        SelenElem.Click(By.id("valid-nombre-card"));
        SelenElem.TypeText(By.id("valid-nombre-card"), Nombre);
        SelenElem.Click(By.id("valid-vence"));
        SelenElem.TypeText(By.id("valid-vence"), Vence);
        SelenElem.Click(By.id("valid-cvc"));
        SelenElem.TypeText(By.id("valid-cvc"), CVV);
        SelenElem.Click(By.id("btnPay"));
        
        SelenElem.Click(By.id("btnPayEfecCard"));
        
        reporte.AddImageToReport("Confirmar pedido", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.id("continuar-checkout-out"));
        
        Thread.sleep(5000);
        reporte.AddImageToReport("Pago realizado con exito", OPG.SS());
    }
}
