
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.OpcionesEnvio;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author LAPTOP-JORGE
 */
public class PortabilidadPospagoSoloSIM {

        public PortabilidadPospagoSoloSIM(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
       
        String dn = CargaVariables.LeerCSV(0, "Portabilidad Pospago Solo SIM", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Portabilidad Pospago Solo SIM", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Portabilidad Pospago Solo SIM", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Portabilidad Pospago Solo SIM", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Portabilidad Pospago Solo SIM", Caso);
        String correo = CargaVariables.LeerCSV(5, "Portabilidad Pospago Solo SIM", Caso);
        String tel = CargaVariables.LeerCSV(6, "Portabilidad Pospago Solo SIM", Caso);
        String calle = CargaVariables.LeerCSV(8, "Portabilidad Pospago Solo SIM", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Portabilidad Pospago Solo SIM", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Portabilidad Pospago Solo SIM", Caso);
        String cp = CargaVariables.LeerCSV(7, "Portabilidad Pospago Solo SIM", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Portabilidad Pospago Solo SIM", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Portabilidad Pospago Solo SIM", Caso);
        String ine = CargaVariables.LeerCSV(14, "Portabilidad Pospago Solo SIM", Caso);
        String año = CargaVariables.LeerCSV(16, "Portabilidad Pospago Solo SIM", Caso);
        String mes = CargaVariables.LeerCSV(17, "Portabilidad Pospago Solo SIM", Caso);
        String  dia= CargaVariables.LeerCSV(18, "Portabilidad Pospago Solo SIM", Caso);
        String curp= CargaVariables.LeerCSV(19, "Portabilidad Pospago Solo SIM", Caso);
       

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        SelenElem.waitForLoadPage();
        Thread.sleep(3000);

        WebDrive.WebDriver.navigate().to(Url + "cambiate-a-movistar");
        reporte.AddImageToReport("Se ingresa a el flujo de Cámbiate a Movistar", OPG.SS());
        SelenElem.focus(By.xpath("//p[.='8 GB ']"));
        reporte.AddImageToReport("Se ingresa a el flujo de Cámbiate a Movistar", OPG.SS());
        SelenElem.Click(By.id("DN"));
        SelenElem.TypeText(By.id("DN"),dn);
        reporte.AddImageToReport(" Ingresar DN", OPG.SS());
        
        SelenElem.Click(By.id("portar_DN"));
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.ScreenShotElement());
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-nombre"), 35);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), año);
        SelenElem.SelectOption(By.className("ui-datepicker-month"), mes);
        SelenElem.Click(By.linkText(dia));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("calleNumeroInterior"), numInterior);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.Click(By.id("checkout-step4-check-02"));
        
        SelenElem.Click(By.id("paso1-pospago-porta"));
        
        OPG.cargaAjax2();
        Thread.sleep(500);
        OPG.cargaAjax2();
        
        Thread.sleep(40000);
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        SelenElem.focus(By.id("99090028cac"));
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        SelenElem.focus(By.id("checkout-paso2-renovaciones"));
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());     
        SelenElem.Click(By.id("checkout-paso2-renovaciones"));
        
        OPG.cargaAjax2();
        
        SelenElem.TypeText(By.id("valid-nip"), "1234");
        SelenElem.TypeText(By.id("valid-curp"), curp);
        SelenElem.Click(By.id("dateToPorta_datepicker"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("ui-datepicker-div"),10);
        SelenElem.Click(By.className("ui-datepicker-days-cell-over"));
        reporte.AddImageToReport("Sección Cámbiate a Movistar del Checkout", OPG.ScreenShotElement());        
        SelenElem.Click(By.id("checkout-paso4-porta-pre"));
        
        
        SelenElem.Click(By.id("checkTeminosCondiciones"));
        reporte.AddImageToReport("Sección Contrato del Checkout", OPG.ScreenShotElement());
        SelenElem.Click(By.id("ver_contrato_portabilidad_pdf"));
        Thread.sleep(4000);
        reporte.AddImageToReport("Sección Contrato del Checkout", OPG.ScreenShotAll());
        Thread.sleep(2000);
        SelenElem.Click(By.id("realizar-pago-2"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Sección Reseumen de compra", OPG.SS());
        Thread.sleep(1000);
        //SelenElem.focus(By.xpath("//*[@id=\"bloque-portabilidad-info-cliente\"]/legend/span/span[2]"));
        

        JavascriptExecutor js = (JavascriptExecutor)WebDrive.WebDriver;
        js.executeScript("arguments[0].scrollIntoView();", SelenElem.Find(By.tagName("header"))); 
        
        reporte.AddImageToReport("Sección Reseumen de compra", OPG.SS());
        Thread.sleep(1000);
        SelenElem.focus(By.xpath("//*[@id=\"numero_portar\"]"));
        
        SelenElem.WaitForLoad(By.id("numeroOrden"), 40);
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/header/div/h1"));
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/section[3]/dl/dd/p"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/footer/a[1]"));
        reporte.AddImageToReport("", OPG.SS());

    
    }
}
