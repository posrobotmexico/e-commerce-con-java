
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.FlapError;
import Operaciones.OpcionesEnvio;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class MigracionSoloSIMTerminal {
    
    public MigracionSoloSIMTerminal()
    {}

    public void Migracion(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
        
        String dn = CargaVariables.LeerCSV(0, "Migraciones SIM + Terminal", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Migraciones SIM + Terminal", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Migraciones SIM + Terminal", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Migraciones SIM + Terminal", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Migraciones SIM + Terminal", Caso);
        String correo = CargaVariables.LeerCSV(5, "Migraciones SIM + Terminal", Caso);
        String tel = CargaVariables.LeerCSV(6, "Migraciones SIM + Terminal", Caso);
        String calle = CargaVariables.LeerCSV(8, "Migraciones SIM + Terminal", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Migraciones SIM + Terminal", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Migraciones SIM + Terminal", Caso);
        String cp = CargaVariables.LeerCSV(7, "Migraciones SIM + Terminal", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Migraciones SIM + Terminal", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Migraciones SIM + Terminal", Caso);
        String ine = CargaVariables.LeerCSV(14, "Migraciones SIM + Terminal", Caso);
        String año = CargaVariables.LeerCSV(16, "Migraciones SIM + Terminal", Caso);
        String mes = CargaVariables.LeerCSV(17, "Migraciones SIM + Terminal", Caso);
        String  dia= CargaVariables.LeerCSV(18, "Migraciones SIM + Terminal", Caso);
        String  nombreTel= CargaVariables.LeerCSV(19, "Migraciones SIM + Terminal", Caso);
       

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        SelenElem.waitForLoadPage();
        Thread.sleep(3000);
        //SelenElem.Click(By.linkText("Cámbiate a un plan"));
        //SelenElem.Click(By.xpath("//a[@title='Cámbiate a Movistar']"));
        SelenElem.WaitForLoad(By.id("menu_list"), 15);
        OPG.SelectMenu("Cámbiate a un plan");
        
        SelenElem.WaitFor(By.id("sc3-dn"));
       
        SelenElem.Click(By.id("sc3-dn"));
        SelenElem.TypeText(By.id("sc3-dn"),dn);
         reporte.AddImageToReport("Se selecciona el flujo de Cámbiate a un plan", OPG.SS());
        Thread.sleep(3000);
        SelenElem.Click(By.id("sc3-goMigraStep2"));
        Thread.sleep(1000);
        //SelenElem.Click(By.id("sc3-goMigraStep2"));
        SelenElem.waitForLoadPage();
        
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        SelenElem.focus(By.id("firstCharacter"));
        reporte.AddImageToReport("Valida tu identidad", OPG.ScreenShotElement());
        Thread.sleep(4000);
        
        //SelenElem.Click(By.id("continue-btn"));
        SelenElem.ClickById(WebDrive.WebDriver, "continue-btn");
        
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("plan1"),10);
        SelenElem.Click(By.id("plan2"));
       // SelenElem.focus(By.linkText("Ver todos los equipos"));
        
        reporte.AddImageToReport("Seleccionamos con smartphone y continuar.", OPG.ScreenShotElement());
        
        Thread.sleep(1000);
        SelenElem.focus(By.linkText("Ver todos los equipos"));
        reporte.AddImageToReport("Damos clic en ver todos los equipos", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html/");
        
        reporte.AddImageToReport("Se selecciona el telefono", OPG.ScreenShotElementFocus(By.xpath("//a[@href='" + Url + "iphone-xr-lte-amarillo-64gb-apple.html']")));
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + nombreTel + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        
        reporte.AddImageToReport("Ver detalle del Smartphone", OPG.SS());
        
        SelenElem.focus(By.id("js_input_dn_button_soy_cliente"));
        reporte.AddImageToReport("Ver detalle del Smartphone", OPG.SS());
        
        SelenElem.focus(By.id("js_submit_miniparrilla_migracion"));
        reporte.AddImageToReport("Ver detalle del Smartphone", OPG.SS());
        
        SelenElem.Click(By.xpath("//*[@id=\"js_slider_mp_Migration\"]/div/div/div[3]/div/div/div/div"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Elegir plan Movistar", OPG.SS());
        SelenElem.Click(By.xpath("//*[@id=\"js_slider_mp_Migration_miniparrillas\"]/div/div/div[3]/div/div/div/div[1]/div[2]/div[7]/div"));
        
        Thread.sleep(1000);
        SelenElem.Click(By.id("js_submit_miniparrilla_migracion"));
        
        
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        SelenElem.focus(By.id("btn_continue_check"));
        reporte.AddImageToReport("", OPG.SS());
        
         SelenElem.Click(By.id("btn_continue_check"));
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-nombre"), 6);
        
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), año);
        SelenElem.SelectOption(By.className("ui-datepicker-month"), mes);
        SelenElem.Click(By.linkText(dia));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("calleNumeroInterior"), numInterior);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.focus(By.id("paso1-pospago-porta"));
        reporte.AddImageToReport("", OPG.SS()); 
        
        SelenElem.Click(By.id("paso1-pospago-porta"));
        OPG.cargaAjax2();
        
        reporte.AddImageToReport("Sección de consulta tu crédito del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("cod"), "1111");
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        SelenElem.Click(By.id("checkout-step2-radio-04"));
        SelenElem.Click(By.id("checkout-step2-radio-06"));
        SelenElem.Click(By.id("checkout-step2"));
        
        reporte.AddImageToReport("Sección de consulta tu crédito del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("creditCheck"));
        
        OPG.cargaAjax2();
        
        /*SelenElem.Click(By.id("inv-mail2"));
        SelenElem.WaitForLoad(By.id("cp"), 4);
        SelenElem.Click(By.id("cp"));
        SelenElem.TypeText(By.id("cp"), "06600");
        SelenElem.Click(By.id("longlat"));
        OPG.cargaAjax2(); 
        
        SelenElem.Click(By.id("99090073cac"));
        SelenElem.focus(By.id("99090005cac"));
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());
        
        SelenElem.focus(By.id("99090003cac"));
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());*/
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        OPG.cargaAjax2();
        
        SelenElem.focus(By.id("pasarAlPaso3"));
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("pasarAlPaso3"));
        OPG.cargaAjax2(); 
        
        SelenElem.WaitForLoad(By.id("checkTeminosCondiciones"), 10);
        SelenElem.Click(By.id("checkTeminosCondiciones"));
        SelenElem.Click(By.id("checkConsentimiento"));
        SelenElem.Click(By.id("checkConsentimientoRecarga"));
        
        SelenElem.Click(By.id("realizar-pago-2"));
        
        new Flap(reporte,OPG,SelenElem,WebDrive);
        
        SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"numero_migrar\"]"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/section[2]/section[2]/div[1]/dl[1]/dt"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/section[2]/section[2]/div[1]/dl[1]/dt"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/section[2]/footer/a[1]"));
        reporte.AddImageToReport("", OPG.SS());
        //*/
        
        
    }
    
    public void ErroDN(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
         
        String dn = CargaVariables.LeerCSV(0, "Migraciones SIM + Terminal", Caso);
        
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        SelenElem.waitForLoadPage();
        Thread.sleep(3000);
        //SelenElem.Click(By.linkText("Cámbiate a un plan"));
        //SelenElem.Click(By.xpath("//a[@title='Cámbiate a Movistar']"));
        SelenElem.WaitForLoad(By.id("menu_list"), 15);
        OPG.SelectMenu("Cámbiate a un plan");
        
        SelenElem.WaitFor(By.id("sc3-dn"));
       
        SelenElem.Click(By.id("sc3-dn"));
        SelenElem.TypeText(By.id("sc3-dn"),dn);
        reporte.AddImageToReport("Se selecciona el flujo de Cámbiate a un plan", OPG.SS());
        Thread.sleep(3000);
        SelenElem.Click(By.id("sc3-goMigraStep2"));
        Thread.sleep(500);
        reporte.AddImageToReport("Error en DN", OPG.SS());
            
        
    }
    
    public void ErrorINE(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url,String Caso) throws InterruptedException, IOException{
        
        String dn = CargaVariables.LeerCSV(0, "Migraciones SIM + Terminal", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Migraciones SIM + Terminal", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Migraciones SIM + Terminal", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Migraciones SIM + Terminal", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Migraciones SIM + Terminal", Caso);
        String correo = CargaVariables.LeerCSV(5, "Migraciones SIM + Terminal", Caso);
        String tel = CargaVariables.LeerCSV(6, "Migraciones SIM + Terminal", Caso);
        String calle = CargaVariables.LeerCSV(8, "Migraciones SIM + Terminal", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Migraciones SIM + Terminal", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Migraciones SIM + Terminal", Caso);
        String cp = CargaVariables.LeerCSV(7, "Migraciones SIM + Terminal", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Migraciones SIM + Terminal", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Migraciones SIM + Terminal", Caso);
        String ine = CargaVariables.LeerCSV(14, "Migraciones SIM + Terminal", Caso);
        String año = CargaVariables.LeerCSV(16, "Migraciones SIM + Terminal", Caso);
        String mes = CargaVariables.LeerCSV(17, "Migraciones SIM + Terminal", Caso);
        String  dia= CargaVariables.LeerCSV(18, "Migraciones SIM + Terminal", Caso);
        String  nombreTel= CargaVariables.LeerCSV(19, "Migraciones SIM + Terminal", Caso);
       

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        SelenElem.waitForLoadPage();
        Thread.sleep(3000);
        //SelenElem.Click(By.linkText("Cámbiate a un plan"));
        //SelenElem.Click(By.xpath("//a[@title='Cámbiate a Movistar']"));
        SelenElem.WaitForLoad(By.id("menu_list"), 15);
        OPG.SelectMenu("Cámbiate a un plan");
        
        SelenElem.WaitFor(By.id("sc3-dn"));
       
        SelenElem.Click(By.id("sc3-dn"));
        SelenElem.TypeText(By.id("sc3-dn"),dn);
         reporte.AddImageToReport("Se selecciona el flujo de Cámbiate a un plan", OPG.SS());
        Thread.sleep(3000);
        SelenElem.Click(By.id("sc3-goMigraStep2"));
        Thread.sleep(1000);
        //SelenElem.Click(By.id("sc3-goMigraStep2"));
        SelenElem.waitForLoadPage();
        
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        SelenElem.focus(By.id("firstCharacter"));
        reporte.AddImageToReport("Valida tu identidad", OPG.ScreenShotElement());
        Thread.sleep(4000);
        
        //SelenElem.Click(By.id("continue-btn"));
        SelenElem.ClickById(WebDrive.WebDriver, "continue-btn");
        
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("plan1"),10);
        SelenElem.focus(By.id("plan1"));
        SelenElem.Click(By.id("plan2"));
        
        
        reporte.AddImageToReport("Seleccionamos con smartphone y continuar.", OPG.ScreenShotElement());
        
        Thread.sleep(1000);
        SelenElem.focus(By.linkText("Ver todos los equipos"));
        reporte.AddImageToReport("Damos clic en ver todos los equipos", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html/");
        
        reporte.AddImageToReport("Se selecciona el telefono", OPG.ScreenShotElementFocus(By.xpath("//a[@href='" + Url + "iphone-xr-lte-amarillo-64gb-apple.html']")));
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + nombreTel + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        
        reporte.AddImageToReport("Ver detalle del Smartphone", OPG.SS());
        
        SelenElem.focus(By.id("js_input_dn_button_soy_cliente"));
        reporte.AddImageToReport("Ver detalle del Smartphone", OPG.SS());
        
        SelenElem.focus(By.id("js_submit_miniparrilla_migracion"));
        reporte.AddImageToReport("Ver detalle del Smartphone", OPG.SS());
        
        SelenElem.Click(By.xpath("//*[@id=\"js_slider_mp_Migration\"]/div/div/div[3]/div/div/div/div"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Elegir plan Movistar", OPG.SS());
        SelenElem.Click(By.xpath("//*[@id=\"js_slider_mp_Migration_miniparrillas\"]/div/div/div[3]/div/div/div/div[1]/div[2]/div[7]/div"));
        
        Thread.sleep(1000);
        SelenElem.Click(By.id("js_submit_miniparrilla_migracion"));
        
        
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        SelenElem.focus(By.id("btn_continue_check"));
        reporte.AddImageToReport("", OPG.SS());
        
         SelenElem.Click(By.id("btn_continue_check"));
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-nombre"), 6);
        
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), año);
        SelenElem.SelectOption(By.className("ui-datepicker-month"), mes);
        SelenElem.Click(By.linkText(dia));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("calleNumeroInterior"), numInterior);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.focus(By.id("paso1-pospago-porta"));
        SelenElem.Click(By.id("paso1-pospago-porta"));
        Thread.sleep(400);
        reporte.AddImageToReport("Error INE", OPG.ScreenShotElementInstant()); 
       
    }

    public void ErrorCP(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
        
        String dn = CargaVariables.LeerCSV(0, "Migraciones SIM + Terminal", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Migraciones SIM + Terminal", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Migraciones SIM + Terminal", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Migraciones SIM + Terminal", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Migraciones SIM + Terminal", Caso);
        String correo = CargaVariables.LeerCSV(5, "Migraciones SIM + Terminal", Caso);
        String tel = CargaVariables.LeerCSV(6, "Migraciones SIM + Terminal", Caso);
        String calle = CargaVariables.LeerCSV(8, "Migraciones SIM + Terminal", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Migraciones SIM + Terminal", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Migraciones SIM + Terminal", Caso);
        String cp = CargaVariables.LeerCSV(7, "Migraciones SIM + Terminal", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Migraciones SIM + Terminal", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Migraciones SIM + Terminal", Caso);
        String ine = CargaVariables.LeerCSV(14, "Migraciones SIM + Terminal", Caso);
        String año = CargaVariables.LeerCSV(16, "Migraciones SIM + Terminal", Caso);
        String mes = CargaVariables.LeerCSV(17, "Migraciones SIM + Terminal", Caso);
        String  dia= CargaVariables.LeerCSV(18, "Migraciones SIM + Terminal", Caso);
        String  nombreTel= CargaVariables.LeerCSV(19, "Migraciones SIM + Terminal", Caso);
       

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        SelenElem.waitForLoadPage();
        Thread.sleep(3000);
        //SelenElem.Click(By.linkText("Cámbiate a un plan"));
        //SelenElem.Click(By.xpath("//a[@title='Cámbiate a Movistar']"));
        SelenElem.WaitForLoad(By.id("menu_list"), 15);
        OPG.SelectMenu("Cámbiate a un plan");
        
        SelenElem.WaitFor(By.id("sc3-dn"));
       
        SelenElem.Click(By.id("sc3-dn"));
        SelenElem.TypeText(By.id("sc3-dn"),dn);
         reporte.AddImageToReport("Se selecciona el flujo de Cámbiate a un plan", OPG.SS());
        Thread.sleep(3000);
        SelenElem.Click(By.id("sc3-goMigraStep2"));
        Thread.sleep(1000);
        //SelenElem.Click(By.id("sc3-goMigraStep2"));
        SelenElem.waitForLoadPage();
        
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        SelenElem.focus(By.id("firstCharacter"));
        reporte.AddImageToReport("Valida tu identidad", OPG.ScreenShotElement());
        Thread.sleep(4000);
        
        //SelenElem.Click(By.id("continue-btn"));
        SelenElem.ClickById(WebDrive.WebDriver, "continue-btn");
        
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("plan1"),10);
        SelenElem.Click(By.id("plan2"));
        SelenElem.focus(By.id("plan2"));
        
        reporte.AddImageToReport("Seleccionamos con smartphone y continuar.", OPG.ScreenShotElement());
        
        Thread.sleep(1000);
        SelenElem.focus(By.linkText("Ver todos los equipos"));
        reporte.AddImageToReport("Damos clic en ver todos los equipos", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html/");
        
        reporte.AddImageToReport("Se selecciona el telefono", OPG.ScreenShotElementFocus(By.xpath("//a[@href='" + Url + "iphone-xr-lte-amarillo-64gb-apple.html']")));
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + nombreTel + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        
        reporte.AddImageToReport("Ver detalle del Smartphone", OPG.SS());
        
        SelenElem.focus(By.id("js_input_dn_button_soy_cliente"));
        reporte.AddImageToReport("Ver detalle del Smartphone", OPG.SS());
        
        SelenElem.focus(By.id("js_submit_miniparrilla_migracion"));
        reporte.AddImageToReport("Ver detalle del Smartphone", OPG.SS());
        
        SelenElem.Click(By.xpath("//*[@id=\"js_slider_mp_Migration\"]/div/div/div[3]/div/div/div/div"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Elegir plan Movistar", OPG.SS());
        SelenElem.Click(By.xpath("//*[@id=\"js_slider_mp_Migration_miniparrillas\"]/div/div/div[3]/div/div/div/div[1]/div[2]/div[7]/div"));
        
        Thread.sleep(1000);
        SelenElem.Click(By.id("js_submit_miniparrilla_migracion"));
        
        
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        SelenElem.focus(By.id("btn_continue_check"));
        reporte.AddImageToReport("", OPG.SS());
        
         SelenElem.Click(By.id("btn_continue_check"));
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-nombre"), 6);
        
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), año);
        SelenElem.SelectOption(By.className("ui-datepicker-month"), mes);
        SelenElem.Click(By.linkText(dia));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("calleNumeroInterior"), numInterior);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.focus(By.id("paso1-pospago-porta"));
        reporte.AddImageToReport("", OPG.SS()); 
        
        SelenElem.Click(By.id("paso1-pospago-porta"));
        OPG.cargaAjax2();
        
        reporte.AddImageToReport("Sección de consulta tu crédito del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("cod"), "1111");
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        SelenElem.Click(By.id("checkout-step2-radio-04"));
        SelenElem.Click(By.id("checkout-step2-radio-06"));
        SelenElem.Click(By.id("checkout-step2"));
        
        reporte.AddImageToReport("Sección de consulta tu crédito del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("creditCheck"));
        
        OPG.cargaAjax2();
        
        /*SelenElem.Click(By.id("inv-mail2"));
        SelenElem.WaitForLoad(By.id("cp"), 4);
        SelenElem.Click(By.id("cp"));
        SelenElem.TypeText(By.id("cp"), "2200");
        SelenElem.Click(By.id("longlat"));
        reporte.AddImageToReport("Error Codigo Postal", OPG.SS());*/
        SelenElem.WaitForLoad(By.id("inv-mail2"), 2);
        SelenElem.Click(By.id("inv-mail2"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("js_codigo_postal"), "2200");
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        Thread.sleep(500);
        reporte.AddImageToReport("Validacion Error en codigo postal", OPG.ScreenShotElementInstant());
        
        
        
        
    }

    public void ErrorPago(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
         
        String dn = CargaVariables.LeerCSV(0, "Migraciones SIM + Terminal", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Migraciones SIM + Terminal", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Migraciones SIM + Terminal", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Migraciones SIM + Terminal", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Migraciones SIM + Terminal", Caso);
        String correo = CargaVariables.LeerCSV(5, "Migraciones SIM + Terminal", Caso);
        String tel = CargaVariables.LeerCSV(6, "Migraciones SIM + Terminal", Caso);
        String calle = CargaVariables.LeerCSV(8, "Migraciones SIM + Terminal", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Migraciones SIM + Terminal", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Migraciones SIM + Terminal", Caso);
        String cp = CargaVariables.LeerCSV(7, "Migraciones SIM + Terminal", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Migraciones SIM + Terminal", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Migraciones SIM + Terminal", Caso);
        String ine = CargaVariables.LeerCSV(14, "Migraciones SIM + Terminal", Caso);
        String año = CargaVariables.LeerCSV(16, "Migraciones SIM + Terminal", Caso);
        String mes = CargaVariables.LeerCSV(17, "Migraciones SIM + Terminal", Caso);
        String  dia= CargaVariables.LeerCSV(18, "Migraciones SIM + Terminal", Caso);
        String  nombreTel= CargaVariables.LeerCSV(19, "Migraciones SIM + Terminal", Caso);
       

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        SelenElem.waitForLoadPage();
        Thread.sleep(3000);
        //SelenElem.Click(By.linkText("Cámbiate a un plan"));
        //SelenElem.Click(By.xpath("//a[@title='Cámbiate a Movistar']"));
        SelenElem.WaitForLoad(By.id("menu_list"), 15);
        OPG.SelectMenu("Cámbiate a un plan");
        
        SelenElem.WaitFor(By.id("sc3-dn"));
       
        SelenElem.Click(By.id("sc3-dn"));
        SelenElem.TypeText(By.id("sc3-dn"),dn);
         reporte.AddImageToReport("Se selecciona el flujo de Cámbiate a un plan", OPG.SS());
        Thread.sleep(3000);
        SelenElem.Click(By.id("sc3-goMigraStep2"));
        Thread.sleep(1000);
        //SelenElem.Click(By.id("sc3-goMigraStep2"));
        SelenElem.waitForLoadPage();
        
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        SelenElem.focus(By.id("firstCharacter"));
        reporte.AddImageToReport("Valida tu identidad", OPG.ScreenShotElement());
        Thread.sleep(4000);
        
        //SelenElem.Click(By.id("continue-btn"));
        SelenElem.ClickById(WebDrive.WebDriver, "continue-btn");
        
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("plan1"),10);
        SelenElem.Click(By.id("plan2"));
        SelenElem.focus(By.id("plan2"));
        
        reporte.AddImageToReport("Seleccionamos con smartphone y continuar.", OPG.ScreenShotElement());
        
        Thread.sleep(1000);
        SelenElem.focus(By.linkText("Ver todos los equipos"));
        reporte.AddImageToReport("Damos clic en ver todos los equipos", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html/");
        
        reporte.AddImageToReport("Se selecciona el telefono", OPG.ScreenShotElementFocus(By.xpath("//a[@href='" + Url + "iphone-xr-lte-amarillo-64gb-apple.html']")));
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + nombreTel + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        
        reporte.AddImageToReport("Ver detalle del Smartphone", OPG.SS());
        
        SelenElem.focus(By.id("js_input_dn_button_soy_cliente"));
        reporte.AddImageToReport("Ver detalle del Smartphone", OPG.SS());
        
        SelenElem.focus(By.id("js_submit_miniparrilla_migracion"));
        reporte.AddImageToReport("Ver detalle del Smartphone", OPG.SS());
        
        SelenElem.Click(By.xpath("//*[@id=\"js_slider_mp_Migration\"]/div/div/div[3]/div/div/div/div"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Elegir plan Movistar", OPG.SS());
        SelenElem.Click(By.xpath("//*[@id=\"js_slider_mp_Migration_miniparrillas\"]/div/div/div[3]/div/div/div/div[1]/div[2]/div[7]/div"));
        
        Thread.sleep(1000);
        SelenElem.Click(By.id("js_submit_miniparrilla_migracion"));
        
        
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        SelenElem.focus(By.id("btn_continue_check"));
        reporte.AddImageToReport("", OPG.SS());
        
         SelenElem.Click(By.id("btn_continue_check"));
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-nombre"), 6);
        
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), año);
        SelenElem.SelectOption(By.className("ui-datepicker-month"), mes);
        SelenElem.Click(By.linkText(dia));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("calleNumeroInterior"), numInterior);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.focus(By.id("paso1-pospago-porta"));
        reporte.AddImageToReport("", OPG.SS()); 
        
        SelenElem.Click(By.id("paso1-pospago-porta"));
        OPG.cargaAjax2();
        
        reporte.AddImageToReport("Sección de consulta tu crédito del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("cod"), "1111");
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        SelenElem.Click(By.id("checkout-step2-radio-04"));
        SelenElem.Click(By.id("checkout-step2-radio-06"));
        SelenElem.Click(By.id("checkout-step2"));
        
        reporte.AddImageToReport("Sección de consulta tu crédito del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("creditCheck"));
        
        OPG.cargaAjax2();
        
        /*SelenElem.Click(By.id("inv-mail2"));
        SelenElem.WaitForLoad(By.id("cp"), 4);
        SelenElem.Click(By.id("cp"));
        SelenElem.TypeText(By.id("cp"), "06600");
        SelenElem.Click(By.id("longlat"));
        OPG.cargaAjax2(); 
        
        SelenElem.Click(By.id("99090073cac"));
        SelenElem.focus(By.id("99090005cac"));
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());
        
        SelenElem.focus(By.id("99090003cac"));
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());
        */
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        OPG.cargaAjax2();
        
        SelenElem.focus(By.id("pasarAlPaso3"));
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("pasarAlPaso3"));
        OPG.cargaAjax2(); 
        
        SelenElem.WaitForLoad(By.id("checkTeminosCondiciones"), 10);
        SelenElem.Click(By.id("checkTeminosCondiciones"));
        SelenElem.Click(By.id("checkConsentimiento"));
        SelenElem.Click(By.id("checkConsentimientoRecarga"));
        
        SelenElem.Click(By.id("realizar-pago-2"));
        
        new FlapError(reporte,OPG,SelenElem,WebDrive);
        
        
        //*/
        
    }
}
