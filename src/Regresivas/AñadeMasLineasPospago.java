
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.FlapError;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class AñadeMasLineasPospago {
    
    public AñadeMasLineasPospago(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
       
        String dn = CargaVariables.LeerCSV(0, "Añade Mas Lineas Pospago", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Añade Mas Lineas Pospago", Caso);
        String email = CargaVariables.LeerCSV(2, "Añade Mas Lineas Pospago", Caso);

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        SelenElem.waitForLoadPage();
        /*
        //SelenElem.Click(By.linkText("Planes"));
        //SelenElem.Click(By.xpath("//*[@id=\"menu_list\"]/li[2]/a"));
        SelenElem.WaitForLoad(By.id("menu_list"), 15);
        OPG.SelectMenu("Planes");
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Seleccionar el flujo de Planes ", OPG.SS());
        SelenElem.WaitForLoad(By.xpath("//*[@id=\"subcontent1\"]/div/div[1]/ul/li[4]/a"), 10);
        Thread.sleep(1000);
        SelenElem.Click(By.xpath("//*[@id=\"subcontent1\"]/div/div[1]/ul/li[4]/a"));
        Thread.sleep(500);
        reporte.AddImageToReport("Seleccionar categoría Family+ Nuevo -> Quiero agregar líneas ", OPG.SS());
        */

        WebDrive.WebDriver.navigate().to(Url + "adiciones/");
        Thread.sleep(1000);
        SelenElem.waitForLoadPage();
        
        SelenElem.TypeText(By.id("dn_movistar"), dn);
        Thread.sleep(2000);
        reporte.AddImageToReport("Ingresar el DN", OPG.SS());
        SelenElem.Click(By.id("btn_additional"));
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.TypeText(By.id("valida-rfc"), "1");
        reporte.AddImageToReport("Ingresar el rfc", OPG.SS());
        SelenElem.Click(By.id("btn_additional"));
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        reporte.AddImageToReport("Validar identidad", OPG.SS());
        Thread.sleep(7000);
        SelenElem.ClickByXpath(WebDrive.WebDriver,"//*[@id=\"continue-btn2\"]/div");
        //SelenElem.ClickById(WebDrive.WebDriver,"continue-btn2");
        //SelenElem.ClickById(WebDrive.WebDriver,"continue-btn2");
        reporte.AddImageToReport("Elegir un plan", OPG.SS());
        
        
        //SelenElem.focus(By.xpath("//*[@id=\"js_setTabIndex0\"]/div/div[7]/button"));
        //reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"js_setTabIndex0\"]/div/div[5]/div[1]/img"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//button[.='Quiero este plan']"));
        reporte.AddImageToReport("", OPG.SS());
        //SelenElem.Click(By.xpath("//button[.='Quiero este plan']"));
        Thread.sleep(2000);
        SelenElem.Click(By.id("Plan-ilimitado-Plus"));
        SelenElem.WaitForLoad(By.id("submitplanes"), 10);
        Thread.sleep(3000);
        //SelenElem.focus(By.id("submitplanes"));
        reporte.AddImageToReport("Sin SVA", OPG.SS());
        SelenElem.Click(By.id("submitplanes"));
        reporte.AddImageToReport("Ver resumen de compra", OPG.SS());
        
        
        SelenElem.focus(By.id("continueToCheckout"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.id("continueToCheckout"));
        SelenElem.TypeText(By.id("valid-email"), email);
         SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        reporte.AddImageToReport("Sección tus datos del checkout", OPG.SS());
        SelenElem.Click(By.id("btn_continuar"));
        OPG.cargaAjax2();
        
          
        
        SelenElem.Click(By.id("inv-mail2"));
        SelenElem.WaitForLoad(By.id("js_codigo_postal"),15);
        SelenElem.TypeText(By.id("js_codigo_postal"), "0660");
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        SelenElem.WaitForLoad(By.id("searchCp"), 4);
        reporte.AddImageToReport("Validacion error codigo postal", OPG.SS());
        
        SelenElem.TypeText(By.id("js_codigo_postal"), "0");
        SelenElem.waitForLoadPage();
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        OPG.cargaAjax2();
        SelenElem.Click(By.id("99090073cac"));
        SelenElem.focus(By.id("99090005cac"));
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        SelenElem.focus(By.id("99090003cac"));
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        SelenElem.focus(By.id("99090028cac"));
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        SelenElem.focus(By.id("btn_continuar_2"));
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        
        SelenElem.Click(By.id("btn_continuar_2"));
        OPG.cargaAjax2();
        
        SelenElem.WaitForLoad(By.xpath("//*[@id=\"msmx-pospago\"]/aside/section[1]/h4"),10);
        SelenElem.focus(By.xpath("//*[@id=\"msmx-pospago\"]/aside/section[1]/h4"));
        reporte.AddImageToReport("Sección contrato del checkout", OPG.SS());
        SelenElem.Click(By.id("checkConsentimiento"));
        SelenElem.Click(By.id("checkTerms"));
        reporte.AddImageToReport("Sección contrato del checkout", OPG.SS());
        
        SelenElem.Click(By.id("end-step"));
        SelenElem.focus(By.id("changeone"));
        
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        SelenElem.focus(By.id("pay"));
        reporte.AddImageToReport("", OPG.SS());
        //SelenElem.Click(By.id("changeone"));
        
        SelenElem.ClickById(WebDrive.WebDriver,"pay");
        
         new Flap( reporte, OPG, SelenElem, WebDrive);
         
        SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"init-additions\"]/button"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[2]/div[2]/div[2]/div[3]/div/div/div/div/ul/li[2]/a"));
        reporte.AddImageToReport("", OPG.SS());
    
    //*/
    }
    
}
