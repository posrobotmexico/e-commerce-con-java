
package Regresivas;

import Core.DeviceActions;
import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.FlapPorta;
import Operaciones.OpcionesEnvio;
import Operaciones.OperacionesGenerales;
import io.appium.java_client.TouchAction;
import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 *
 * @author LAPTOP-JORGE
 */
public class PortabilidadPospago {
    
     public PortabilidadPospago() {
    
    }

    public void PortabilidadPospagosoloSIMterminalLiferay(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
         
        String dn = CargaVariables.LeerCSV(0, "Portabilidad Pospago", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Portabilidad Pospago", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Portabilidad Pospago", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Portabilidad Pospago", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Portabilidad Pospago", Caso);
        String correo = CargaVariables.LeerCSV(5, "Portabilidad Pospago", Caso);
        String tel = CargaVariables.LeerCSV(6, "Portabilidad Pospago", Caso);
        String calle = CargaVariables.LeerCSV(8, "Portabilidad Pospago", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Portabilidad Pospago", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Portabilidad Pospago", Caso);
        String cp = CargaVariables.LeerCSV(7, "Portabilidad Pospago", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Portabilidad Pospago", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Portabilidad Pospago", Caso);
        String ine = CargaVariables.LeerCSV(14, "Portabilidad Pospago", Caso);
        String año = CargaVariables.LeerCSV(16, "Portabilidad Pospago", Caso);
        String mes = CargaVariables.LeerCSV(17, "Portabilidad Pospago", Caso);
        String  dia= CargaVariables.LeerCSV(18, "Portabilidad Pospago", Caso);
        String curp= CargaVariables.LeerCSV(19, "Portabilidad Pospago", Caso);
        String skuplan= CargaVariables.LeerCSV(20, "Portabilidad Pospago", Caso);
        String skuterminal= CargaVariables.LeerCSV(21, "Portabilidad Pospago", Caso);
        String NumTarjeta = CargaVariables.LeerCSV(22, "Portabilidad Pospago", Caso);
        String MesTarjeta = CargaVariables.LeerCSV(23, "Portabilidad Pospago", Caso);
        String AñoTarjeta = CargaVariables.LeerCSV(24, "Portabilidad Pospago", Caso);
        String Cvv = CargaVariables.LeerCSV(25, "Portabilidad Pospago", Caso);

        WebDrive.WebDriver.navigate().to(Url + "liferay.php");
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        Thread.sleep(2000);
        SelenElem.SelectOption(By.id("flowCode"), "portabilidad");
        Thread.sleep(1000);
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.Find(By.id("skuPlan")).clear();
        SelenElem.Find(By.id("namePlan")).clear();
        SelenElem.TypeText(By.id("skuPlan"), skuplan);
        SelenElem.Find(By.id("skuRecarga")).clear();
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.TypeText(By.id("skuTerminal"), skuterminal);
        SelenElem.Find(By.id("monto")).clear();
        SelenElem.Find(By.id("rfc")).clear();
        SelenElem.Find(By.id("skuDn")).clear();
        SelenElem.TypeText(By.id("skuDn"), dn);
        reporte.AddImageToReport("Ingresamos los datos y damos clic en enviar", OPG.SS());
        SelenElem.Click(By.name("Comprar"));
       
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-name"), 15);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-name"), nombre);
        SelenElem.TypeText(By.id("valid-apaterno"), apellidop);
        SelenElem.TypeText(By.id("valid-amaterno"), apellidom);
        
        SelenElem.TypeText(By.id("valid-phone"), tel);
        //da.slide("Up", 2);
        da.hideKeyBoard();
        SelenElem.Click(By.id("validaPrivacidad"));
        Thread.sleep(3000);
        da.clickCoordenadas(0, 45);
        SelenElem.Click(By.id("validaFecha"));
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("btnStep1Principal"));
        
        SelenElem.Click(By.id("dateTo_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), año);
        SelenElem.SelectOption(By.className("ui-datepicker-month"), mes);
        SelenElem.Click(By.linkText(dia));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("valid-curp"), curp);
        SelenElem.TypeText(By.id("cod-posta-porta"), cp);
        SelenElem.TypeText(By.id("calle-porta-ref"), calle);

        OPG.cargaAjax2();
        
        SelenElem.TypeText(By.id("nExter-porta"), numdomicilio);
        SelenElem.TypeText(By.id("nInter-porta"), numInterior);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.SelectOption(By.id("colonia-porta-ref"), colonia);

        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        
        SelenElem.Click(By.id("btnStep1-2Principal"));
        Thread.sleep(1000);
        
        SelenElem.WaitForLoad(By.id("cod"), 15);
        Thread.sleep(3000);
        SelenElem.TypeText(By.id("cod"), "1111");
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        SelenElem.Click(By.id("validaScore"));
        reporte.AddImageToReport("Sección Consulta de crédito del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("btnStep1-3Principal"));
        
        Thread.sleep(7000);
        reporte.AddImageToReport("Sección opciones de envío del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("btnStep2"));
        
        SelenElem.WaitForLoad(By.xpath("//input[@placeholder='1']"), 10);
        Thread.sleep(2000);
        SelenElem.Click(By.xpath("//input[@placeholder='1']"));
        SelenElem.TypeText(By.xpath("//input[@placeholder='1']"), "1234");
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección Nip del Checkout", OPG.SS());  
        SelenElem.Click(By.id("btnStep3"));
        
        OPG.cargaAjax2();
        Thread.sleep(2000);
        SelenElem.focus(By.id("validaContrato"));
        SelenElem.Click(By.id("validaContrato"));
        da.slide("Down", 1);
        reporte.AddImageToReport("Sección Contrato del Checkout", OPG.ScreenShotElement());
        
        Thread.sleep(2000);
        SelenElem.Click(By.id("btnStep4_resume"));
        
        SelenElem.WaitForLoad(By.id("ui-accordion-itemsSale-header-0"), 20);
        reporte.AddImageToReport("Medio de pago Flap", OPG.ScreenShotElementInstant());
        
        SelenElem.TypeText(By.id("numTarjeta"), NumTarjeta);
        SelenElem.SelectOption(By.id("vigenciames"), MesTarjeta);
        SelenElem.SelectOption(By.id("vigenciaanio"), AñoTarjeta);
        SelenElem.TypeText(By.id("cvv2"), Cvv);
        da.hideKeyBoard();
        reporte.AddImageToReport("", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("continuar"));
        
        SelenElem.WaitForLoad(By.id("btnFinalizar"), 10);
        da.slide("Down", 2);
        reporte.AddImageToReport("Información acerca del pago Flap", OPG.ScreenShotElementInstant());
        
        SelenElem.WaitForLoad(By.id("numero_portar"), 20);
        reporte.AddImageToReport("Página de pago realizada con exito ", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.ScreenShotElementInstant());
        
    }
    public void PortabilidadPospagosoloSIMterminal(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
         
        String dn = CargaVariables.LeerCSV(0, "Portabilidad Pospago", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Portabilidad Pospago", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Portabilidad Pospago", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Portabilidad Pospago", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Portabilidad Pospago", Caso);
        String correo = CargaVariables.LeerCSV(5, "Portabilidad Pospago", Caso);
        String tel = CargaVariables.LeerCSV(6, "Portabilidad Pospago", Caso);
        String calle = CargaVariables.LeerCSV(8, "Portabilidad Pospago", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Portabilidad Pospago", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Portabilidad Pospago", Caso);
        String cp = CargaVariables.LeerCSV(7, "Portabilidad Pospago", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Portabilidad Pospago", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Portabilidad Pospago", Caso);
        String ine = CargaVariables.LeerCSV(14, "Portabilidad Pospago", Caso);
        String año = CargaVariables.LeerCSV(16, "Portabilidad Pospago", Caso);
        String mes = CargaVariables.LeerCSV(17, "Portabilidad Pospago", Caso);
        String  dia= CargaVariables.LeerCSV(18, "Portabilidad Pospago", Caso);
        String curp= CargaVariables.LeerCSV(19, "Portabilidad Pospago", Caso);
        String modeloTel= CargaVariables.LeerCSV(20, "Portabilidad Pospago", Caso);

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        
        SelenElem.Click(By.xpath("//span[.='Portabilidad']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar Portabilidad > Prepago", OPG.SS());
        
        SelenElem.Click(By.xpath("//span[.='Pospago']"));
        Thread.sleep(4000);
        reporte.AddImageToReport("Seleccionar smartphone ", OPG.SS());
        SelenElem.focus(By.xpath("//img[contains(@alt, '" + modeloTel + "')]"));
        SelenElem.ClickByXpath(WebDrive.WebDriver,"//img[contains(@alt, '" + modeloTel + "')]"); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        
        SelenElem.WaitForLoad(By.xpath("//*[@id=\"solo-smartphone__content\"]/form/button"),10);
        reporte.AddImageToReport("Visualizar el detalle del smartphone e ingresar DN y dar click en “Usar este número”", OPG.SS());
        
        Thread.sleep(1000);
        SelenElem.Click(By.id("portabilidad"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_input_dn_button_portabilidad"), 10);
        SelenElem.TypeText(By.id("js_dn_input_portabilidad"),dn);
        reporte.AddImageToReport(" ", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport(" ", OPG.SS());
        
        SelenElem.Click(By.id("js_input_dn_button_portabilidad"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.className("temm-2022-modal"), 10);
        Thread.sleep(2000);
        reporte.AddImageToReport(" ", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.xpath("//span[.='Continuar']"));
        
        
        SelenElem.Click(By.xpath("//*[@id=\"js_slider_mp_portabilidad-pospago\"]/div/div/div[3]/div/div/div/div"));
        Thread.sleep(500);
        SelenElem.WaitForLoad(By.xpath("//button[.='Quiero este plan']"), 10);
        reporte.AddImageToReport(" ", OPG.SS());
        
        SelenElem.Click(By.xpath("//button[.='Quiero este plan']"));
        
        Thread.sleep(2000);
        SelenElem.Click(By.id("js_submit_miniparrilla_portabilidad_pospago"));
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("continueToCheckout"), 10);
        
        /*reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementFocus(By.id("section-equipo")));
        SelenElem.Click(By.xpath("//button[@title='Da clic aquí para ir al carrito de compra']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("continueToCheckout"));*/
       
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-name"), 15);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-name"), nombre);
        SelenElem.TypeText(By.id("valid-apaterno"), apellidop);
        SelenElem.TypeText(By.id("valid-amaterno"), apellidom);
        
        SelenElem.TypeText(By.id("valid-phone"), tel);
        da.hideKeyBoard();
        SelenElem.Click(By.id("validaPrivacidad"));
        Thread.sleep(5000);
        da.clickCoordenadas(0, 45);
        da.slide("Down", 1);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.Click(By.id("btnStep1Principal"));
        
        SelenElem.Click(By.id("dateTo_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), año);
        SelenElem.SelectOption(By.className("ui-datepicker-month"), mes);
        SelenElem.Click(By.linkText(dia));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("valid-curp"), curp);
        SelenElem.TypeText(By.id("calle-porta-ref"), calle);
        SelenElem.TypeText(By.id("cod-posta-porta"), cp);
        //SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        //SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        
        SelenElem.Click(By.id("colonia-porta-ref"));
        SelenElem.SelectOption(By.id("colonia-porta-ref"), colonia);
        SelenElem.TypeText(By.id("nExter-porta"), numdomicilio);
        SelenElem.TypeText(By.id("nInter-porta"), numInterior);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());     
        //SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        //SelenElem.TypeText(By.id("INE-IFE"), ine);
        //SelenElem.Click(By.id("checkPrivacyOne"));
        //da.hideKeyBoard();
        //reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        
        //SelenElem.Click(By.id("checkout-step4-check-02"));
        //Thread.sleep(500);
        //SelenElem.Click(By.id("checkout-step4-check-02"));
        
        SelenElem.Click(By.id("btnStep1-2Principal"));
        
        Thread.sleep(5000);
        
        SelenElem.WaitForLoad(By.id("cod"), 10);
        SelenElem.TypeText(By.id("cod"), "1111");
        da.hideKeyBoard();
        da.slide("Up", 2);
        reporte.AddImageToReport("Sección Consulta de credito del Checkout", OPG.SS()); 
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        SelenElem.Click(By.id("checkout-step2-radio-04"));
        SelenElem.Click(By.id("checkout-step2-radio-06"));
        SelenElem.Click(By.id("validaScore"));
        
        reporte.AddImageToReport("Sección Consulta de credito del Checkout", OPG.SS()); 
        
        SelenElem.Click(By.id("btnStep1-3Principal"));
        
        OPG.cargaAjax2();
        /*SelenElem.WaitForLoad(By.id("inv-mail2"), 6);
        SelenElem.Click(By.id("inv-mail2"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("cp"), 15);
        SelenElem.Click(By.id("cp"));
        SelenElem.TypeText(By.id("cp"), cp);
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("longlat"));
        OPG.cargaAjax2();
        
        SelenElem.WaitForLoad(By.id("99090073cac"),6);
        SelenElem.Click(By.id("99090073cac"));
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());*/
        //new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        SelenElem.Click(By.id("btnStep2"));
        
        //da.slide("Down", 2);
        //reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        //da.slide("Down", 2);
        //reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        //da.slide("Down", 1);
        //reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());     
        //SelenElem.Click(By.id("checkout-paso2-renovaciones"));
        
        Thread.sleep(3000);
        //OPG.cargaAjax2();
        
        SelenElem.Click(By.xpath("//input[@class='nip__input']"));
        SelenElem.TypeText(By.xpath("//input[@class='nip__input']"), "1234");
        //SelenElem.TypeText(By.id("valid-nip"), "1234");
        da.hideKeyBoard();
        //SelenElem.TypeText(By.id("valid-curp"), curp);
        //SelenElem.Click(By.id("dateToPorta"));
        reporte.AddImageToReport("Sección validar NIP", OPG.SS());  
        SelenElem.Click(By.id("btnStep3"));
        
        OPG.cargaAjax2();
        
        //SelenElem.Click(By.id("checkTeminosCondiciones"));
        SelenElem.Click(By.id("validaContrato"));
        reporte.AddImageToReport("Sección Contrato del Checkout", OPG.ScreenShotElement());
        
        Thread.sleep(2000);
        SelenElem.Click(By.id("btnStep4_resume"));
        
        new FlapPorta(reporte,OPG,SelenElem,WebDrive);
        
        //SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
        Thread.sleep(5000);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        //da.slide("Down", 2);
        //reporte.AddImageToReport("", OPG.SS());
        //*/
    
    }

     public void ErroresNIPPortabilidadPospagosoloSIMterminal(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
         
        String dn = CargaVariables.LeerCSV(0, "Portabilidad Pospago", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Portabilidad Pospago", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Portabilidad Pospago", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Portabilidad Pospago", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Portabilidad Pospago", Caso);
        String correo = CargaVariables.LeerCSV(5, "Portabilidad Pospago", Caso);
        String tel = CargaVariables.LeerCSV(6, "Portabilidad Pospago", Caso);
        String calle = CargaVariables.LeerCSV(8, "Portabilidad Pospago", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Portabilidad Pospago", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Portabilidad Pospago", Caso);
        String cp = CargaVariables.LeerCSV(7, "Portabilidad Pospago", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Portabilidad Pospago", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Portabilidad Pospago", Caso);
        String ine = CargaVariables.LeerCSV(14, "Portabilidad Pospago", Caso);
        String año = CargaVariables.LeerCSV(16, "Portabilidad Pospago", Caso);
        String mes = CargaVariables.LeerCSV(17, "Portabilidad Pospago", Caso);
        String  dia= CargaVariables.LeerCSV(18, "Portabilidad Pospago", Caso);
        String curp= CargaVariables.LeerCSV(19, "Portabilidad Pospago", Caso);
        String modeloTel= CargaVariables.LeerCSV(20, "Portabilidad Pospago", Caso);
       

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        
        SelenElem.Click(By.xpath("//span[.='Portabilidad']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar Portabilidad > Prepago", OPG.SS());
        
        SelenElem.Click(By.xpath("//span[.='Pospago']"));
        Thread.sleep(4000);
        reporte.AddImageToReport("Seleccionar smartphone ", OPG.SS());
        
        da.slide("Down", 4);
        SelenElem.ClickByXpath(WebDrive.WebDriver,"//img[contains(@alt, '" + modeloTel + "')]"); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        
        SelenElem.WaitForLoad(By.xpath("//*[@id=\"solo-smartphone__content\"]/form/button"),10);
        reporte.AddImageToReport("Visualizar el detalle del smartphone e ingresar DN y dar click en “Usar este número”", OPG.SS());
        
        Thread.sleep(1000);
        SelenElem.Click(By.id("portabilidad"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_input_dn_button_portabilidad"), 10);
        SelenElem.TypeText(By.id("js_dn_input_portabilidad"),dn);
        reporte.AddImageToReport(" ", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport(" ", OPG.SS());
        
        SelenElem.Click(By.id("js_input_dn_button_portabilidad"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.className("temm-2022-modal"), 10);
        Thread.sleep(2000);
        reporte.AddImageToReport(" ", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.xpath("//span[.='Continuar']"));
        
        
        SelenElem.Click(By.xpath("//*[@id=\"js_slider_mp_portabilidad-pospago\"]/div/div/div[3]/div/div/div/div"));
        Thread.sleep(500);
        SelenElem.WaitForLoad(By.xpath("//div[.='Quiero este plan']"), 10);
        reporte.AddImageToReport(" ", OPG.SS());
        
        SelenElem.Click(By.xpath("//div[.='Quiero este plan']"));
        
        Thread.sleep(2000);
        SelenElem.Click(By.id("js_submit_miniparrilla_portabilidad_pospago"));
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("continueToCheckout"), 10);
        
        
       
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-nombre"), 15);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        da.slide("Up", 2);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), año);
        SelenElem.SelectOption(By.className("ui-datepicker-month"), mes);
        SelenElem.Click(By.linkText(dia));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("calleNumeroInterior"), numInterior);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        
        SelenElem.Click(By.id("checkout-step4-check-02"));
        Thread.sleep(500);
        SelenElem.Click(By.id("checkout-step4-check-02"));
        
        SelenElem.Click(By.id("paso1-pospago-porta"));
        
        Thread.sleep(5000);
        
        SelenElem.WaitForLoad(By.id("cod"), 10);
        SelenElem.TypeText(By.id("cod"), "1111");
        da.hideKeyBoard();
        da.slide("Up", 2);
        reporte.AddImageToReport("Sección Consulta de credito del Checkout", OPG.SS()); 
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        SelenElem.Click(By.id("checkout-step2-radio-04"));
        SelenElem.Click(By.id("checkout-step2-radio-06"));
        SelenElem.Click(By.id("checkout-step2"));
        
        reporte.AddImageToReport("Sección Consulta de credito del Checkout", OPG.SS()); 
        
        SelenElem.Click(By.id("botonConsultaCredito"));
        
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        da.slide("Down", 1);
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());     
        SelenElem.Click(By.id("checkout-paso2-renovaciones"));
        
        Thread.sleep(3000);
        //OPG.cargaAjax2();
        
        SelenElem.Click(By.id("checkout-paso4-porta-pre"));
        Thread.sleep(500);
        reporte.AddImageToReport("Error en NIP, CURP y Fecha Ventana de Cambio", OPG.ScreenShotElementInstant());  
        
        OPG.cargaAjax2();
    }

    public void ErrorCPPortabilidadPospagosoloSIMterminal(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
         
        String dn = CargaVariables.LeerCSV(0, "Portabilidad Pospago", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Portabilidad Pospago", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Portabilidad Pospago", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Portabilidad Pospago", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Portabilidad Pospago", Caso);
        String correo = CargaVariables.LeerCSV(5, "Portabilidad Pospago", Caso);
        String tel = CargaVariables.LeerCSV(6, "Portabilidad Pospago", Caso);
        String calle = CargaVariables.LeerCSV(8, "Portabilidad Pospago", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Portabilidad Pospago", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Portabilidad Pospago", Caso);
        String cp = CargaVariables.LeerCSV(7, "Portabilidad Pospago", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Portabilidad Pospago", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Portabilidad Pospago", Caso);
        String ine = CargaVariables.LeerCSV(14, "Portabilidad Pospago", Caso);
        String año = CargaVariables.LeerCSV(16, "Portabilidad Pospago", Caso);
        String mes = CargaVariables.LeerCSV(17, "Portabilidad Pospago", Caso);
        String  dia= CargaVariables.LeerCSV(18, "Portabilidad Pospago", Caso);
        String curp= CargaVariables.LeerCSV(19, "Portabilidad Pospago", Caso);
        String modeloTel= CargaVariables.LeerCSV(20, "Portabilidad Pospago", Caso);
       

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        
        SelenElem.Click(By.xpath("//span[.='Portabilidad']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar Portabilidad > Prepago", OPG.SS());
        
        SelenElem.Click(By.xpath("//span[.='Pospago']"));
        Thread.sleep(4000);
        reporte.AddImageToReport("Seleccionar smartphone ", OPG.SS());
        
        da.slide("Down", 4);
        SelenElem.ClickByXpath(WebDrive.WebDriver,"//img[contains(@alt, '" + modeloTel + "')]"); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        
        SelenElem.WaitForLoad(By.xpath("//*[@id=\"solo-smartphone__content\"]/form/button"),10);
        reporte.AddImageToReport("Visualizar el detalle del smartphone e ingresar DN y dar click en “Usar este número”", OPG.SS());
        
        Thread.sleep(1000);
        SelenElem.Click(By.id("portabilidad"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_input_dn_button_portabilidad"), 10);
        SelenElem.TypeText(By.id("js_dn_input_portabilidad"),dn);
        reporte.AddImageToReport(" ", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport(" ", OPG.SS());
        
        SelenElem.Click(By.id("js_input_dn_button_portabilidad"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.className("temm-2022-modal"), 10);
        Thread.sleep(2000);
        reporte.AddImageToReport(" ", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.xpath("//span[.='Continuar']"));
        
        
        SelenElem.Click(By.xpath("//*[@id=\"js_slider_mp_portabilidad-pospago\"]/div/div/div[3]/div/div/div/div"));
        Thread.sleep(500);
        SelenElem.WaitForLoad(By.xpath("//div[.='Quiero este plan']"), 10);
        reporte.AddImageToReport(" ", OPG.SS());
        
        SelenElem.Click(By.xpath("//div[.='Quiero este plan']"));
        
        Thread.sleep(2000);
        SelenElem.Click(By.id("js_submit_miniparrilla_portabilidad_pospago"));
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("continueToCheckout"), 10);
        
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-nombre"), 15);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        da.slide("Up", 2);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), año);
        SelenElem.SelectOption(By.className("ui-datepicker-month"), mes);
        SelenElem.Click(By.linkText(dia));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("calleNumeroInterior"), numInterior);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        
        SelenElem.Click(By.id("checkout-step4-check-02"));
        Thread.sleep(500);
        SelenElem.Click(By.id("checkout-step4-check-02"));
        
        SelenElem.Click(By.id("paso1-pospago-porta"));
        
        Thread.sleep(5000);
        
        SelenElem.WaitForLoad(By.id("cod"), 10);
        SelenElem.TypeText(By.id("cod"), "1111");
        da.hideKeyBoard();
        da.slide("Up", 2);
        reporte.AddImageToReport("Sección Consulta de credito del Checkout", OPG.SS()); 
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        SelenElem.Click(By.id("checkout-step2-radio-04"));
        SelenElem.Click(By.id("checkout-step2-radio-06"));
        SelenElem.Click(By.id("checkout-step2"));
        
        reporte.AddImageToReport("Sección Consulta de credito del Checkout", OPG.SS()); 
        
        SelenElem.Click(By.id("botonConsultaCredito"));
        
        OPG.cargaAjax2();
        /*SelenElem.WaitForLoad(By.id("inv-mail2"), 6);
        SelenElem.Click(By.id("inv-mail2"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("cp"), 15);
        SelenElem.Click(By.id("cp"));
        SelenElem.TypeText(By.id("cp"), "066");
        SelenElem.Click(By.id("longlat"));
        Thread.sleep(600);
        OPG.cargaAjax2();
        SelenElem.Click(By.id("longlat"));
        Thread.sleep(600);
        reporte.AddImageToReport("Error en Código Postal", OPG.ScreenShotElementInstant());
        */
        SelenElem.WaitForLoad(By.id("inv-mail2"), 15);
        
        SelenElem.Click(By.id("inv-mail2"));
        //OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("js_codigo_postal"), "066");
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        Thread.sleep(500);
        reporte.AddImageToReport("Validacion Error en codigo postal", OPG.ScreenShotElementInstant());
        
    
    }

    public void ErrorDNPortabilidadPospagosoloSIMterminal(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
         
        String dn = CargaVariables.LeerCSV(0, "Portabilidad Pospago", Caso);
       
        String modeloTel= CargaVariables.LeerCSV(1, "Portabilidad Pospago", Caso);
       

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        
        SelenElem.Click(By.xpath("//span[.='Portabilidad']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar Portabilidad > Prepago", OPG.SS());
        
        SelenElem.Click(By.xpath("//span[.='Pospago']"));
        Thread.sleep(4000);
        reporte.AddImageToReport("Seleccionar smartphone ", OPG.SS());
        
        SelenElem.ClickByXpath(WebDrive.WebDriver,"//img[contains(@alt, '" + modeloTel + "')]"); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        
        
        SelenElem.WaitForLoad(By.xpath("//*[@id=\"solo-smartphone__content\"]/form/button"),10);
        reporte.AddImageToReport("Visualizar el detalle del smartphone e ingresar DN y dar click en “Usar este número”", OPG.SS());
        
        Thread.sleep(1000);
        SelenElem.Click(By.id("portabilidad"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_input_dn_button_portabilidad"), 10);
        SelenElem.TypeText(By.id("js_dn_input_portabilidad"),dn);
        reporte.AddImageToReport(" ", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport(" ", OPG.SS());
        
        SelenElem.Click(By.id("js_input_dn_button_portabilidad"));
        Thread.sleep(600);
        reporte.AddImageToReport("Error en DN ", OPG.ScreenShotElementInstant());
        
        Thread.sleep(2000);
        SelenElem.Click(By.xpath("//span[.='Continuar']"));
        reporte.AddImageToReport("Error en DN ", OPG.ScreenShotElementInstant());
    
    }

     public void ErrorINEPortabilidadPospagosoloSIMterminal(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
         
        String dn = CargaVariables.LeerCSV(0, "Portabilidad Pospago", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Portabilidad Pospago", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Portabilidad Pospago", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Portabilidad Pospago", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Portabilidad Pospago", Caso);
        String correo = CargaVariables.LeerCSV(5, "Portabilidad Pospago", Caso);
        String tel = CargaVariables.LeerCSV(6, "Portabilidad Pospago", Caso);
        String calle = CargaVariables.LeerCSV(8, "Portabilidad Pospago", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Portabilidad Pospago", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Portabilidad Pospago", Caso);
        String cp = CargaVariables.LeerCSV(7, "Portabilidad Pospago", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Portabilidad Pospago", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Portabilidad Pospago", Caso);
        String ine = CargaVariables.LeerCSV(14, "Portabilidad Pospago", Caso);
        String año = CargaVariables.LeerCSV(16, "Portabilidad Pospago", Caso);
        String mes = CargaVariables.LeerCSV(17, "Portabilidad Pospago", Caso);
        String  dia= CargaVariables.LeerCSV(18, "Portabilidad Pospago", Caso);
        String curp= CargaVariables.LeerCSV(19, "Portabilidad Pospago", Caso);
        String modeloTel= CargaVariables.LeerCSV(20, "Portabilidad Pospago", Caso);
       

         WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        
        SelenElem.Click(By.xpath("//span[.='Portabilidad']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar Portabilidad > Prepago", OPG.SS());
        
        SelenElem.Click(By.xpath("//span[.='Pospago']"));
        Thread.sleep(4000);
        reporte.AddImageToReport("Seleccionar smartphone ", OPG.SS());
        
        da.slide("Down", 4);
        SelenElem.ClickByXpath(WebDrive.WebDriver,"//img[contains(@alt, '" + modeloTel + "')]"); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        
        SelenElem.WaitForLoad(By.xpath("//*[@id=\"solo-smartphone__content\"]/form/button"),10);
        reporte.AddImageToReport("Visualizar el detalle del smartphone e ingresar DN y dar click en “Usar este número”", OPG.SS());
        
        Thread.sleep(1000);
        SelenElem.Click(By.id("portabilidad"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_input_dn_button_portabilidad"), 10);
        SelenElem.TypeText(By.id("js_dn_input_portabilidad"),dn);
        reporte.AddImageToReport(" ", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport(" ", OPG.SS());
        
        SelenElem.Click(By.id("js_input_dn_button_portabilidad"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.className("temm-2022-modal"), 10);
        Thread.sleep(2000);
        reporte.AddImageToReport(" ", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.xpath("//span[.='Continuar']"));
        
        
        SelenElem.Click(By.xpath("//*[@id=\"js_slider_mp_portabilidad-pospago\"]/div/div/div[3]/div/div/div/div"));
        Thread.sleep(500);
        SelenElem.WaitForLoad(By.xpath("//div[.='Quiero este plan']"), 10);
        reporte.AddImageToReport(" ", OPG.SS());
        
        SelenElem.Click(By.xpath("//div[.='Quiero este plan']"));
        
        Thread.sleep(2000);
        SelenElem.Click(By.id("js_submit_miniparrilla_portabilidad_pospago"));
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("continueToCheckout"), 10);
        
       
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-nombre"), 15);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        da.slide("Up", 2);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), año);
        SelenElem.SelectOption(By.className("ui-datepicker-month"), mes);
        SelenElem.Click(By.linkText(dia));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("calleNumeroInterior"), numInterior);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        
        SelenElem.Click(By.id("checkout-step4-check-02"));
        Thread.sleep(500);
        SelenElem.Click(By.id("checkout-step4-check-02"));
        
        SelenElem.Click(By.id("paso1-pospago-porta"));
        
        Thread.sleep(700);
        reporte.AddImageToReport("Error 18 dígitos INE", OPG.SS()); 
    }
     
    public void PortabilidadPospagoSIMterminal200(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
        
        String ModTel = CargaVariables.LeerCSV(0, "Portabilidad Pospago", Caso);
        String Dn = CargaVariables.LeerCSV(1, "Portabilidad Pospago", Caso);
        String Nombre = CargaVariables.LeerCSV(2, "Portabilidad Pospago", Caso);
        String ApellPat = CargaVariables.LeerCSV(3, "Portabilidad Pospago", Caso);
        String ApellMat = CargaVariables.LeerCSV(4, "Portabilidad Pospago", Caso);
        String Correo = CargaVariables.LeerCSV(5, "Portabilidad Pospago", Caso);
        String Telefono = CargaVariables.LeerCSV(6, "Portabilidad Pospago", Caso);
        String Mes = CargaVariables.LeerCSV(7, "Portabilidad Pospago", Caso);
        String Año = CargaVariables.LeerCSV(8, "Portabilidad Pospago", Caso);
        String Dia = CargaVariables.LeerCSV(9, "Portabilidad Pospago", Caso);
        String Ine = CargaVariables.LeerCSV(10, "Portabilidad Pospago", Caso);
        String CodPostal = CargaVariables.LeerCSV(11, "Portabilidad Pospago", Caso);
        String Calle = CargaVariables.LeerCSV(12, "Portabilidad Pospago", Caso);
        String NumExt = CargaVariables.LeerCSV(13, "Portabilidad Pospago", Caso);
        String NumInt = CargaVariables.LeerCSV(14, "Portabilidad Pospago", Caso);
        String Colonia = CargaVariables.LeerCSV(15, "Portabilidad Pospago", Caso);
        String Curp = CargaVariables.LeerCSV(16, "Portabilidad Pospago", Caso);
        String Tarjeta = CargaVariables.LeerCSV(17, "Portabilidad Pospago", Caso);
        String Cvv = CargaVariables.LeerCSV(18, "Portabilidad Pospago", Caso);
        String MesTarjeta = CargaVariables.LeerCSV(19, "Portabilidad Pospago", Caso);
        String AñoTarjeta = CargaVariables.LeerCSV(20, "Portabilidad Pospago", Caso);
        
        WebDrive.WebDriver.navigate().to(Url);
        Thread.sleep(3000);
        reporte.AddImageToReport("Pagina principal", OPG.SS());
        SelenElem.Click(By.id("checkbox"));
        SelenElem.Click(By.id("submenu_smartphone"));
        SelenElem.Click(By.xpath("//a[@title='Todas las marcas']"));
        
        SelenElem.Click(By.xpath("//span[.='Portabilidad']"));
        SelenElem.Click(By.xpath("//span[.='Pospago']"));
        reporte.AddImageToReport("Sección Portabilidad Pospago", OPG.SS());
        
        SelenElem.focus(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        reporte.AddImageToReport("Se procede a seleccionar el Smartphone", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Detalle del smartphone", OPG.SS());
        da.slide("Down", 3);
        SelenElem.Click(By.id("portabilidad"));
        SelenElem.TypeText(By.id("js_dn_input_portabilidad"), Dn);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.id("js_input_dn_button_portabilidad"));
        SelenElem.WaitForLoad(By.xpath("//span[.='Continuar']"), 10);
        SelenElem.Click(By.xpath("//span[.='Continuar']"));
        da.slide("Down", 2);
        SelenElem.Click(By.xpath("//*[@id=\"js_slider_mp_portabilidad-pospago\"]/div/div/div[3]/div/div/div/div"));
        reporte.AddImageToReport("Plan movistar", OPG.SS());
        SelenElem.Click(By.xpath("//div[.='Quiero este plan']"));
        
        Thread.sleep(2000);
        SelenElem.Click(By.id("js_submit_miniparrilla_portabilidad_pospago"));
        
        
        SelenElem.TypeText(By.id("valid-name"), Nombre);
        SelenElem.TypeText(By.id("valid-apaterno"), ApellPat);
        SelenElem.TypeText(By.id("valid-amaterno"), ApellMat);
        SelenElem.TypeText(By.id("valid-email"), Correo);
        SelenElem.TypeText(By.id("valid-phone"), Telefono);
        da.hideKeyBoard();
        SelenElem.Click(By.id("validaPrivacidad"));
        Thread.sleep(5000);
        da.clickCoordenadas(0, 45);
        reporte.AddImageToReport("Sección tus datos del checkout", OPG.SS());
        
        SelenElem.Click(By.id("btnStep1Principal"));
        
        SelenElem.Click(By.id("dateTo_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-month"), Mes);
        SelenElem.SelectOption(By.className("ui-datepicker-year"), Año);
        SelenElem.Click(By.linkText(Dia));
        SelenElem.TypeText(By.id("INE-IFE"), Ine);
        SelenElem.TypeText(By.id("cod-posta-porta"), CodPostal);
        SelenElem.TypeText(By.id("calle-porta-ref"), Calle);
        SelenElem.TypeText(By.id("nExter-porta"), NumExt);
        SelenElem.TypeText(By.id("nInter-porta"), NumInt);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("colonia-porta-ref"), Colonia);
        da.hideKeyBoard();
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("btnStep1-2Principal"));
        
        Thread.sleep(5000);
        SelenElem.WaitForLoad(By.id("validaScore"), 10);
        SelenElem.TypeText(By.id("cod"), "1111");
        //da.hideKeyBoard();
        SelenElem.Click(By.id("checkout-step2-select-01"));
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        SelenElem.Click(By.id("validaScore"));
        da.slide("Down", 1);
        reporte.AddImageToReport("Consulta de credito del checkout", OPG.SS());
        
        SelenElem.Click(By.id("btnStep1-3Principal"));
        
        SelenElem.WaitForLoad(By.id("input-shippingOption1"), 10);
        reporte.AddImageToReport("Opciones de envío del checkout", OPG.SS());
        
        SelenElem.Click(By.id("btnStep2"));
        
        SelenElem.WaitForLoad(By.id("valid-nip"), 10);
        SelenElem.TypeText(By.id("valid-nip"), "1234");
        SelenElem.TypeText(By.id("valid-curp"), Curp);
        da.hideKeyBoard();
        SelenElem.Click(By.id("validaNip"));
        reporte.AddImageToReport("Valida tu NIP", OPG.SS());
        
        SelenElem.Click(By.id("btnStep3"));
        
        SelenElem.Click(By.id("validaContrato"));
        SelenElem.Click(By.id("validaContrato2"));
        reporte.AddImageToReport("Contrato del checkout", OPG.SS());
        SelenElem.Click(By.id("btnStep4_resume"));
        
        da.slide("Up", 3);
        reporte.AddImageToReport("Confirma tu pedido del checkout", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.id("btnConfirm"));
        
        SelenElem.WaitForLoad(By.id("numTarjeta"), 10);
        reporte.AddImageToReport("Medio de pago Flap", OPG.SS());
        
        SelenElem.Click(By.id("numTarjeta"));
        SelenElem.TypeText(By.id("numTarjeta"), Tarjeta);
        SelenElem.Click(By.id("vigenciames"));
        SelenElem.TypeText(By.id("vigenciames"), MesTarjeta);
        SelenElem.Click(By.id("vigenciaanio"));
        SelenElem.TypeText(By.id("vigenciaanio"), AñoTarjeta);
        SelenElem.Click(By.id("cvv2"));
        SelenElem.TypeText(By.id("cvv2"), Cvv);
        da.hideKeyBoard();
        reporte.AddImageToReport(" ", OPG.SS());
        SelenElem.Click(By.id("continuar"));
        
        Thread.sleep(2000);
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.WaitForLoad(By.id("numero_portar"), 20);
        reporte.AddImageToReport("Pagina de pago realizada con exito", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
       
        
    }
    
    public void PortabilidadPospagosoloSIMRefactor(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
        
        String Dn = CargaVariables.LeerCSV(0, "Portabilidad Pospago", Caso);
        String SkuPlan = CargaVariables.LeerCSV(1, "Portabilidad Pospago", Caso);
        String NamePlan = CargaVariables.LeerCSV(2, "Portabilidad Pospago", Caso);
        String SkuTerminal = CargaVariables.LeerCSV(3, "Portabilidad Pospago", Caso);
        String Nombre = CargaVariables.LeerCSV(4, "Portabilidad Pospago", Caso);
        String ApellPat = CargaVariables.LeerCSV(5, "Portabilidad Pospago", Caso);
        String ApellMat = CargaVariables.LeerCSV(6, "Portabilidad Pospago", Caso);
        String Correo = CargaVariables.LeerCSV(7, "Portabilidad Pospago", Caso);
        String Telefono = CargaVariables.LeerCSV(8, "Portabilidad Pospago", Caso);
        String Mes = CargaVariables.LeerCSV(9, "Portabilidad Pospago", Caso);
        String Año = CargaVariables.LeerCSV(10, "Portabilidad Pospago", Caso);
        String Dia = CargaVariables.LeerCSV(11, "Portabilidad Pospago", Caso);
        String Ine = CargaVariables.LeerCSV(12, "Portabilidad Pospago", Caso);
        String CodPostal = CargaVariables.LeerCSV(13, "Portabilidad Pospago", Caso);
        String Calle = CargaVariables.LeerCSV(14, "Portabilidad Pospago", Caso);
        String NumExt = CargaVariables.LeerCSV(15, "Portabilidad Pospago", Caso);
        String NumInt = CargaVariables.LeerCSV(16, "Portabilidad Pospago", Caso);
        String Colonia = CargaVariables.LeerCSV(17, "Portabilidad Pospago", Caso);
        String Curp = CargaVariables.LeerCSV(18, "Portabilidad Pospago", Caso);
        String DiaPorta = CargaVariables.LeerCSV(19, "Portabilidad Pospago", Caso);
       
          
        WebDrive.WebDriver.navigate().to(Url + "liferay.php");
        Thread.sleep(3000);
         
        SelenElem.SelectOption(By.id("flowCode"), "portabilidad refactor");
        SelenElem.Find(By.id("skuPlan")).clear();
        SelenElem.TypeText(By.id("skuPlan"), SkuPlan);
        SelenElem.Find(By.id("namePlan")).clear();
        SelenElem.TypeText(By.id("namePlan"), NamePlan);
        SelenElem.Find(By.id("skuDn")).clear();
        SelenElem.Find(By.id("skuRecarga")).clear();
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.TypeText(By.id("skuTerminal"), SkuTerminal);
        SelenElem.Find(By.id("monto")).clear();
        SelenElem.Find(By.id("rfc")).clear();
        reporte.AddImageToReport("Pagina principal", OPG.SS());
        
        SelenElem.Click(By.name("Comprar"));
        
        SelenElem.TypeText(By.id("dnPorta"), Dn);
        reporte.AddImageToReport("Ingresar DN", OPG.SS());
        SelenElem.Click(By.id("btn-porta"));
        
        SelenElem.WaitForLoad(By.id("valid-name"), 10);
        SelenElem.TypeText(By.id("valid-name"), Nombre);
        SelenElem.TypeText(By.id("valid-apaterno"), ApellPat);
        SelenElem.TypeText(By.id("valid-amaterno"), ApellMat);
        SelenElem.TypeText(By.id("valid-email"), Correo);
        SelenElem.TypeText(By.id("valid-phone"), Telefono);
        da.hideKeyBoard();
        SelenElem.Click(By.id("validaPrivacidad"));
        Thread.sleep(5000);
        da.clickCoordenadas(0, 45);
        reporte.AddImageToReport("Sección tus datos del checkout", OPG.SS());
        
        SelenElem.Click(By.id("btnStep1Principal"));
        
        SelenElem.Click(By.id("dateTo_datepicker"));
        SelenElem.Click(By.className("ui-datepicker-month"));
        Thread.sleep(1000);
        SelenElem.SelectOption(By.className("ui-datepicker-month"), Mes);
        SelenElem.SelectOption(By.className("ui-datepicker-year"), Año);
        SelenElem.Click(By.linkText(Dia));
        //SelenElem.TypeText(By.id("INE-IFE"), Ine);
        SelenElem.TypeText(By.id("valid-curp"), Curp);
        SelenElem.TypeText(By.id("cod-posta-porta"), CodPostal);
        SelenElem.TypeText(By.id("calle-porta-ref"), Calle);
        SelenElem.TypeText(By.id("nExter-porta"), NumExt);
        SelenElem.TypeText(By.id("nInter-porta"), NumInt);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("colonia-porta-ref"), Colonia);
        da.hideKeyBoard();
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("btnStep1-2Principal"));
        
        SelenElem.WaitForLoad(By.id("toggleShipping2"), 10);
        Thread.sleep(3000);
        SelenElem.Click(By.id("toggleShipping2"));
        SelenElem.TypeText(By.id("js_codigo_postal"), CodPostal);
        da.hideKeyBoard();
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        SelenElem.WaitForLoad(By.id("99090073cac"), 15);
        Thread.sleep(3000);
        SelenElem.Click(By.id("99090073cac"));
        da.slide("Down", 1);
        reporte.AddImageToReport("Opciones envío del checkout", OPG.SS());
        
        SelenElem.Click(By.id("btnStep2-CAC-porta"));
        
        Thread.sleep(5000);
        SelenElem.Click(By.xpath("//input[@placeholder='1']"));
        SelenElem.TypeText(By.xpath("//input[@placeholder='1']"), "1234");
        SelenElem.Click(By.id("dateToPorta_datepicker"));
        SelenElem.Click(By.linkText(DiaPorta));
        da.hideKeyBoard();
        reporte.AddImageToReport("Validar NIP", OPG.SS());
        
        SelenElem.Click(By.id("btnStep3"))    ;
        
        SelenElem.WaitForLoad(By.id("validaContrato"), 10);
        SelenElem.Click(By.id("validaContrato"));
        reporte.AddImageToReport("Contrato del checkout", OPG.SS());
        SelenElem.Click(By.id("btnStep4_resume"));
        
        Thread.sleep(3000);
        reporte.AddImageToReport("Página de pago realizado con exito", OPG.SS());
        da.slide("Down", 1);
        reporte.AddImageToReport("", OPG.SS());
        
        
        
    }
    
    public void PortabilidadPospagosoloSIMterminalLiferay0(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
        String Dn = CargaVariables.LeerCSV(0, "Portabilidad Pospago", Caso);
        String SkuPlan = CargaVariables.LeerCSV(1, "Portabilidad Pospago", Caso);
        String SkuTerminal = CargaVariables.LeerCSV(2, "Portabilidad Pospago", Caso);
        String Nombre = CargaVariables.LeerCSV(3, "Portabilidad Pospago", Caso);
        String ApellPat = CargaVariables.LeerCSV(4, "Portabilidad Pospago", Caso);
        String ApellMat = CargaVariables.LeerCSV(5, "Portabilidad Pospago", Caso);
        String Correo = CargaVariables.LeerCSV(6, "Portabilidad Pospago", Caso);
        String Telefono = CargaVariables.LeerCSV(7, "Portabilidad Pospago", Caso);
        String Mes = CargaVariables.LeerCSV(8, "Portabilidad Pospago", Caso);
        String Año = CargaVariables.LeerCSV(9, "Portabilidad Pospago", Caso);
        String Dia = CargaVariables.LeerCSV(10, "Portabilidad Pospago", Caso);
        String CodPostal = CargaVariables.LeerCSV(11, "Portabilidad Pospago", Caso);
        String Calle = CargaVariables.LeerCSV(12, "Portabilidad Pospago", Caso);
        String NumExt = CargaVariables.LeerCSV(13, "Portabilidad Pospago", Caso);
        String NumInt = CargaVariables.LeerCSV(14, "Portabilidad Pospago", Caso);
        String Colonia = CargaVariables.LeerCSV(15, "Portabilidad Pospago", Caso);
        String Curp = CargaVariables.LeerCSV(16, "Portabilidad Pospago", Caso);
        String CalleEnvio = CargaVariables.LeerCSV(17, "Portabilidad Pospago", Caso);
        String NumExtEnvio = CargaVariables.LeerCSV(18, "Portabilidad Pospago", Caso);
        String NumIntEnvio = CargaVariables.LeerCSV(19, "Portabilidad Pospago", Caso);
        String NumTarjeta = CargaVariables.LeerCSV(20, "Portabilidad Pospago", Caso);
        String Cvv = CargaVariables.LeerCSV(21, "Portabilidad Pospago", Caso);
        String VigenciaMes = CargaVariables.LeerCSV(22, "Portabilidad Pospago", Caso);
        String VigenciaAño = CargaVariables.LeerCSV(23, "Portabilidad Pospago", Caso);
        
        WebDrive.WebDriver.navigate().to(Url + "liferay.php");
        Thread.sleep(3000);
         
        SelenElem.SelectOption(By.id("flowCode"), "portabilidad");
        SelenElem.Find(By.id("skuPlan")).clear();
        SelenElem.TypeText(By.id("skuPlan"), SkuPlan);
        SelenElem.Find(By.id("namePlan")).clear();
        SelenElem.Find(By.id("skuRecarga")).clear();
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.TypeText(By.id("skuTerminal"), SkuTerminal);
        SelenElem.Find(By.id("skuDn")).clear();
        SelenElem.TypeText(By.id("skuDn"), Dn);
        SelenElem.Find(By.id("monto")).clear();
        SelenElem.Find(By.id("rfc")).clear();
        reporte.AddImageToReport("Pagina principal", OPG.SS());
        
        SelenElem.Click(By.name("Comprar"));
        
        SelenElem.TypeText(By.id("valid-name"), Nombre);
        SelenElem.TypeText(By.id("valid-apaterno"), ApellPat);
        SelenElem.TypeText(By.id("valid-amaterno"), ApellMat);
        SelenElem.TypeText(By.id("valid-email"), Correo);
        SelenElem.TypeText(By.id("valid-phone"), Telefono);
        da.hideKeyBoard();
        SelenElem.Click(By.id("validaPrivacidad"));
        Thread.sleep(5000);
        da.clickCoordenadas(0, 45);
        reporte.AddImageToReport("Sección tus datos del checkout", OPG.SS());
        
        SelenElem.Click(By.id("btnStep1Principal"));
        
        SelenElem.Click(By.id("dateTo_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-month"), Mes);
        SelenElem.SelectOption(By.className("ui-datepicker-year"), Año);
        SelenElem.Click(By.linkText(Dia));
        //SelenElem.TypeText(By.id("INE-IFE"), Ine);
        SelenElem.TypeText(By.id("valid-curp"), Curp);
        SelenElem.TypeText(By.id("cod-posta-porta"), CodPostal);
        SelenElem.TypeText(By.id("calle-porta-ref"), Calle);
        SelenElem.TypeText(By.id("nExter-porta"), NumExt);
        SelenElem.TypeText(By.id("nInter-porta"), NumInt);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("colonia-porta-ref"), Colonia);
        da.hideKeyBoard();
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("btnStep1-2Principal"));
        
        SelenElem.WaitForLoad(By.id("cod"), 10);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("cod"), "1111");
        da.hideKeyBoard();
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        SelenElem.Click(By.id("checkout-step2-radio-04"));
        SelenElem.Click(By.id("checkout-step2-radio-06"));
        SelenElem.Click(By.id("validaScore"));
        reporte.AddImageToReport("Sección Consulta de credito del Checkout", OPG.SS()); 
        
        SelenElem.Click(By.id("btnStep1-3Principal"));
        
        SelenElem.WaitForLoad(By.id("input-shippingOption3"), 10);
        SelenElem.Click(By.id("input-shippingOption3"));
        SelenElem.TypeText(By.id("calle-porta-other"), CalleEnvio);
        SelenElem.TypeText(By.id("nExter-porta-other"), NumExtEnvio);
        SelenElem.TypeText(By.id("nInter-porta-other"), NumIntEnvio);
        SelenElem.TypeText(By.id("cod-posta-porta-other"), CodPostal);
        SelenElem.Click(By.id("calle-porta-other"));
        //SelenElem.Click(By.id("colonia-porta-ref-other"));
        SelenElem.TypeText(By.id("colonia-porta-ref-other"), Colonia);
        reporte.AddImageToReport("Opciones envío del checkout", OPG.SS());
        
        SelenElem.Click(By.id("btnStep2"));
       
        //SelenElem.Click(By.id("input-shippingOption2"));
        //SelenElem.TypeText(By.id("js_codigo_postal"), CodPostal);
        //SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        //SelenElem.WaitForLoad(By.id("js_geo_option_cards"), 10);
        //Thread.sleep(5000);
        //da.hideKeyBoard();
        //SelenElem.Click(By.id("99090073cac"));
        //da.slide("Down", 1);
        //reporte.AddImageToReport("Opciones envío del checkout", OPG.SS());
        
        //SelenElem.Click(By.id("btnStep2-CAC-porta"));
        
        Thread.sleep(3000);
        SelenElem.Click(By.xpath("//input[@class='nip__input']"));
        SelenElem.TypeText(By.xpath("//input[@class='nip__input']"), "1234");
        //SelenElem.TypeText(By.id("valid-curp"), Curp);
        //SelenElem.Click(By.id("dateToPorta_datepicker"));
        //SelenElem.Click(By.linkText(DiaPorta));
        da.hideKeyBoard();
        reporte.AddImageToReport("Validar NIP", OPG.SS());
        
        SelenElem.Click(By.id("btnStep3"));
        
        Thread.sleep(3000);
        SelenElem.WaitForLoad(By.id("validaContrato"), 10);
        SelenElem.Click(By.id("validaContrato"));
        reporte.AddImageToReport("Contrato del checkout", OPG.SS());
        SelenElem.Click(By.id("btnStep4_resume"));
        
        SelenElem.WaitForLoad(By.id("imagen-cliente"),10);
        reporte.AddImageToReport("Ingresamos a la pagina principal de FLAP", OPG.ScreenShotElement());
        SelenElem.focus(By.id("numTarjeta"));
        SelenElem.TypeText(By.id("numTarjeta"), NumTarjeta);
        SelenElem.TypeText(By.id("cvv2"), Cvv);
        SelenElem.SelectOption(By.id("vigenciames"), VigenciaMes);
        SelenElem.SelectOption(By.id("vigenciaanio"), VigenciaAño);
        reporte.AddImageToReport("Llenamos los datos de pago y damos click en pagar", OPG.ScreenShotElement());
        SelenElem.Click(By.id("continuar"));
        
        Thread.sleep(1000);
        reporte.AddImageToReport("Metodo de Pago Flap", OPG.SS());
                
        Thread.sleep(5000);
        //SelenElem.Click(By.id("btnConfirm"));
        
        Thread.sleep(3000);
        reporte.AddImageToReport("Página de pago realizado con exito", OPG.SS());
        da.slide("Down", 1);
        reporte.AddImageToReport("", OPG.SS());
        
    }
    
    public void PortabilidadPospagosoloSIMterminalLiferay200(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url,DeviceActions da, String Caso) throws InterruptedException, IOException
    {
        String Dn = CargaVariables.LeerCSV(0, "Portabilidad Pospago", Caso);
        String SkuPlan = CargaVariables.LeerCSV(1, "Portabilidad Pospago", Caso);
        String Nombre = CargaVariables.LeerCSV(2, "Portabilidad Pospago", Caso);
        String ApellPat = CargaVariables.LeerCSV(3, "Portabilidad Pospago", Caso);
        String ApellMat = CargaVariables.LeerCSV(4, "Portabilidad Pospago", Caso);
        String Correo = CargaVariables.LeerCSV(5, "Portabilidad Pospago", Caso);
        String Telefono = CargaVariables.LeerCSV(6, "Portabilidad Pospago", Caso);
        String Mes = CargaVariables.LeerCSV(7, "Portabilidad Pospago", Caso);
        String Año = CargaVariables.LeerCSV(8, "Portabilidad Pospago", Caso);
        String Dia = CargaVariables.LeerCSV(9, "Portabilidad Pospago", Caso);
        String DiaPorta = CargaVariables.LeerCSV(10, "Portabilidad Pospago", Caso);
        String CodPostal = CargaVariables.LeerCSV(11, "Portabilidad Pospago", Caso);
        String Calle = CargaVariables.LeerCSV(12, "Portabilidad Pospago", Caso);
        String NumExt = CargaVariables.LeerCSV(13, "Portabilidad Pospago", Caso);
        String NumInt = CargaVariables.LeerCSV(14, "Portabilidad Pospago", Caso);
        String Colonia = CargaVariables.LeerCSV(15, "Portabilidad Pospago", Caso);
        String Curp = CargaVariables.LeerCSV(16, "Portabilidad Pospago", Caso);
        String Tarjeta = CargaVariables.LeerCSV(17, "Portabilidad Pospago", Caso);
        String Cvv = CargaVariables.LeerCSV(18, "Portabilidad Pospago", Caso);
        String MesTarjeta = CargaVariables.LeerCSV(19, "Portabilidad Pospago", Caso);
        String AñoTarjeta = CargaVariables.LeerCSV(20, "Portabilidad Pospago", Caso);
        
        WebDrive.WebDriver.navigate().to(Url + "liferay.php");
        Thread.sleep(3000);
        
        SelenElem.SelectOption(By.id("flowCode"), "portabilidad");
        SelenElem.Find(By.id("skuPlan")).clear();
        SelenElem.TypeText(By.id("skuPlan"), SkuPlan);
        SelenElem.Find(By.id("namePlan")).clear();
        SelenElem.Find(By.id("skuRecarga")).clear();
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.Find(By.id("skuDn")).clear();
        SelenElem.TypeText(By.id("skuDn"), Dn);
        SelenElem.Find(By.id("monto")).clear();
        SelenElem.Find(By.id("rfc")).clear();
        reporte.AddImageToReport("Pagina principal", OPG.SS());
        
        SelenElem.Click(By.name("Comprar"));
        
        SelenElem.TypeText(By.id("valid-name"), Nombre);
        SelenElem.TypeText(By.id("valid-apaterno"), ApellPat);
        SelenElem.TypeText(By.id("valid-amaterno"), ApellMat);
        SelenElem.TypeText(By.id("valid-email"), Correo);
        SelenElem.TypeText(By.id("valid-phone"), Telefono);
        da.hideKeyBoard();
        SelenElem.Click(By.id("validaPrivacidad"));
        Thread.sleep(5000);
        da.clickCoordenadas(0, 45);
        
        reporte.AddImageToReport("Sección tus datos del checkout", OPG.SS());
        
        SelenElem.Click(By.id("btnStep1Principal"));
        
        SelenElem.Click(By.id("dateTo_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-month"), Mes);
        SelenElem.SelectOption(By.className("ui-datepicker-year"), Año);
        SelenElem.Click(By.linkText(Dia));
        //SelenElem.TypeText(By.id("INE-IFE"), Ine);
        SelenElem.TypeText(By.id("valid-curp"), Curp);
        SelenElem.TypeText(By.id("cod-posta-porta"), CodPostal);
        SelenElem.TypeText(By.id("calle-porta-ref"), Calle);
        SelenElem.TypeText(By.id("nExter-porta"), NumExt);
        SelenElem.TypeText(By.id("nInter-porta"), NumInt);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("colonia-porta-ref"), Colonia);
        da.hideKeyBoard();
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("btnStep1-2Principal"));
        
        Thread.sleep(3000);
        SelenElem.Click(By.id("input-shippingOption2"));
        SelenElem.TypeText(By.id("js_codigo_postal"), CodPostal);
        da.hideKeyBoard();
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        SelenElem.WaitForLoad(By.id("js_geo_option_cards"), 10);
        SelenElem.Click(By.id("99090073cac"));
        reporte.AddImageToReport("Opciones envío del checkout", OPG.SS());
        
        SelenElem.Click(By.id("btnStep2"));
       
        Thread.sleep(5000);
        SelenElem.Click(By.xpath("//input[@class='nip__input']"));
        SelenElem.TypeText(By.xpath("//input[@class='nip__input']"), "1234");
        SelenElem.Click(By.id("dateToPorta_datepicker"));
        SelenElem.Click(By.linkText(DiaPorta));
        //SelenElem.TypeText(By.id("valid-curp"), Curp);
        //da.hideKeyBoard();
        //SelenElem.Click(By.id("validaNip"));
        reporte.AddImageToReport("Valida tu NIP", OPG.SS());
        
        SelenElem.Click(By.id("btnStep3"));
        
        SelenElem.WaitForLoad(By.id("validaContrato"), 10);
        SelenElem.Click(By.id("validaContrato"));
        reporte.AddImageToReport("Contrato del checkout", OPG.SS());
        SelenElem.Click(By.id("btnStep4_resume"));
        
        SelenElem.WaitForLoad(By.id("imagen-cliente"),10);
        reporte.AddImageToReport("Ingresamos a la pagina principal de FLAP", OPG.ScreenShotElement());
        SelenElem.focus(By.id("numTarjeta"));
        SelenElem.TypeText(By.id("numTarjeta"), Tarjeta);
        SelenElem.TypeText(By.id("cvv2"), Cvv);
        SelenElem.SelectOption(By.id("vigenciames"), MesTarjeta);
        SelenElem.SelectOption(By.id("vigenciaanio"), AñoTarjeta);
        reporte.AddImageToReport("Llenamos los datos de pago y damos click en pagar", OPG.ScreenShotElement());
        SelenElem.Click(By.id("continuar"));
        
        //da.slide("Up", 3);
        //reporte.AddImageToReport("Confirma tu pedido del checkout", OPG.SS());
        //da.slide("Down", 3);
        //reporte.AddImageToReport("", OPG.SS());
        //SelenElem.Click(By.id("btnConfirm"));
        
        //SelenElem.WaitForLoad(By.id("numTarjeta"), 10);
        //reporte.AddImageToReport("Medio de pago Flap", OPG.SS());
        
        //SelenElem.Click(By.id("numTarjeta"));
        //SelenElem.TypeText(By.id("numTarjeta"), Tarjeta);
        //SelenElem.Click(By.id("vigenciames"));
        //SelenElem.TypeText(By.id("vigenciames"), MesTarjeta);
        //SelenElem.Click(By.id("vigenciaanio"));
        //SelenElem.TypeText(By.id("vigenciaanio"), AñoTarjeta);
        //SelenElem.Click(By.id("cvv2"));
        //SelenElem.TypeText(By.id("cvv2"), Cvv);
        //da.hideKeyBoard();
        //reporte.AddImageToReport(" ", OPG.SS());
        //SelenElem.Click(By.id("continuar"));
        Thread.sleep(2000);
        reporte.AddImageToReport("Metodo de pago de Flap", OPG.SS());
        
        Thread.sleep(5000);
        reporte.AddImageToReport("Pagina de pago realizada con exito", OPG.SS());
        da.slide("Down", 1);
        reporte.AddImageToReport("", OPG.SS()); 
        
    }
    
    public void PortaPosSoloSIMLiferay(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException
    {
        String Dn = CargaVariables.LeerCSV(0, "Portabilidad Pospago", Caso);
        String SkuPlan = CargaVariables.LeerCSV(1, "Portabilidad Pospago", Caso);
        String Nombre = CargaVariables.LeerCSV(2, "Portabilidad Pospago", Caso);
        String ApellPat = CargaVariables.LeerCSV(3, "Portabilidad Pospago", Caso);
        String ApellMat = CargaVariables.LeerCSV(4, "Portabilidad Pospago", Caso);
        String Correo = CargaVariables.LeerCSV(5, "Portabilidad Pospago", Caso);
        String Telefono = CargaVariables.LeerCSV(6, "Portabilidad Pospago", Caso);
        String Mes = CargaVariables.LeerCSV(7, "Portabilidad Pospago", Caso);
        String Año = CargaVariables.LeerCSV(8, "Portabilidad Pospago", Caso);
        String Dia = CargaVariables.LeerCSV(9, "Portabilidad Pospago", Caso);
        String CodPostal = CargaVariables.LeerCSV(10, "Portabilidad Pospago", Caso);
        String Calle = CargaVariables.LeerCSV(11, "Portabilidad Pospago", Caso);
        String NumExt = CargaVariables.LeerCSV(12, "Portabilidad Pospago", Caso);
        String NumInt = CargaVariables.LeerCSV(13, "Portabilidad Pospago", Caso);
        String Colonia = CargaVariables.LeerCSV(14, "Portabilidad Pospago", Caso);
        String Curp = CargaVariables.LeerCSV(15, "Portabilidad Pospago", Caso);
        
        WebDrive.WebDriver.navigate().to(Url + "liferay.php");
        Thread.sleep(3000);
        
        SelenElem.SelectOption(By.id("flowCode"), "portabilidad");
        SelenElem.Find(By.id("skuPlan")).clear();
        SelenElem.TypeText(By.id("skuPlan"), SkuPlan);
        SelenElem.Find(By.id("namePlan")).clear();
        SelenElem.Find(By.id("skuRecarga")).clear();
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.Find(By.id("skuDn")).clear();
        SelenElem.TypeText(By.id("skuDn"), Dn);
        SelenElem.Find(By.id("monto")).clear();
        SelenElem.Find(By.id("rfc")).clear();
        reporte.AddImageToReport("Pagina principal", OPG.SS());
        
        SelenElem.Click(By.name("Comprar"));
        
        SelenElem.TypeText(By.id("valid-name"), Nombre);
        SelenElem.TypeText(By.id("valid-apaterno"), ApellPat);
        SelenElem.TypeText(By.id("valid-amaterno"), ApellMat);
        SelenElem.TypeText(By.id("valid-email"), Correo);
        SelenElem.TypeText(By.id("valid-phone"), Telefono);
        SelenElem.Click(By.id("validaPrivacidad"));
        Thread.sleep(5000);
        
        reporte.AddImageToReport("Sección tus datos del checkout", OPG.SS());
        
        SelenElem.Click(By.id("btnStep1Principal"));
        
        SelenElem.Click(By.id("dateTo_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-month"), Mes);
        SelenElem.SelectOption(By.className("ui-datepicker-year"), Año);
        SelenElem.Click(By.linkText(Dia));
        SelenElem.TypeText(By.id("valid-curp"), Curp);
        SelenElem.TypeText(By.id("cod-posta-porta"), CodPostal);
        SelenElem.TypeText(By.id("calle-porta-ref"), Calle);
        SelenElem.TypeText(By.id("nExter-porta"), NumExt);
        SelenElem.TypeText(By.id("nInter-porta"), NumInt);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("colonia-porta-ref"), Colonia);
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("btnStep1-2Principal"));
        
        Thread.sleep(3000);
        reporte.AddImageToReport("Opciones envío del checkout", OPG.SS());
        
        SelenElem.Click(By.id("btnStep2"));
        
        Thread.sleep(5000);
        SelenElem.Click(By.xpath("//input[@class='nip__input']"));
        SelenElem.TypeText(By.xpath("//input[@class='nip__input']"), "1234");

        reporte.AddImageToReport("Valida tu NIP", OPG.SS());
        
        SelenElem.Click(By.id("btnStep3"));
        
        SelenElem.WaitForLoad(By.id("validaContrato"), 10);
        SelenElem.Click(By.id("validaContrato"));
        reporte.AddImageToReport("Contrato del checkout", OPG.SS());
        SelenElem.Click(By.id("btnStep4_resume"));
        
        Thread.sleep(5000);
        reporte.AddImageToReport("Pagina de pago realizada con exito", OPG.SS());
    }
    
    public void PortaPosLPSIMLiferay200(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException
    {
        String Dn = CargaVariables.LeerCSV(0, "Portabilidad Pospago", Caso);
        String SkuPlan = CargaVariables.LeerCSV(1, "Portabilidad Pospago", Caso);
        String NamePlan = CargaVariables.LeerCSV(2, "Portabilidad Pospago", Caso);
        String Nombre = CargaVariables.LeerCSV(3, "Portabilidad Pospago", Caso);
        String ApellPat = CargaVariables.LeerCSV(4, "Portabilidad Pospago", Caso);
        String ApellMat = CargaVariables.LeerCSV(5, "Portabilidad Pospago", Caso);
        String Correo = CargaVariables.LeerCSV(6, "Portabilidad Pospago", Caso);
        String Telefono = CargaVariables.LeerCSV(7, "Portabilidad Pospago", Caso);
        String Mes = CargaVariables.LeerCSV(8, "Portabilidad Pospago", Caso);
        String Año = CargaVariables.LeerCSV(9, "Portabilidad Pospago", Caso);
        String Dia = CargaVariables.LeerCSV(10, "Portabilidad Pospago", Caso);
        String CodPostal = CargaVariables.LeerCSV(11, "Portabilidad Pospago", Caso);
        String Calle = CargaVariables.LeerCSV(12, "Portabilidad Pospago", Caso);
        String NumExt = CargaVariables.LeerCSV(13, "Portabilidad Pospago", Caso);
        String NumInt = CargaVariables.LeerCSV(14, "Portabilidad Pospago", Caso);
        String Colonia = CargaVariables.LeerCSV(15, "Portabilidad Pospago", Caso);
        String Curp = CargaVariables.LeerCSV(16, "Portabilidad Pospago", Caso);
        String DiaPorta = CargaVariables.LeerCSV(17, "Portabilidad Pospago", Caso);
        String Tarjeta = CargaVariables.LeerCSV(18, "Portabilidad Pospago", Caso);
        String Cvv = CargaVariables.LeerCSV(19, "Portabilidad Pospago", Caso);
        String MesTarjeta = CargaVariables.LeerCSV(20, "Portabilidad Pospago", Caso);
        String AñoTarjeta = CargaVariables.LeerCSV(21, "Portabilidad Pospago", Caso);
        
        WebDrive.WebDriver.navigate().to(Url + "liferay.php");
        Thread.sleep(3000);
        
        SelenElem.SelectOption(By.id("flowCode"), "portabilidad refactor");
        SelenElem.Find(By.id("skuPlan")).clear();
        SelenElem.TypeText(By.id("skuPlan"), SkuPlan);
        SelenElem.Find(By.id("namePlan")).clear();
        SelenElem.TypeText(By.id("namePlan"), NamePlan);
        SelenElem.Find(By.id("skuRecarga")).clear();
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.Find(By.id("skuDn")).clear();
        SelenElem.TypeText(By.id("skuDn"), Dn);
        //SelenElem.Find(By.id("monto")).clear();
        SelenElem.Find(By.id("rfc")).clear();
        reporte.AddImageToReport("Pagina principal", OPG.SS());
        
        SelenElem.Click(By.name("Comprar"));
        
        SelenElem.WaitForLoad(By.id("dnPorta"), 10);
        SelenElem.TypeText(By.id("dnPorta"), Dn);
        reporte.AddImageToReport("Ingresar DN", OPG.SS());
        
        SelenElem.Click(By.id("btn-porta"));
        
        SelenElem.TypeText(By.id("valid-name"), Nombre);
        SelenElem.TypeText(By.id("valid-apaterno"), ApellPat);
        SelenElem.TypeText(By.id("valid-amaterno"), ApellMat);
        SelenElem.TypeText(By.id("valid-email"), Correo);
        SelenElem.TypeText(By.id("valid-phone"), Telefono);
        SelenElem.Click(By.id("validaPrivacidad"));
        Thread.sleep(5000);
        
        reporte.AddImageToReport("Sección tus datos del checkout", OPG.SS());
        
        SelenElem.Click(By.id("btnStep1Principal"));
        
        SelenElem.Click(By.id("dateTo_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-month"), Mes);
        SelenElem.SelectOption(By.className("ui-datepicker-year"), Año);
        SelenElem.Click(By.linkText(Dia));
        SelenElem.TypeText(By.id("valid-curp"), Curp);
        SelenElem.TypeText(By.id("cod-posta-porta"), CodPostal);
        SelenElem.TypeText(By.id("calle-porta-ref"), Calle);
        SelenElem.TypeText(By.id("nExter-porta"), NumExt);
        SelenElem.TypeText(By.id("nInter-porta"), NumInt);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("colonia-porta-ref"), Colonia);
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("btnStep1-2Principal"));
        
        Thread.sleep(3000);
        SelenElem.Click(By.id("input-shippingOption2"));
        SelenElem.TypeText(By.id("js_codigo_postal"), CodPostal);

        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        SelenElem.WaitForLoad(By.id("js_geo_option_cards"), 10);
        Thread.sleep(3000);
        SelenElem.Click(By.id("99090073cac"));
        reporte.AddImageToReport("Opciones envío del checkout", OPG.SS());
        
        SelenElem.Click(By.id("btnStep2"));
       
        Thread.sleep(5000);
        SelenElem.Click(By.xpath("//input[@class='nip__input']"));
        SelenElem.TypeText(By.xpath("//input[@class='nip__input']"), "1234");
        SelenElem.Click(By.id("dateToPorta_datepicker"));
        SelenElem.Click(By.linkText(DiaPorta));
        reporte.AddImageToReport("Valida tu NIP", OPG.SS());
        
        SelenElem.Click(By.id("btnStep3"));
        
        SelenElem.WaitForLoad(By.id("validaContrato"), 10);
        SelenElem.Click(By.id("validaContrato"));
        reporte.AddImageToReport("Contrato del checkout", OPG.SS());
        SelenElem.Click(By.id("btnStep4_resume"));
        
        SelenElem.WaitForLoad(By.id("imagen-cliente"),10);
        reporte.AddImageToReport("Ingresamos a la pagina principal de FLAP", OPG.ScreenShotElement());
        SelenElem.focus(By.id("numTarjeta"));
        SelenElem.TypeText(By.id("numTarjeta"), Tarjeta);
        SelenElem.TypeText(By.id("cvv2"), Cvv);
        SelenElem.SelectOption(By.id("vigenciames"), MesTarjeta);
        SelenElem.SelectOption(By.id("vigenciaanio"), AñoTarjeta);
        reporte.AddImageToReport("Llenamos los datos de pago y damos click en pagar", OPG.ScreenShotElement());
        SelenElem.Click(By.id("continuar"));
        
        SelenElem.WaitForLoad(By.id("btnFinalizar"), 10);
        SelenElem.focus(By.id("btnFinalizar"));
        reporte.AddImageToReport("Información acerca del pago Flap", OPG.SS());
        
        SelenElem.WaitForLoad(By.id("numero_portar"), 20);
        reporte.AddImageToReport("Página de pago realizado con éxito", OPG.SS());
        SelenElem.focus(By.id("numero_portar"));
        reporte.AddImageToReport("", OPG.SS());
        
    }    
}

        
       