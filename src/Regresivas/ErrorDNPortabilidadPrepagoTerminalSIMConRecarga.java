
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class ErrorDNPortabilidadPrepagoTerminalSIMConRecarga {

    public ErrorDNPortabilidadPrepagoTerminalSIMConRecarga(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
       
        String dn = CargaVariables.LeerCSV(0, "Error DN Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        //SelenElem.Click(By.linkText("Cámbiate a Movistar"));
        SelenElem.WaitForLoad(By.id("menu_list"), 15);
        OPG.SelectMenu("Cámbiate a Movistar");
        
        SelenElem.WaitFor(By.id("sc2-dn"));
       
        SelenElem.Click(By.id("sc2-dn"));
        SelenElem.TypeText(By.id("sc2-dn"),dn);
        
        SelenElem.Click(By.id("sc2-goPortaStep2"));
        Thread.sleep(500);
        reporte.AddImageToReport("Validación de error en el DN", OPG.SS());
        SelenElem.waitForLoadPage();
        SelenElem.waitForLoadPage();
        
        
    }
    
}
