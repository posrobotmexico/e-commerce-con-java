
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class ErrorRFCPrepago {

    public ErrorRFCPrepago (Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
        
        String dn = CargaVariables.LeerCSV(0, "Error de RFC Prepago", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Error de RFC Prepago", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Error de RFC Prepago", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Error de RFC Prepago", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Error de RFC Prepago", Caso);
        String correo = CargaVariables.LeerCSV(5, "Error de RFC Prepago", Caso);
        String tel = CargaVariables.LeerCSV(6, "Error de RFC Prepago", Caso);
        String calle = CargaVariables.LeerCSV(8, "Error de RFC Prepago", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Error de RFC Prepago", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Error de RFC Prepago", Caso);
        String cp = CargaVariables.LeerCSV(7, "Error de RFC Prepago", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Error de RFC Prepago", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Error de RFC Prepago", Caso);
        String ine = CargaVariables.LeerCSV(14, "Error de RFC Prepago", Caso);
        String rfcc = CargaVariables.LeerCSV(15, "Error de RFC Prepago", Caso);

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        
        reporte.AddImageToReport("Se selecciona el telefono", OPG.ScreenShotElementFocus(By.xpath("//a[@href='" + Url + "iphone-xr-lte-amarillo-64gb-apple.html']")));
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + CargaVariables.LeerCSV(16,"Error de RFC Prepago",Caso) + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        Thread.sleep(2000);
        
        reporte.AddImageToReport("Ver detalle de Smartphone", OPG.SS());
        //SelenElem.focus(By.xpath("btn-buy-three"));
        //reporte.AddImageToReport("", OPG.SS());
        //SelenElem.Click(By.id("btn-buy-three"));
        SelenElem.Click(By.xpath("//p[.='Comprar']"));
        reporte.AddImageToReport("Resumen de compra ",OPG.SS() );
        SelenElem.focus(By.id("continueToCheckout"));
        reporte.AddImageToReport("",OPG.SS() );
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 5);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        SelenElem.Click(By.id("checkPrivacyOne"));
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("recibeFactura"));
        
        SelenElem.WaitForLoad(By.id("regimen_fiscal"), 30);
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        SelenElem.Click(By.id("valida-rfc"));
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        
        SelenElem.TypeText(By.id("codigo-postal-valida"), "06600");
        SelenElem.Click(By.id("valida-rfc"));
        OPG.cargaAjax2();
        SelenElem.Click(By.xpath("//*[@id=\"btn_continuar\"]"));
        Thread.sleep(1000);
        SelenElem.focus(By.id("valida-rfc"));
        reporte.AddImageToReport("Error en el RFC", OPG.SS());
        
    
    }
}
