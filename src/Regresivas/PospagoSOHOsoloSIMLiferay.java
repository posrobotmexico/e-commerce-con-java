
package Regresivas;

import Core.DeviceActions;
import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.FlapPorta;
import Operaciones.OpcionesEnvio;
import Operaciones.OperacionesGenerales;
import io.appium.java_client.AppiumDriver;
import java.io.File;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class PospagoSOHOsoloSIMLiferay {
    
     public PospagoSOHOsoloSIMLiferay() {
    
    }

     public void PospagoSOHOsoloSIMFianza200Liferay  (Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso)throws InterruptedException, IOException{
       
        String skuterminal = CargaVariables.LeerCSV(0, "Pospago SOHO Liferay", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Pospago SOHO Liferay", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Pospago SOHO Liferay", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Pospago SOHO Liferay", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Pospago SOHO Liferay", Caso);
        String correo = CargaVariables.LeerCSV(5, "Pospago SOHO Liferay", Caso);
        String tel = CargaVariables.LeerCSV(6, "Pospago SOHO Liferay", Caso);
        String calle = CargaVariables.LeerCSV(8, "Pospago SOHO Liferay", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Pospago SOHO Liferay", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Pospago SOHO Liferay", Caso);
        String cp = CargaVariables.LeerCSV(7, "Pospago SOHO Liferay", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Pospago SOHO Liferay", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Pospago SOHO Liferay", Caso);
        String ine = CargaVariables.LeerCSV(14, "Pospago SOHO Liferay", Caso);
        String rfcc = CargaVariables.LeerCSV(15, "Pospago SOHO Liferay", Caso);

        WebDrive.WebDriver.navigate().to(Url + "liferay.php");
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        Thread.sleep(2000);
        SelenElem.SelectOption(By.id("flowCode"), "pospago_b2b");
        SelenElem.Find(By.id("skuPlan")).clear();
        SelenElem.TypeText(By.id("skuPlan"), "0001_FJ");
        SelenElem.Find(By.id("namePlan")).clear();
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.Find(By.id("skuRecarga")).clear();
        SelenElem.Find(By.id("monto")).clear();
        SelenElem.Find(By.id("skuDn")).clear();
        SelenElem.Find(By.id("rfc")).clear();
        reporte.AddImageToReport("Ingresamos los datos y damos clic en enviar", OPG.SS());
        SelenElem.Click(By.name("Comprar"));
        
        //SelenElem.WaitForLoad(By.id("continueToCheckout"), 10);
        //reporte.AddImageToReport("Resumen de compra", OPG.SS());
        //SelenElem.focus(By.id("continueToCheckout"));
        //SelenElem.Click(By.id("detallePlan"));
        Thread.sleep(500);
        //reporte.AddImageToReport("Resumen de compra", OPG.SS());
        //SelenElem.Click(By.id("continueToCheckout"));
        SelenElem.WaitForLoad(By.id("valid-nombre"), 10);
        SelenElem.Click(By.id("valid-nombre"));
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("numero-domicilio-interior"), numInterior);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        OPG.cargaAjax2();
        Thread.sleep(3000);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.SelectOption(By.id("cfdi"), "G03 | Gastos en general");
        SelenElem.SelectOption(By.id("typeIdentification"), "INE/IFE");
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        
        

        File file = new File("A:\\DataEcommerce\\Pospago SOHO Liferay\\INE.pdf");
        String path = file.getAbsolutePath();
        SelenElem.Find(By.id("INE-FILE")).sendKeys(path);

        File file2 = new File("A:\\DataEcommerce\\Pospago SOHO Liferay\\cedula.pdf");
        String path2 = file2.getAbsolutePath();
        SelenElem.Find(By.id("CEDULA-FILE")).sendKeys(path2);
        
        SelenElem.WaitForLoad(By.xpath("//*[@id=\"msmx-pospago\"]/div[1]/fieldset[1]/div[2]/div/div/div[15]/div[2]/small"), 5);
        SelenElem.Click(By.id("INE-IFE"));
        reporte.AddImageToReport("", OPG.SS());  
        
               
        SelenElem.Click(By.id("checkPrivacyOne"));
        Thread.sleep(2000);
        SelenElem.focus(By.xpath("//button[@title='Da clic aquí para continuar a opciones de envío']"));
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.Click(By.xpath("//button[@title='Da clic aquí para continuar a opciones de envío']"));
        OPG.cargaAjax2();
        
        
        SelenElem.WaitForLoad(By.id("toggleShipping2"), 20);
        
        //new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        SelenElem.Click(By.id("toggleShipping2"));
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("js_codigo_postal"), "06600");
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        SelenElem.WaitForLoad(By.id("99090073cac"), 10);
        SelenElem.Click(By.id("99090073cac"));
        reporte.AddImageToReport("Sección opciones de envio del checkout", OPG.SS());
        
        SelenElem.focus(By.id("21090020cac"));
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.focus(By.xpath("//button[@title='Da clic aquí para continuar']"));
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
        
        
        SelenElem.Click(By.xpath("//button[@title='Da clic aquí para continuar']"));
        OPG.cargaAjax2();  
        SelenElem.Click(By.id("checkTerms"));
        SelenElem.focus(By.id("viewDelivery"));
        reporte.AddImageToReport("Sección Contrato del checkout", OPG.SS());
        SelenElem.Click(By.id("ver_clausulado_pdf"));
        OPG.cargaAjax2();
        Thread.sleep(10000);
        reporte.AddImageToReport("Sección Contrato del checkout", OPG.ScreenShotAll());
        SelenElem.Click(By.id("end-step"));
        OPG.cargaAjax2();
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        //SelenElem.Click(By.id("continuar-checkout-out"));
        SelenElem.ClickById(WebDrive.WebDriver, "continuar-checkout-out");
        
       
        new Flap( reporte, OPG, SelenElem, WebDrive);
        SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        SelenElem.focus(By.id("js_botonQuieroQueMeLlamen"));
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        
        
     }
     public void PospagoSOHOsoloSIMFianza0Liferay (Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso)throws InterruptedException, IOException{

               
        String nombre = CargaVariables.LeerCSV(0, "Pospago SOHO Liferay", Caso);
        String apellidop = CargaVariables.LeerCSV(1, "Pospago SOHO Liferay", Caso);
        String apellidom = CargaVariables.LeerCSV(2, "Pospago SOHO Liferay", Caso);
        String correo = CargaVariables.LeerCSV(3, "Pospago SOHO Liferay", Caso);
        String tel = CargaVariables.LeerCSV(4, "Pospago SOHO Liferay", Caso);
        String calle = CargaVariables.LeerCSV(6, "Pospago SOHO Liferay", Caso);
        String numdomicilio = CargaVariables.LeerCSV(7, "Pospago SOHO Liferay", Caso);
        String numInterior = CargaVariables.LeerCSV(8, "Pospago SOHO Liferay", Caso);
        String cp = CargaVariables.LeerCSV(5, "Pospago SOHO Liferay", Caso);
        String ciudad = CargaVariables.LeerCSV(11, "Pospago SOHO Liferay", Caso);
        String colonia = CargaVariables.LeerCSV(10, "Pospago SOHO Liferay", Caso);
        String cfdi = CargaVariables.LeerCSV(15, "Pospago SOHO Liferay", Caso);
        String ine = CargaVariables.LeerCSV(13, "Pospago SOHO Liferay", Caso);
        String rfc = CargaVariables.LeerCSV(16, "Pospago SOHO Liferay", Caso);
        String tarjeta = CargaVariables.LeerCSV(17, "Pospago SOHO Liferay", Caso);
        String expiracion = CargaVariables.LeerCSV(18, "Pospago SOHO Liferay", Caso);
        String cvv = CargaVariables.LeerCSV(19, "Pospago SOHO Liferay", Caso);
        String skuterminal = CargaVariables.LeerCSV(20, "Pospago SOHO Liferay", Caso);
        String skuPlan = CargaVariables.LeerCSV(21, "Pospago SOHO Liferay", Caso);

        WebDrive.WebDriver.navigate().to(Url + "liferay.php");
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        Thread.sleep(2000);
        SelenElem.SelectOption(By.id("flowCode"), "pospago_b2b");
        SelenElem.Find(By.id("skuPlan")).clear();
        SelenElem.TypeText(By.id("skuPlan"), skuPlan);
        SelenElem.Find(By.id("namePlan")).clear();
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.Find(By.id("skuRecarga")).clear();
        SelenElem.Find(By.id("monto")).clear();
        SelenElem.Find(By.id("skuDn")).clear();
        SelenElem.Find(By.id("rfc")).clear();
        reporte.AddImageToReport("Ingresamos los datos y damos clic en enviar", OPG.SS());
        SelenElem.Click(By.name("Comprar"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 40);
        SelenElem.waitForLoadPage();
        SelenElem.Click(By.id("checkPrivacyOne"));
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout ", OPG.SS());
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("numero-domicilio-interior"), numInterior);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.Click(By.id("valid-ciudad"));
        Thread.sleep(5000);
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        
        SelenElem.SelectOption(By.id("colonia"), colonia);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout ", OPG.SS());
        SelenElem.SelectOption(By.id("cfdi"), cfdi);
        SelenElem.SelectOption(By.id("typeIdentification"), "INE/IFE");
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        da.hideKeyBoard();
        
        SelenElem.Click(By.id("archivoINE"));
        SelenElem.Find(By.id("archivoINE")).click();
        AppiumDriver ad = (AppiumDriver) WebDrive.WebDriver;
        String Chrome = ad.getContext();
        ad.context("NATIVE_APP");
        Thread.sleep(5000);  
        ad.findElement(By.xpath("//*[@text='Documentos']")).click();
        Thread.sleep(3000);
        ad.findElement(By.xpath("//*[@text='INE.pdf']")).click();
        Thread.sleep(3000);        
        ad.context(Chrome);
        
        SelenElem.Click(By.id("archivoCEDULA"));
        ad.context("NATIVE_APP");
        Thread.sleep(5000);     
        ad.findElement(By.xpath("//*[@text='Documentos']")).click();
        Thread.sleep(3000);
        ad.findElement(By.xpath("//*[@text='cedula.pdf']")).click();
        Thread.sleep(3000);        
        ad.context(Chrome);
        
        da.slide("Up", 1);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.Click(By.xpath("//button[@title='Da clic aquí para continuar a opciones de envío']"));
        //SelenElem.ClickByXpath(WebDrive.WebDriver,"//button[@title='Da clic aquí para continuar a opciones de envío']");
        OPG.cargaAjax2();
        Thread.sleep(5000);   
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 15);
        da.hideKeyBoard();
        SelenElem.Click(By.id("inv-mail2"));
        //OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
        Thread.sleep(3000);
        SelenElem.TypeText(By.id("js_codigo_postal"), "06600");
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        SelenElem.ClickById(WebDrive.WebDriver,"js_search_cacs_by_postal_code");
        OPG.cargaAjax2();   
        SelenElem.Click(By.id("99090073cac"));
        da.slide("Up", 1);
        da.hideKeyBoard();
        Thread.sleep(1000);
        reporte.AddImageToReport("Sección opciones de envio del checkout", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección opciones de envio del checkout", OPG.SS());
        
        SelenElem.focus(By.id("21090020cac"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección opciones de envio", OPG.SS());
        
        SelenElem.Click(By.xpath("//button[@title='Da clic aquí para continuar']"));
        OPG.cargaAjax2();  
        SelenElem.Click(By.id("checkTerms"));
        da.slide("Up", 2);
        reporte.AddImageToReport("Sección Contrato del checkout", OPG.SS());
        
        SelenElem.Click(By.id("end-step"));
        OPG.cargaAjax2();
        //reporte.AddImageToReport("Resumen de compra", OPG.SS());
        //SelenElem.Click(By.id("continuar-checkout-out"));
        
        //SelenElem.Click(By.xpath("//button[@title='Al dar clic aquí serás enviado a una página segura para realizar el pago']"));
        
        //SelenElem.ClickById(WebDrive.WebDriver, "continuar-checkout-out");
        
        SelenElem.WaitForLoad(By.name("cc_owner"), 20);
        Thread.sleep(2000);
        SelenElem.Click(By.name("cc_owner"));
        SelenElem.TypeText(By.name("cc_owner"), nombre);
        SelenElem.Click(By.name("cc_number"));
        SelenElem.TypeText(By.name("cc_number"), tarjeta);
        SelenElem.Click(By.name("cc_exp_month"));
        SelenElem.TypeText(By.name("cc_exp_month"), expiracion);
        SelenElem.Click(By.name("cc_cid"));
        SelenElem.TypeText(By.name("cc_cid"), cvv);
        da.hideKeyBoard();
        da.slide("Up", 1);
        reporte.AddImageToReport("Sección Pago del Checkout", OPG.SS());
        SelenElem.Click(By.id("end-step2"));
        Thread.sleep(5000);
        reporte.AddImageToReport("Sección Pago del Checkout", OPG.SS());
        SelenElem.Click(By.id("grandTotalMobile"));
        reporte.AddImageToReport("Sección Pago del Checkout", OPG.SS());
        SelenElem.Click(By.id("continuar-checkout-out"));
        
        SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        
        
     }
    
}
