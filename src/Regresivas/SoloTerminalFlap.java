
package Regresivas;

import Core.DeviceActions;
import Core.WebAction;
import Core.WebDriv;
import Core.Report;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

/**
 *
 * @author LAPTOP-JORGE
 */
public class SoloTerminalFlap {
    
    public SoloTerminalFlap(){
        
    }

    public void SoloTerminalCupon(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, DeviceActions da,String Caso) throws InterruptedException, IOException 
    {
        
        String ModTel = CargaVariables.LeerCSV(0, "Solo Terminal - Flap", Caso);
        String Cupon = CargaVariables.LeerCSV(1, "Solo Terminal - Flap", Caso);
        String Nombre = CargaVariables.LeerCSV(2, "Solo Terminal - Flap", Caso);
        String ApellPa = CargaVariables.LeerCSV(3, "Solo Terminal - Flap", Caso);
        String ApellMa = CargaVariables.LeerCSV(4, "Solo Terminal - Flap", Caso);
        String Correo = CargaVariables.LeerCSV(5, "Solo Terminal - Flap", Caso);
        String Telefono = CargaVariables.LeerCSV(6, "Solo Terminal - Flap", Caso);
        String TelefAdic = CargaVariables.LeerCSV(7, "Solo Terminal - Flap", Caso);
        String CodPostal = CargaVariables.LeerCSV(8, "Solo Terminal - Flap", Caso);
        String Tarjeta = CargaVariables.LeerCSV(9, "Solo Terminal - Flap", Caso);
        String CVV = CargaVariables.LeerCSV(10, "Solo Terminal - Flap", Caso);
        String Mes = CargaVariables.LeerCSV(11, "Solo Terminal - Flap", Caso);
        String Año = CargaVariables.LeerCSV(10, "Solo Terminal - Flap", Caso);

        WebDrive.WebDriver.navigate().to(Url);
        Thread.sleep(3000);
        reporte.AddImageToReport("Continuamos con la pagina principal", OPG.SS());
        SelenElem.Click(By.id("checkbox"));
        SelenElem.Click(By.id("boton_smartphone"));
        SelenElem.Click(By.xpath("//a[@title='Todas las marcas']"));
        
        reporte.AddImageToReport("Seleccionar el flujo de telefonos", OPG.SS());
        SelenElem.focus(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        reporte.AddImageToReport("Se procede a seleccionar el Smartphone", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Detalle del smartphone", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//button[@class='fondo-boton-comprar']"));
        
        SelenElem.Click(By.id("coupon_code"));
        SelenElem.TypeText(By.id("coupon_code"), Cupon);
        SelenElem.Click(By.xpath("//button[@value='Aplicar Descuento']"));
        reporte.AddImageToReport("Despues de ingresar cupon", OPG.SS());
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 15);
        SelenElem.TypeText(By.id("valid-nombre"), Nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), ApellPa);
        SelenElem.TypeText(By.id("valid-appelido-m"), ApellMa);
        SelenElem.TypeText(By.id("valid-email"), Correo);
        SelenElem.TypeText(By.id("telefono-contacto"), Telefono);
        SelenElem.TypeText(By.id("additionalContactPhone"), TelefAdic);
        SelenElem.Click(By.id("checkPrivacyOne"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del checkout", OPG.SS());
        SelenElem.Click(By.id("btn_continuar"));
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 15);
        SelenElem.Click(By.id("inv-mail2"));
        SelenElem.Click(By.id("js_codigo_postal"));
        SelenElem.TypeText(By.id("js_codigo_postal"), CodPostal);
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        SelenElem.WaitForLoad(By.id("js_geo_option_cards"), 20);
        SelenElem.ClickButton(WebDrive.WebDriver, SelenElem, "ClassName", "geo-cac-elocker-option__place-title");
        reporte.AddImageToReport("Sección opciones envío del checkout", OPG.SS());
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        
        SelenElem.WaitForLoad(By.id("continuar-checkout-out"), 10);
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        SelenElem.Click(By.id("continuar-checkout-out"));
        
        reporte.AddImageToReport("Medio de pago Flap", OPG.SS());
        SelenElem.Click(By.id("mp2-button-logo-visa-master-card"));
        SelenElem.WaitForLoad(By.id("mp2-payment-title"), 10);
        SelenElem.Click(By.id("contrato_108553_1"));
        
        SelenElem.Click(By.id("numTarjeta"));
        SelenElem.TypeText(By.id("numTarjeta"), Tarjeta);
        SelenElem.Click(By.id("vigenciames"));
        SelenElem.TypeText(By.id("vigenciames"), Mes);
        SelenElem.Click(By.id("vigenciaanio"));
        SelenElem.TypeText(By.id("vigenciaanio"), Año);
        SelenElem.Click(By.id("cvv2"));
        SelenElem.TypeText(By.id("cvv2"), CVV);
        da.hideKeyBoard();
        reporte.AddImageToReport("Medio de pago Flap", OPG.SS());
        SelenElem.Click(By.id("continuar"));
        
        Thread.sleep(2000);
        reporte.AddImageToReport("", OPG.SS());
        
        Thread.sleep(5000);
        reporte.AddImageToReport("Pagina de pago realizada con exito", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        
        
        
        
    }

    public void SoloTerminalDescuento(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, DeviceActions da,String Caso) throws InterruptedException, IOException 
    {
        
        String ModTel = CargaVariables.LeerCSV(0, "Solo Terminal - Flap", Caso);
        String Nombre = CargaVariables.LeerCSV(1, "Solo Terminal - Flap", Caso);
        String ApellPa = CargaVariables.LeerCSV(2, "Solo Terminal - Flap", Caso);
        String ApellMa = CargaVariables.LeerCSV(3, "Solo Terminal - Flap", Caso);
        String Correo = CargaVariables.LeerCSV(4, "Solo Terminal - Flap", Caso);
        String Telefono = CargaVariables.LeerCSV(5, "Solo Terminal - Flap", Caso);
        String TelefAdic = CargaVariables.LeerCSV(6, "Solo Terminal - Flap", Caso);
        String CodPostal = CargaVariables.LeerCSV(7, "Solo Terminal - Flap", Caso);
        String Tarjeta = CargaVariables.LeerCSV(8, "Solo Terminal - Flap", Caso);
        String CVV = CargaVariables.LeerCSV(9, "Solo Terminal - Flap", Caso);
        String Mes = CargaVariables.LeerCSV(10, "Solo Terminal - Flap", Caso);
        String Año = CargaVariables.LeerCSV(11, "Solo Terminal - Flap", Caso);
        
        WebDrive.WebDriver.navigate().to(Url);
        Thread.sleep(3000);
        reporte.AddImageToReport("Continuamos con la pagina principal", OPG.SS());
        SelenElem.Click(By.id("checkbox"));
        SelenElem.Click(By.id("boton_smartphone"));
        SelenElem.Click(By.xpath("//a[@title='Todas las marcas']"));
        
        reporte.AddImageToReport("Seleccionar el flujo de telefonos", OPG.SS());
        SelenElem.focus(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        reporte.AddImageToReport("Se procede a seleccionar el Smartphone", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Detalle del smartphone", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//button[@class='fondo-boton-comprar']"));
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 15);
        SelenElem.TypeText(By.id("valid-nombre"), Nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), ApellPa);
        SelenElem.TypeText(By.id("valid-appelido-m"), ApellMa);
        SelenElem.TypeText(By.id("valid-email"), Correo);
        SelenElem.TypeText(By.id("telefono-contacto"), Telefono);
        SelenElem.TypeText(By.id("additionalContactPhone"), TelefAdic);
        SelenElem.Click(By.id("checkPrivacyOne"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Seccióntus datos del checkout", OPG.SS());
        SelenElem.Click(By.id("btn_continuar"));
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 15);
        SelenElem.Click(By.id("inv-mail2"));
        SelenElem.Click(By.id("js_codigo_postal"));
        SelenElem.TypeText(By.id("js_codigo_postal"), CodPostal);
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        SelenElem.WaitForLoad(By.id("js_geo_option_cards"), 20);
        SelenElem.ClickButton(WebDrive.WebDriver, SelenElem, "ClassName", "geo-cac-elocker-option__place-title");
        reporte.AddImageToReport("Sección opciones envío del checkout", OPG.SS());
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        
        SelenElem.WaitForLoad(By.id("continuar-checkout-out"), 10);
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        SelenElem.Click(By.id("continuar-checkout-out"));
        
        reporte.AddImageToReport("Medio de pago Flap", OPG.SS());
        SelenElem.Click(By.id("mp2-button-logo-visa-master-card"));
        SelenElem.WaitForLoad(By.id("mp2-payment-title"), 10);
        SelenElem.Click(By.id("contrato_108553_1"));
        
        SelenElem.Click(By.id("numTarjeta"));
        SelenElem.TypeText(By.id("numTarjeta"), Tarjeta);
        SelenElem.Click(By.id("vigenciames"));
        SelenElem.TypeText(By.id("vigenciames"), Mes);
        SelenElem.Click(By.id("vigenciaanio"));
        SelenElem.TypeText(By.id("vigenciaanio"), Año);
        SelenElem.Click(By.id("cvv2"));
        SelenElem.TypeText(By.id("cvv2"), CVV);
        da.hideKeyBoard();
        reporte.AddImageToReport(" ", OPG.SS());
        SelenElem.Click(By.id("continuar"));
        
        Thread.sleep(2000);
        reporte.AddImageToReport("", OPG.SS());
        
        Thread.sleep(5000);
        reporte.AddImageToReport("Pagina de pago realizada con exito", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        
    }
    
    public void SoloTerminalDescuentoCupon(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, DeviceActions da,String Caso) throws InterruptedException, IOException 
    {
        
        String ModTel = CargaVariables.LeerCSV(0, "Solo Terminal - Flap", Caso);
        String Cupon = CargaVariables.LeerCSV(1, "Solo Terminal - Flap", Caso);
        String Nombre = CargaVariables.LeerCSV(2, "Solo Terminal - Flap", Caso);
        String ApellPa = CargaVariables.LeerCSV(3, "Solo Terminal - Flap", Caso);
        String ApellMa = CargaVariables.LeerCSV(4, "Solo Terminal - Flap", Caso);
        String Correo = CargaVariables.LeerCSV(5, "Solo Terminal - Flap", Caso);
        String Telefono = CargaVariables.LeerCSV(6, "Solo Terminal - Flap", Caso);
        String TelefAdic = CargaVariables.LeerCSV(7, "Solo Terminal - Flap", Caso);
        String RFC = CargaVariables.LeerCSV(8, "Solo Terminal - Flap", Caso);
        String Calle = CargaVariables.LeerCSV(9, "Solo Terminal - Flap", Caso);
        String NumExt = CargaVariables.LeerCSV(10, "Solo Terminal - Flap", Caso);
        String NumInt = CargaVariables.LeerCSV(11, "Solo Terminal - Flap", Caso);
        String Colonia = CargaVariables.LeerCSV(12, "Solo Terminal - Flap", Caso);
        String CodPostal = CargaVariables.LeerCSV(13, "Solo Terminal - Flap", Caso);
        String Tarjeta = CargaVariables.LeerCSV(14, "Solo Terminal - Flap", Caso);
        String CVV = CargaVariables.LeerCSV(15, "Solo Terminal - Flap", Caso);
        String Mes = CargaVariables.LeerCSV(16, "Solo Terminal - Flap", Caso);
        String Año = CargaVariables.LeerCSV(17, "Solo Terminal - Flap", Caso);
        
        WebDrive.WebDriver.navigate().to(Url);
        Thread.sleep(3000);
        reporte.AddImageToReport("Continuamos con la pagina principal", OPG.SS());
        SelenElem.Click(By.id("checkbox"));
        SelenElem.Click(By.id("boton_smartphone"));
        SelenElem.Click(By.xpath("//a[@title='Todas las marcas']"));
        
        reporte.AddImageToReport("Seleccionar el flujo de telefonos", OPG.SS());
        SelenElem.focus(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        reporte.AddImageToReport("Se procede a seleccionar el Smartphone", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Detalle del smartphone", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//button[@class='fondo-boton-comprar']"));
        
        SelenElem.Click(By.id("coupon_code"));
        SelenElem.TypeText(By.id("coupon_code"), Cupon);
        SelenElem.Click(By.xpath("//button[@value='Aplicar Descuento']"));
        SelenElem.Click(By.id("continueToCheckout"));
       
        SelenElem.WaitForLoad(By.id("valid-nombre"), 15);
        SelenElem.TypeText(By.id("valid-nombre"), Nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), ApellPa);
        SelenElem.TypeText(By.id("valid-appelido-m"), ApellMa);
        SelenElem.TypeText(By.id("valid-email"), Correo);
        SelenElem.TypeText(By.id("telefono-contacto"), Telefono);
        SelenElem.TypeText(By.id("additionalContactPhone"), TelefAdic);
        da.hideKeyBoard();
        reporte.AddImageToReport("Seccióntus datos del checkout", OPG.SS());
        
        SelenElem.Click(By.id("recibeFactura"));
        
        SelenElem.TypeText(By.id("valida-rfc"), RFC);
        SelenElem.TypeText(By.id("codigo-postal-valida"), CodPostal);
        SelenElem.TypeText(By.id("calle-domicilio"), Calle);
        SelenElem.TypeText(By.id("numero-domicilio"), NumExt);
        SelenElem.TypeText(By.id("numero-interior"), NumInt);
        Thread.sleep(2000);
        SelenElem.Click(By.id("colonia"));
        SelenElem.TypeText(By.id("colonia"), Colonia);
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("checkPrivacyOne"));
        SelenElem.Click(By.id("btn_continuar"));
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 15);
        SelenElem.Click(By.id("inv-mail2"));
        SelenElem.Click(By.id("js_codigo_postal"));
        SelenElem.TypeText(By.id("js_codigo_postal"), CodPostal);
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        SelenElem.WaitForLoad(By.id("js_geo_option_cards"), 20);
        SelenElem.ClickButton(WebDrive.WebDriver, SelenElem, "ClassName", "geo-cac-elocker-option__place-title");
        reporte.AddImageToReport("Sección opciones envío del checkout", OPG.SS());
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        
        SelenElem.WaitForLoad(By.id("continuar-checkout-out"), 10);
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        SelenElem.Click(By.id("continuar-checkout-out"));
        
        reporte.AddImageToReport("Medio de pago Flap", OPG.SS());
        SelenElem.Click(By.id("mp2-button-logo-visa-master-card"));
        SelenElem.WaitForLoad(By.id("mp2-payment-title"), 10);
        SelenElem.Click(By.id("contrato_108553_1"));
        
        SelenElem.Click(By.id("numTarjeta"));
        SelenElem.TypeText(By.id("numTarjeta"), Tarjeta);
        SelenElem.Click(By.id("vigenciames"));
        SelenElem.TypeText(By.id("vigenciames"), Mes);
        SelenElem.Click(By.id("vigenciaanio"));
        SelenElem.TypeText(By.id("vigenciaanio"), Año);
        SelenElem.Click(By.id("cvv2"));
        SelenElem.TypeText(By.id("cvv2"), CVV);
        da.hideKeyBoard();
        reporte.AddImageToReport(" ", OPG.SS());
        SelenElem.Click(By.id("continuar"));
        Thread.sleep(2000);
        reporte.AddImageToReport("", OPG.SS());
        
        Thread.sleep(5000);
        reporte.AddImageToReport("Pagina de pago realizada con exito", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        
        
    }

    public void SoloTerminalPrecioCupon(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, DeviceActions da,String Caso) throws InterruptedException, IOException 
    {
        
        String ModTel = CargaVariables.LeerCSV(0, "Solo Terminal - Flap", Caso);
        String Cupon = CargaVariables.LeerCSV(1, "Solo Terminal - Flap", Caso);
        
        WebDrive.WebDriver.navigate().to(Url);
        Thread.sleep(3000);
        reporte.AddImageToReport("Continuamos con la pagina principal", OPG.SS());
        SelenElem.Click(By.id("checkbox"));
        SelenElem.Click(By.id("boton_smartphone"));
        SelenElem.Click(By.xpath("//a[@title='Todas las marcas']"));
        
        reporte.AddImageToReport("Seleccionar el flujo de telefonos", OPG.SS());
        SelenElem.focus(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        reporte.AddImageToReport("Se procede a seleccionar el Smartphone", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Detalle del smartphone", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//button[@class='fondo-boton-comprar']"));
        
        SelenElem.Click(By.id("coupon_code"));
        SelenElem.TypeText(By.id("coupon_code"), Cupon);
        SelenElem.Click(By.xpath("//button[@value='Aplicar Descuento']"));
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        Thread.sleep(5000);
        reporte.AddImageToReport("Malla de terminales", OPG.SS());
    }
    
    public void SoloTerminalPrecioDescuento(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, DeviceActions da,String Caso) throws InterruptedException, IOException 
    {
        
        String ModTel = CargaVariables.LeerCSV(0, "Solo Terminal - Flap", Caso);
        
        WebDrive.WebDriver.navigate().to(Url);
        Thread.sleep(3000);
        reporte.AddImageToReport("Continuamos con la pagina principal", OPG.SS());
        SelenElem.Click(By.id("checkbox"));
        SelenElem.Click(By.id("boton_smartphone"));
        SelenElem.Click(By.xpath("//a[@title='Todas las marcas']"));
        
        reporte.AddImageToReport("Seleccionar el flujo de telefonos", OPG.SS());
        SelenElem.focus(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        reporte.AddImageToReport("Se procede a seleccionar el Smartphone", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Detalle del smartphone", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//button[@class='fondo-boton-comprar']"));
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        Thread.sleep(5000);
        reporte.AddImageToReport("Malla de terminales", OPG.SS());
    }
}
