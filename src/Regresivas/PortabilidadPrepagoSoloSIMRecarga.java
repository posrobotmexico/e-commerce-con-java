
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class PortabilidadPrepagoSoloSIMRecarga {
    
    public PortabilidadPrepagoSoloSIMRecarga(){
        
    }
    
    public void PortabilidadPrepagoSoloSIMRecarga(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive,String URL, String Caso) throws InterruptedException, IOException{
         
        String dn = CargaVariables.LeerCSV(0, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String correo = CargaVariables.LeerCSV(5, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String tel = CargaVariables.LeerCSV(6, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String calle = CargaVariables.LeerCSV(8, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String cp = CargaVariables.LeerCSV(7, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String ine = CargaVariables.LeerCSV(14, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String año = CargaVariables.LeerCSV(16, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String mes = CargaVariables.LeerCSV(17, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String  dia= CargaVariables.LeerCSV(18, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String curp= CargaVariables.LeerCSV(19, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        
        WebDrive.WebDriver.navigate().to( URL);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        SelenElem.Click(By.linkText("Cámbiate a Movistar"));
        
        
        SelenElem.WaitFor(By.id("sc2-dn"));
       
        SelenElem.Click(By.id("sc2-dn"));
        SelenElem.TypeText(By.id("sc2-dn"),dn);
        reporte.AddImageToReport("Seleccionamos el flujo de Cámbiate a Movistar", OPG.SS());
        
        SelenElem.Click(By.id("sc2-goPortaStep2"));
        SelenElem.waitForLoadPage();
       
        SelenElem.WaitForLoad(By.id("option_prepaid"),10);
        SelenElem.Click(By.id("option_prepaid"));
       
        reporte.AddImageToReport("Seleccionamos Quiero comprar un prepago", OPG.ScreenShotElement());
        SelenElem.Click(By.id("sc2-goToPortability"));
        
        
        /*
        SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        SelenElem.focus(By.id("firstCharacter"));
        reporte.AddImageToReport("Valida tu identidad", OPG.ScreenShotElement());
        SelenElem.Click(By.id("continue-btn"));*/
        
        SelenElem.WaitForLoad(By.id("plan1"),10);
        SelenElem.Click(By.id("plan1"));
       
        reporte.AddImageToReport("Seleccionar sin Smartphone y continuar", OPG.SS());
        SelenElem.Click(By.id("gtm-plans-continue-button"));
        
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Seleccionamos un plan con recarga", OPG.SS());
        SelenElem.Click(By.id("Plan-ilimitado-Plus"));
        reporte.AddImageToReport("", OPG.SS());
        Thread.sleep(1000);
        
    }
    
    public void PortaPrepagoSIMRecargaModalPorta(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive,String URL, String Caso) throws InterruptedException, IOException{
        
        String Dn = CargaVariables.LeerCSV(0, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String SkuRecarga = CargaVariables.LeerCSV(1, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String Monto = CargaVariables.LeerCSV(2, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String Nombre = CargaVariables.LeerCSV(3, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String ApellidoPa = CargaVariables.LeerCSV(4, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String ApellidoMa = CargaVariables.LeerCSV(5, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String Telefono = CargaVariables.LeerCSV(6, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String Correo = CargaVariables.LeerCSV(7, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String Rfc = CargaVariables.LeerCSV(8, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String Calle = CargaVariables.LeerCSV(9, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String NumExt = CargaVariables.LeerCSV(10, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String NumInt = CargaVariables.LeerCSV(11, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String Cp = CargaVariables.LeerCSV(12, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String Colonia = CargaVariables.LeerCSV(13, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String Curp = CargaVariables.LeerCSV(14, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String Dia = CargaVariables.LeerCSV(15, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String NumTarjeta = CargaVariables.LeerCSV(16, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String MesTarjeta = CargaVariables.LeerCSV(17, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String AñoTarjeta = CargaVariables.LeerCSV(18, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        String Cvv = CargaVariables.LeerCSV(19, "Portabilidad Prepago Solo SIM + Recarga", Caso);
        
        WebDrive.WebDriver.navigate().to(URL + "liferay.php");
        reporte.AddImageToReport("Se ingresa a la pagina principal", OPG.SS());
        
        SelenElem.SelectOption(By.id("flowCode"), "portabilidad_prepago_login");
        SelenElem.Find(By.id("skuPlan")).clear();
        SelenElem.Find(By.id("namePlan")).clear();
        SelenElem.Find(By.id("skuRecarga")).clear();
        SelenElem.TypeText(By.id("skuRecarga"), SkuRecarga);
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.Find(By.id("monto")).clear();
        SelenElem.TypeText(By.id("monto"), Monto);
        SelenElem.Find(By.id("rfc")).clear();
        reporte.AddImageToReport("Se llena el formulario", OPG.SS());
        
        SelenElem.Click(By.name("Comprar"));
        
        SelenElem.WaitForLoad(By.id("mismo"), 15);
        SelenElem.Click(By.id("mismo"));
        SelenElem.TypeText(By.id("dnPorta"), Dn);
        reporte.AddImageToReport("Se ingresa Dn", OPG.SS());
        
        SelenElem.Click(By.id("btn-porta"));
        
        SelenElem.WaitForLoad(By.id("continueToCheckout"), 15);
        Thread.sleep(4000);
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        SelenElem.focus(By.id("continueToCheckout"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 15);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("valid-nombre"), Nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), ApellidoPa);
        SelenElem.TypeText(By.id("valid-appelido-m"), ApellidoMa);
        SelenElem.TypeText(By.id("telefono-contacto"), Telefono);
        SelenElem.TypeText(By.id("valid-email"), Correo);
        SelenElem.TypeText(By.id("valida-rfc"), Rfc);
        SelenElem.Click(By.id("recibeFactura"));
        Thread.sleep(2000);
        reporte.AddImageToReport("Sección tus datos del checkout", OPG.SS());
        
        SelenElem.TypeText(By.id("calle-domicilio"), Calle);
        SelenElem.TypeText(By.id("numero-domicilio"), NumExt);
        SelenElem.TypeText(By.id("numero-interior"), NumInt);
        SelenElem.TypeText(By.id("codigo-postal-valida"), Cp);
        SelenElem.Click(By.id("colonia"));
        SelenElem.Click(By.id("checkPrivacyOne"));
        Thread.sleep(3000);
        SelenElem.TypeText(By.id("colonia"), Colonia);
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("botonTusDatos"));
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 15);
        Thread.sleep(2000);
        SelenElem.Click(By.id("inv-mail2"));
        SelenElem.WaitForLoad(By.id("js_codigo_postal"), 15);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("js_codigo_postal"), Cp);
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        SelenElem.WaitForLoad(By.id("99090073cac"), 10);
        SelenElem.Click(By.id("99090073cac"));
        reporte.AddImageToReport("Sección opciones de envío del checkout", OPG.SS());
        SelenElem.focus(By.id("21090020cac"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("checkout-paso2-renovaciones"));
        
        SelenElem.WaitForLoad(By.id("valid-nip"), 15);
        SelenElem.TypeText(By.id("valid-nip"), "1234");
        SelenElem.TypeText(By.id("valid-curp"), Curp);
        SelenElem.Click(By.id("dateToPorta_datepicker"));
        SelenElem.Click(By.linkText(Dia));
        Thread.sleep(2000);
        reporte.AddImageToReport("Sección cambiate a movistar", OPG.SS());
        
        SelenElem.Click(By.id("checkout-paso4-porta-pre"));
        
        SelenElem.WaitForLoad(By.id("imagen-cliente"), 20);
        reporte.AddImageToReport("Medio de pago Flap", OPG.SS());
        SelenElem.focus(By.id("1"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.id("1"));
        SelenElem.WaitForLoad(By.id("numTarjeta"), 10);
        SelenElem.TypeText(By.id("numTarjeta"), NumTarjeta);
        SelenElem.SelectOption(By.id("vigenciames"), MesTarjeta);
        SelenElem.SelectOption(By.id("vigenciaanio"), AñoTarjeta);
        SelenElem.TypeText(By.id("cvv2"), Cvv);
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("continuar"));
        
        SelenElem.WaitForLoad(By.id("btnFinalizar"), 10);
        SelenElem.focus(By.id("btnFinalizar"));
        reporte.AddImageToReport("Información acerca del pago Flap", OPG.SS());
        
        SelenElem.WaitForLoad(By.id("numero_portar"), 20);
        reporte.AddImageToReport("Página de pago realizado con éxito", OPG.SS());
        SelenElem.focus(By.id("numero_portar"));
        reporte.AddImageToReport("", OPG.SS());
 
        
    }
         
    
}
