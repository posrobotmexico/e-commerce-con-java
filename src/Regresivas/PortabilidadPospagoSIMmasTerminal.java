
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.FlapPorta;
import Operaciones.OpcionesEnvio;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class PortabilidadPospagoSIMmasTerminal {

    public PortabilidadPospagoSIMmasTerminal(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
         
        String dn = CargaVariables.LeerCSV(0, "Portabilidad Pospago SIM + Terminal", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Portabilidad Pospago SIM + Terminal", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Portabilidad Pospago SIM + Terminal", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Portabilidad Pospago SIM + Terminal", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Portabilidad Pospago SIM + Terminal", Caso);
        String correo = CargaVariables.LeerCSV(5, "Portabilidad Pospago SIM + Terminal", Caso);
        String tel = CargaVariables.LeerCSV(6, "Portabilidad Pospago SIM + Terminal", Caso);
        String calle = CargaVariables.LeerCSV(8, "Portabilidad Pospago SIM + Terminal", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Portabilidad Pospago SIM + Terminal", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Portabilidad Pospago SIM + Terminal", Caso);
        String cp = CargaVariables.LeerCSV(7, "Portabilidad Pospago SIM + Terminal", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Portabilidad Pospago SIM + Terminal", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Portabilidad Pospago SIM + Terminal", Caso);
        String ine = CargaVariables.LeerCSV(14, "Portabilidad Pospago SIM + Terminal", Caso);
        String año = CargaVariables.LeerCSV(16, "Portabilidad Pospago SIM + Terminal", Caso);
        String mes = CargaVariables.LeerCSV(17, "Portabilidad Pospago SIM + Terminal", Caso);
        String  dia= CargaVariables.LeerCSV(18, "Portabilidad Pospago SIM + Terminal", Caso);
        String curp= CargaVariables.LeerCSV(19, "Portabilidad Pospago SIM + Terminal", Caso);
        String modeloTel= CargaVariables.LeerCSV(20, "Portabilidad Pospago SIM + Terminal", Caso);
       

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        
        reporte.AddImageToReport("Se selecciona el telefono", OPG.ScreenShotElementFocus(By.xpath("//a[@href='" + Url + "iphone-xr-lte-amarillo-64gb-apple.html?category=43']")));
        Thread.sleep(2000);
        SelenElem.ClickByXpath(WebDrive.WebDriver,"//img[contains(@alt, '" + modeloTel + "')]"); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        
        /*
        SelenElem.WaitForLoad(By.xpath("//*[@id=\"solo-smartphone__content\"]/form/button"),15);
        reporte.AddImageToReport("Visualizar el detalle del smartphone e ingresar DN y dar click en “Usar este número”", OPG.SS());
        
        Thread.sleep(1000);
        SelenElem.Click(By.id("portabilidad"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_input_dn_button_portabilidad"), 10);
        SelenElem.TypeText(By.id("js_dn_input_portabilidad"),dn);
        SelenElem.focus(By.id("js_dn_input_portabilidad"));
        reporte.AddImageToReport(" ", OPG.SS());
        
        SelenElem.Click(By.id("js_input_dn_button_portabilidad"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.className("temm-2022-modal"), 10);
        reporte.AddImageToReport(" ", OPG.SS());
        
        SelenElem.Click(By.xpath("/html/body/div[8]/aside[2]/div[2]/footer/button/span"));
        
        
        SelenElem.Click(By.xpath("//*[@id=\"js_slider_mp_portabilidad-pospago\"]/div/div/div[3]/div/div/div/div"));
        Thread.sleep(500);
        SelenElem.WaitForLoad(By.xpath("//div[.='Quiero este plan']"), 10);
        reporte.AddImageToReport(" ", OPG.SS());
        
        //SelenElem.Click(By.xpath("//div[.='Quiero este plan']"));
        SelenElem.Click(By.xpath("//*[@id=\"js_slider_mp_Migration\"]/div/div/div[5]/div/div/div/div/p[1]"));
        SelenElem.WaitForLoad(By.xpath("//div[.='Quiero este plan']"), 10);
        reporte.AddImageToReport(" ", OPG.SS());
        SelenElem.Click(By.xpath("//div[.='Quiero este plan']"));
        
        Thread.sleep(2000);
        SelenElem.Click(By.id("js_submit_miniparrilla_migracion"));
      
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("continueToCheckout"), 10);
        */
        SelenElem.WaitForLoad(By.xpath("//*[@id=\"solo-smartphone__content\"]/form/button"),10);
        reporte.AddImageToReport("Visualizar el detalle del smartphone e ingresar DN y dar click en “Usar este número”", OPG.SS());
        
        Thread.sleep(1000);
        SelenElem.Click(By.id("portabilidad"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_input_dn_button_portabilidad"), 10);
        SelenElem.TypeText(By.id("js_dn_input_portabilidad"),dn);
        reporte.AddImageToReport(" ", OPG.SS());
       
        
        SelenElem.Click(By.id("js_input_dn_button_portabilidad"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.className("temm-2022-modal"), 10);
        Thread.sleep(2000);
        reporte.AddImageToReport(" ", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.xpath("//span[.='Continuar']"));
        
        
        SelenElem.Click(By.xpath("//*[@id=\"js_slider_mp_portabilidad-pospago\"]/div/div/div[3]/div/div/div/div"));
        Thread.sleep(500);
        SelenElem.WaitForLoad(By.xpath("//div[.='Quiero este plan']"), 10);
        reporte.AddImageToReport(" ", OPG.SS());
        
        SelenElem.Click(By.xpath("//button[.='Quiero este plan']"));
        
        Thread.sleep(2000);
        SelenElem.Click(By.id("js_submit_miniparrilla_portabilidad_pospago"));
        
        //SelenElem.waitForLoadPage();
        //SelenElem.WaitForLoad(By.id("continueToCheckout"), 10);
        
        //reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementFocus(By.id("section-equipo")));
        //SelenElem.Click(By.xpath("//button[@title='Da clic aquí para continuar con la captura de tus datos']"));
       
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-name"), 15);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-name"), nombre);
        SelenElem.TypeText(By.id("valid-apaterno"), apellidop);
        SelenElem.TypeText(By.id("valid-amaterno"), apellidom);
        
        SelenElem.TypeText(By.id("valid-phone"), tel);
        SelenElem.Click(By.id("validaPrivacidad"));
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("btnStep1Principal"));
        
        SelenElem.Click(By.id("dateTo_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), año);
        SelenElem.SelectOption(By.className("ui-datepicker-month"), mes);
        SelenElem.Click(By.linkText(dia));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("valid-curp"), curp);
        SelenElem.TypeText(By.id("calle-porta-ref"), calle);
        SelenElem.TypeText(By.id("cod-posta-porta"), cp);
        //SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        //SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        
        SelenElem.TypeText(By.id("nExter-porta"), numdomicilio);
        SelenElem.TypeText(By.id("nInter-porta"), numInterior);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        Thread.sleep(2000);
        SelenElem.SelectOption(By.id("colonia-porta-ref"), colonia);
        //SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        //SelenElem.TypeText(By.id("INE-IFE"), ine);
        //SelenElem.Click(By.id("checkPrivacyOne"));
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        
        SelenElem.Click(By.id("btnStep1-2Principal"));
        
        Thread.sleep(5000);
        
        SelenElem.WaitForLoad(By.id("cod"), 10);
        Thread.sleep(1000);
        SelenElem.TypeText(By.id("cod"), "1111");
        reporte.AddImageToReport("Sección Consulta de credito del Checkout", OPG.SS()); 
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        //SelenElem.Click(By.id("checkout-step2-radio-04"));
        SelenElem.Click(By.id("checkout-step2-radio-06"));
        SelenElem.Click(By.id("validaScore"));
        
        reporte.AddImageToReport("Sección Consulta de credito del Checkout", OPG.SS()); 
        
        SelenElem.Click(By.id("btnStep1-3Principal"));
         
        //new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        SelenElem.WaitForLoad(By.id("toggleShipping2"), 20);
        Thread.sleep(2000);
        SelenElem.Click(By.id("toggleShipping2"));
        SelenElem.TypeText(By.id("js_codigo_postal"), "06600");
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        SelenElem.WaitForLoad(By.id("99090073cac"), 10);
        SelenElem.Click(By.id("99090073cac"));
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        
        SelenElem.focus(By.id("99090028cac"));
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        //SelenElem.focus(By.id("checkout-paso2-renovaciones"));    
        SelenElem.Click(By.id("btnStep2-CAC-porta"));
        
        Thread.sleep(3000);
        //OPG.cargaAjax2();
        
        SelenElem.Click(By.xpath("//input[@placeholder='1']"));
        SelenElem.TypeText(By.xpath("//input[@placeholder='1']"), "1234");
        reporte.AddImageToReport("Sección Cambiate a Movistar del Checkout", OPG.SS());  
        SelenElem.Click(By.id("btnStep3"));
        
        OPG.cargaAjax2();
        
        SelenElem.Click(By.id("validaContrato"));
        reporte.AddImageToReport("Sección Contrato del Checkout", OPG.ScreenShotElement());
        /*SelenElem.Click(By.id("ver_contrato_portabilidad_pdf"));
        Thread.sleep(4000);
        reporte.AddImageToReport("Sección Contrato del Checkout", OPG.ScreenShotAll());*/
        Thread.sleep(2000);
        SelenElem.Click(By.id("btnStep4_resume"));
        
        new FlapPorta(reporte,OPG,SelenElem,WebDrive);
        
        SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/section[3]/dl/dd/p"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/footer/a[1]"));
        reporte.AddImageToReport("", OPG.SS());
        //*/
    
    }
    
    
    
}
