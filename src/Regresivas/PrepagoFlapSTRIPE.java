
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.OperacionesGenerales;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class PrepagoFlapSTRIPE {
    

    public PrepagoFlapSTRIPE(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive ,String Url,String Caso) throws InterruptedException{
        
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        reporte.AddImageToReport("Se selecciona el telefono", OPG.ScreenShotElementFocus(By.xpath("//a[@href='" +Url+ "iphone-xr-lte-amarillo-64gb-apple.html']")));
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + CargaVariables.LeerCSV(0,"Prepago Flap (Stripe)",Caso) + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        reporte.AddImageToReport("Ver detalle de Smartphone", OPG.SS());
        
        SelenElem.focus(By.id("solo-smartphone"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//p[.='Comprar']"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.xpath("//p[.='Comprar']"));
        
        /*
        WebDrive.WebDriver.navigate().to(Url + "iphone-xr-lte-amarillo-64gb-apple.html");
        SelenElem.waitForLoadPage();
        SelenElem.Click(By.id("btn-buy-three"));
        SelenElem.waitForLoadPage();*/
        
        
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        //SelenElem.focus(By.id("continueToCheckout"));
        SelenElem.waitForLoadPage();
        SelenElem.focus(By.id("continueToCheckout"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        
        SelenElem.TypeText(By.id("valid-nombre"), CargaVariables.LeerCSV(1,"Prepago Flap (Stripe)",Caso));
        SelenElem.TypeText(By.id("valid-appelido-p"), CargaVariables.LeerCSV(2,"Prepago Flap (Stripe)",Caso));
        SelenElem.TypeText(By.id("valid-appelido-m"), CargaVariables.LeerCSV(3,"Prepago Flap (Stripe)",Caso));
        SelenElem.TypeText(By.id("valid-email"), CargaVariables.LeerCSV(4,"Prepago Flap (Stripe)",Caso));
        SelenElem.TypeText(By.id("telefono-contacto"), CargaVariables.LeerCSV(5,"Prepago Flap (Stripe)",Caso));
        SelenElem.Click(By.id("checkPrivacyOne"));      
        reporte.AddImageToReport("Seccion de datos del checkout", OPG.SS());
        SelenElem.Click(By.xpath("//button[@title='Continuar']"));
        SelenElem.Click(By.id("inv-mail2"));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("cp"), "06600");
        SelenElem.Click(By.xpath("//*[@id=\"longlat\"]"));
        OPG.cargaAjax2();
        SelenElem.Click(By.id("99090073cac"));
        reporte.AddImageToReport("Seleccionar opcion de envio", OPG.SS());
        
        SelenElem.focus(By.id("js-continuar-metodos-envio"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("card-name"), "Vianey Macias Nieves");
        
        SelenElem.SwitchToFrameByTargetElement(By.xpath("//*[@id=\"root\"]/form/span[2]/div/div[2]/span/input"));        
        SelenElem.TypeText(By.xpath("//*[@id=\"root\"]/form/span[2]/div/div[2]/span/input"), CargaVariables.LeerCSV(6,"Prepago Flap (Stripe)",Caso));
        SelenElem.BacktoMainFrame();
        
        
        SelenElem.SwitchToFrame(By.xpath("//*[@id=\"stripe-payments-card-expiry\"]/div/iframe"));
        SelenElem.TypeText(By.xpath("//*[@id=\"root\"]/form/span[2]/span/input"), CargaVariables.LeerCSV(7,"Prepago Flap (Stripe)",Caso));
        SelenElem.BacktoMainFrame();
        
        SelenElem.SwitchToFrame(By.xpath("//*[@id=\"stripe-payments-card-cvc\"]/div/iframe"));
        SelenElem.TypeText(By.xpath("//*[@id=\"root\"]/form/span[2]/span/input"), CargaVariables.LeerCSV(8,"Prepago Flap (Stripe)",Caso));
        SelenElem.BacktoMainFrame();
        
        OPG.cargaAjax2();
        
        SelenElem.Click(By.id("js-stripe-card"));
        
        SelenElem.WaitFor(By.id("selecPlan"));
        SelenElem.SelectOptionByValue(By.id("selecPlan"), "3");
        reporte.AddImageToReport("Sección “Medios de pago” con stripe", OPG.SS());
        
        SelenElem.Click(By.id("continuar-checkout-out-validation"));
        //OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.xpath("//*[@id=\"contenedor-card-mobile\"]/div[2]/div[2]/div[1]"), 50);
        reporte.AddImageToReport("Pagina de pago realizada con exito", OPG.SS());
        reporte.AddImageToReport("Pagina de pago realizada con exito", OPG.ScreenShotElementFocus(By.xpath("//p[.='Datos de envío']")));
        
        
    }
    
    
    
}
