
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class SoloTerminalStripe {

     public SoloTerminalStripe (Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
        
       String dn = CargaVariables.LeerCSV(0, "Solo Terminal – Stripe", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Solo Terminal – Stripe", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Solo Terminal – Stripe", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Solo Terminal – Stripe", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Solo Terminal – Stripe", Caso);
        String correo = CargaVariables.LeerCSV(5, "Solo Terminal – Stripe", Caso);
        String tel = CargaVariables.LeerCSV(6, "Solo Terminal – Stripe", Caso);
        String calle = CargaVariables.LeerCSV(8, "Solo Terminal – Stripe", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Solo Terminal – Stripe", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Solo Terminal – Stripe", Caso);
        String cp = CargaVariables.LeerCSV(7, "Solo Terminal – Stripe", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Solo Terminal – Stripe", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Solo Terminal – Stripe", Caso);
        String ine = CargaVariables.LeerCSV(14, "Solo Terminal – Stripe", Caso);
        String rfcc = CargaVariables.LeerCSV(15, "Solo Terminal – Stripe", Caso);
        String tarjeta = CargaVariables.LeerCSV(17, "Solo Terminal – Stripe", Caso);
        String vencimiento = CargaVariables.LeerCSV(18, "Solo Terminal – Stripe", Caso);
        String cvv = CargaVariables.LeerCSV(19, "Solo Terminal – Stripe", Caso);
        String telefono = CargaVariables.LeerCSV(16,"Solo Terminal – Stripe",Caso);
        
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        reporte.AddImageToReport("Se selecciona el telefono", OPG.ScreenShotElementFocus(By.xpath("//a[@href='" + Url + "iphone-xr-lte-amarillo-64gb-apple.html']")));
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + telefono + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        Thread.sleep(3000);
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Ver detalle de Smartphone", OPG.SS());
        SelenElem.focus(By.xpath("//p[.='Precio final del equipo']"));
        reporte.AddImageToReport("", OPG.SS());
        
        Thread.sleep(2000);
        SelenElem.Click(By.xpath("//p[.='Comprar']"));
        reporte.AddImageToReport("Resumen de compra ",OPG.SS() );
        SelenElem.focus(By.xpath("//p[.='¿Tienes una promoción?']"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 5);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        SelenElem.Click(By.id("checkPrivacyOne"));
        
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("recibeFactura"));
        
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Arrendamiento");
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.focus(By.id("btn_continuar"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("btn_continuar"));
        OPG.cargaAjax2();
        
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        OPG.cargaAjax2();
        
         SelenElem.Find(By.id("form-checkout__cardholderName")).clear();
        SelenElem.TypeText(By.id("form-checkout__cardholderName"), "APRO APRO");
        
        /*SelenElem.Click(By.id("form-checkout__expirationDate"));
        SelenElem.TypeText(By.id("form-checkout__cardNumber"), tarjeta);
        SelenElem.Click(By.id("form-checkout__expirationDate"));
        SelenElem.TypeText(By.id("form-checkout__expirationDate"), vencimiento);
        SelenElem.Click(By.id("form-checkout__securityCode"));
        SelenElem.TypeText(By.id("form-checkout__securityCode"), cvv);
        SelenElem.Click(By.id("form-checkout__expirationDate"));
        SelenElem.TypeText(By.id("form-checkout__expirationDate"), vencimiento);
        SelenElem.Click(By.id("labelSameBillingAddress"));
        reporte.AddImageToReport("Sección “Medios de pago” pagar con STRIPE", OPG.SS());
        
        SelenElem.SelectOption(By.id("form-checkout__installments"), "12 mensualidades de $ 666.67 ($ 8,000.00)");
        Thread.sleep(1000);
        reporte.AddImageToReport("Sección “Medios de pago” pagar con STRIPE", OPG.SS());
        
        SelenElem.focus(By.id("form-checkout__submit"));
        SelenElem.Click(By.id("form-checkout__submit"));
        reporte.AddImageToReport("Sección “Medios de pago” pagar con STRIPE", OPG.SS());
        
        SelenElem.WaitForLoad(By.id("card-name"), 20);
        SelenElem.Click(By.id("card-name"));
        SelenElem.TypeText(By.id("card-name"), "Vianey Macias Nieves");
        
       */
        //usar iframes
        
        SelenElem.SwitchToFrameByTargetElement(By.xpath("//*[@id=\"root\"]/form/span[2]/div/div[2]/span/input"));        
        SelenElem.TypeText(By.xpath("//*[@id=\"root\"]/form/span[2]/div/div[2]/span/input"),tarjeta);
        SelenElem.BacktoMainFrame();
        
        
        SelenElem.SwitchToFrame(By.xpath("//*[@id=\"stripe-payments-card-expiry\"]/div/iframe"));
        SelenElem.TypeText(By.xpath("//*[@id=\"root\"]/form/span[2]/span/input"), vencimiento);
        SelenElem.BacktoMainFrame();
        
        SelenElem.SwitchToFrame(By.xpath("//*[@id=\"stripe-payments-card-cvc\"]/div/iframe"));
        SelenElem.TypeText(By.xpath("//*[@id=\"root\"]/form/span[2]/span/input"), cvv);
        SelenElem.BacktoMainFrame();
        
        Thread.sleep(500);
        reporte.AddImageToReport("Sección “Medios de pago” pagar con stripe", OPG.SS());
        
        SelenElem.Click(By.id("labelSameBillingAddress"));
        OPG.cargaAjax2();
        SelenElem.focus(By.id("continuar-checkout-out-validation"));
        reporte.AddImageToReport("Sección “Medios de pago” pagar con STRIPE", OPG.SS());
        SelenElem.Click(By.id("continuar-checkout-out-validation"));
      
        //OPG.cargaAjax2();
        
        
        SelenElem.WaitForLoad(By.xpath("//*[@id=\"contenedor-card-desktop\"]/div[2]/div[1]/div[1]"), 30);
        reporte.AddImageToReport("Página de pago realizado con éxito", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"contenedor-card-desktop\"]/div[2]/div[2]/div[1]/p"));
        reporte.AddImageToReport("Página de pago realizado con éxito", OPG.SS());
       
        
        //*/

     }
    
}
