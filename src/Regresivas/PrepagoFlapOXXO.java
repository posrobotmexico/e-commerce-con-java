
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.OpcionesEnvio;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class PrepagoFlapOXXO {

     public PrepagoFlapOXXO(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive ,String Url,String Caso) throws InterruptedException, IOException{
         
       String telefono = CargaVariables.LeerCSV(0,"Prepago Flap (Oxxo)",Caso);
        
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        SelenElem.Click(By.id("boton_smartphone")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        Thread.sleep(1000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        reporte.AddImageToReport("Se selecciona el telefono", OPG.ScreenShotElementFocus(By.xpath("//a[@href='" + Url + "iphone-xr-lte-amarillo-64gb-apple.html']")));
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + telefono + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        Thread.sleep(3000);
        
        /*reporte.AddImageToReport("Ver detalle de Smartphone", OPG.SS());
        SelenElem.focus(By.id("solo-smartphone"));
        reporte.AddImageToReport("", OPG.SS());
        */
        
        reporte.AddImageToReport("Ver detalle de Smartphone", OPG.SS());
        SelenElem.focus(By.xpath("//p[.='Precio final del equipo']"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//p[.='Comprar']"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.xpath("//p[.='Comprar']"));
        
        SelenElem.waitForLoadPage();
        
        reporte.AddImageToReport("Resumen de compra ",OPG.SS() );
        SelenElem.focus(By.xpath("//p[.='¿Tienes una promoción?']"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("continueToCheckout"));
        
//        SelenElem.Click(By.id("continueToCheckout"));
        SelenElem.TypeText(By.id("valid-nombre"), CargaVariables.LeerCSV(1,"Prepago Flap (Oxxo)",Caso));
        SelenElem.TypeText(By.id("valid-appelido-p"), CargaVariables.LeerCSV(2,"Prepago Flap (Oxxo)",Caso));
        SelenElem.TypeText(By.id("valid-appelido-m"), CargaVariables.LeerCSV(3,"Prepago Flap (Oxxo)",Caso));
        SelenElem.TypeText(By.id("valid-email"), CargaVariables.LeerCSV(4,"Prepago Flap (Oxxo)",Caso));
        SelenElem.TypeText(By.id("telefono-contacto"), CargaVariables.LeerCSV(5,"Prepago Flap (Oxxo)",Caso));
        SelenElem.Click(By.id("recibeFactura")); 
        SelenElem.WaitForLoad(By.id("valida-rfc"), 3);
        SelenElem.TypeText(By.id("valida-rfc"), CargaVariables.LeerCSV(6,"Prepago Flap (Oxxo)",Caso));
        SelenElem.TypeText(By.id("codigo-postal-valida"), CargaVariables.LeerCSV(7,"Prepago Flap (Oxxo)",Caso));
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        reporte.AddImageToReport("Seccion de datos del checkout", OPG.SS());
        SelenElem.TypeText(By.id("calle-domicilio"), CargaVariables.LeerCSV(8,"Prepago Flap (Oxxo)",Caso));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("numero-domicilio"), CargaVariables.LeerCSV(9,"Prepago Flap (Oxxo)",Caso));
        SelenElem.SelectOption(By.id("colonia"), CargaVariables.LeerCSV(11,"Prepago Flap (Oxxo)",Caso));
        SelenElem.Click(By.id("checkPrivacyOne"));      
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//button[@title='Continuar']"));
        
        /*SelenElem.Click(By.id("inv-mail2"));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("cp"), "06600");
        SelenElem.Click(By.xpath("//*[@id=\"longlat\"]"));
        OPG.cargaAjax2();
        SelenElem.Click(By.id("99090073cac"));
        reporte.AddImageToReport("Seleccionar opcion de envio", OPG.SS());
        */
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        SelenElem.focus(By.id("js-continuar-metodos-envio"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        OPG.cargaAjax2();
        SelenElem.Click(By.id("js-stripe-payment-method-oxxo"));
        reporte.AddImageToReport("Seccion medios de pago en oxxo", OPG.SS());
        reporte.AddImageToReport("", OPG.ScreenShotElementFocus(By.id("continuar-checkout-out-validation-oxxo")));
        SelenElem.Click(By.id("continuar-checkout-out-validation-oxxo"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.xpath("//p[.='Datos de envío']"), 40);
        reporte.AddImageToReport("Página de pago realizado con éxito  ", OPG.SS());
        
        SelenElem.SwitchToFrame(By.id("recibOXXO"));
        try{
            SelenElem.focus(By.xpath("//button[.='Imprimir']"));
        }catch(Exception ex){}
        
        SelenElem.BacktoMainFrame();
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/div[3]/div[2]/div[2]/div[1]/p"));
        reporte.AddImageToReport("", OPG.SS());
        reporte.AddImageToReport("", OPG.ScreenShotElementFocus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/div[3]/div[2]/div[3]/ul/li[1]")));
        
    }
    
}
