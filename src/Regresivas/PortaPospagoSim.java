
package Regresivas;

import Core.DeviceActions;
import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.OpcionesEnvio;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

/**
 *
 * @author LAPTOP-JORGE
 */
public class PortaPospagoSim {
    public PortaPospagoSim(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,DeviceActions da,String caso) throws InterruptedException, IOException
    {
        reporte.AddImageToReport("Iniciamos desde la pagina principal", OPG.ScreenShotElement());
        SelenElem.waitForLoadPage();
        
        Point p = da.getMiddleElement(By.xpath("//android.view.View[3]/android.view.View/android.view.View"));
        for(int i=0;i<6;i++)
            da.slide(p.x, p.y, p.x-300, p.y);
        
        //SelenElem.Click(By.linkText("Cámbiate a Movistar"));
        OPG.SelectMenu("Cámbiate a Movistar");
        
        SelenElem.WaitFor(By.id("sc2-dn"));
        SelenElem.TypeText(By.id("sc2-dn"), CargaVariables.LeerCSV(0,"Portabilidad Pospago Sim",caso));
        da.hideKeyBoard();
        reporte.AddImageToReport("Seleccionamos el flujo cambiate a Movistar", OPG.ScreenShotElement());
        SelenElem.Click(By.id("sc2-goPortaStep2"));
        SelenElem.Click(By.id("option_plan"));
        Thread.sleep(1500);
        reporte.AddImageToReport("Seleccionamos quiero contratar un plan", OPG.ScreenShotElement());
        SelenElem.Click(By.id("sc2-goToPortability"));
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("plan1"), 15);
        SelenElem.focus(By.id("plan1"));
        SelenElem.Click(By.id("plan1"));
        reporte.AddImageToReport("Seleccionamos sin smartphone y damos click en continuar", OPG.ScreenShotElement());
        SelenElem.Click(By.id("gtm-plans-continue-button"));
        
        SelenElem.WaitForLoad(By.className("mb-parrilla_card"), 10);
        List<WebElement> items = SelenElem.FindElementsByClassName("mb-parrilla_card");
        for(WebElement i : items)
        {
            SelenElem.focus(i);
            reporte.AddImageToReport("Vemos los planes", OPG.ScreenShotElement());
        }
        
        reporte.AddImageToReport("Elegimos el plan Movistar", OPG.ScreenShotElement()); // Hay que visualizar todos los planes
        SelenElem.Click(By.xpath("//button[.='Lo quiero']"));
        SelenElem.WaitForLoad(By.id("check-spotify"),5);
        SelenElem.focus(By.id("check-spotify"));
        da.slide("Down",1);
        reporte.AddImageToReport("Sin SVA", OPG.ScreenShotElement());
        SelenElem.Click(By.id("submitplanes"));
        
        SelenElem.WaitForLoad(By.linkText("Cambiar plan"), 5);
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementFocus(By.id("section-equipo")));
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementFocus(By.linkText("Cambiar plan")));
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementFocus(By.className("envios")));
        SelenElem.Click(By.xpath("//button[@title='Da clic aquí para ir al carrito de compra']"));
        SelenElem.WaitFor(By.id("continueToCheckout"));
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElement());
        SelenElem.Click(By.id("continueToCheckout"));
        SelenElem.ClickById(WebDrive.WebDriver,"continueToCheckout");
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-nombre"), 6);
        SelenElem.TypeText(By.id("valid-nombre"), CargaVariables.LeerCSV(1,"Portabilidad Pospago Sim",caso));
        //SelenElem.TypeText(By.id("valid-dapelido-p"), CargaVariables.LeerCSV(2,"Portabilidad Pospago Sim",caso));
        SelenElem.TypeText(By.id("valid-appelido-p"), CargaVariables.LeerCSV(2,"Portabilidad Pospago Sim",caso));
        //SelenElem.TypeText(By.id("valid-dapelido-m"), CargaVariables.LeerCSV(3,"Portabilidad Pospago Sim",caso));
        SelenElem.TypeText(By.id("valid-appelido-m"), CargaVariables.LeerCSV(3,"Portabilidad Pospago Sim",caso));
        SelenElem.TypeText(By.id("valid-email"), CargaVariables.LeerCSV(4,"Portabilidad Pospago Sim",caso));
        SelenElem.TypeText(By.id("telefono-contacto"), CargaVariables.LeerCSV(5,"Portabilidad Pospago Sim",caso));
        
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), "1999");
        SelenElem.SelectOption(By.className("ui-datepicker-month"), "Mar");
        SelenElem.Click(By.linkText("3"));
        OPG.cargaAjax2();
        //SelenElem.TypeText(By.id("valida-rfc"), CargaVariables.LeerCSV(6,"Portabilidad Pospago Sim",caso));
        SelenElem.TypeText(By.id("codigo-postal-valida"), CargaVariables.LeerCSV(7,"Portabilidad Pospago Sim",caso));
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");        
        SelenElem.TypeText(By.id("calle-domicilio"), CargaVariables.LeerCSV(8,"Portabilidad Pospago Sim",caso));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("numero-domicilio"), CargaVariables.LeerCSV(9,"Portabilidad Pospago Sim",caso));
        SelenElem.TypeText(By.id("valida-estado"), CargaVariables.LeerCSV(10,"Portabilidad Pospago Sim",caso));
        SelenElem.SelectOption(By.id("colonia"), CargaVariables.LeerCSV(11,"Portabilidad Pospago Sim",caso));
        SelenElem.TypeText(By.id("valid-ciudad"), CargaVariables.LeerCSV(12,"Portabilidad Pospago Sim",caso));
        SelenElem.TypeText(By.id("ciudad-valida"), CargaVariables.LeerCSV(13,"Portabilidad Pospago Sim",caso));
        SelenElem.TypeText(By.id("INE-IFE"), CargaVariables.LeerCSV(14,"Portabilidad Pospago Sim",caso));
        da.hideKeyBoard();
        SelenElem.Click(By.id("checkPrivacyOne"));
        //reporte.AddImageToReport("Datos de llenado", OPG.ScreenShotElementFocus(By.id("valid-dapelido-m")));
        reporte.AddImageToReport("Datos de llenado", OPG.ScreenShotElementFocus(By.id("valid-appelido-m")));
        reporte.AddImageToReport("Datos de llenado", OPG.ScreenShotElementFocus(By.id("valida-rfc")));
        reporte.AddImageToReport("Datos de llenado", OPG.ScreenShotElementFocus(By.id("colonia")));
        reporte.AddImageToReport("Datos de llenado", OPG.ScreenShotElementFocus(By.id("INE-IFE")));
        SelenElem.Click(By.id("paso1-pospago-porta"));
        SelenElem.ClickById(WebDrive.WebDriver,"paso1-pospago-porta");
        
        OPG.cargaAjax2();
        /*SelenElem.WaitForLoad(By.id("inv-mail2"), 6);
        SelenElem.Click(By.id("inv-mail2"));
        SelenElem.TypeText(By.id("cp"), "22000");
        OPG.cargaAjax2();
        SelenElem.Click(By.id("longlat"));
        
        SelenElem.WaitForLoad(By.className("geo-cac-elocker-option__place-title"),6);
        SelenElem.focus(By.className("geo-cac-elocker-option__place-title"));
        SelenElem.Click(By.className("geo-cac-elocker-option__place-title"));
        reporte.AddImageToReport("Seleccionamos el CAC", OPG.ScreenShotElement()); */
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        SelenElem.Click(By.id("checkout-paso2-renovaciones"));
        
        SelenElem.TypeText(By.id("valid-nip"), "1234");
        SelenElem.TypeText(By.id("valid-curp"), CargaVariables.LeerCSV(15,"Portabilidad Pospago Sim",caso));
        SelenElem.Click(By.id("dateToPorta_datepicker"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("ui-datepicker-div"),10);
        SelenElem.Click(By.className("ui-datepicker-days-cell-over"));
        reporte.AddImageToReport("Llenamos el pin y curp de portabilidad", OPG.ScreenShotElement());        
        SelenElem.Click(By.id("checkout-paso4-porta-pre"));
        
        SelenElem.Click(By.id("checkTeminosCondiciones"));
        SelenElem.Click(By.id("checkConsentimiento"));
        reporte.AddImageToReport("Aceptamos la sección del contrato", OPG.ScreenShotElement());
        SelenElem.Click(By.id("realizar-pago-2"));
        
        new Flap(reporte,OPG,SelenElem,WebDrive);
        
        SelenElem.WaitForLoad(By.className("descripcion-orden-terminal-card-texto"),30);
        SelenElem.waitForLoadPage();                
        da.slide("Down",2);        
        reporte.AddImageToReport("Vemos el detalle de la compra", OPG.ScreenShotElement());
        da.slide("Down",2);        
        reporte.AddImageToReport("Vemos el detalle de datos personales", OPG.ScreenShotElement());
        da.slide("Down",3);
        reporte.AddImageToReport("Vemos el detalle del envio", OPG.ScreenShotElement());
    }
}
