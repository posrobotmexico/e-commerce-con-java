
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.OperacionesGenerales;
import Operaciones.OperacionesRecargas;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
  public class Recargas {
    private String resultado;
    public Recargas(Report reporte,OperacionesGenerales OPG,OperacionesRecargas OpRecargas,String DN,String Monto,WebAction SelenElem,WebDriv WebDrive) throws InterruptedException, IOException
    {
        resultado = "Prueba fallida";
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement());
        //OpRecargas.SeleccionaFlujoRecargas();
        SelenElem.WaitForLoad(By.id("menu_list"), 15);
        OPG.SelectMenu("Recarga aquí");
        
        reporte.AddImageToReport("Seleccionar el flujo de recargas", OPG.ScreenShotElement());
        OpRecargas.IngresaNumeroRecarga(DN);
        reporte.AddImageToReport("Ingresamos el numero", OPG.ScreenShotElement());  
        OpRecargas.ContinuaAlMontodeRecarga();
        OpRecargas.SeleccionaPaqueteRecarga(Monto);
        Thread.sleep(500);
        OpRecargas.enfocaPaqueteRecarga(Monto);
        Thread.sleep(500);
        reporte.AddImageToReport("Seleccionamos el monto de recarga de $"+Monto, OPG.ScreenShotElement());
        OpRecargas.EnfocaBotonMP();
        reporte.AddImageToReport("Navegamos a la parte de Mercado Pago y damos click", OPG.ScreenShotElement());
        OpRecargas.SeleccionaPagoMP();
       
        OpRecargas.SeleccionaNuevaTarjeta();
        reporte.AddImageToReport("Seleccionamos nueva tarjeta", OPG.ScreenShotElement());
        OpRecargas.RellenaDatosRecargaMP();
        reporte.AddImageToReport("Llenamos los datos de la tarjeta", OPG.ScreenShotElement());
        OpRecargas.EnviarRecargaMP();
        OpRecargas.RellenaDatosCorreoRecargaMP();
        reporte.AddImageToReport("Llenamos el correo y damos click en pagar", OPG.ScreenShotElement());
        OpRecargas.PagarRecargaMP();

        if(SelenElem.FindElementByID(WebDrive.WebDriver, "auto_return_row") != null)
        {
            reporte.AddImageToReport("Finalizamos", OPG.ScreenShotElement()); 
            resultado = "Prueba exitosa";
        }
    }

    public String getResultado() {
        return resultado;
    }
    
    // Unused
    /*case "Recarga Movistar (Paquetes) - Mercado Pago Pro (Sin login)":
    case "Recarga Movistar (Montos) - Mercado Pago Pro (Sin login)":
    case "Recarga Movistar (Paquetes) - Mercado Pago Pro (Con login)":
    case "Recarga Movistar (Montos) - Mercado Pago Pro (Con login)":
        DN = CargaVariables.LeerCSV(0,"Recargas",Caso.substring(0,Caso.length()-12));
        Monto = CargaVariables.LeerCSV(1,"Recargas",Caso.substring(0,Caso.length()-12));
        WebDrive.WebDriver.navigate().to("https://certi-tienda-movistar.canalesdigitales.com.mx/recarga-movistar");

        reporte.AddImageToReport("Pagina principal de recargas movistar", OPG.ScreenShotElement());
        recarga = new Recargas(WebDrive,SelenElem);
        recarga.IngresaNumeroRecargaMovistar(DN);
        reporte.AddImageToReport("Ingresamos el numero", OPG.ScreenShotElement());
        recarga.SeleccionaPaqueteRecargaMovistar(Monto);
        reporte.AddImageToReport("Seleccionamos el monto de recarga de $"+Monto, OPG.ScreenShotElement());
        reporte.AddImageToReport("Verificamos los datos antes de la compra", OPG.ScreenShotElement());
        if(Caso.contains("Recarga Movistar (Paquetes) - Mercado Pago Pro"))
            recarga.ConfirmarRecargaMovistarPaquete();
        else
            recarga.ConfirmarRecargaMovistarMonto();
        recarga.SeleccionaPagoMPMovistar();
        Thread.sleep(1000); // Hay que probar si se puede quitar al ejecutar de nuevo los reportes
        reporte.AddImageToReport("Navegamos a la parte de Mercado Pago y damos click en pagar", OPG.ScreenShotElement());
        recarga.DireccionaPagoMPMovistar();
        recarga.SeleccionaNuevaTarjeta();
        reporte.AddImageToReport("Seleccionamos nueva tarjeta", OPG.ScreenShotElement());
        recarga.RellenaDatosRecargaMP();
        reporte.AddImageToReport("Llenamos los datos de la tarjeta", OPG.ScreenShotElement());
        recarga.EnviarRecargaMP();
        recarga.RellenaDatosCorreoRecargaMP();
        reporte.AddImageToReport("Llenamos el correo y damos click en pagar", OPG.ScreenShotElement());
        recarga.PagarRecargaMP();

        if(SelenElem.FindElementByID(WebDrive.WebDriver, "auto_return_row") != null)
        {
            reporte.AddImageToReport("Finalizamos", OPG.ScreenShotElement()); 
            Resultado.setText("Exito. Prueba exitosa");
        }
        Resultado.setText("Exito. Prueba exitosa");                    
    break;*/
}
