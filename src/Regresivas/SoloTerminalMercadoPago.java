
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Core.DeviceActions;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class SoloTerminalMercadoPago {
    
    public SoloTerminalMercadoPago(){
        
    }
    
    public SoloTerminalMercadoPago (Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
    
       String dn = CargaVariables.LeerCSV(0, "Solo Terminal - Mercado Pago", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Solo Terminal - Mercado Pago", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Solo Terminal - Mercado Pago", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Solo Terminal - Mercado Pago", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Solo Terminal - Mercado Pago", Caso);
        String correo = CargaVariables.LeerCSV(5, "Solo Terminal - Mercado Pago", Caso);
        String tel = CargaVariables.LeerCSV(6, "Solo Terminal - Mercado Pago", Caso);
        String calle = CargaVariables.LeerCSV(8, "Solo Terminal - Mercado Pago", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Solo Terminal - Mercado Pago", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Solo Terminal - Mercado Pago", Caso);
        String cp = CargaVariables.LeerCSV(7, "Solo Terminal - Mercado Pago", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Solo Terminal - Mercado Pago", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Solo Terminal - Mercado Pago", Caso);
        String ine = CargaVariables.LeerCSV(14, "Solo Terminal - Mercado Pago", Caso);
        String rfcc = CargaVariables.LeerCSV(15, "Solo Terminal - Mercado Pago", Caso);
        String tarjeta = CargaVariables.LeerCSV(17, "Solo Terminal - Mercado Pago", Caso);
        String vencimiento = CargaVariables.LeerCSV(18, "Solo Terminal - Mercado Pago", Caso);
        String cvv = CargaVariables.LeerCSV(19, "Solo Terminal - Mercado Pago", Caso);
        
        WebDrive.WebDriver.navigate().to(Url);

        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        SelenElem.Click(By.id("boton_smartphone")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        Thread.sleep(1000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        
        reporte.AddImageToReport("Se selecciona el telefono", OPG.ScreenShotElementFocus(By.xpath("//a[@href='" + Url + "iphone-xr-lte-amarillo-64gb-apple.html?category=43']")));
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + CargaVariables.LeerCSV(16,"Solo Terminal - Mercado Pago",Caso) + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Ver detalle de Smartphone", OPG.SS());
        SelenElem.focus(By.xpath("//p[.='Precio final del equipo']"));
        reporte.AddImageToReport("", OPG.SS());
        
        Thread.sleep(2000);
        SelenElem.Click(By.xpath("//p[.='Comprar']"));
        reporte.AddImageToReport("Resumen de compra ",OPG.SS() );
        SelenElem.focus(By.xpath("//p[.='¿Tienes una promoción?']"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 5);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        SelenElem.Click(By.id("checkPrivacyOne"));
        
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("recibeFactura"));
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.focus(By.id("btn_continuar"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("btn_continuar"));
        OPG.cargaAjax2();
        
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        OPG.cargaAjax2();
        
        SelenElem.Find(By.id("form-checkout__cardholderName")).clear();
        SelenElem.TypeText(By.id("form-checkout__cardholderName"), "APRO APRO");
        
        SelenElem.Click(By.id("form-checkout__expirationDate"));
        SelenElem.TypeText(By.id("form-checkout__cardNumber"), tarjeta);
        SelenElem.Click(By.id("form-checkout__expirationDate"));
        SelenElem.TypeText(By.id("form-checkout__expirationDate"), vencimiento);
        SelenElem.Click(By.id("form-checkout__securityCode"));
        SelenElem.TypeText(By.id("form-checkout__securityCode"), cvv);
        SelenElem.Click(By.id("form-checkout__expirationDate"));
        SelenElem.TypeText(By.id("form-checkout__expirationDate"), vencimiento);
        SelenElem.Click(By.id("labelSameBillingAddress"));
        reporte.AddImageToReport("Sección “Medios de pago” pagar con Mercado Pago", OPG.SS());
        
        SelenElem.SelectOption(By.id("form-checkout__installments"), "12 mensualidades de $ 666.67 ($ 8,000.00)");
        Thread.sleep(1000);
        reporte.AddImageToReport("Sección “Medios de pago” pagar con Mercado Pago", OPG.SS());
        
        SelenElem.focus(By.id("form-checkout__submit"));
        SelenElem.Click(By.id("form-checkout__submit"));
        reporte.AddImageToReport("Sección “Medios de pago” pagar con Mercado Pago", OPG.SS());
        
        
        //OPG.cargaAjax2();
        
        SelenElem.WaitForLoad(By.xpath("//*[@id=\"contenedor-card-desktop\"]/div[2]/div[1]/div[1]"), 30);
        reporte.AddImageToReport("Página de pago realizado con éxito", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"contenedor-card-desktop\"]/div[2]/div[2]/div[1]/p"));
        reporte.AddImageToReport("Página de pago realizado con éxito", OPG.SS());
       
        
        //*/

     }
    
    public void SoloTermialMercadoCupon(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, DeviceActions da,String Caso) throws InterruptedException, IOException
    {
        String ModTel = CargaVariables.LeerCSV(0, "Solo Terminal - Mercado Pago", Caso);
        String Cupon = CargaVariables.LeerCSV(1, "Solo Terminal - Mercado Pago", Caso);
        String Nombre = CargaVariables.LeerCSV(2, "Solo Terminal - Mercado Pago", Caso);
        String ApellPa = CargaVariables.LeerCSV(3, "Solo Terminal - Mercado Pago", Caso);
        String ApellMa = CargaVariables.LeerCSV(4, "Solo Terminal - Mercado Pago", Caso);
        String Correo = CargaVariables.LeerCSV(5, "Solo Terminal - Mercado Pago", Caso);
        String Telefono = CargaVariables.LeerCSV(6, "Solo Terminal - Mercado Pago", Caso);
        String TelefAdic = CargaVariables.LeerCSV(7, "Solo Terminal - Mercado Pago", Caso);
        String CodPostal = CargaVariables.LeerCSV(8, "Solo Terminal - Mercado Pago", Caso);
        String RFC = CargaVariables.LeerCSV(9, "Solo Terminal - Mercado Pago", Caso);
        String Calle = CargaVariables.LeerCSV(10, "Solo Terminal - Mercado Pago", Caso);
        String NumExt = CargaVariables.LeerCSV(11, "Solo Terminal - Mercado Pago", Caso);
        String NumInt = CargaVariables.LeerCSV(12, "Solo Terminal - Mercado Pago", Caso);
        String Colonia = CargaVariables.LeerCSV(13, "Solo Terminal - Mercado Pago", Caso);
        String Tarjeta = CargaVariables.LeerCSV(14, "Solo Terminal - Mercado Pago", Caso);
        String CVV = CargaVariables.LeerCSV(15, "Solo Terminal - Mercado Pago", Caso);
        String Vence = CargaVariables.LeerCSV(16, "Solo Terminal - Mercado Pago", Caso);

        WebDrive.WebDriver.navigate().to(Url);
        Thread.sleep(3000);
        reporte.AddImageToReport("Continuamos con la pagina principal", OPG.SS());
        SelenElem.Click(By.id("checkbox"));
        SelenElem.Click(By.id("boton_smartphone"));
        SelenElem.Click(By.xpath("//a[@title='Todas las marcas']"));
        
        reporte.AddImageToReport("Seleccionar el flujo de telefonos", OPG.SS());
        SelenElem.focus(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        reporte.AddImageToReport("Se procede a seleccionar el Smartphone", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Detalle del smartphone", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//button[@class='fondo-boton-comprar']"));
        
        SelenElem.Click(By.id("coupon_code"));
        SelenElem.TypeText(By.id("coupon_code"), Cupon);
        SelenElem.Click(By.xpath("//button[@value='Aplicar Descuento']"));
        Thread.sleep(2000);
        reporte.AddImageToReport("Despues de ingresar cupon", OPG.SS());
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 15);
        SelenElem.TypeText(By.id("valid-nombre"), Nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), ApellPa);
        SelenElem.TypeText(By.id("valid-appelido-m"), ApellMa);
        SelenElem.TypeText(By.id("valid-email"), Correo);
        SelenElem.TypeText(By.id("telefono-contacto"), Telefono);
        SelenElem.TypeText(By.id("additionalContactPhone"), TelefAdic);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del checkout", OPG.SS());
        
        SelenElem.Click(By.id("recibeFactura"));
        SelenElem.TypeText(By.id("valida-rfc"), RFC);
        SelenElem.TypeText(By.id("codigo-postal-valida"), CodPostal);
        SelenElem.TypeText(By.id("calle-domicilio"), Calle);
        SelenElem.TypeText(By.id("numero-domicilio"), NumExt);
        SelenElem.TypeText(By.id("numero-interior"), NumInt);
        Thread.sleep(2000);
        SelenElem.Click(By.id("colonia"));
        SelenElem.TypeText(By.id("colonia"), Colonia);
        SelenElem.Click(By.id("checkPrivacyOne"));
        reporte.AddImageToReport("", OPG.SS());
              
        SelenElem.Click(By.id("btn_continuar"));
        
        SelenElem.WaitForLoad(By.id("inv-mail1"), 20);
        reporte.AddImageToReport("Sección opciones envío del checkout", OPG.SS());
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        
        SelenElem.WaitForLoad(By.id("form-checkout__cardholderName"), 10);
        SelenElem.Find(By.id("form-checkout__cardholderName")).clear();
        SelenElem.TypeText(By.id("form-checkout__cardholderName"), "APRO APRO APRO");
        SelenElem.TypeText(By.id("form-checkout__cardNumber"), Tarjeta);
        SelenElem.TypeText(By.id("form-checkout__expirationDate"), Vence);
        SelenElem.TypeText(By.id("form-checkout__securityCode"), CVV);
        SelenElem.Click(By.id("sameBillingAddress"));
        da.hideKeyBoard();
        da.slide("Down", 1);
        reporte.AddImageToReport("Sección medios de pago", OPG.SS());
        
        SelenElem.Click(By.id("form-checkout__submit"));
        
        Thread.sleep(5000);
        reporte.AddImageToReport("Pago realizado con exito", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        
        
    }    

    public void SoloTerminalMercadoDescuento(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, DeviceActions da,String Caso) throws InterruptedException, IOException
    {
        
        String ModTel = CargaVariables.LeerCSV(0, "Solo Terminal - Mercado Pago", Caso);
        String Nombre = CargaVariables.LeerCSV(1, "Solo Terminal - Mercado Pago", Caso);
        String ApellPa = CargaVariables.LeerCSV(2, "Solo Terminal - Mercado Pago", Caso);
        String ApellMa = CargaVariables.LeerCSV(3, "Solo Terminal - Mercado Pago", Caso);
        String Correo = CargaVariables.LeerCSV(4, "Solo Terminal - Mercado Pago", Caso);
        String Telefono = CargaVariables.LeerCSV(5, "Solo Terminal - Mercado Pago", Caso);
        String TelefAdic = CargaVariables.LeerCSV(6, "Solo Terminal - Mercado Pago", Caso);
        String CodPostal = CargaVariables.LeerCSV(7, "Solo Terminal - Mercado Pago", Caso);
        String Tarjeta = CargaVariables.LeerCSV(8, "Solo Terminal - Mercado Pago", Caso);
        String CVV = CargaVariables.LeerCSV(9, "Solo Terminal - Mercado Pago", Caso);
        String Vence = CargaVariables.LeerCSV(10, "Solo Terminal - Mercado Pago", Caso);
        String Calle = CargaVariables.LeerCSV(11, "Solo Terminal - Mercado Pago", Caso);
        String Numero = CargaVariables.LeerCSV(12, "Solo Terminal - Mercado Pago", Caso);

        WebDrive.WebDriver.navigate().to(Url);
        Thread.sleep(3000);
        reporte.AddImageToReport("Continuamos con la pagina principal", OPG.SS());
        SelenElem.Click(By.id("checkbox"));
        SelenElem.Click(By.id("boton_smartphone"));
        SelenElem.Click(By.xpath("//a[@title='Todas las marcas']"));
        
        reporte.AddImageToReport("Seleccionar el flujo de telefonos", OPG.SS());
        SelenElem.focus(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        reporte.AddImageToReport("Se procede a seleccionar el Smartphone", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Detalle del smartphone", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//button[@class='fondo-boton-comprar']"));
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 15);
        SelenElem.TypeText(By.id("valid-nombre"), Nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), ApellPa);
        SelenElem.TypeText(By.id("valid-appelido-m"), ApellMa);
        SelenElem.TypeText(By.id("valid-email"), Correo);
        SelenElem.TypeText(By.id("telefono-contacto"), Telefono);
        SelenElem.TypeText(By.id("additionalContactPhone"), TelefAdic);
        da.hideKeyBoard();
        SelenElem.Click(By.id("checkPrivacyOne"));
        reporte.AddImageToReport("Sección tus datos del checkout", OPG.SS());
        
        SelenElem.Click(By.id("btn_continuar"));
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 15);
        SelenElem.Click(By.id("inv-mail2"));
        SelenElem.TypeText(By.id("js_codigo_postal"), CodPostal);
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        
        SelenElem.WaitForLoad(By.id("js_geo_option_cards"), 15);
        SelenElem.ClickButton(WebDrive.WebDriver, SelenElem, "ClassName", "geo-cac-elocker-option__place-title");
        reporte.AddImageToReport("Sección opciones de envío", OPG.SS());
        
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        
        SelenElem.WaitForLoad(By.id("form-checkout__cardholderName"), 10);
        SelenElem.Find(By.id("form-checkout__cardholderName")).clear();
        SelenElem.TypeText(By.id("form-checkout__cardholderName"), "APRO APRO APRO");
        SelenElem.TypeText(By.id("form-checkout__cardNumber"), Tarjeta);
        SelenElem.TypeText(By.id("form-checkout__expirationDate"), Vence);
        SelenElem.TypeText(By.id("form-checkout__securityCode"), CVV);
        
        SelenElem.TypeText(By.id("street_name"), Calle);
        SelenElem.TypeText(By.id("street_number"), Numero);
        SelenElem.TypeText(By.id("zip_code"), CodPostal);
        reporte.AddImageToReport("Sección medios de pago", OPG.SS());
        
        SelenElem.Click(By.id("form-checkout__submit"));
        
        Thread.sleep(5000);
        reporte.AddImageToReport("Pago realizado con exito", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        
        
    }
    
    public void SoloTerminalMercadoDescuentoCupon(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
        
        String ModTel = CargaVariables.LeerCSV(0, "Solo Terminal - Mercado Pago", Caso);
        String Cupon = CargaVariables.LeerCSV(1, "Solo Terminal - Mercado Pago", Caso);
        String Nombre = CargaVariables.LeerCSV(2, "Solo Terminal - Mercado Pago", Caso);
        String ApellPa = CargaVariables.LeerCSV(3, "Solo Terminal - Mercado Pago", Caso);
        String ApellMa = CargaVariables.LeerCSV(4, "Solo Terminal - Mercado Pago", Caso);
        String Correo = CargaVariables.LeerCSV(5, "Solo Terminal - Mercado Pago", Caso);
        String Telefono = CargaVariables.LeerCSV(6, "Solo Terminal - Mercado Pago", Caso);
        String TelefAdic = CargaVariables.LeerCSV(7, "Solo Terminal - Mercado Pago", Caso);
        String CodPostal = CargaVariables.LeerCSV(8, "Solo Terminal - Mercado Pago", Caso);
        String RFC = CargaVariables.LeerCSV(9, "Solo Terminal - Mercado Pago", Caso);
        String Calle = CargaVariables.LeerCSV(10, "Solo Terminal - Mercado Pago", Caso);
        String NumExt = CargaVariables.LeerCSV(11, "Solo Terminal - Mercado Pago", Caso);
        String NumInt = CargaVariables.LeerCSV(12, "Solo Terminal - Mercado Pago", Caso);
        String Colonia = CargaVariables.LeerCSV(13, "Solo Terminal - Mercado Pago", Caso);
        String Tarjeta = CargaVariables.LeerCSV(14, "Solo Terminal - Mercado Pago", Caso);
        String CVV = CargaVariables.LeerCSV(15, "Solo Terminal - Mercado Pago", Caso);
        String Vence = CargaVariables.LeerCSV(16, "Solo Terminal - Mercado Pago", Caso);
        
        WebDrive.WebDriver.navigate().to(Url);
        Thread.sleep(3000);
        reporte.AddImageToReport("Continuamos con la pagina principal", OPG.SS());
        SelenElem.Click(By.id("checkbox"));
        SelenElem.Click(By.id("boton_smartphone"));
        SelenElem.Click(By.xpath("//a[@title='Todas las marcas']"));
        
        reporte.AddImageToReport("Seleccionar el flujo de telefonos", OPG.SS());
        SelenElem.focus(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        reporte.AddImageToReport("Se procede a seleccionar el Smartphone", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Detalle del smartphone", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//button[@class='fondo-boton-comprar']"));
        
        SelenElem.Click(By.id("coupon_code"));
        SelenElem.TypeText(By.id("coupon_code"), Cupon);
        SelenElem.Click(By.xpath("//button[@value='Aplicar Descuento']"));
        Thread.sleep(2000);
        reporte.AddImageToReport("Despues de ingresar cupon", OPG.SS());
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 15);
        SelenElem.TypeText(By.id("valid-nombre"), Nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), ApellPa);
        SelenElem.TypeText(By.id("valid-appelido-m"), ApellMa);
        SelenElem.TypeText(By.id("valid-email"), Correo);
        SelenElem.TypeText(By.id("telefono-contacto"), Telefono);
        SelenElem.TypeText(By.id("additionalContactPhone"), TelefAdic);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del checkout", OPG.SS());
        
        SelenElem.Click(By.id("recibeFactura"));
        SelenElem.TypeText(By.id("valida-rfc"), RFC);
        SelenElem.TypeText(By.id("codigo-postal-valida"), CodPostal);
        SelenElem.TypeText(By.id("calle-domicilio"), Calle);
        SelenElem.TypeText(By.id("numero-domicilio"), NumExt);
        SelenElem.TypeText(By.id("numero-interior"), NumInt);
        Thread.sleep(2000);
        SelenElem.Click(By.id("colonia"));
        SelenElem.TypeText(By.id("colonia"), Colonia);
        SelenElem.Click(By.id("checkPrivacyOne"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("btn_continuar"));
            
        SelenElem.WaitForLoad(By.id("inv-mail1"), 20);
        reporte.AddImageToReport("Sección opciones envío del checkout", OPG.SS());
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        
        SelenElem.WaitForLoad(By.id("form-checkout__cardholderName"), 10);
        SelenElem.Find(By.id("form-checkout__cardholderName")).clear();
        SelenElem.TypeText(By.id("form-checkout__cardholderName"), "APRO APRO APRO");
        SelenElem.TypeText(By.id("form-checkout__cardNumber"), Tarjeta);
        SelenElem.TypeText(By.id("form-checkout__expirationDate"), Vence);
        SelenElem.TypeText(By.id("form-checkout__securityCode"), CVV);
        SelenElem.Click(By.id("sameBillingAddress"));
        da.hideKeyBoard();
        da.slide("Down", 1);
        reporte.AddImageToReport("Sección medios de pago", OPG.SS());
        
        SelenElem.Click(By.id("form-checkout__submit"));
        
        Thread.sleep(5000);
        reporte.AddImageToReport("Pago realizado con exito", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        
    }
    
    public void SoloTerminalMercadoPrecioCupon(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
        String ModTel = CargaVariables.LeerCSV(0, "Solo Terminal - Mercado Pago", Caso);
        String Cupon = CargaVariables.LeerCSV(1, "Solo Terminal - Mercado Pago", Caso);
        
        WebDrive.WebDriver.navigate().to(Url);
        Thread.sleep(3000);
        reporte.AddImageToReport("Continuamos con la pagina principal", OPG.SS());
        SelenElem.Click(By.id("checkbox"));
        SelenElem.Click(By.id("dropdown_smartphones"));
        SelenElem.Click(By.xpath("//a[@title='Todas las marcas']"));
        
        reporte.AddImageToReport("Seleccionar el flujo de telefonos", OPG.SS());
        SelenElem.focus(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        reporte.AddImageToReport("Se procede a seleccionar el Smartphone", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Detalle del smartphone", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//button[@class='fondo-boton-comprar']"));
        
        SelenElem.Click(By.id("coupon_code"));
        SelenElem.TypeText(By.id("coupon_code"), Cupon);
        SelenElem.Click(By.xpath("//button[@value='Aplicar Descuento']"));
        Thread.sleep(2000);
        reporte.AddImageToReport("Resumen de compra despues de ingresar cupon", OPG.SS());
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        Thread.sleep(5000);
        reporte.AddImageToReport("Malla de terminales", OPG.SS());
        
    }
    
    public void SoloTerminalMercadoPrecioDescuento(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
        
        String ModTel = CargaVariables.LeerCSV(0, "Solo Terminal - Mercado Pago", Caso);
        
        WebDrive.WebDriver.navigate().to(Url);
        Thread.sleep(3000);
        reporte.AddImageToReport("Continuamos con la pagina principal", OPG.SS());
        SelenElem.Click(By.id("checkbox"));
        SelenElem.Click(By.id("dropdown_smartphones"));
        SelenElem.Click(By.xpath("//a[@title='Todas las marcas']"));
        
        reporte.AddImageToReport("Seleccionar el flujo de telefonos", OPG.SS());
        SelenElem.focus(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        reporte.AddImageToReport("Se procede a seleccionar el Smartphone", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Detalle del smartphone", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//button[@class='fondo-boton-comprar']"));
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        Thread.sleep(5000);
        reporte.AddImageToReport("Malla de terminales", OPG.SS());
        
        
        
    }
        
    
    
}
