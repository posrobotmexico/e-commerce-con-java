
package Regresivas;

import Core.DeviceActions;
import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

/**
 *
 * @author LAPTOP-JORGE
 */
public class RecargaFlap {
    public RecargaFlap(){    
    }
    
    public void Montos(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String caso) throws InterruptedException, IOException
    {
        //SelenElem.WaitForLoad(By.linkText("Recargas"), 15);
        SelenElem.WaitForLoad(By.id("menu_list"), 15);
        
        reporte.AddImageToReport("Iniciamos desde la pagina principal", OPG.SS());
        //SelenElem.Click(By.linkText("Recargas"));
        OPG.SelectMenu("Recargas");
        WebDrive.WebDriver.navigate().to(Url + "recarga-en-linea");
        
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Seleccionamos el flujo de recargas", OPG.SS());
        SelenElem.waitForLoadPage();
        SelenElem.Click(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/section/h1"));
        Thread.sleep(5000);
        /*SelenElem.Click(By.xpath("/html/body/div[5]/header/div/div/div/div/div/div/ul/li[1]/a/picture/img"));
        SelenElem.WaitForLoad(By.linkText("Recargas"), 15);
        SelenElem.Click(By.linkText("Recargas"));
        Thread.sleep(5000);*/
        SelenElem.TypeText(By.id("codeMov"), CargaVariables.LeerCSV(0,"Recargas",caso)); // DN
        SelenElem.Click(By.id("codeMov"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección 1: ingresa numero", OPG.SS(By.id("codeMov")));
        da.slide("Up", 1);
        Thread.sleep(2000);
        SelenElem.Click(By.id("btn_fiststep"));
        Thread.sleep(5000);
        
        SelenElem.WaitForLoad(By.xpath("//span[@data-price='200']"), 12); // Ambos tienen de 200
        //reporte.AddImageToReport("Sección 2: selección del monto", OPG.SS());
        String monto = CargaVariables.LeerCSV(1,"Recargas",caso);
        reporte.AddImageToReport("Seleccionamos el monto de recarga de $"+monto, OPG.SS());
        
        
        //By dibujo = By.xpath("//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.webkit.WebView/android.view.View/android.view.View[2]/android.view.View[1]/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[2]/android.view.View/android.widget.ListView/android.view.View[5]/android.widget.Button");
        //da.press(3000, dibujo);
        //reporte.AddImageToReport("Seleccionamos el monto de recarga de $"+monto, OPG.SS());
        //da.press(3000, dibujo);
        
        
        SelenElem.Click(By.xpath("//span[@data-price='"+ monto +"']")); // Aquí poner el monto
        
        reporte.AddImageToReport("Seleccionamos el monto de recarga de $"+monto, OPG.SS());
        
        SelenElem.WaitForLoad(By.name("UserCredit"), 30);
        SelenElem.Click(By.name("UserCredit"));
        SelenElem.TypeText(By.name("UserCredit"), "Prueba robot");
        SelenElem.TypeText(By.id("NumCredit2"), "4111111111111111");
        SelenElem.TypeText(By.id("expiration"), "1222");
        SelenElem.TypeText(By.id("ccv"), "123");
        SelenElem.Click(By.id("terms"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección 3: ingresar tarjeta", OPG.SS());
        SelenElem.Click(By.id("btn_recargar"));
        
        SelenElem.WaitForLoad(By.id("rechargechosen"), 12);
        reporte.AddImageToReport("Confirma tus datos para recargar", OPG.SS());
        SelenElem.Click(By.id("recharge"));
        
        SelenElem.waitForLoadPage(); 
        SelenElem.WaitForLoad(By.className("card-ico__ok"), 15);
        da.slide("Down",3);        
        reporte.AddImageToReport("Vemos el detalle de la compra", OPG.ScreenShotElement());
    }
     public void montosWeb(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Caso) throws InterruptedException, IOException{
     
        //SelenElem.WaitForLoad(By.linkText("Recargas"), 15);
        SelenElem.WaitForLoad(By.id("menu_list"), 15);
        reporte.AddImageToReport("Iniciamos desde la pagina principal", OPG.SS());
        //SelenElem.Click(By.linkText("Recargas"));
        OPG.SelectMenu("Recarga aquí");
        
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Seleccionamos el flujo de recargas", OPG.SS());
        SelenElem.waitForLoadPage();
        //SelenElem.Click(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/section/h1"));
        SelenElem.WaitForLoad(By.id("codeMov"), 10);
        Thread.sleep(5000);
        /*SelenElem.Click(By.xpath("/html/body/div[5]/header/div/div/div/div/div/div/ul/li[1]/a/picture/img"));
        SelenElem.WaitForLoad(By.linkText("Recargas"), 15);
        SelenElem.Click(By.linkText("Recargas"));
        Thread.sleep(5000);*/
        
        SelenElem.TypeText(By.id("codeMov"), CargaVariables.LeerCSV(0,"Recargas",Caso)); // DN
        SelenElem.Click(By.id("codeMov"));
        
        reporte.AddImageToReport("Sección 1: ingresa numero", OPG.SS(By.id("codeMov")));
        Thread.sleep(10000);
        SelenElem.Click(By.id("btn_fiststep"));
        Thread.sleep(5000);
        
        
        SelenElem.WaitForLoad(By.xpath("//span[@data-price='200']"), 12); // Ambos tienen de 200
        //reporte.AddImageToReport("Sección 2: selección del monto", OPG.SS());
        String monto = CargaVariables.LeerCSV(1,"Recargas",Caso);
        
        By dibujo = By.xpath("//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.webkit.WebView/android.view.View/android.view.View[2]/android.view.View[1]/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[2]/android.view.View/android.widget.ListView/android.view.View[5]/android.widget.Button");
        
        SelenElem.Click(By.xpath("//span[@data-price='"+ monto +"']")); // Aquí poner el monto
        SelenElem.highLight(By.xpath("//span[@data-price='"+ monto +"']"));
        Thread.sleep(2000);
        reporte.AddImageToReport("Seleccionamos el monto de recarga de $"+monto, OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"paq_recargas\"]/li[6]/label"));
        reporte.AddImageToReport("", OPG.SS());
        
        //reporte.AddImageToReport("Seleccionamos el monto de recarga de $"+monto, OPG.SS());
        
        SelenElem.WaitForLoad(By.name("UserCredit"), 30);
        SelenElem.Click(By.name("UserCredit"));
        SelenElem.TypeText(By.name("UserCredit"), "Prueba robot");
        SelenElem.TypeText(By.id("NumCredit2"), "4111111111111111");
        SelenElem.TypeText(By.id("expiration"), "1222");
        SelenElem.TypeText(By.id("ccv"), "123");
        SelenElem.Click(By.id("terms"));
        reporte.AddImageToReport("Sección 3: ingresar tarjeta", OPG.SS());
        SelenElem.Click(By.id("btn_recargar"));
        
        SelenElem.WaitForLoad(By.id("rechargechosen"), 12);
        Thread.sleep(2000);
        reporte.AddImageToReport("Confirma tus datos para recargar", OPG.SS());
        SelenElem.Click(By.id("recharge"));
        
        SelenElem.waitForLoadPage(); 
        SelenElem.WaitForLoad(By.className("card-ico__ok"), 15);     
        reporte.AddImageToReport("Vemos el detalle de la compra", OPG.ScreenShotElement());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[2]/div/div[2]/article/div[2]/div[1]/div[2]/dl[2]"));
        reporte.AddImageToReport("", OPG.ScreenShotElement());
     
     }
     public void paquetesWeb(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Caso) throws InterruptedException, IOException{
     
        //SelenElem.WaitForLoad(By.linkText("Recargas"), 15);
        SelenElem.WaitForLoad(By.id("menu_list"), 15);
        reporte.AddImageToReport("Iniciamos desde la pagina principal", OPG.SS());
        //SelenElem.Click(By.linkText("Recargas"));
        OPG.SelectMenu("Recarga aquí");
        
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Seleccionamos el flujo de recargas", OPG.SS());
        SelenElem.waitForLoadPage();
        SelenElem.Click(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/section/h1"));
        Thread.sleep(5000);
        /*SelenElem.Click(By.xpath("/html/body/div[5]/header/div/div/div/div/div/div/ul/li[1]/a/picture/img"));
        SelenElem.WaitForLoad(By.linkText("Recargas"), 15);
        SelenElem.Click(By.linkText("Recargas"));
        Thread.sleep(5000);*/
        SelenElem.TypeText(By.id("codeMov"), CargaVariables.LeerCSV(0,"Recargas",Caso)); // DN
        SelenElem.Click(By.id("codeMov"));
        
        reporte.AddImageToReport("Sección 1: ingresa numero", OPG.SS(By.id("codeMov")));
        Thread.sleep(10000);
        SelenElem.Click(By.id("btn_fiststep"));
        Thread.sleep(5000);
        
        SelenElem.WaitForLoad(By.xpath("//span[@data-price='200']"), 12); // Ambos tienen de 200
        //reporte.AddImageToReport("Sección 2: selección del monto", OPG.SS());
        String monto = CargaVariables.LeerCSV(1,"Recargas",Caso);
        
        By dibujo = By.xpath("//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.webkit.WebView/android.view.View/android.view.View[2]/android.view.View[1]/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[2]/android.view.View/android.widget.ListView/android.view.View[5]/android.widget.Button");
        
        SelenElem.Click(By.xpath("//span[@data-price='"+ monto +"']")); // Aquí poner el monto
        SelenElem.highLight(By.xpath("//span[@data-price='"+ monto +"']")); // Aquí poner el monto
        Thread.sleep(2000);
        reporte.AddImageToReport("Seleccionamos el paquete de $"+monto, OPG.SS());
        SelenElem.focus(By.xpath("//span[@data-price='"+ monto +"']"));
        reporte.AddImageToReport("", OPG.SS());
        
        //reporte.AddImageToReport("Seleccionamos el monto de recarga de $"+monto, OPG.SS());
        
        SelenElem.WaitForLoad(By.name("UserCredit"), 30);
        SelenElem.Click(By.name("UserCredit"));
        SelenElem.TypeText(By.name("UserCredit"), "Prueba robot");
        SelenElem.TypeText(By.id("NumCredit2"), "4111111111111111");
        SelenElem.TypeText(By.id("expiration"), "1222");
        SelenElem.TypeText(By.id("ccv"), "123");
        SelenElem.Click(By.id("terms"));
        reporte.AddImageToReport("Sección 3: ingresar tarjeta", OPG.SS());
        SelenElem.Click(By.id("btn_recargar"));
        
        SelenElem.WaitForLoad(By.id("rechargechosen"), 12);
        Thread.sleep(2000);
        reporte.AddImageToReport("Confirma tus datos para recargar", OPG.SS());
        SelenElem.Click(By.id("recharge"));
        
        SelenElem.waitForLoadPage(); 
        SelenElem.WaitForLoad(By.className("card-ico__ok"), 15);     
        reporte.AddImageToReport(" Success", OPG.ScreenShotElement());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[2]/div/div[2]/article/div[2]/div[1]/div[2]/dl[2]"));
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[2]/div/div[2]/article/div[2]/div[1]/div[2]/dl[2]"));
        reporte.AddImageToReport("", OPG.ScreenShotElement());
     
     }
     
    public void Paquetes(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String caso) throws InterruptedException, IOException
    {
        //SelenElem.WaitForLoad(By.linkText("Recargas"), 15);
        SelenElem.WaitForLoad(By.id("menu_list"), 15);
        reporte.AddImageToReport("Iniciamos desde la pagina principal", OPG.SS());
        //SelenElem.Click(By.linkText("Recargas"));
        OPG.SelectMenu("Recargas");
        WebDrive.WebDriver.navigate().to(Url + "recarga-en-linea");
        
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Seleccionamos el flujo de recargas", OPG.SS());
        SelenElem.TypeText(By.id("codeMov"), CargaVariables.LeerCSV(0,"Recargas",caso)); // DN
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección 1: ingresa numero", OPG.SS(By.id("codeMov")));
        da.slide("Up", 1);
        Thread.sleep(2000);
        SelenElem.Click(By.id("btn_fiststep"));
        Thread.sleep(5000);
        SelenElem.WaitForLoad(By.xpath("//span[@data-price='200']"), 12); // Ambos tienen de 200
        String monto = CargaVariables.LeerCSV(1,"Recargas",caso);
        reporte.AddImageToReport("Seleccionamos el monto de recarga de $"+monto, OPG.SS());
        Thread.sleep(1000);
        SelenElem.Click(By.xpath("//span[@data-price='"+ monto +"']")); // Aquí poner el monto
        reporte.AddImageToReport("Seleccionamos el monto de recarga de $"+monto, OPG.SS());
        
        SelenElem.WaitForLoad(By.id("UserCredit"), 12);
        SelenElem.TypeText(By.id("UserCredit"), "Prueba robot");
        SelenElem.TypeText(By.id("NumCredit2"), "4111111111111111");
        SelenElem.TypeText(By.id("expiration"), "1222");
        SelenElem.TypeText(By.id("ccv"), "123");
        SelenElem.Click(By.id("terms"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección 3: ingresar tarjeta", OPG.SS());
        SelenElem.Click(By.id("btn_recargar"));
        
        SelenElem.WaitForLoad(By.id("rechargechosen"), 12);
        reporte.AddImageToReport("Confirma tus datos para recargar", OPG.SS());
        SelenElem.Click(By.id("recharge"));
        
        SelenElem.waitForLoadPage(); 
        SelenElem.WaitForLoad(By.className("card-ico__ok"), 15);
        da.slide("Down",3);        
        reporte.AddImageToReport("Vemos el detalle de la compra", OPG.ScreenShotElement());
    
    }
}
