/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Regresivas;

import Core.DeviceActions;
import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.OpcionesEnvio;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

/**
 *
 * @author LAPTOP-JORGE
 */
public class Migraciones {
    public Migraciones(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,DeviceActions da,String caso) throws InterruptedException, IOException
    {
        reporte.AddImageToReport("Iniciamos desde la pagina principal", OPG.ScreenShotElement());
        SelenElem.waitForLoadPage();
        
        Point p = da.getMiddleElement(By.xpath("//android.view.View[3]/android.view.View/android.view.View"));
        for(int i=0;i<8;i++)
            da.slide(p.x, p.y, p.x-300, p.y);
        
        //SelenElem.Click(By.linkText("Cámbiate a un plan"));
        OPG.SelectMenu("Cámbiate a un plan");
        
        SelenElem.WaitFor(By.id("sc3-dn"));
        SelenElem.TypeText(By.id("sc3-dn"), CargaVariables.LeerCSV(0,"Migraciones",caso));
        da.hideKeyBoard();
        reporte.AddImageToReport("Seleccionamos el flujo cambiate a Movistar", OPG.ScreenShotElement());
        SelenElem.Click(By.id("sc3-goMigraStep2"));
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        SelenElem.focus(By.id("firstCharacter"));
        reporte.AddImageToReport("Valida tu identidad", OPG.ScreenShotElement());
        SelenElem.Click(By.id("continue-btn"));
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("plan1"),10);
        SelenElem.Click(By.id("plan2"));
        SelenElem.focus(By.id("plan2"));
        reporte.AddImageToReport("Seleccionamos con smartphone y continuar.", OPG.ScreenShotElement());
        SelenElem.Click(By.linkText("Ver todos los equipos"));
        
        SelenElem.waitForLoadPage();
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + CargaVariables.LeerCSV(16,"Migraciones",caso) + "')]"));
        SelenElem.waitForLoadPage();
        Thread.sleep(2000);
        
        /*
        SelenElem.WaitForLoad(By.linkText("Ver detalle"), 10);
        SelenElem.Click(By.linkText("Ver detalle"));
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.linkText("Este no es mi número"), 10);

*/
        reporte.AddImageToReport("Ver detalle del smartphone", OPG.ScreenShotElementFocus(By.linkText("Este no es mi número")));
       // reporte.AddImageToReport("Ver detalle del smartphone", OPG.ScreenShotElementFocus(By.className("list-ico")));
       da.slide("Down", 2);
       reporte.AddImageToReport("Ver detalle del smartphone", OPG.SS());
        
       /*SelenElem.Click(By.id("btn-buy-one"));
        
        SelenElem.WaitForLoad(By.className("mb-parrilla_card"), 10);
        List<WebElement> items = SelenElem.FindElementsByClassName("mb-parrilla_card");
        for(WebElement i : items)
        {
            SelenElem.focus(i);
            reporte.AddImageToReport("Vemos los planes", OPG.ScreenShotElement());
        }
        
        //SelenElem.focus(By.id("checkplan2466"));
        SelenElem.Click(By.id("Plan-Video"));
        //SelenElem.Click(By.xpath("//button[.='Quiero este plan']"));
        reporte.AddImageToReport("Seleccionamos el plan y damos click en contratar ahora.", OPG.ScreenShotElement());
        //SelenElem.Click(By.xpath("//*[@id=\"js_setTabIndex0\"]/div[4]/div[4]/button"));
        
        SelenElem.WaitForLoad(By.id("spotify"), 10);
        SelenElem.focus(By.id("spotify"));
        reporte.AddImageToReport("Sin SVA.", OPG.ScreenShotElement());
        SelenElem.focus(By.id("proteccion"));
        reporte.AddImageToReport("Sin SVA.", OPG.ScreenShotElement());
        SelenElem.Click(By.id("submitplanes"));
        
        SelenElem.WaitForLoad(By.linkText("Eliminar"), 10);
        SelenElem.focus(By.linkText("Eliminar"));
        reporte.AddImageToReport("Resumen de la compra.", OPG.ScreenShotElement());
        SelenElem.Click(By.xpath("//button[.='Ver detalle de plan']"));
        SelenElem.focus(By.xpath("//dt[.='Cargo mensual']"));
        da.slide("Down",2);    
        reporte.AddImageToReport("Resumen de la compra.", OPG.ScreenShotElement());
        SelenElem.Click(By.xpath("//button[@title='Da clic aquí para ir al carrito de compra']"));
        
        SelenElem.WaitForLoad(By.id("btn_continue_check"), 5);
        SelenElem.Click(By.id("btn_continue_check"));*/
       SelenElem.Click(By.xpath("//*[@id=\"js_slider_mp_Migration\"]/div/div/div[3]/div/div/div/div"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Elegir plan Movistar", OPG.SS());
        SelenElem.Click(By.xpath("//*[@id=\"js_slider_mp_Migration_miniparrillas\"]/div/div/div[3]/div/div/div/div[1]/div[2]/div[7]/div"));
        
        Thread.sleep(1000);
        SelenElem.Click(By.id("js_submit_miniparrilla_migracion"));
        
        
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        SelenElem.Click(By.id("total-mobile"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
       
        
        SelenElem.Click(By.id("btn_continue_check"));
        
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-nombre"), 6);
        SelenElem.TypeText(By.id("valid-nombre"), CargaVariables.LeerCSV(1,"Migraciones",caso));
        //SelenElem.TypeText(By.id("valid-dapelido-p"), CargaVariables.LeerCSV(2,"Portabilidad Pospago Sim",caso));
        SelenElem.TypeText(By.id("valid-appelido-p"), CargaVariables.LeerCSV(2,"Migraciones",caso));
        //SelenElem.TypeText(By.id("valid-dapelido-m"), CargaVariables.LeerCSV(3,"Portabilidad Pospago Sim",caso));
        SelenElem.TypeText(By.id("valid-appelido-m"), CargaVariables.LeerCSV(3,"Migraciones",caso));
        SelenElem.TypeText(By.id("valid-email"), CargaVariables.LeerCSV(4,"Migraciones",caso));
        SelenElem.TypeText(By.id("telefono-contacto"), CargaVariables.LeerCSV(5,"Migraciones",caso));
        
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), CargaVariables.LeerCSV(17,"Migraciones",caso));
        SelenElem.SelectOption(By.className("ui-datepicker-month"), CargaVariables.LeerCSV(18,"Migraciones",caso));
        SelenElem.Click(By.linkText(CargaVariables.LeerCSV(19,"Migraciones",caso)));
        OPG.cargaAjax2();
        
        SelenElem.TypeText(By.id("valida-rfc"), CargaVariables.LeerCSV(6,"Migraciones",caso));
        SelenElem.TypeText(By.id("codigo-postal-valida"), CargaVariables.LeerCSV(7,"Migraciones",caso));        
        SelenElem.TypeText(By.id("calle-domicilio"), CargaVariables.LeerCSV(8,"Migraciones",caso));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("numero-domicilio"), CargaVariables.LeerCSV(9,"Migraciones",caso));
        SelenElem.TypeText(By.id("valida-estado"), CargaVariables.LeerCSV(10,"Migraciones",caso));
        SelenElem.SelectOption(By.id("colonia"), CargaVariables.LeerCSV(11,"Migraciones",caso));
        SelenElem.TypeText(By.id("valid-ciudad"), CargaVariables.LeerCSV(12,"Migraciones",caso));
        SelenElem.TypeText(By.id("ciudad-valida"), CargaVariables.LeerCSV(13,"Migraciones",caso));
        SelenElem.TypeText(By.id("INE-IFE"), CargaVariables.LeerCSV(14,"Migraciones",caso));
        da.hideKeyBoard();
        SelenElem.Click(By.id("checkPrivacyOne"));
        //reporte.AddImageToReport("Datos de llenado", OPG.ScreenShotElementFocus(By.id("valid-dapelido-m")));
        reporte.AddImageToReport("Datos de llenado", OPG.ScreenShotElementFocus(By.id("valid-appelido-m")));
        reporte.AddImageToReport("Datos de llenado", OPG.ScreenShotElementFocus(By.id("valida-rfc")));
        reporte.AddImageToReport("Datos de llenado", OPG.ScreenShotElementFocus(By.id("colonia")));
        reporte.AddImageToReport("Datos de llenado", OPG.ScreenShotElementFocus(By.id("INE-IFE")));        
        SelenElem.Click(By.id("paso1-pospago-porta"));
        
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("cod"), "1111");
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        reporte.AddImageToReport("Sección de consulta de credito checkout", OPG.ScreenShotElement());
        SelenElem.Click(By.xpath("//*[@id=\"msmx-pospago\"]/div[1]/fieldset[2]/div[2]/div/div[7]/div/div/div[2]/label"));
        SelenElem.Click(By.xpath("//*[@id=\"msmx-pospago\"]/div[1]/fieldset[2]/div[2]/div/div[9]/div/div/div[2]/label"));
        reporte.AddImageToReport("Sección de consulta de credito checkout", OPG.ScreenShotElement());
        SelenElem.Click(By.id("checkout-step2"));
        SelenElem.Click(By.id("creditCheck"));
        
        OPG.cargaAjax2();
        /*SelenElem.WaitForLoad(By.id("inv-mail2"), 6);
        SelenElem.Click(By.id("inv-mail2"));
        SelenElem.TypeText(By.id("cp"), "22000");
        OPG.cargaAjax2();
        SelenElem.Click(By.id("longlat"));
        OPG.cargaAjax2();
        
        SelenElem.WaitForLoad(By.className("geo-cac-elocker-option__place-title"),6);
        SelenElem.focus(By.className("geo-cac-elocker-option__place-title"));
        SelenElem.Click(By.className("geo-cac-elocker-option__place-title"));
        reporte.AddImageToReport("Seleccionamos el CAC", OPG.ScreenShotElement());*/
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        SelenElem.Click(By.id("pasarAlPaso3"));
               
        SelenElem.Click(By.id("checkTeminosCondiciones"));
        SelenElem.Click(By.id("checkConsentimiento"));
        SelenElem.Click(By.id("checkConsentimientoRecarga"));
        reporte.AddImageToReport("Aceptamos la sección del contrato", OPG.ScreenShotElement());
        SelenElem.Click(By.id("realizar-pago-2"));
        
        new Flap(reporte,OPG,SelenElem,WebDrive);
        
        SelenElem.WaitForLoad(By.className("descripcion-orden-terminal-card-texto"),30);
        SelenElem.waitForLoadPage();                
        da.slide("Down",2);        
        reporte.AddImageToReport("Vemos el detalle de la compra", OPG.ScreenShotElement());
        da.slide("Down",2);        
        reporte.AddImageToReport("Vemos el detalle de datos personales", OPG.ScreenShotElement());
        da.slide("Down",3);
        reporte.AddImageToReport("Vemos el detalle del envio", OPG.ScreenShotElement());
    }
}
