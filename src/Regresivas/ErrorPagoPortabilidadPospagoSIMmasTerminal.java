
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.FlapErrorVariosCampos;
import Operaciones.FlapPortaVariosCampos;
import Operaciones.OpcionesEnvio;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class ErrorPagoPortabilidadPospagoSIMmasTerminal {

    public ErrorPagoPortabilidadPospagoSIMmasTerminal(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url,String Caso) throws InterruptedException, IOException{
        
        String dn = CargaVariables.LeerCSV(0, "Error Pago Portabilidad Pospago SIM + Terminal", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Error Pago Portabilidad Pospago SIM + Terminal", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Error Pago Portabilidad Pospago SIM + Terminal", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Error Pago Portabilidad Pospago SIM + Terminal", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Error Pago Portabilidad Pospago SIM + Terminal", Caso);
        String correo = CargaVariables.LeerCSV(5, "Error Pago Portabilidad Pospago SIM + Terminal", Caso);
        String tel = CargaVariables.LeerCSV(6, "Error Pago Portabilidad Pospago SIM + Terminal", Caso);
        String calle = CargaVariables.LeerCSV(8, "Error Pago Portabilidad Pospago SIM + Terminal", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Error Pago Portabilidad Pospago SIM + Terminal", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Error Pago Portabilidad Pospago SIM + Terminal", Caso);
        String cp = CargaVariables.LeerCSV(7, "Error Pago Portabilidad Pospago SIM + Terminal", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Error Pago Portabilidad Pospago SIM + Terminal", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Error Pago Portabilidad Pospago SIM + Terminal", Caso);
        String ine = CargaVariables.LeerCSV(14, "Error Pago Portabilidad Pospago SIM + Terminal", Caso);
        String año = CargaVariables.LeerCSV(16, "Error Pago Portabilidad Pospago SIM + Terminal", Caso);
        String mes = CargaVariables.LeerCSV(17, "Error Pago Portabilidad Pospago SIM + Terminal", Caso);
        String  dia= CargaVariables.LeerCSV(18, "Error Pago Portabilidad Pospago SIM + Terminal", Caso);
        String curp= CargaVariables.LeerCSV(19, "Error Pago Portabilidad Pospago SIM + Terminal", Caso);
        String modeloTel= CargaVariables.LeerCSV(20, "Error Pago Portabilidad Pospago SIM + Terminal", Caso);
       

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        
        reporte.AddImageToReport("Se selecciona el telefono", OPG.ScreenShotElementFocus(By.xpath("//a[@href='" + Url + "iphone-xr-lte-amarillo-64gb-apple.html']")));
        Thread.sleep(2000);
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + modeloTel + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        
        SelenElem.WaitForLoad(By.xpath("//*[@id=\"solo-smartphone__content\"]/form/button"),10);
        reporte.AddImageToReport("Visualizar el detalle del smartphone e ingresar DN y dar click en “Usar este número”", OPG.SS());
        
        Thread.sleep(1000);
        SelenElem.Click(By.id("portabilidad"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_input_dn_button_portabilidad"), 10);
        SelenElem.TypeText(By.id("js_dn_input_portabilidad"),dn);
        SelenElem.focus(By.id("js_dn_input_portabilidad"));
        reporte.AddImageToReport(" ", OPG.SS());
        
        SelenElem.Click(By.id("js_input_dn_button_portabilidad"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.className("temm-2022-modal"), 10);
        reporte.AddImageToReport(" ", OPG.SS());
        
        SelenElem.Click(By.xpath("/html/body/div[8]/aside[2]/div[2]/footer/button/span"));
        
        
        SelenElem.Click(By.xpath("//*[@id=\"js_slider_mp_portabilidad-pospago\"]/div/div/div[3]/div/div/div/div"));
        Thread.sleep(500);
        SelenElem.WaitForLoad(By.xpath("//div[.='Quiero este plan']"), 10);
        reporte.AddImageToReport(" ", OPG.SS());
        
        SelenElem.Click(By.xpath("//div[.='Quiero este plan']"));
        
        Thread.sleep(2000);
        SelenElem.Click(By.id("js_submit_miniparrilla_portabilidad_pospago"));
      
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("continueToCheckout"), 10);
        
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementFocus(By.id("section-equipo")));
        SelenElem.Click(By.xpath("//button[@title='Da clic aquí para continuar con la captura de tus datos']"));
       
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-nombre"), 15);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), año);
        SelenElem.SelectOption(By.className("ui-datepicker-month"), mes);
        SelenElem.Click(By.linkText(dia));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("calleNumeroInterior"), numInterior);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        
        SelenElem.Click(By.id("checkout-step4-check-02"));
        Thread.sleep(500);
        SelenElem.Click(By.id("checkout-step4-check-02"));
        
        SelenElem.Click(By.id("paso1-pospago-porta"));
        
        Thread.sleep(5000);
        
        SelenElem.WaitForLoad(By.id("cod"), 10);
        SelenElem.TypeText(By.id("cod"), "1111");
        reporte.AddImageToReport("Sección Consulta de credito del Checkout", OPG.SS()); 
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        SelenElem.Click(By.id("checkout-step2-radio-04"));
        SelenElem.Click(By.id("checkout-step2-radio-06"));
        SelenElem.Click(By.id("checkout-step2"));
        
        reporte.AddImageToReport("Sección Consulta de credito del Checkout", OPG.SS()); 
        
        SelenElem.Click(By.id("botonConsultaCredito"));
        
        OPG.cargaAjax2();
        /*SelenElem.WaitForLoad(By.id("inv-mail2"), 6);
        SelenElem.Click(By.id("inv-mail2"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("cp"), 15);
        SelenElem.Click(By.id("cp"));
        SelenElem.TypeText(By.id("cp"), cp);
        SelenElem.Click(By.id("longlat"));
        OPG.cargaAjax2();
        
        SelenElem.WaitForLoad(By.id("99090073cac"),6);
        SelenElem.Click(By.id("99090073cac"));
         SelenElem.focus(By.id("99090005cac"));
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        SelenElem.focus(By.id("99090003cac"));
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());*/
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        SelenElem.focus(By.id("99090028cac"));
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        SelenElem.focus(By.id("checkout-paso2-renovaciones"));
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());     
        SelenElem.Click(By.id("checkout-paso2-renovaciones"));
        
        OPG.cargaAjax2();
        Thread.sleep(3000);
        
        SelenElem.TypeText(By.id("valid-nip"), "1234");
        SelenElem.TypeText(By.id("valid-curp"), curp);
        SelenElem.Click(By.id("dateToPorta"));
        reporte.AddImageToReport("Sección Cambiate a Movistar del Checkout", OPG.SS());  
        SelenElem.Click(By.id("checkout-paso4-porta-pre"));
        
        OPG.cargaAjax2();
        
        SelenElem.Click(By.id("checkTeminosCondiciones"));
        reporte.AddImageToReport("Sección Contrato del Checkout", OPG.ScreenShotElement());
        SelenElem.Click(By.id("ver_contrato_portabilidad_pdf"));
        Thread.sleep(4000);
        reporte.AddImageToReport("Sección Contrato del Checkout", OPG.ScreenShotAll());
        Thread.sleep(2000);
        SelenElem.Click(By.id("realizar-pago-2"));
        
        new FlapPortaVariosCampos(reporte,OPG,SelenElem,WebDrive);
        
        
    
    }
    
}
