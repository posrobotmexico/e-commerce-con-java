
package Regresivas;

import Core.DeviceActions;
import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.FlapErrorVariosCampos;
import Operaciones.FlapPortaVariosCampos;
import Operaciones.OpcionesEnvio;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class PrepagoNuevoNumeroFlap {
    
    public PrepagoNuevoNumeroFlap() {
    
    }
    
     public void PrepagoSoloSIMTerminalRecarga(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
        String dn = CargaVariables.LeerCSV(0, "Prepago con nuevo numero – Flap", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Prepago con nuevo numero – Flap", Caso);
        
        String nombre =         CargaVariables.LeerCSV(2, "Prepago con nuevo numero – Flap", Caso);
        String apellidop =      CargaVariables.LeerCSV(3, "Prepago con nuevo numero – Flap", Caso);
        String apellidom =      CargaVariables.LeerCSV(4, "Prepago con nuevo numero – Flap", Caso);
        String correo =         CargaVariables.LeerCSV(5, "Prepago con nuevo numero – Flap", Caso);
        String tel =            CargaVariables.LeerCSV(6, "Prepago con nuevo numero – Flap", Caso);
        String modTel=          CargaVariables.LeerCSV(7,"Prepago con nuevo numero – Flap",Caso);
        String NumTarjeta =     CargaVariables.LeerCSV(8,"Prepago con nuevo numero – Flap",Caso);
        String MesTarjeta =     CargaVariables.LeerCSV(9,"Prepago con nuevo numero – Flap",Caso);
        String AñoTarjeta =     CargaVariables.LeerCSV(10,"Prepago con nuevo numero – Flap",Caso);
        String Cvv =            CargaVariables.LeerCSV(11,"Prepago con nuevo numero – Flap",Caso);
        
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        
        Thread.sleep(1000);
        SelenElem.Click(By.id("checkbox"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar el menu principal la opcion Smartphones", OPG.SS());
        
        SelenElem.Click(By.xpath("//input[@value='Smartphones']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar el flujo Smartphones > Todas las marcas", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        
        
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar un Smartphones y continuar", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Se selecciona el telefono", OPG.SS());
        Thread.sleep(2000);
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + modTel + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        
        reporte.AddImageToReport("Ver detalle, seleccionar llévatelo con prepago y comprar ", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Ver detalle, seleccionar llévatelo con prepago y comprar ", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("soy-nuevo"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("llevatelo_con_prepago_soy_nuevo"), 10);
        SelenElem.Click(By.xpath("//*[@id=\"soy-nuevo__content\"]/div[2]/div[2]/label"));
        Thread.sleep(1500);
        SelenElem.Click(By.xpath("//*[@id=\"soy-nuevo__content\"]/div[2]/div[2]/label"));
        
        Thread.sleep(3000);
        da.slide("Down", 2);
        reporte.AddImageToReport("En planes seleccionar plan de recarga  ", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("js_submit_miniparrilla_prepago"));
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.xpath("continueToCheckout"), 20);
        
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("total-mobile"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("continueToCheckout"));

        SelenElem.WaitForLoad(By.id("valid-nombre"), 5);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.ScreenShotElementInstant());
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.ScreenShotElementInstant());
        
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        SelenElem.Click(By.id("checkPrivacyOne"));
        da.hideKeyBoard();
        
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.ScreenShotElementInstant());
        SelenElem.focus(By.id("btn_continuar"));
        SelenElem.Click(By.id("btn_continuar"));
        OPG.cargaAjax2();
        //new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 15);
        SelenElem.Click(By.id("inv-mail2"));
        SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("js_codigo_postal"), "06600");
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección opciones de envio del checkout", OPG.SS());
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        SelenElem.WaitForLoad(By.id("99090073cac"), 10);
        SelenElem.Click(By.id("99090073cac"));
        reporte.AddImageToReport("Sección opciones de envio del checkout", OPG.SS());
        
        SelenElem.focus(By.id("99090003cac"));
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.ScreenShotElementInstant());
        
        SelenElem.focus(By.id("end-step"));
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.ScreenShotElementInstant());
        
        Thread.sleep(2000);
        
        SelenElem.Click(By.id("end-step"));

        Thread.sleep(5000);
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("grandTotalMobile"));
        Thread.sleep(2000);
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        
        Thread.sleep(2000);

        SelenElem.ClickById(WebDrive.WebDriver, "continuar-checkout-out");

        SelenElem.WaitForLoad(By.id("imagen-cliente"), 20);
        reporte.AddImageToReport("Medio de pago Flap", OPG.SS());
        SelenElem.focus(By.id("1"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.id("1"));
        SelenElem.WaitForLoad(By.id("contrato_108553_1"), 0);
        SelenElem.focus(By.id("contrato_108553_1"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.id("contrato_108553_1"));
        
        SelenElem.WaitForLoad(By.id("numTarjeta"), 10);
        SelenElem.TypeText(By.id("numTarjeta"), NumTarjeta);
        SelenElem.SelectOption(By.id("vigenciames"), MesTarjeta);
        SelenElem.SelectOption(By.id("vigenciaanio"), AñoTarjeta);
        SelenElem.TypeText(By.id("cvv2"), Cvv);
        da.hideKeyBoard();
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("continuar"));
        
        Thread.sleep(2000);
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.WaitForLoad(By.id("numeroOrden"), 50);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.ScreenShotElementInstant());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.ScreenShotElementInstant());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.ScreenShotElementInstant());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.ScreenShotElementInstant());
    
    }
     
       public void ErrorPagoPrepagoSoloSIMTerminalRecarga(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
        String dn = CargaVariables.LeerCSV(0, "Prepago con nuevo numero – Flap", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Prepago con nuevo numero – Flap", Caso);
        
        String nombre =         CargaVariables.LeerCSV(2, "Prepago con nuevo numero – Flap", Caso);
        String apellidop =      CargaVariables.LeerCSV(3, "Prepago con nuevo numero – Flap", Caso);
        String apellidom =      CargaVariables.LeerCSV(4, "Prepago con nuevo numero – Flap", Caso);
        String correo =         CargaVariables.LeerCSV(5, "Prepago con nuevo numero – Flap", Caso);
        String tel =            CargaVariables.LeerCSV(6, "Prepago con nuevo numero – Flap", Caso);
        String calle =          CargaVariables.LeerCSV(8, "Prepago con nuevo numero – Flap", Caso);
        String numdomicilio =   CargaVariables.LeerCSV(9, "Prepago con nuevo numero – Flap", Caso);
        String numInterior =    CargaVariables.LeerCSV(10, "Prepago con nuevo numero – Flap", Caso);
        String cp =             CargaVariables.LeerCSV(7, "Prepago con nuevo numero – Flap", Caso);
        String ciudad =         CargaVariables.LeerCSV(13, "Prepago con nuevo numero – Flap", Caso);
        String colonia =        CargaVariables.LeerCSV(12, "Prepago con nuevo numero – Flap", Caso);
        String ine =            CargaVariables.LeerCSV(14, "Prepago con nuevo numero – Flap", Caso);
        String rfcc =           CargaVariables.LeerCSV(15, "Prepago con nuevo numero – Flap", Caso);
        String modTel=          CargaVariables.LeerCSV(16,"Prepago con nuevo numero – Flap",Caso);
        
          WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        
        Thread.sleep(1000);
        SelenElem.Click(By.id("checkbox"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar el menu principal la opcion Smartphones", OPG.SS());
        
        SelenElem.Click(By.xpath("//input[@value='Smartphones']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar el flujo Smartphones > Todas las marcas", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        
        
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar un Smartphones y continuar", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Se selecciona el telefono", OPG.SS());
        Thread.sleep(2000);
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + modTel + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        
        reporte.AddImageToReport("Ver detalle, seleccionar llévatelo con prepago y comprar ", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Ver detalle, seleccionar llévatelo con prepago y comprar ", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("soy-nuevo"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("llevatelo_con_prepago_soy_nuevo"), 10);
        SelenElem.Click(By.xpath("//*[@id=\"soy-nuevo__content\"]/div[2]/div[2]/label"));
        Thread.sleep(1500);
        SelenElem.Click(By.xpath("//*[@id=\"soy-nuevo__content\"]/div[2]/div[2]/label"));
        
        Thread.sleep(3000);
        da.slide("Down", 2);
        reporte.AddImageToReport("En planes seleccionar plan de recarga  ", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("js_submit_miniparrilla_prepago"));
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.xpath("continueToCheckout"), 20);
        
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("total-mobile"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 5);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.ScreenShotElementInstant());
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.ScreenShotElementInstant());
        
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        SelenElem.Click(By.id("checkPrivacyOne"));
        da.hideKeyBoard();
        
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.ScreenShotElementInstant());
        SelenElem.focus(By.id("btn_continuar"));
        SelenElem.Click(By.id("btn_continuar"));
        OPG.cargaAjax2();
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        Thread.sleep(2000);
        
        SelenElem.Click(By.id("end-step"));
        //OPG.cargaAjax2(); 
        
       
        Thread.sleep(5000);
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("grandTotalMobile"));
        Thread.sleep(2000);
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        
        Thread.sleep(2000);
        
        //SelenElem.Click(By.id("continuar-checkout-out"));
        SelenElem.ClickById(WebDrive.WebDriver, "continuar-checkout-out");
        
        
        new FlapErrorVariosCampos(reporte,OPG,SelenElem,WebDrive);
        
        
    
    }
     
     
     public void ErrorCPPrepagoSoloSIMTerminalRecarga(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
        String dn = CargaVariables.LeerCSV(0, "Prepago con nuevo numero – Flap", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Prepago con nuevo numero – Flap", Caso);
        
        String nombre =         CargaVariables.LeerCSV(2, "Prepago con nuevo numero – Flap", Caso);
        String apellidop =      CargaVariables.LeerCSV(3, "Prepago con nuevo numero – Flap", Caso);
        String apellidom =      CargaVariables.LeerCSV(4, "Prepago con nuevo numero – Flap", Caso);
        String correo =         CargaVariables.LeerCSV(5, "Prepago con nuevo numero – Flap", Caso);
        String tel =            CargaVariables.LeerCSV(6, "Prepago con nuevo numero – Flap", Caso);
        String calle =          CargaVariables.LeerCSV(8, "Prepago con nuevo numero – Flap", Caso);
        String numdomicilio =   CargaVariables.LeerCSV(9, "Prepago con nuevo numero – Flap", Caso);
        String numInterior =    CargaVariables.LeerCSV(10, "Prepago con nuevo numero – Flap", Caso);
        String cp =             CargaVariables.LeerCSV(7, "Prepago con nuevo numero – Flap", Caso);
        String ciudad =         CargaVariables.LeerCSV(13, "Prepago con nuevo numero – Flap", Caso);
        String colonia =        CargaVariables.LeerCSV(12, "Prepago con nuevo numero – Flap", Caso);
        String ine =            CargaVariables.LeerCSV(14, "Prepago con nuevo numero – Flap", Caso);
        String rfcc =           CargaVariables.LeerCSV(15, "Prepago con nuevo numero – Flap", Caso);
        String modTel=          CargaVariables.LeerCSV(16,"Prepago con nuevo numero – Flap",Caso);
        
         WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        
        Thread.sleep(1000);
        SelenElem.Click(By.id("checkbox"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar el menu principal la opcion Smartphones", OPG.SS());
        
        SelenElem.Click(By.xpath("//input[@value='Smartphones']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar el flujo Smartphones > Todas las marcas", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        
        
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar un Smartphones y continuar", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Se selecciona el telefono", OPG.SS());
        Thread.sleep(2000);
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + modTel + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        
        reporte.AddImageToReport("Ver detalle, seleccionar llévatelo con prepago y comprar ", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Ver detalle, seleccionar llévatelo con prepago y comprar ", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("soy-nuevo"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("llevatelo_con_prepago_soy_nuevo"), 10);
        SelenElem.Click(By.xpath("//*[@id=\"soy-nuevo__content\"]/div[2]/div[2]/label"));
        Thread.sleep(1500);
        SelenElem.Click(By.xpath("//*[@id=\"soy-nuevo__content\"]/div[2]/div[2]/label"));
        
        Thread.sleep(3000);
        da.slide("Down", 2);
        reporte.AddImageToReport("En planes seleccionar plan de recarga  ", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("js_submit_miniparrilla_prepago"));
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.xpath("continueToCheckout"), 20);
        
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("total-mobile"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 5);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.ScreenShotElementInstant());
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.ScreenShotElementInstant());
        
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        SelenElem.Click(By.id("checkPrivacyOne"));
        da.hideKeyBoard();
        
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.ScreenShotElementInstant());
        SelenElem.focus(By.id("btn_continuar"));
        SelenElem.Click(By.id("btn_continuar"));
        OPG.cargaAjax2();
        
         SelenElem.WaitForLoad(By.id("inv-mail2"), 2);
        SelenElem.Click(By.id("inv-mail2"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("js_codigo_postal"), "0");
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        OPG.cargaAjax2();
        
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        Thread.sleep(500);
        reporte.AddImageToReport("Validacion Error en codigo postal", OPG.ScreenShotElementInstant());
        
        Thread.sleep(1500);
        SelenElem.TypeText(By.id("js_codigo_postal"), "0000");
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
       
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Validacion Error en codigo postal", OPG.ScreenShotElementInstant());
        
      
    }
     
     public void ErrorCPPrepagoSoloTerminalOxxo(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
        
        String telefono = CargaVariables.LeerCSV(0,"Prepago con nuevo numero – Flap",Caso);
       String nombre = CargaVariables.LeerCSV(1,"Prepago con nuevo numero – Flap",Caso);
       String apellidop = CargaVariables.LeerCSV(2,"Prepago con nuevo numero – Flap",Caso);
       String apellidom = CargaVariables.LeerCSV(3,"Prepago con nuevo numero – Flap",Caso);
       String email = CargaVariables.LeerCSV(4,"Prepago con nuevo numero – Flap",Caso);
       String tel = CargaVariables.LeerCSV(5,"Prepago con nuevo numero – Flap",Caso);
       String rfc = CargaVariables.LeerCSV(6,"Prepago con nuevo numero – Flap",Caso);
       String cp = CargaVariables.LeerCSV(7,"Prepago con nuevo numero – Flap",Caso);
       String calle = CargaVariables.LeerCSV(8,"Prepago con nuevo numero – Flap",Caso);
       String numero = CargaVariables.LeerCSV(9,"Prepago con nuevo numero – Flap",Caso);
       String colonia = CargaVariables.LeerCSV(11,"Prepago con nuevo numero – Flap",Caso);
       
       WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        
        Thread.sleep(1000);
        SelenElem.Click(By.id("checkbox"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar el menu principal la opcion Smartphones", OPG.SS());
        
        SelenElem.Click(By.xpath("//input[@value='Smartphones']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar el flujo Smartphones > Todas las marcas", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        
        
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar un Smartphones y continuar", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Se selecciona el telefono", OPG.SS());
        Thread.sleep(2000);
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + telefono + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        
        reporte.AddImageToReport("Ver detalle de Smartphone", OPG.SS());
        SelenElem.focus(By.id("solo-smartphone"));
        reporte.AddImageToReport("", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.xpath("//p[.='Comprar']"));
        
       SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        //SelenElem.focus(By.id("continueToCheckout"));
        
        SelenElem.Click(By.id("total-mobile"));
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        
        SelenElem.Click(By.id("continueToCheckout"));

        
        
//        SelenElem.Click(By.id("continueToCheckout"));
        SelenElem.WaitForLoad(By.id("valid-nombre"), 10);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        
        Thread.sleep(1000);
        SelenElem.Click(By.id("js_ocultar_mostrar_flecha"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Sección de tus datos del checkout", OPG.ScreenShotElementInstant()); 
        
        SelenElem.TypeText(By.id("valid-appelido-m"),apellidom);
        reporte.AddImageToReport("Seccion de datos del checkout", OPG.SS());
        SelenElem.TypeText(By.id("valid-email"), email);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        SelenElem.Click(By.id("recibeFactura")); 
        SelenElem.WaitForLoad(By.id("valida-rfc"), 3);
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        reporte.AddImageToReport("Seccion de datos del checkout", OPG.SS());
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("numero-domicilio"), numero);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.Click(By.id("checkPrivacyOne"));      
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.xpath("//button[@title='Continuar']"));
        
       
        SelenElem.WaitForLoad(By.id("inv-mail2"), 2);
        SelenElem.Click(By.id("inv-mail2"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("js_codigo_postal"), "0");
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        Thread.sleep(500);
        reporte.AddImageToReport("Validacion Error en codigo postal", OPG.ScreenShotElementInstant());
        
        SelenElem.TypeText(By.id("js_codigo_postal"), "0000");
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        Thread.sleep(500);
        OPG.cargaAjax2();
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        Thread.sleep(500);
        reporte.AddImageToReport("Validacion Error en codigo postal", OPG.ScreenShotElementInstant());
        
       
    
    }
     public void PrepagoSoloTerminalOxxo(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
       String telefono = CargaVariables.LeerCSV(0,"Prepago con nuevo numero – Flap",Caso);
       String nombre = CargaVariables.LeerCSV(1,"Prepago con nuevo numero – Flap",Caso);
       String apellidop = CargaVariables.LeerCSV(2,"Prepago con nuevo numero – Flap",Caso);
       String apellidom = CargaVariables.LeerCSV(3,"Prepago con nuevo numero – Flap",Caso);
       String email = CargaVariables.LeerCSV(4,"Prepago con nuevo numero – Flap",Caso);
       String tel = CargaVariables.LeerCSV(5,"Prepago con nuevo numero – Flap",Caso);
       String rfc = CargaVariables.LeerCSV(6,"Prepago con nuevo numero – Flap",Caso);
       String cp = CargaVariables.LeerCSV(7,"Prepago con nuevo numero – Flap",Caso);
       String calle = CargaVariables.LeerCSV(8,"Prepago con nuevo numero – Flap",Caso);
       String numero = CargaVariables.LeerCSV(9,"Prepago con nuevo numero – Flap",Caso);
       String colonia = CargaVariables.LeerCSV(11,"Prepago con nuevo numero – Flap",Caso);
       
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        
        Thread.sleep(1000);
        SelenElem.Click(By.id("checkbox"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar el menu principal la opcion Smartphones", OPG.SS());
        
        SelenElem.Click(By.xpath("//input[@value='Smartphones']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar el flujo Smartphones > Todas las marcas", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        
        
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar un Smartphones y continuar", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Se selecciona el telefono", OPG.SS());
        Thread.sleep(2000);
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + telefono + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        
        reporte.AddImageToReport("Ver detalle de Smartphone", OPG.SS());
        SelenElem.focus(By.id("solo-smartphone"));
        reporte.AddImageToReport("", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.xpath("//p[.='Comprar']"));
        
       SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        //SelenElem.focus(By.id("continueToCheckout"));
        
        SelenElem.Click(By.id("total-mobile"));
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        
        SelenElem.Click(By.id("continueToCheckout"));

        
        
//        SelenElem.Click(By.id("continueToCheckout"));
        SelenElem.WaitForLoad(By.id("valid-nombre"), 10);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        
        Thread.sleep(1000);
        SelenElem.Click(By.id("js_ocultar_mostrar_flecha"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Sección de tus datos del checkout", OPG.ScreenShotElementInstant()); 
        
        SelenElem.TypeText(By.id("valid-appelido-m"),apellidom);
        
        SelenElem.TypeText(By.id("valid-email"), email);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        reporte.AddImageToReport("Seccion de datos del checkout", OPG.SS());
        SelenElem.Click(By.id("recibeFactura")); 
        SelenElem.WaitForLoad(By.id("valida-rfc"), 3);
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        reporte.AddImageToReport("Seccion de datos del checkout", OPG.SS());
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("numero-domicilio"), numero);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.Click(By.id("checkPrivacyOne"));      
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.xpath("//button[@title='Continuar']"));
        
        OPG.cargaAjax2();
       
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        OPG.cargaAjax2();
        SelenElem.Click(By.id("js-stripe-payment-method-oxxo"));
        da.slide("Up", 1);
        reporte.AddImageToReport("Seccion medios de pago en oxxo", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Seccion medios de pago en oxxo", OPG.SS());
        SelenElem.Click(By.id("continuar-checkout-out-validation-oxxo"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.xpath("//p[.='Datos de envío']"), 40);
        reporte.AddImageToReport("Página de pago realizado con éxito  ", OPG.SS());
        
        SelenElem.SwitchToFrame(By.id("recibOXXO"));
        try{
            SelenElem.focus(By.xpath("//button[.='Imprimir']"));
        }catch(Exception ex){}
        
        SelenElem.BacktoMainFrame();
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.ScreenShotElementFocus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/div[3]/div[2]/div[3]/ul/li[1]")));
        
     
    }
    public void StripMppMpe(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
       String telefono = CargaVariables.LeerCSV(0,"Prepago con nuevo numero – Flap",Caso);
       String nombre = CargaVariables.LeerCSV(1,"Prepago con nuevo numero – Flap",Caso);
       String apellidop = CargaVariables.LeerCSV(2,"Prepago con nuevo numero – Flap",Caso);
       String apellidom = CargaVariables.LeerCSV(3,"Prepago con nuevo numero – Flap",Caso);
       String email = CargaVariables.LeerCSV(4,"Prepago con nuevo numero – Flap",Caso);
       String tel = CargaVariables.LeerCSV(5,"Prepago con nuevo numero – Flap",Caso);
       String rfc = CargaVariables.LeerCSV(6,"Prepago con nuevo numero – Flap",Caso);
       String cp = CargaVariables.LeerCSV(7,"Prepago con nuevo numero – Flap",Caso);
       String calle = CargaVariables.LeerCSV(8,"Prepago con nuevo numero – Flap",Caso);
       String numero = CargaVariables.LeerCSV(9,"Prepago con nuevo numero – Flap",Caso);
       String colonia = CargaVariables.LeerCSV(11,"Prepago con nuevo numero – Flap",Caso);
       
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        SelenElem.Click(By.className("icon"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionamos el flujo de Telefonos", OPG.ScreenShotElement());
        SelenElem.Click(By.xpath("//input[@value='Smartphones']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionamos el flujo de Telefonos", OPG.ScreenShotElement());
        
        
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        SelenElem.waitForLoadPage();
        Thread.sleep(5000);
        reporte.AddImageToReport("Se selecciona el telefono", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + telefono + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        Thread.sleep(3000);
        
        reporte.AddImageToReport("Ver detalle de Smartphone", OPG.SS());
        SelenElem.focus(By.id("solo-smartphone"));
        reporte.AddImageToReport("", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.xpath("//p[.='Comprar']"));
        
       SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        //SelenElem.focus(By.id("continueToCheckout"));
        
        SelenElem.Click(By.id("total-mobile"));
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        
        
//        SelenElem.Click(By.id("continueToCheckout"));
        SelenElem.WaitForLoad(By.id("valid-nombre"), 20);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"),apellidom);
        SelenElem.Click(By.id("js_ocultar_mostrar_flecha"));
        da.slide("Up", 1);
        reporte.AddImageToReport("Sección de tus datos del checkout", OPG.SS());
        
        SelenElem.TypeText(By.id("valid-email"), email);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        reporte.AddImageToReport("Seccion de datos del checkout", OPG.SS());
        SelenElem.Click(By.id("recibeFactura")); 
        SelenElem.WaitForLoad(By.id("valida-rfc"), 3);
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        reporte.AddImageToReport("Seccion de datos del checkout", OPG.SS());
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("numero-domicilio"), numero);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.Click(By.id("checkPrivacyOne"));      
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.xpath("//button[@title='Continuar']"));
        
        
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        Thread.sleep(2000);
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        OPG.cargaAjax2();
        
        SelenElem.WaitForLoad(By.id("js-mpcash-payment-method"),5);
        Thread.sleep(3000);
        SelenElem.Click(By.xpath("//img[contains(@alt, 'Mercado-Pago-Efectivo')]"));
        Thread.sleep(2000);
        SelenElem.Click(By.xpath("//img[contains(@alt, 'Farmacias del Ahorro')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        
        
        reporte.AddImageToReport("Sección Medios de Pago, pagar con MPE", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección Medios de Pago, pagar con MPE", OPG.ScreenShotElementInstant());
       
        
        SelenElem.Click(By.id("continuar-checkout-mp-ticket"));
        //OPG.cargaAjax2();
        //SelenElem.WaitForLoad(By.xpath("//p[.='Datos de envío']"), 40);
        SelenElem.waitForLoadPage();
        Thread.sleep(2000);
        reporte.AddImageToReport("Página de pago realizado con éxito  ", OPG.SS());
        
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
       
    }
     public void ErrorPagoStripMppMpe(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
       String telefono = CargaVariables.LeerCSV(0,"Prepago con nuevo numero – Flap",Caso);
       String nombre = CargaVariables.LeerCSV(1,"Prepago con nuevo numero – Flap",Caso);
       String apellidop = CargaVariables.LeerCSV(2,"Prepago con nuevo numero – Flap",Caso);
       String apellidom = CargaVariables.LeerCSV(3,"Prepago con nuevo numero – Flap",Caso);
       String email = CargaVariables.LeerCSV(4,"Prepago con nuevo numero – Flap",Caso);
       String tel = CargaVariables.LeerCSV(5,"Prepago con nuevo numero – Flap",Caso);
       String rfc = CargaVariables.LeerCSV(6,"Prepago con nuevo numero – Flap",Caso);
       String cp = CargaVariables.LeerCSV(7,"Prepago con nuevo numero – Flap",Caso);
       String calle = CargaVariables.LeerCSV(8,"Prepago con nuevo numero – Flap",Caso);
       String numero = CargaVariables.LeerCSV(9,"Prepago con nuevo numero – Flap",Caso);
       String colonia = CargaVariables.LeerCSV(11,"Prepago con nuevo numero – Flap",Caso);
       
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url+ "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        SelenElem.waitForLoadPage();
        Thread.sleep(5000);
        reporte.AddImageToReport("Se selecciona el telefono", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + telefono + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        Thread.sleep(3000);
        
        reporte.AddImageToReport("Ver detalle de Smartphone", OPG.SS());
        SelenElem.focus(By.id("solo-smartphone"));
        reporte.AddImageToReport("", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.xpath("//p[.='Comprar']"));
        
       SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        //SelenElem.focus(By.id("continueToCheckout"));
        
        SelenElem.Click(By.id("total-mobile"));
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        
        
//        SelenElem.Click(By.id("continueToCheckout"));
        SelenElem.WaitForLoad(By.id("valid-nombre"), 10);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"),apellidom);
        reporte.AddImageToReport("Seccion de datos del checkout", OPG.SS());
        SelenElem.TypeText(By.id("valid-email"), email);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        reporte.AddImageToReport("Seccion de datos del checkout", OPG.SS());
        SelenElem.Click(By.id("recibeFactura")); 
        SelenElem.WaitForLoad(By.id("valida-rfc"), 3);
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        reporte.AddImageToReport("Seccion de datos del checkout", OPG.SS());
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("numero-domicilio"), numero);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.Click(By.id("checkPrivacyOne"));      
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.xpath("//button[@title='Continuar']"));
        
       
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        OPG.cargaAjax2();
        
        SelenElem.WaitForLoad(By.id("js-mpcash-payment-method"),5);
        Thread.sleep(3000);
        SelenElem.Click(By.id("js-mpcash-payment-method"));
        Thread.sleep(2000);
        reporte.AddImageToReport("Sección Medios de Pago, pagar con MPE", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección Medios de Pago, pagar con MPE", OPG.ScreenShotElementInstant());
       
        
        SelenElem.Click(By.id("continuar-checkout-mp-ticket"));
        SelenElem.Click(By.id("continuar-checkout-mp-ticket"));
        //OPG.cargaAjax2();
        //SelenElem.WaitForLoad(By.xpath("//p[.='Datos de envío']"), 40);
        
        Thread.sleep(700);
        reporte.AddImageToReport("Error en forma de pago sin seleccionar una tienda ", OPG.ScreenShotElementInstant());
        
     
    }
    public void ErrorCPStripMppMpe(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
       String telefono = CargaVariables.LeerCSV(0,"Prepago con nuevo numero – Flap",Caso);
       String nombre = CargaVariables.LeerCSV(1,"Prepago con nuevo numero – Flap",Caso);
       String apellidop = CargaVariables.LeerCSV(2,"Prepago con nuevo numero – Flap",Caso);
       String apellidom = CargaVariables.LeerCSV(3,"Prepago con nuevo numero – Flap",Caso);
       String email = CargaVariables.LeerCSV(4,"Prepago con nuevo numero – Flap",Caso);
       String tel = CargaVariables.LeerCSV(5,"Prepago con nuevo numero – Flap",Caso);
       String rfc = CargaVariables.LeerCSV(6,"Prepago con nuevo numero – Flap",Caso);
       String cp = CargaVariables.LeerCSV(7,"Prepago con nuevo numero – Flap",Caso);
       String calle = CargaVariables.LeerCSV(8,"Prepago con nuevo numero – Flap",Caso);
       String numero = CargaVariables.LeerCSV(9,"Prepago con nuevo numero – Flap",Caso);
       String colonia = CargaVariables.LeerCSV(11,"Prepago con nuevo numero – Flap",Caso);
       
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        SelenElem.waitForLoadPage();
        Thread.sleep(5000);
        reporte.AddImageToReport("Se selecciona el telefono", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + telefono + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        Thread.sleep(3000);
        
        reporte.AddImageToReport("Ver detalle de Smartphone", OPG.SS());
        SelenElem.focus(By.id("solo-smartphone"));
        reporte.AddImageToReport("", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.xpath("//p[.='Comprar']"));
        
       SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        //SelenElem.focus(By.id("continueToCheckout"));
        
        SelenElem.Click(By.id("total-mobile"));
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        
        
//        SelenElem.Click(By.id("continueToCheckout"));
        SelenElem.WaitForLoad(By.id("valid-nombre"), 10);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"),apellidom);
        reporte.AddImageToReport("Seccion de datos del checkout", OPG.SS());
        SelenElem.TypeText(By.id("valid-email"), email);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        reporte.AddImageToReport("Seccion de datos del checkout", OPG.SS());
        SelenElem.Click(By.id("recibeFactura")); 
        SelenElem.WaitForLoad(By.id("valida-rfc"), 3);
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        reporte.AddImageToReport("Seccion de datos del checkout", OPG.SS());
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("numero-domicilio"), numero);
        Thread.sleep(1000);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.Click(By.id("checkPrivacyOne"));      
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.xpath("//button[@title='Continuar']"));
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 2);
        SelenElem.Click(By.id("inv-mail2"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("js_codigo_postal"), "0");
        da.hideKeyBoard();
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        Thread.sleep(500);
        reporte.AddImageToReport("Validacion Error en codigo postal", OPG.ScreenShotElementInstant());
        
        SelenElem.TypeText(By.id("js_codigo_postal"), "0000");
        da.hideKeyBoard();
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        Thread.sleep(500);
        OPG.cargaAjax2();
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        Thread.sleep(500);
        reporte.AddImageToReport("Validacion Error en codigo postal", OPG.ScreenShotElementInstant());
        /*SelenElem.Click(By.id("inv-mail2"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("cp"),15);
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        SelenElem.TypeText(By.id("cp"), "0");
        SelenElem.Click(By.id("longlat"));
        SelenElem.WaitForLoad(By.id("searchCp"), 4);
        reporte.AddImageToReport("Validacion error codigo postal", OPG.SS());
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("cp"), "0000");
        SelenElem.Click(By.id("longlat"));
        SelenElem.WaitForLoad(By.id("searchCp"), 4);
        reporte.AddImageToReport("Validacion error codigo postal", OPG.SS());
        Thread.sleep(2000);
        SelenElem.Find(By.id("cp")).clear();
        SelenElem.Click(By.id("longlat"));
        SelenElem.WaitForLoad(By.id("searchCp"), 4);
        reporte.AddImageToReport("Validacion error codigo postal", OPG.SS());*/
        
    }
        public void ErrorDatosStripMppMpe(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
       String telefono = CargaVariables.LeerCSV(0,"Prepago con nuevo numero – Flap",Caso);
       String nombre = CargaVariables.LeerCSV(1,"Prepago con nuevo numero – Flap",Caso);
       String apellidop = CargaVariables.LeerCSV(2,"Prepago con nuevo numero – Flap",Caso);
       String apellidom = CargaVariables.LeerCSV(3,"Prepago con nuevo numero – Flap",Caso);
       String email = CargaVariables.LeerCSV(4,"Prepago con nuevo numero – Flap",Caso);
       String tel = CargaVariables.LeerCSV(5,"Prepago con nuevo numero – Flap",Caso);
       String rfc = CargaVariables.LeerCSV(6,"Prepago con nuevo numero – Flap",Caso);
       String cp = CargaVariables.LeerCSV(7,"Prepago con nuevo numero – Flap",Caso);
       String calle = CargaVariables.LeerCSV(8,"Prepago con nuevo numero – Flap",Caso);
       String numero = CargaVariables.LeerCSV(9,"Prepago con nuevo numero – Flap",Caso);
       String colonia = CargaVariables.LeerCSV(11,"Prepago con nuevo numero – Flap",Caso);
       
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        SelenElem.waitForLoadPage();
        Thread.sleep(5000);
        reporte.AddImageToReport("Se selecciona el telefono", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + telefono + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        Thread.sleep(3000);
        
        reporte.AddImageToReport("Ver detalle de Smartphone", OPG.SS());
        SelenElem.focus(By.id("solo-smartphone"));
        reporte.AddImageToReport("", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.xpath("//p[.='Comprar']"));
        
       SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        //SelenElem.focus(By.id("continueToCheckout"));
        
        SelenElem.Click(By.id("total-mobile"));
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        
        
//        SelenElem.Click(By.id("continueToCheckout"));
        SelenElem.WaitForLoad(By.id("valid-nombre"), 10);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"),apellidom);
        reporte.AddImageToReport("Seccion de datos del checkout", OPG.SS());
        SelenElem.TypeText(By.id("valid-email"), email);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        reporte.AddImageToReport("Seccion de datos del checkout", OPG.SS());
        SelenElem.Click(By.id("recibeFactura")); 
        SelenElem.WaitForLoad(By.id("valida-rfc"), 3);
        //SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        reporte.AddImageToReport("Seccion de datos del checkout", OPG.SS());
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("numero-domicilio"), numero);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.Click(By.id("checkPrivacyOne"));      
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.xpath("//button[@title='Continuar']"));
        Thread.sleep(500);
        reporte.AddImageToReport("Error en TUS DATOS - Sin datos en el campo RFC", OPG.ScreenShotElementInstant());
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("valida-rfc"), "PELL931008");
        SelenElem.Click(By.xpath("//button[@title='Continuar']"));
        Thread.sleep(500);
        reporte.AddImageToReport("Error en TUS DATOS - Con RFC incompleto", OPG.ScreenShotElementInstant());
        Thread.sleep(2000);
        SelenElem.Find(By.id("valida-rfc")).clear();
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.Click(By.id("checkPrivacyOne"));
        SelenElem.Click(By.xpath("//button[@title='Continuar']"));
        Thread.sleep(500);
        reporte.AddImageToReport("Error en TUS DATOS - Con RFC incompleto", OPG.ScreenShotElementInstant());
        
        
    }
    
     public void PrepagoMercadoPago(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
       String dn = CargaVariables.LeerCSV(0, "Prepago con nuevo numero – Flap", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Prepago con nuevo numero – Flap", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Prepago con nuevo numero – Flap", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Prepago con nuevo numero – Flap", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Prepago con nuevo numero – Flap", Caso);
        String correo = CargaVariables.LeerCSV(5, "Prepago con nuevo numero – Flap", Caso);
        String tel = CargaVariables.LeerCSV(6, "Prepago con nuevo numero – Flap", Caso);
        String calle = CargaVariables.LeerCSV(8, "Prepago con nuevo numero – Flap", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Prepago con nuevo numero – Flap", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Prepago con nuevo numero – Flap", Caso);
        String cp = CargaVariables.LeerCSV(7, "Prepago con nuevo numero – Flap", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Prepago con nuevo numero – Flap", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Prepago con nuevo numero – Flap", Caso);
        String ine = CargaVariables.LeerCSV(14, "Prepago con nuevo numero – Flap", Caso);
        String rfcc = CargaVariables.LeerCSV(15, "Prepago con nuevo numero – Flap", Caso);
        String tarjeta = CargaVariables.LeerCSV(17, "Prepago con nuevo numero – Flap", Caso);
        String vencimiento = CargaVariables.LeerCSV(18, "Prepago con nuevo numero – Flap", Caso);
        String cvv = CargaVariables.LeerCSV(19, "Prepago con nuevo numero – Flap", Caso);
        
        reporte.AddImageToReport("Iniciamos desde la pagina principal y seleccionamos telefonos", OPG.ScreenShotElement());
        SelenElem.Click(By.className("icon"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionamos el flujo de Telefonos", OPG.ScreenShotElement());
        SelenElem.Click(By.xpath("//input[@value='Smartphones']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionamos el flujo de Telefonos", OPG.ScreenShotElement());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        reporte.AddImageToReport("Seleccionamos la opción Telefonos", OPG.ScreenShotElement());
        
        
        reporte.AddImageToReport("Se selecciona el telefono", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + CargaVariables.LeerCSV(16,"Prepago con nuevo numero – Flap",Caso) + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        
        reporte.AddImageToReport("Vemos el detalle del equipo", OPG.ScreenShotElement());
        da.slide("Down", 2);
        reporte.AddImageToReport("Vemos mas detalle del equipo", OPG.ScreenShotElement()); 
        da.slide("Down", 2);
        reporte.AddImageToReport("Vemos mas detalle del equipo", OPG.ScreenShotElement()); 
        
        SelenElem.Click(By.xpath("//*[@id=\"solo-smartphone__content\"]/form/button/p"));
        SelenElem.WaitForLoad(By.linkText("Cambiar equipo"),6);
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElement()); 
        SelenElem.Click(By.id("total-mobile"));
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElement()); 
        Thread.sleep(4000);
        
     
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 5);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.Click(By.id("js_ocultar_mostrar_flecha"));
        da.slide("Up", 1);
        reporte.AddImageToReport("Sección de tus datos del checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        SelenElem.Click(By.id("checkPrivacyOne"));
        
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("recibeFactura"));
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        
        da.slide("Up", 2);
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.focus(By.id("btn_continuar"));
        da.slide("Up", 1);
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("btn_continuar"));
        OPG.cargaAjax2();
        
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        reporte.AddImageToReport("", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        OPG.cargaAjax2();
        
        SelenElem.Find(By.id("form-checkout__cardholderName")).clear();
        SelenElem.TypeText(By.id("form-checkout__cardholderName"), "APRO APRO");
        
        SelenElem.Click(By.id("form-checkout__expirationDate"));
        SelenElem.TypeText(By.id("form-checkout__cardNumber"), tarjeta);
        SelenElem.Click(By.id("form-checkout__expirationDate"));
        SelenElem.TypeText(By.id("form-checkout__expirationDate"), vencimiento);
        SelenElem.Click(By.id("form-checkout__securityCode"));
        SelenElem.TypeText(By.id("form-checkout__securityCode"), cvv);
        SelenElem.Click(By.id("form-checkout__expirationDate"));
        SelenElem.TypeText(By.id("form-checkout__expirationDate"), vencimiento);
        SelenElem.Click(By.id("labelSameBillingAddress"));
        reporte.AddImageToReport("Sección “Medios de pago” pagar con Mercado Pago", OPG.SS());
        
        SelenElem.SelectOption(By.id("form-checkout__installments"), "12 mensualidades de $ 666.67 ($ 8,000.00)");
        Thread.sleep(1000);
        reporte.AddImageToReport("Sección “Medios de pago” pagar con Mercado Pago", OPG.SS());
        
        SelenElem.focus(By.id("form-checkout__submit"));
        
        reporte.AddImageToReport("Sección “Medios de pago” pagar con Mercado Pago", OPG.SS());
        SelenElem.Click(By.id("form-checkout__submit"));
        
        //OPG.cargaAjax2();
        
        SelenElem.WaitForLoad(By.xpath("//*[@id=\"contenedor-card-desktop\"]/div[2]/div[1]/div[1]"), 30);
        da.slide("Down", 1);
        reporte.AddImageToReport("Página de pago realizado con éxito", OPG.SS());
//        SelenElem.focus(By.xpath("//*[@id=\"contenedor-card-desktop\"]/div[2]/div[2]/div[1]/p"));
        da.slide("Down", 2);
        reporte.AddImageToReport("Página de pago realizado con éxito", OPG.SS());
        
    }
  
    
     public void ErrorCPPrepagoMercadoPago (Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
        
        String dn = CargaVariables.LeerCSV(0, "Prepago con nuevo numero – Flap", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Prepago con nuevo numero – Flap", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Prepago con nuevo numero – Flap", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Prepago con nuevo numero – Flap", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Prepago con nuevo numero – Flap", Caso);
        String correo = CargaVariables.LeerCSV(5, "Prepago con nuevo numero – Flap", Caso);
        String tel = CargaVariables.LeerCSV(6, "Prepago con nuevo numero – Flap", Caso);
        String calle = CargaVariables.LeerCSV(8, "Prepago con nuevo numero – Flap", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Prepago con nuevo numero – Flap", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Prepago con nuevo numero – Flap", Caso);
        String cp = CargaVariables.LeerCSV(7, "Prepago con nuevo numero – Flap", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Prepago con nuevo numero – Flap", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Prepago con nuevo numero – Flap", Caso);
        String ine = CargaVariables.LeerCSV(14, "Prepago con nuevo numero – Flap", Caso);
        String rfcc = CargaVariables.LeerCSV(15, "Prepago con nuevo numero – Flap", Caso);
        String tarjeta = CargaVariables.LeerCSV(17, "Prepago con nuevo numero – Flap", Caso);
        String vencimiento = CargaVariables.LeerCSV(18, "Prepago con nuevo numero – Flap", Caso);
        String cvv = CargaVariables.LeerCSV(19, "Prepago con nuevo numero – Flap", Caso);
    
        
        reporte.AddImageToReport("Iniciamos desde la pagina principal y seleccionamos telefonos", OPG.ScreenShotElement());
        SelenElem.Click(By.className("icon"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionamos el flujo de Telefonos", OPG.ScreenShotElement());
        SelenElem.Click(By.xpath("//input[@value='Smartphones']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionamos el flujo de Telefonos", OPG.ScreenShotElement());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        reporte.AddImageToReport("Seleccionamos la opción Telefonos", OPG.ScreenShotElement());
        
        
        reporte.AddImageToReport("Se selecciona el telefono", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + CargaVariables.LeerCSV(16,"Prepago con nuevo numero – Flap",Caso) + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        
        reporte.AddImageToReport("Vemos el detalle del equipo", OPG.ScreenShotElement());
        da.slide("Down", 2);
        reporte.AddImageToReport("Vemos mas detalle del equipo", OPG.ScreenShotElement()); 
        da.slide("Down", 2);
        reporte.AddImageToReport("Vemos mas detalle del equipo", OPG.ScreenShotElement()); 
        
        SelenElem.Click(By.xpath("//*[@id=\"solo-smartphone__content\"]/form/button/p"));
        SelenElem.WaitForLoad(By.linkText("Cambiar equipo"),6);
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElement()); 
        SelenElem.Click(By.id("total-mobile"));
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElement()); 
        Thread.sleep(4000);
        
     
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 5);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        SelenElem.Click(By.id("checkPrivacyOne"));
        
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
        
        SelenElem.focus(By.id("btn_continuar"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("btn_continuar"));
        OPG.cargaAjax2();
        
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());
        da.hideKeyBoard();
        //SelenElem.Click(By.id("js-continuar-metodos-envio"));
        OPG.cargaAjax2();
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 2);
        SelenElem.Click(By.id("inv-mail2"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("js_codigo_postal"), "0");
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        Thread.sleep(500);
        reporte.AddImageToReport("Validacion Error en codigo postal", OPG.ScreenShotElementInstant());
        
        SelenElem.TypeText(By.id("js_codigo_postal"), "0000");
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        Thread.sleep(500);
        OPG.cargaAjax2();
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        Thread.sleep(500);
        reporte.AddImageToReport("Validacion Error en codigo postal", OPG.ScreenShotElementInstant());
       

    }
     
     public void ErrorPagoPrepagoMercadoPago (Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
        
        String dn = CargaVariables.LeerCSV(0, "Prepago con nuevo numero – Flap", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Prepago con nuevo numero – Flap", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Prepago con nuevo numero – Flap", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Prepago con nuevo numero – Flap", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Prepago con nuevo numero – Flap", Caso);
        String correo = CargaVariables.LeerCSV(5, "Prepago con nuevo numero – Flap", Caso);
        String tel = CargaVariables.LeerCSV(6, "Prepago con nuevo numero – Flap", Caso);
        String calle = CargaVariables.LeerCSV(8, "Prepago con nuevo numero – Flap", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Prepago con nuevo numero – Flap", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Prepago con nuevo numero – Flap", Caso);
        String cp = CargaVariables.LeerCSV(7, "Prepago con nuevo numero – Flap", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Prepago con nuevo numero – Flap", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Prepago con nuevo numero – Flap", Caso);
        String ine = CargaVariables.LeerCSV(14, "Prepago con nuevo numero – Flap", Caso);
        String rfcc = CargaVariables.LeerCSV(15, "Prepago con nuevo numero – Flap", Caso);
        String tarjeta = CargaVariables.LeerCSV(17, "Prepago con nuevo numero – Flap", Caso);
        String vencimiento = CargaVariables.LeerCSV(18, "Prepago con nuevo numero – Flap", Caso);
        String cvv = CargaVariables.LeerCSV(19, "Prepago con nuevo numero – Flap", Caso);
    
        reporte.AddImageToReport("Iniciamos desde la pagina principal y seleccionamos telefonos", OPG.ScreenShotElement());
        SelenElem.Click(By.className("icon"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionamos el flujo de Telefonos", OPG.ScreenShotElement());
        SelenElem.Click(By.xpath("//input[@value='Smartphones']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionamos el flujo de Telefonos", OPG.ScreenShotElement());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        reporte.AddImageToReport("Seleccionamos la opción Telefonos", OPG.ScreenShotElement());
        
        
        reporte.AddImageToReport("Se selecciona el telefono", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + CargaVariables.LeerCSV(16,"Prepago con nuevo numero – Flap",Caso) + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        
        reporte.AddImageToReport("Vemos el detalle del equipo", OPG.ScreenShotElement());
        da.slide("Down", 2);
        reporte.AddImageToReport("Vemos mas detalle del equipo", OPG.ScreenShotElement()); 
        da.slide("Down", 2);
        reporte.AddImageToReport("Vemos mas detalle del equipo", OPG.ScreenShotElement()); 
        
        SelenElem.Click(By.xpath("//*[@id=\"solo-smartphone__content\"]/form/button/p"));
        SelenElem.WaitForLoad(By.linkText("Cambiar equipo"),6);
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElement()); 
        SelenElem.Click(By.id("total-mobile"));
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElement()); 
        Thread.sleep(4000);
        
     
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 5);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        SelenElem.Click(By.id("checkPrivacyOne"));
        
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        da.slide("Up", 1);
        
        SelenElem.Click(By.id("recibeFactura"));
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        
        da.slide("Down", 1);
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.focus(By.id("btn_continuar"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("btn_continuar"));
        OPG.cargaAjax2();
        
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        reporte.AddImageToReport("", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        OPG.cargaAjax2();
        
        
        SelenElem.TypeText(By.id("form-checkout__cardNumber"), "4075595716483764");
        SelenElem.Click(By.id("form-checkout__expirationDate"));
        SelenElem.TypeText(By.id("form-checkout__expirationDate"), vencimiento);
        SelenElem.Click(By.id("form-checkout__securityCode"));
        //SelenElem.TypeText(By.id("securityCode_mask"), cvv);
        SelenElem.Click(By.id("form-checkout__expirationDate"));
        SelenElem.TypeText(By.id("form-checkout__expirationDate"), vencimiento);
        SelenElem.Click(By.id("labelSameBillingAddress"));
        reporte.AddImageToReport("Sección “Medios de pago” pagar con Mercado Pago", OPG.SS());
        
        //SelenElem.SelectOption(By.id("form-checkout__installments"), "12 mensualidades de $ 666.67 ($ 8,000.00)");
        Thread.sleep(1000);
        reporte.AddImageToReport("Sección “Medios de pago” pagar con Mercado Pago", OPG.SS());
        //reporte.AddImageToReport("Sección “Medios de pago” pagar con Mercado Pago", OPG.SS());
        
        //SelenElem.focus(By.id("continuar-checkout-out-validation-mp"));
        da.hideKeyBoard();
        SelenElem.Click(By.id("form-checkout__submit"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Error forma de pago falta codigo cvv", OPG.SS());
        
        //////////////////////
        /*Thread.sleep(5000);      
              
        //SelenElem.TypeText(By.id("cardNumber"), "64");
        SelenElem.FindElementByID("form-checkout__cardNumber").clear();
        SelenElem.TypeText(By.id("form-checkout__cardNumber"), "407559571648");
        SelenElem.TypeText(By.id("form-checkout__securityCode"), cvv);
        //SelenElem.FindElementByID("full-name").clear();
        SelenElem.Click(By.id("form-checkout__expirationDate"));
        SelenElem.TypeText(By.id("form-checkout__expirationDate"), vencimiento);
        SelenElem.Click(By.id("form-checkout__securityCode"));
        //SelenElem.TypeText(By.id("form-checkout__securityCode"), cvv);
        SelenElem.Click(By.id("form-checkout__expirationDate"));
        SelenElem.TypeText(By.id("form-checkout__expirationDate"), vencimiento);
        //SelenElem.Click(By.id("labelSameBillingAddress"));
        //reporte.AddImageToReport("Sección “Medios de pago” pagar con Mercado Pago", OPG.SS());
        
        //SelenElem.SelectOption(By.id("form-checkout__installments"), "12 mensualidades de $ 666.67 ($ 8,000.00)");
        Thread.sleep(1000);
        da.hideKeyBoard();
        SelenElem.Click(By.id("form-checkout__submit"));
        Thread.sleep(500);
        OPG.cargaAjax2();
        Thread.sleep(3000);
        reporte.AddImageToReport("Error forma de pago tarjeta no valida", OPG.SS());
        //reporte.AddImageToReport("Error forma de pago tarjeta sin mensualidades", OPG.SS());
        
       
        
        SelenElem.FindElementByID("form-checkout__cardNumber").clear();
        SelenElem.TypeText(By.id("form-checkout__cardNumber"), tarjeta);
        SelenElem.Click(By.id("form-checkout__expirationDate"));
        SelenElem.TypeText(By.id("form-checkout__expirationDate"), vencimiento);
        SelenElem.Click(By.id("form-checkout__securityCode"));
        SelenElem.TypeText(By.id("form-checkout__securityCode"), cvv);
        SelenElem.Click(By.id("form-checkout__expirationDate"));
        SelenElem.TypeText(By.id("form-checkout__expirationDate"), vencimiento);
        
        da.hideKeyBoard();
        SelenElem.Click(By.id("form-checkout__submit"));
        Thread.sleep(500);
        reporte.AddImageToReport("Error forma de pago sin parcialidades", OPG.SS());
        
        SelenElem.FindElementByID("form-checkout__cardholderName").clear();
        SelenElem.SelectOption(By.id("form-checkout__installments"), "12 mensualidades de $ 666.67 ($ 8,000.00)");
        Thread.sleep(1000);
        
        SelenElem.Click(By.id("form-checkout__submit"));
        Thread.sleep(500);
        reporte.AddImageToReport("Error forma de pago tarjeta sin nombre del titular", OPG.SS());
        
        Thread.sleep(1500);
        SelenElem.TypeText(By.id("form-checkout__cardholderName"), "Vianey Macias Nieves");
        SelenElem.FindElementByID("form-checkout__expirationDate").clear();
        SelenElem.TypeText(By.id("form-checkout__expirationDate"), "01/22");
        
        da.hideKeyBoard();
        SelenElem.Click(By.id("form-checkout__submit"));
        Thread.sleep(500);
        reporte.AddImageToReport("Error forma de pago Fecha de tarjeta expirada", OPG.SS());
        
        Thread.sleep(1500);
        SelenElem.FindElementByID("form-checkout__expirationDate").clear();
        SelenElem.TypeText(By.id("form-checkout__expirationDate"), vencimiento);
        SelenElem.Click(By.id("labelSameBillingAddress"));
        SelenElem.Click(By.id("form-checkout__submit"));
        Thread.sleep(500);
        reporte.AddImageToReport("Error forma de pago Sin direccion", OPG.SS());
        
        //validar vencimiento 
        Thread.sleep(1500);
        SelenElem.FindElementByID("form-checkout__expirationDate").clear();
        SelenElem.Click(By.id("labelSameBillingAddress"));
        SelenElem.Click(By.id("form-checkout__submit"));
        Thread.sleep(500);
        reporte.AddImageToReport("Error forma de pago sin fecha de vencimiento", OPG.SS());
       */

    }
    
    
     public void PrepagoFlapSTRIPE (Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
    
        String telefono = CargaVariables.LeerCSV(0,"Prepago con nuevo numero – Flap",Caso);
        String nombre = CargaVariables.LeerCSV(1,"Prepago con nuevo numero – Flap",Caso);
        String apellidopa = CargaVariables.LeerCSV(2,"Prepago con nuevo numero – Flap",Caso);
        String apema = CargaVariables.LeerCSV(3,"Prepago con nuevo numero – Flap",Caso);
        String email = CargaVariables.LeerCSV(4,"Prepago con nuevo numero – Flap",Caso);
        String tel = CargaVariables.LeerCSV(5,"Prepago con nuevo numero – Flap",Caso);
        String tarjeta = CargaVariables.LeerCSV(6,"Prepago con nuevo numero – Flap",Caso);
        String vencimiento = CargaVariables.LeerCSV(7,"Prepago con nuevo numero – Flap",Caso);
        String cvv = CargaVariables.LeerCSV(8,"Prepago con nuevo numero – Flap",Caso);
        
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        reporte.AddImageToReport("Se selecciona el telefono", OPG.ScreenShotElementFocus(By.xpath("//a[@href='" + Url + "iphone-xr-lte-amarillo-64gb-apple.html']")));
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + telefono + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        Thread.sleep(3000);
        
        reporte.AddImageToReport("Ver detalle de Smartphone", OPG.SS());
        SelenElem.focus(By.id("solo-smartphone"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//p[.='Comprar']"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.xpath("//p[.='Comprar']"));
        
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        //SelenElem.focus(By.id("continueToCheckout"));
        
        SelenElem.Click(By.id("total-mobile"));
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 20);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidopa);
        reporte.AddImageToReport("Seccion de datos del checkout", OPG.SS());
        SelenElem.TypeText(By.id("valid-appelido-m"), apema);
        SelenElem.TypeText(By.id("valid-email"), email);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        SelenElem.Click(By.id("checkPrivacyOne"));      
        reporte.AddImageToReport("Seccion de datos del checkout", OPG.SS());
        SelenElem.Click(By.xpath("//button[@title='Continuar']"));
        
        /*
        SelenElem.Click(By.id("inv-mail2"));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("cp"), "06600");
        SelenElem.Click(By.xpath("//*[@id=\"longlat\"]"));
        OPG.cargaAjax2();
        SelenElem.Click(By.id("99090073cac"));
        reporte.AddImageToReport("Seleccionar opcion de envio", OPG.ScreenShotElementInstant());
        
        SelenElem.focus(By.id("js-continuar-metodos-envio"));
        reporte.AddImageToReport("", OPG.ScreenShotElementInstant());*/
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("card-name"), "Vianey Macias Nieves");
        
        //SelenElem.TypeText(By.xpath("//*[@id='stripe-payments-card-number']"), tarjeta);
        
        
        
        
        SelenElem.SwitchToFrame(By.xpath("//*[@id=\"stripe-payments-card-number\"]/div/iframe"));  
        SelenElem.Click(By.xpath("//*[@id='root']/form/span[2]/div/div[2]/span/input"));
        SelenElem.TypeText(By.xpath("//*[@id='root']/form/span[2]/div/div[2]/span/input"), tarjeta);
        SelenElem.BacktoMainFrame();
        
        
        
        
        SelenElem.SwitchToFrame(By.xpath("//*[@id=\"stripe-payments-card-expiry\"]/div/iframe"));
        SelenElem.TypeText(By.xpath("//*[@id=\"root\"]/form/span[2]/span/input"),vencimiento);
        SelenElem.BacktoMainFrame();
        
        
        SelenElem.SwitchToFrame(By.xpath("//*[@id=\"stripe-payments-card-cvc\"]/div/iframe"));
        SelenElem.TypeText(By.xpath("//*[@id=\"root\"]/form/span[2]/span/input"), cvv);
        //SelenElem.TypeText(By.xpath("//*[@id=\"root\"]/form/input[2]"), CargaVariables.LeerCSV(8,"Prepago con nuevo numero – Flap",Caso));
        SelenElem.BacktoMainFrame();
        
        OPG.cargaAjax2();
        
        SelenElem.Click(By.id("js-stripe-card"));
        
        SelenElem.WaitFor(By.id("selecPlan"));
        SelenElem.SelectOptionByValue(By.id("selecPlan"), "3");
        reporte.AddImageToReport("Sección “Medios de pago” con stripe", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("continuar-checkout-out-validation"));
        //OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.xpath("//*[@id=\"contenedor-card-mobile\"]/div[2]/div[2]/div[1]"), 50);
        reporte.AddImageToReport("Pagina de pago realizada con exito", OPG.ScreenShotElementInstant());
        SelenElem.focus(By.xpath("//*[@id=\"contenedor-card-mobile\"]/div[2]/div[2]/div[1]"));
        reporte.AddImageToReport("Pagina de pago realizada con exito", OPG.ScreenShotElementInstant());
        
        
    }
     
     public void ErrorCPPrepagoFlapSTRIPE (Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
    
        String telefono = CargaVariables.LeerCSV(0,"Prepago con nuevo numero – Flap",Caso);
        String nombre = CargaVariables.LeerCSV(1,"Prepago con nuevo numero – Flap",Caso);
        String apellidopa = CargaVariables.LeerCSV(2,"Prepago con nuevo numero – Flap",Caso);
        String apema = CargaVariables.LeerCSV(3,"Prepago con nuevo numero – Flap",Caso);
        String email = CargaVariables.LeerCSV(4,"Prepago con nuevo numero – Flap",Caso);
        String tel = CargaVariables.LeerCSV(5,"Prepago con nuevo numero – Flap",Caso);
        
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        reporte.AddImageToReport("Se selecciona el telefono", OPG.ScreenShotElementFocus(By.xpath("//a[@href='" + Url + "iphone-xr-lte-amarillo-64gb-apple.html']")));
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + telefono + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        Thread.sleep(3000);
        reporte.AddImageToReport("Ver detalle de Smartphone", OPG.SS());
        SelenElem.focus(By.id("solo-smartphone"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//p[.='Comprar']"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.xpath("//p[.='Comprar']"));
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        //SelenElem.focus(By.id("continueToCheckout"));
        
        SelenElem.Click(By.id("total-mobile"));
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 20);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidopa);
        reporte.AddImageToReport("Seccion de datos del checkout", OPG.SS());
        SelenElem.TypeText(By.id("valid-appelido-m"), apema);
        SelenElem.TypeText(By.id("valid-email"), email);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        SelenElem.Click(By.id("checkPrivacyOne"));      
        reporte.AddImageToReport("Seccion de datos del checkout", OPG.SS());
        SelenElem.Click(By.xpath("//button[@title='Continuar']"));
        /*
        SelenElem.Click(By.id("inv-mail2"));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("cp"), "0");
        da.hideKeyBoard();
        SelenElem.Click(By.xpath("//*[@id=\"longlat\"]"));
        Thread.sleep(500);
        reporte.AddImageToReport("Error en Código Postal", OPG.SS());
        Thread.sleep(3000);
        SelenElem.TypeText(By.id("cp"), "0000");
        da.hideKeyBoard();
        SelenElem.Click(By.xpath("//*[@id=\"longlat\"]"));
        OPG.cargaAjax2();
        Thread.sleep(500);
        reporte.AddImageToReport("Error en Código Postal", OPG.SS());*/
        SelenElem.WaitForLoad(By.id("inv-mail2"), 2);
        SelenElem.Click(By.id("inv-mail2"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("js_codigo_postal"), "0");
        da.hideKeyBoard();
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        Thread.sleep(500);
        reporte.AddImageToReport("Validacion Error en codigo postal", OPG.ScreenShotElementInstant());
        
        SelenElem.TypeText(By.id("js_codigo_postal"), "0000");
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        Thread.sleep(500);
        OPG.cargaAjax2();
        da.hideKeyBoard();
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        Thread.sleep(500);
        reporte.AddImageToReport("Validacion Error en codigo postal", OPG.ScreenShotElementInstant());
    
    }
     
    public void ErrorPagoPrepagoFlapSTRIPE (Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
    
        String telefono = CargaVariables.LeerCSV(0,"Prepago con nuevo numero – Flap",Caso);
        String nombre = CargaVariables.LeerCSV(1,"Prepago con nuevo numero – Flap",Caso);
        String apellidopa = CargaVariables.LeerCSV(2,"Prepago con nuevo numero – Flap",Caso);
        String apema = CargaVariables.LeerCSV(3,"Prepago con nuevo numero – Flap",Caso);
        String email = CargaVariables.LeerCSV(4,"Prepago con nuevo numero – Flap",Caso);
        String tel = CargaVariables.LeerCSV(5,"Prepago con nuevo numero – Flap",Caso);
        String tarjeta = CargaVariables.LeerCSV(6,"Prepago con nuevo numero – Flap",Caso);
        String vencimiento = CargaVariables.LeerCSV(7,"Prepago con nuevo numero – Flap",Caso);
        String cvv = CargaVariables.LeerCSV(8,"Prepago con nuevo numero – Flap",Caso);
        
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        reporte.AddImageToReport("Se selecciona el telefono", OPG.ScreenShotElementFocus(By.xpath("//a[@href='" + Url + "iphone-xr-lte-amarillo-64gb-apple.html']")));
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + telefono + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        Thread.sleep(3000);
        reporte.AddImageToReport("Ver detalle de Smartphone", OPG.SS());
        SelenElem.focus(By.id("solo-smartphone"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//p[.='Comprar']"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.xpath("//p[.='Comprar']"));
        
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        //SelenElem.focus(By.id("continueToCheckout"));
        
        SelenElem.Click(By.id("total-mobile"));
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 20);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidopa);
        reporte.AddImageToReport("Seccion de datos del checkout", OPG.SS());
        SelenElem.TypeText(By.id("valid-appelido-m"), apema);
        SelenElem.TypeText(By.id("valid-email"), email);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        SelenElem.Click(By.id("checkPrivacyOne"));      
        reporte.AddImageToReport("Seccion de datos del checkout", OPG.SS());
        SelenElem.Click(By.xpath("//button[@title='Continuar']"));
        
        /*SelenElem.Click(By.id("inv-mail2"));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("cp"), "06600");
        SelenElem.Click(By.xpath("//*[@id=\"longlat\"]"));
        OPG.cargaAjax2();
        SelenElem.Click(By.id("99090073cac"));
        reporte.AddImageToReport("Seleccionar opcion de envio", OPG.SS());
        
        SelenElem.focus(By.id("js-continuar-metodos-envio"));
        reporte.AddImageToReport("", OPG.ScreenShotElementInstant());*/
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("card-name"), "Vianey Macias Nieves"); 
        
        SelenElem.SwitchToFrameByTargetElement(By.xpath("//*[@id=\"root\"]/form/span[2]/div/div[2]/span/input"));        
        SelenElem.TypeText(By.xpath("//*[@id=\"root\"]/form/span[2]/div/div[2]/span/input"), "40000048400080");
        SelenElem.BacktoMainFrame();
        
        da.hideKeyBoard();
        SelenElem.SwitchToFrame(By.xpath("//*[@id=\"stripe-payments-card-expiry\"]/div/iframe"));
        SelenElem.Click(By.xpath("//*[@id=\"root\"]/form/span[2]/span/input"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Error forma de pago Tarjeta incompleta", OPG.ScreenShotElementInstant());
        SelenElem.TypeText(By.xpath("//*[@id=\"root\"]/form/span[2]/span/input"), vencimiento);
        SelenElem.BacktoMainFrame();
        
        
        SelenElem.Find(By.id("card-name")).clear(); 
        SelenElem.SwitchToFrameByTargetElement(By.xpath("//*[@id=\"root\"]/form/span[2]/div/div[2]/span/input"));        
        SelenElem.Find(By.xpath("//*[@id=\"root\"]/form/span[2]/div/div[2]/span/input")).clear();
        SelenElem.TypeText(By.xpath("//*[@id=\"root\"]/form/span[2]/div/div[2]/span/input"), tarjeta);
        da.hideKeyBoard();
        SelenElem.BacktoMainFrame();
        
        reporte.AddImageToReport("Error forma de pago Sin nombre del titular", OPG.ScreenShotElementInstant());
        
        SelenElem.TypeText(By.id("card-name"), "Vianey Macias Nieves"); 
        
        SelenElem.SwitchToFrame(By.xpath("//*[@id=\"stripe-payments-card-expiry\"]/div/iframe"));
        SelenElem.Click(By.xpath("//*[@id=\"root\"]/form/span[2]/span/input"));
        SelenElem.Find(By.xpath("//*[@id=\"root\"]/form/span[2]/span/input")).clear();
        SelenElem.BacktoMainFrame();
        reporte.AddImageToReport("Error forma de pago Sin fecha de vencimiento", OPG.ScreenShotElementInstant());
        
        
        SelenElem.SwitchToFrame(By.xpath("//*[@id=\"stripe-payments-card-expiry\"]/div/iframe"));
        SelenElem.Click(By.xpath("//*[@id=\"root\"]/form/span[2]/span/input"));
        SelenElem.TypeText(By.xpath("//*[@id=\"root\"]/form/span[2]/span/input"), "1221");
        Thread.sleep(200);
        da.hideKeyBoard();
        reporte.AddImageToReport("Error forma de pago Fecha de tarjeta expirada", OPG.ScreenShotElementInstant());
        SelenElem.BacktoMainFrame();
        
        SelenElem.SwitchToFrame(By.xpath("//*[@id=\"stripe-payments-card-expiry\"]/div/iframe"));
        SelenElem.Click(By.xpath("//*[@id=\"root\"]/form/span[2]/span/input"));
        SelenElem.Find(By.xpath("//*[@id=\"root\"]/form/span[2]/span/input")).clear();
        SelenElem.TypeText(By.xpath("//*[@id=\"root\"]/form/span[2]/span/input"), vencimiento);
        da.hideKeyBoard();
        reporte.AddImageToReport("Error forma de pago Sin CVV", OPG.ScreenShotElementInstant());
        SelenElem.BacktoMainFrame();
        
    
    }
    
     public void PrepagoSoloTerminalMPP(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
       String telefono = CargaVariables.LeerCSV(0,"Prepago con nuevo numero – Flap",Caso);
       String nombre = CargaVariables.LeerCSV(1,"Prepago con nuevo numero – Flap",Caso);
       String apellidop = CargaVariables.LeerCSV(2,"Prepago con nuevo numero – Flap",Caso);
       String apellidom = CargaVariables.LeerCSV(3,"Prepago con nuevo numero – Flap",Caso);
       String email = CargaVariables.LeerCSV(4,"Prepago con nuevo numero – Flap",Caso);
       String tel = CargaVariables.LeerCSV(5,"Prepago con nuevo numero – Flap",Caso);
       String rfc = CargaVariables.LeerCSV(6,"Prepago con nuevo numero – Flap",Caso);
       String cp = CargaVariables.LeerCSV(7,"Prepago con nuevo numero – Flap",Caso);
       String calle = CargaVariables.LeerCSV(8,"Prepago con nuevo numero – Flap",Caso);
       String numero = CargaVariables.LeerCSV(9,"Prepago con nuevo numero – Flap",Caso);
       String colonia = CargaVariables.LeerCSV(11,"Prepago con nuevo numero – Flap",Caso);
       String tarjeta = CargaVariables.LeerCSV(12,"Prepago con nuevo numero – Flap",Caso);
       String nombreTarjeta = CargaVariables.LeerCSV(13,"Prepago con nuevo numero – Flap",Caso);
       String expiracion = CargaVariables.LeerCSV(14,"Prepago con nuevo numero – Flap",Caso);
       String cvv = CargaVariables.LeerCSV(15,"Prepago con nuevo numero – Flap",Caso);
       
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        SelenElem.Click(By.className("icon"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionamos el flujo de Telefonos", OPG.ScreenShotElement());
        SelenElem.Click(By.xpath("//input[@value='Smartphones']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionamos el flujo de Telefonos", OPG.ScreenShotElement());
        
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        SelenElem.waitForLoadPage();
        Thread.sleep(5000);
        reporte.AddImageToReport("Se selecciona el telefono", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + telefono + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        Thread.sleep(3000);
        
        reporte.AddImageToReport("Ver detalle de Smartphone", OPG.SS());
        SelenElem.focus(By.id("solo-smartphone"));
        reporte.AddImageToReport("", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.xpath("//p[.='Comprar']"));
        
       SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        //SelenElem.focus(By.id("continueToCheckout"));
        
        SelenElem.Click(By.id("total-mobile"));
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        
        
//        SelenElem.Click(By.id("continueToCheckout"));
        SelenElem.Click(By.id("js_ocultar_mostrar_flecha"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 10);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        
       
        SelenElem.TypeText(By.id("valid-appelido-m"),apellidom);
        da.slide("Up", 1);
        
        SelenElem.TypeText(By.id("valid-email"), email);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        da.hideKeyBoard();
        da.slide("Up", 1);
        reporte.AddImageToReport("Sección de tus datos del checkout", OPG.ScreenShotElementInstant()); 
        
        
        SelenElem.Click(By.id("recibeFactura")); 
        SelenElem.WaitForLoad(By.id("valida-rfc"), 3);
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("numero-domicilio"), numero);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección de tus datos del checkout", OPG.ScreenShotElementInstant()); 
        
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.Click(By.id("checkPrivacyOne"));      
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección de tus datos del checkout", OPG.ScreenShotElementInstant()); 
        
        SelenElem.Click(By.xpath("//button[@title='Continuar']"));
        
        OPG.cargaAjax2();
        /*
        SelenElem.Click(By.id("inv-mail2"));
        OPG.cargaAjax2();
        
        SelenElem.TypeText(By.id("cp"), "06600");
        SelenElem.Click(By.xpath("//*[@id=\"longlat\"]"));
        OPG.cargaAjax2();
        
        SelenElem.Click(By.id("99090073cac"));
        reporte.AddImageToReport("Seleccionar opcion de envio", OPG.SS());*/
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        OPG.cargaAjax2();
        SelenElem.Click(By.xpath("//img[contains(@alt, 'Mercado-Pago')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        Thread.sleep(10000);
        //da.slide("Up", 2);
        reporte.AddImageToReport("Sección Medios de Pago ", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección Medios de Pago ", OPG.SS());
        SelenElem.Click(By.id("continuar-checkout-mp-pro")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        //OPG.cargaAjax2();
        
        SelenElem.WaitForLoad(By.xpath("//span[.='Nueva Tarjeta']"), 20);
        Thread.sleep(2000);
        reporte.AddImageToReport("Sección Medios de Pago MPP 1 ", OPG.SS());
        SelenElem.Click(By.xpath("//span[.='Nueva Tarjeta']"));
        
        SelenElem.WaitFor(By.id("card_number"));
        Thread.sleep(1000);
        SelenElem.Click(By.id("card_number"));
        SelenElem.TypeText(By.id("card_number"), tarjeta);
        SelenElem.Click(By.id("fullname"));
        SelenElem.TypeText(By.id("fullname"), nombreTarjeta);
        SelenElem.Click(By.id("input_expiration_date"));
        SelenElem.TypeText(By.id("input_expiration_date"), expiracion);
        SelenElem.Click(By.id("cvv"));
        SelenElem.TypeText(By.id("cvv"), cvv);
        
        da.slide("Up", 1);
        reporte.AddImageToReport("Sección Medios de Pago MPP 2 ", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección Medios de Pago MPP 2 ", OPG.SS());
        SelenElem.Click(By.xpath("//span[.='Continuar']"));
        Thread.sleep(2000);
        
        SelenElem.WaitForLoad(By.xpath("//p[.='1x']"), 0);
        reporte.AddImageToReport("Sección Medios de Pago MPP 3", OPG.SS());
        SelenElem.Click(By.xpath("//p[.='1x']"));
        
        SelenElem.WaitForLoad(By.id("email"), 0);
        SelenElem.Click(By.id("email"));
        SelenElem.TypeText(By.id("email"),"test_user_23645227@testuser.com");
        da.slide("Up", 1);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección Medios de Pago MPP 4", OPG.SS());
        SelenElem.Click(By.xpath("//span[.='Pagar']"));
        
        
        //SelenElem.WaitForLoad(By.xpath("//span[.='¡Listo! Se acreditó tu pago']"),10);
        Thread.sleep(6000);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.ScreenShotElementInstant());
        
        SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        
        /*SelenElem.Click(By.id("js-stripe-payment-method-oxxo"));
        reporte.AddImageToReport("Seccion medios de pago en oxxo", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Seccion medios de pago en oxxo", OPG.SS());
        SelenElem.Click(By.id("continuar-checkout-out-validation-oxxo"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.xpath("//p[.='Datos de envío']"), 40);
        reporte.AddImageToReport("Página de pago realizado con éxito  ", OPG.SS());
        
        SelenElem.SwitchToFrame(By.id("recibOXXO"));
        try{
            SelenElem.focus(By.xpath("//button[.='Imprimir']"));
        }catch(Exception ex){}
        
        SelenElem.BacktoMainFrame();
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.ScreenShotElementFocus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/div[3]/div[2]/div[3]/ul/li[1]")));
        */
     
    }
    
     public void PrepagoSIMRecargaTerminalLiferay(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
     {
  
        String Dn = CargaVariables.LeerCSV(0,"Prepago Solo SIM + Terminal + Recarga",Caso);
        String SkuRecarga = CargaVariables.LeerCSV(1,"Prepago Solo SIM + Terminal + Recarga",Caso);
        String Nombre = CargaVariables.LeerCSV(2,"Prepago Solo SIM + Terminal + Recarga",Caso);
        String ApellPa = CargaVariables.LeerCSV(3,"Prepago Solo SIM + Terminal + Recarga",Caso);
        String ApellMa = CargaVariables.LeerCSV(4,"Prepago Solo SIM + Terminal + Recarga",Caso);
        String Correo = CargaVariables.LeerCSV(5,"Prepago Solo SIM + Terminal + Recarga",Caso);
        String Telefono = CargaVariables.LeerCSV(6,"Prepago Solo SIM + Terminal + Recarga",Caso);
        String NumTarjeta = CargaVariables.LeerCSV(7,"Prepago Solo SIM + Terminal + Recarga",Caso);
        String MesTarjeta = CargaVariables.LeerCSV(8,"Prepago Solo SIM + Terminal + Recarga",Caso);
        String AñoTarjeta = CargaVariables.LeerCSV(9,"Prepago Solo SIM + Terminal + Recarga",Caso);
        String Cvv = CargaVariables.LeerCSV(10,"Prepago Solo SIM + Terminal + Recarga",Caso);
       
        WebDrive.WebDriver.navigate().to(Url + "liferay.php");
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        
        SelenElem.SelectOption(By.id("flowCode"), "portabilidad_prepago_login");
        SelenElem.Find(By.id("skuPlan")).clear();
        SelenElem.Find(By.id("namePlan")).clear();
        SelenElem.Find(By.id("skuRecarga")).clear();
        SelenElem.TypeText(By.id("skuRecarga"), SkuRecarga);
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.Find(By.id("skuDn")).clear();
        SelenElem.TypeText(By.id("skuDn"), Dn);
        SelenElem.Find(By.id("rfc")).clear();
        reporte.AddImageToReport("Se llena el formulario Liferay", OPG.SS());
        
        SelenElem.Click(By.name("Comprar"));
        
        SelenElem.WaitForLoad(By.id("nuevo"), 20);
        SelenElem.Click(By.id("nuevo"));
        reporte.AddImageToReport("Obtener nuevo numero", OPG.SS());
        
        SelenElem.Click(By.id("btnCapta"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 15);
        SelenElem.TypeText(By.id("valid-nombre"), Nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), ApellPa);
        SelenElem.TypeText(By.id("valid-appelido-m"), ApellMa);
        SelenElem.TypeText(By.id("valid-email"), Correo);
        SelenElem.TypeText(By.id("telefono-contacto"), Telefono);
        da.hideKeyBoard();
        SelenElem.Click(By.id("checkPrivacyOne"));
        reporte.AddImageToReport("Sección tus datos del checkout", OPG.SS());
        
        SelenElem.Click(By.id("btn_continuar"));
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 20);
        SelenElem.Click(By.id("inv-mail2"));
        SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("js_codigo_postal"), "06600");
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        SelenElem.WaitForLoad(By.id("99090073cac"), 10);
        Thread.sleep(3000);
        SelenElem.focus(By.id("99090073cac"));
        SelenElem.Click(By.id("99090073cac"));
        reporte.AddImageToReport("Sección opciones envío del checkout", OPG.SS());
        SelenElem.focus(By.id("99090088cac"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("end-step"));
        
        SelenElem.WaitForLoad(By.id("continuar-checkout-out"), 15);
        SelenElem.Click(By.xpath("//span[@class='data__total']"));
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        
        SelenElem.Click(By.id("continuar-checkout-out"));
        
        SelenElem.WaitForLoad(By.id("imagen-cliente"), 20);
        reporte.AddImageToReport("Medio de pago Flap", OPG.SS());
        SelenElem.focus(By.id("1"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.id("1"));
        
        SelenElem.WaitForLoad(By.id("numTarjeta"), 10);
        SelenElem.TypeText(By.id("numTarjeta"), NumTarjeta);
        SelenElem.SelectOption(By.id("vigenciames"), MesTarjeta);
        SelenElem.SelectOption(By.id("vigenciaanio"), AñoTarjeta);
        SelenElem.TypeText(By.id("cvv2"), Cvv);
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("continuar"));
        
        Thread.sleep(2000);
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.WaitForLoad(By.id("numero_portar"), 20);
        reporte.AddImageToReport("Pagina de pago realizada con exito", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        
        
     }        
}
