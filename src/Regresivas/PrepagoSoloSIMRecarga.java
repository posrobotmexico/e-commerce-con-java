
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.OpcionesEnvio;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class PrepagoSoloSIMRecarga {

     public PrepagoSoloSIMRecarga (Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
        
        String dn = CargaVariables.LeerCSV(0, "Prepago Solo SIM + Recarga", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Prepago Solo SIM + Recarga", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Prepago Solo SIM + Recarga", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Prepago Solo SIM + Recarga", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Prepago Solo SIM + Recarga", Caso);
        String correo = CargaVariables.LeerCSV(5, "Prepago Solo SIM + Recarga", Caso);
        String tel = CargaVariables.LeerCSV(6, "Prepago Solo SIM + Recarga", Caso);
        String calle = CargaVariables.LeerCSV(8, "Prepago Solo SIM + Recarga", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Prepago Solo SIM + Recarga", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Prepago Solo SIM + Recarga", Caso);
        String cp = CargaVariables.LeerCSV(7, "Prepago Solo SIM + Recarga", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Prepago Solo SIM + Recarga", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Prepago Solo SIM + Recarga", Caso);
        String ine = CargaVariables.LeerCSV(14, "Prepago Solo SIM + Recarga", Caso);
        String rfcc = CargaVariables.LeerCSV(15, "Prepago Solo SIM + Recarga", Caso);
        
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        //SelenElem.Click(By.linkText("Prepago"));
        WebDrive.WebDriver.navigate().to(Url + "prepago/oferta");
        reporte.AddImageToReport("Seleccionamos el flujo de Prepago", OPG.SS());
        
        SelenElem.Click(By.id("sin_numero"));
        reporte.AddImageToReport("Seleccionar quiero un número nuevo", OPG.SS());
        SelenElem.Click(By.id("btnArt"));
        
        SelenElem.Click(By.id("plan1"));
        reporte.AddImageToReport("Seleccionar sin Smartphone y continuar", OPG.SS());
        SelenElem.Click(By.id("continueWithoutSmartphone"));
        
        //OPG.cargaAjax2();
        reporte.AddImageToReport(" Elegir plan con recarga", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"parrilla_planes_form_sim\"]/div[1]/div/div[1]/div[4]/img"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.xpath("//button[.='Lo quiero']"));
        //SelenElem.Click(By.xpath("//*[@id=\"parrilla_planes_form_sim\"]/div[1]/div/div[1]/div[10]/button"));
        
        
        //OPG.cargaAjax2();
        SelenElem.WaitFor(By.id("continueToCheckout"));
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        SelenElem.focus(By.id("continueToCheckout"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.id("continueToCheckout"));
        

        WebDrive.WebDriver.navigate().to(Url + "flujo-ventas/prepago-sin-equipo/cart/");
        SelenElem.waitForLoadPage();
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 5);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        SelenElem.Click(By.id("checkPrivacyOne"));
        
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        SelenElem.Click(By.id("btn_continuar"));
        
        OPG.cargaAjax2();
        
        /*SelenElem.Click(By.id("inv-mail2"));
        SelenElem.WaitForLoad(By.id("cp"), 4);
        SelenElem.Click(By.id("cp"));
        SelenElem.TypeText(By.id("cp"), "06600");
        SelenElem.Click(By.id("longlat"));
        OPG.cargaAjax2(); 
        
        SelenElem.Click(By.id("99090073cac"));
        SelenElem.focus(By.id("99090005cac"));
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());*/
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        SelenElem.Click(By.id("end-step"));
        OPG.cargaAjax2(); 
        //SelenElem.focus(By.id("returnToCarts"));
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        SelenElem.Click(By.id("continuar-checkout-out"));
        OPG.cargaAjax2();
        new Flap( reporte, OPG, SelenElem, WebDrive);
        
        SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/h2"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/section[3]"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/footer/a"));
        reporte.AddImageToReport("", OPG.SS());

     }
}
