
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class ErrorElectorPospagoSIMmasTerminal {

    public ErrorElectorPospagoSIMmasTerminal(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
         
        String dn = CargaVariables.LeerCSV(0, "Error en Clave de Elector Pospago SIM + Terminal", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Error en Clave de Elector Pospago SIM + Terminal", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Error en Clave de Elector Pospago SIM + Terminal", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Error en Clave de Elector Pospago SIM + Terminal", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Error en Clave de Elector Pospago SIM + Terminal", Caso);
        String correo = CargaVariables.LeerCSV(5, "Error en Clave de Elector Pospago SIM + Terminal", Caso);
        String tel = CargaVariables.LeerCSV(6, "Error en Clave de Elector Pospago SIM + Terminal", Caso);
        String calle = CargaVariables.LeerCSV(8, "Error en Clave de Elector Pospago SIM + Terminal", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Error en Clave de Elector Pospago SIM + Terminal", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Error en Clave de Elector Pospago SIM + Terminal", Caso);
        String cp = CargaVariables.LeerCSV(7, "Error en Clave de Elector Pospago SIM + Terminal", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Error en Clave de Elector Pospago SIM + Terminal", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Error en Clave de Elector Pospago SIM + Terminal", Caso);
        String ine = CargaVariables.LeerCSV(14, "Error en Clave de Elector Pospago SIM + Terminal", Caso);
        String rfcc = CargaVariables.LeerCSV(15, "Error en Clave de Elector Pospago SIM + Terminal", Caso);
        

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Vemos los telefonos  ", OPG.SS());
        reporte.AddImageToReport("Se selecciona el telefono", OPG.ScreenShotElementFocus(By.xpath("//a[@href='" + Url + "iphone-xr-lte-amarillo-64gb-apple.html']")));
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + CargaVariables.LeerCSV(16,"Error en Clave de Elector Pospago SIM + Terminal",Caso) + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        
        SelenElem.waitForLoadPage();
        
        
        SelenElem.Click(By.id("soy-nuevo"));
        OPG.cargaAjax2();
        
        SelenElem.Click(By.id("js_next_mp_pospago"));
        
        Thread.sleep(1000);
        reporte.AddImageToReport("Ver detalle de Smartphone y seleccionar plan", OPG.SS());
        SelenElem.focus(By.id("js_submit_miniparrilla_pospago"));
        reporte.AddImageToReport("", OPG.SS());
        /*
        SelenElem.Click(By.xpath("//*[@id=\"tituloSelectorFlujoPadre\"]/div[1]/div[2]/label"));
        SelenElem.WaitForLoad(By.id("goSubmit_ptdb"), 4);
        Thread.sleep(2000);
        SelenElem.focus(By.className("mp-pospago__table-cell"));
        Thread.sleep(2000);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.id("js_submit_mp_pospago"));
        reporte.AddImageToReport("", OPG.SS());
        */
        SelenElem.Click(By.className("js_mp-pospago__table-cell"));
        
        
        Thread.sleep(2100);
        reporte.AddImageToReport("Ver detalle del Plan seleccionado", OPG.SS());
        SelenElem.Click(By.xpath("//div[.='Quiero este plan']"));
        
        SelenElem.Click(By.id("js_submit_miniparrilla_pospago"));
        /*
        SelenElem.Click(By.id("btn-buy-one"));
        reporte.AddImageToReport("Elegir plan Movistar", OPG.SS());
        SelenElem.Click(By.id("Plan-Video"));
        reporte.AddImageToReport("Sin SVA ", OPG.SS());
        SelenElem.Click(By.id("submitplanes"));
        */
        
        /*
        SelenElem.WaitForLoad(By.className("mini-ecorating__title"),2000);
        SelenElem.Click(By.id("js_submit_mp_pospago"));
        */
        
        SelenElem.WaitForLoad(By.id("continueToCheckout"), 10);
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElementFocus(By.id("continueToCheckout")));
        SelenElem.WaitForLoad(By.id("continueToCheckout"), 4);
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-email"), 10);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
         
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.focus(By.id("telefono-contacto"));
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), "1994");
        SelenElem.SelectOption(By.className("ui-datepicker-month"), "Mar");
        SelenElem.Click(By.linkText("3"));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("numero-domicilio-interior"), numInterior);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("INE-IFE"), "1234567890123456");
        SelenElem.Click(By.id("checkPrivacyOne"));
        SelenElem.Click(By.id("valid-ciudad"));
        SelenElem.Click(By.id("btn_continuar"));
        Thread.sleep(500);
        reporte.AddImageToReport("Error en INE menos de 18 caracteres", OPG.SS()); 
       
     }
}
