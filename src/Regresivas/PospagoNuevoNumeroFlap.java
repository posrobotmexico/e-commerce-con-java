
package Regresivas;

import Core.DeviceActions;
import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.FlapError;
import Operaciones.FlapErrorPospago;
import Operaciones.FlapErrorVariosCampos;
import Operaciones.FlapPorta;
import Operaciones.FlapPortaVariosCampos;
import Operaciones.OpcionesEnvio;
import Operaciones.OperacionesGenerales;
import io.appium.java_client.AppiumDriver;
import java.io.File;
import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

/**
 *
 * @author LAPTOP-JORGE
 */
public class PospagoNuevoNumeroFlap {
    
     public PospagoNuevoNumeroFlap(){
    
    }

     public void PospagoSOHOFianza0 (Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException{
       
        String nombre = CargaVariables.LeerCSV(0, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String apellidop = CargaVariables.LeerCSV(1, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String apellidom = CargaVariables.LeerCSV(2, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String correo = CargaVariables.LeerCSV(3, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String tel = CargaVariables.LeerCSV(4, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String calle = CargaVariables.LeerCSV(6, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String numdomicilio = CargaVariables.LeerCSV(7, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String numInterior = CargaVariables.LeerCSV(8, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String cp = CargaVariables.LeerCSV(5, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String ciudad = CargaVariables.LeerCSV(11, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String colonia = CargaVariables.LeerCSV(10, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String cfdi = CargaVariables.LeerCSV(15, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String ine = CargaVariables.LeerCSV(13, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String rfc = CargaVariables.LeerCSV(16, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String tarjeta = CargaVariables.LeerCSV(17, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String expiracion = CargaVariables.LeerCSV(18, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String cvv = CargaVariables.LeerCSV(19, "POSPAGO CON NUEVO NUMERO FLAP", Caso);

        WebDrive.WebDriver.navigate().to(Url + "negocios/");
        reporte.AddImageToReport("Iniciamos con la pagina principal", OPG.SS());
        SelenElem.Click(By.xpath("//a[@href='/negocios/parrillas']"));
        da.slide("Down", 1);
        reporte.AddImageToReport("Elegir plan movistar", OPG.SS());
        SelenElem.Click(By.xpath("//button[.='Lo quiero']"));
        //SelenElem.focus(By.id("submitplanes"));
        //reporte.AddImageToReport("Sin SVA", OPG.SS());
        //SelenElem.Click(By.id("submitplanes"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        SelenElem.Click(By.xpath("//button[@title='Da clic aquí para ir al carrito de compras']"));
        Thread.sleep(500);
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 40);
        SelenElem.waitForLoadPage();
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout ", OPG.SS());
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("numero-domicilio-interior"), numInterior);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.Click(By.id("valid-ciudad"));
        Thread.sleep(30000);
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        
        SelenElem.SelectOption(By.id("colonia"), colonia);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout ", OPG.SS());
        SelenElem.SelectOption(By.id("cfdi"), cfdi);
        SelenElem.SelectOption(By.id("typeIdentification"), "INE/IFE");
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        
        SelenElem.Click(By.id("INE-FILE"));
        AppiumDriver ad = (AppiumDriver) WebDrive.WebDriver;
        String Chrome = ad.getContext();
        ad.context("NATIVE_APP");
        Thread.sleep(3000);        
        ad.findElement(By.xpath("//*[@text='INE.pdf']")).click();
        Thread.sleep(3000);        
        ad.context(Chrome);
        
        SelenElem.Click(By.id("checkPrivacyOne"));
        SelenElem.focus(By.id("checkout-step4-check-02"));
        SelenElem.Click(By.id("checkout-step4-check-02"));
        da.slide("Up", 1);
        SelenElem.Click(By.id("archivoCEDULA"));
        OPG.cargaAjax2();
        SelenElem.Click(By.id("CEDULA-FILE"));
        ad.context("NATIVE_APP");
        Thread.sleep(3000);        
        ad.findElement(By.xpath("//*[@text='cedula.pdf']")).click();
        Thread.sleep(3000);        
        ad.context(Chrome);
        
        da.slide("Up", 1);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.Click(By.xpath("//button[@title='Da clic aquí para continuar a opciones de envío']"));
        SelenElem.ClickByXpath(WebDrive.WebDriver,"//button[@title='Da clic aquí para continuar a opciones de envío']");
        OPG.cargaAjax2();
        Thread.sleep(100000);   
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 6);
        
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        SelenElem.Click(By.xpath("//button[@title='Da clic aquí para continuar']"));
        OPG.cargaAjax2();  
        Thread.sleep(20000);
        SelenElem.Click(By.id("checkTerms"));
        SelenElem.focus(By.id("viewDelivery"));
        reporte.AddImageToReport("Sección Contrato del checkout", OPG.SS());
        SelenElem.Click(By.id("end-step"));
        OPG.cargaAjax2();
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        //SelenElem.Click(By.id("continuar-checkout-out"));
        SelenElem.ClickById(WebDrive.WebDriver, "continuar-checkout-out");
        
        SelenElem.WaitForLoad(By.id("validate_cc_owner"), 30);
        SelenElem.TypeText(By.name("cc_owner"), nombre);
        reporte.AddImageToReport("Sección Pago del Checkout", OPG.ScreenShotElementInstant());
        SelenElem.TypeText(By.name("cc_number"), tarjeta);
        SelenElem.TypeText(By.name("cc_exp_month"), expiracion);
        SelenElem.TypeText(By.name("cc_cid"), cvv);
        
        SelenElem.Click(By.id("end-step2"));
        Thread.sleep(2000);
        reporte.AddImageToReport("Sección Pago del Checkout", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("grandTotalMobile"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("continuar-checkout-out"));
        Thread.sleep(500);
        //reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.ScreenShotElementInstant());
     }

     public void ErrorCPPospagoAdiciones(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException{
         
          String dn = CargaVariables.LeerCSV(0, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
          String rfc = CargaVariables.LeerCSV(1, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
          String correo = CargaVariables.LeerCSV(2, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
          
          WebDrive.WebDriver.navigate().to(Url);
          reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
     
          WebDrive.WebDriver.navigate().to(Url + "adiciones");
          reporte.AddImageToReport("Ingresamos al flujo adiciones", OPG.SS());
          
          SelenElem.TypeText(By.id("dn_movistar"), dn);
          OPG.cargaAjax2();
          reporte.AddImageToReport("Ingresa DN", OPG.SS());
          
          
          SelenElem.Click(By.id("btn_additional"));
          SelenElem.TypeText(By.id("valida-rfc"), rfc);
          SelenElem.TypeText(By.id("valida-rfc"), "1");
          reporte.AddImageToReport("Ingresar RFC", OPG.SS());
          SelenElem.Click(By.id("btn_additional"));
          
          //SelenElem.TypeText(By.id("otp-input"), "123456");
          SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
          SelenElem.TypeText(By.id("firstCharacter"), "1");
          SelenElem.TypeText(By.id("secondCharacter"), "2");
          SelenElem.TypeText(By.id("thirdCharacter"), "3");
          SelenElem.TypeText(By.id("fourthCharacter"), "4");
          SelenElem.TypeText(By.id("fifthCharacter"), "5");
          SelenElem.TypeText(By.id("sixthCharacter"), "6");
          Thread.sleep(4000);
          da.hideKeyBoard();
          reporte.AddImageToReport("Validar identidad", OPG.SS());
          SelenElem.Click(By.id("continue-btn2"));
          
          SelenElem.Click(By.id("Plan-ilimitado-Plus"));
          reporte.AddImageToReport("Elegir plan", OPG.SS());
          SelenElem.focus(By.id("submitplanes"));
          reporte.AddImageToReport("Sin SVA", OPG.SS());
          SelenElem.Click(By.id("submitplanes"));
          da.slide("Up", 1);
          reporte.AddImageToReport("Resumen de compra", OPG.SS());
          SelenElem.Click(By.id("total-mobile"));
          Thread.sleep(1000);
          reporte.AddImageToReport("Resumen de compra", OPG.SS());
     
          SelenElem.Click(By.id("continueToCheckout"));
          SelenElem.waitForLoadPage();
          SelenElem.focus(By.id("valid-email"));
          SelenElem.Click(By.id("valid-email"));
          SelenElem.TypeText(By.id("valid-email"), correo);
          da.hideKeyBoard();
          da.slide("Up", 1);
          reporte.AddImageToReport("Sección tus datos del checkout", OPG.SS());
          
          SelenElem.Click(By.id("btn_continuar"));
          OPG.cargaAjax2();
          SelenElem.Click(By.id("inv-mail2"));
          Thread.sleep(2000);
          SelenElem.TypeText(By.id("js_codigo_postal"), "066");
          SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
          Thread.sleep(500);
          reporte.AddImageToReport("Error Código Postal", OPG.SS());
     
     }

     public void ErrorPagoPospagoAdiciones(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException{
          
          String dn = CargaVariables.LeerCSV(0, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
          String rfc = CargaVariables.LeerCSV(1, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
          String correo = CargaVariables.LeerCSV(2, "POSPAGO CON NUEVO NUMERO FLAP", Caso);

          WebDrive.WebDriver.navigate().to(Url);
          reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
     
          WebDrive.WebDriver.navigate().to(Url + "adiciones");
          reporte.AddImageToReport("Ingresamos al flujo adiciones", OPG.SS());
          
          SelenElem.TypeText(By.id("dn_movistar"), dn);
          OPG.cargaAjax2();
          reporte.AddImageToReport("Ingresa DN", OPG.SS());
          
          
          SelenElem.Click(By.id("btn_additional"));
          SelenElem.TypeText(By.id("valida-rfc"), rfc);
          SelenElem.TypeText(By.id("valida-rfc"), "1");
          reporte.AddImageToReport("Ingresar RFC", OPG.SS());
          SelenElem.Click(By.id("btn_additional"));
          
          //SelenElem.TypeText(By.id("otp-input"), "123456");
          SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
          SelenElem.TypeText(By.id("firstCharacter"), "1");
          SelenElem.TypeText(By.id("secondCharacter"), "2");
          SelenElem.TypeText(By.id("thirdCharacter"), "3");
          SelenElem.TypeText(By.id("fourthCharacter"), "4");
          SelenElem.TypeText(By.id("fifthCharacter"), "5");
          SelenElem.TypeText(By.id("sixthCharacter"), "6");
          Thread.sleep(4000);
          da.hideKeyBoard();
          reporte.AddImageToReport("Validar identidad", OPG.SS());
          SelenElem.Click(By.id("continue-btn2"));
          
          SelenElem.Click(By.id("Plan-ilimitado-Plus"));
          reporte.AddImageToReport("Elegir plan", OPG.SS());
          SelenElem.focus(By.id("submitplanes"));
          reporte.AddImageToReport("Sin SVA", OPG.SS());
          SelenElem.Click(By.id("submitplanes"));
          da.slide("Up", 1);
          reporte.AddImageToReport("Resumen de compra", OPG.SS());
          SelenElem.Click(By.id("total-mobile"));
          Thread.sleep(1000);
          reporte.AddImageToReport("Resumen de compra", OPG.SS());
     
          SelenElem.Click(By.id("continueToCheckout"));
          SelenElem.waitForLoadPage();
          SelenElem.focus(By.id("valid-email"));
          SelenElem.Click(By.id("valid-email"));
          SelenElem.TypeText(By.id("valid-email"), correo);
          da.hideKeyBoard();
          da.slide("Up", 1);
          reporte.AddImageToReport("Sección tus datos del checkout", OPG.SS());
          
          SelenElem.Click(By.id("btn_continuar"));
          OPG.cargaAjax2();
          SelenElem.Click(By.id("inv-mail2"));
          Thread.sleep(2000);
          SelenElem.TypeText(By.id("js_codigo_postal"), "06600");
          SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
          OPG.cargaAjax2();   
          SelenElem.Click(By.id("99090073cac"));
          da.slide("Up", 2);
          reporte.AddImageToReport("Sección opciones de envio del checkout", OPG.SS());
          da.slide("Down", 2);
          reporte.AddImageToReport("Sección opciones de envio del checkout", OPG.SS());
          
          Thread.sleep(2000);
          SelenElem.Click(By.id("btn_continuar_2"));
          OPG.cargaAjax2();   
          SelenElem.Click(By.id("checkConsentimiento"));
          SelenElem.Click(By.id("checkTerms"));
          da.slide("Up", 1);
          reporte.AddImageToReport("Sección Contrato del checkout", OPG.SS());
          
          Thread.sleep(1000);
          SelenElem.Click(By.id("end-step"));
          Thread.sleep(1000);
          da.slide("Up", 2);
          reporte.AddImageToReport("Resumen de compra", OPG.SS());
          SelenElem.Click(By.id("total-mobile"));
          Thread.sleep(1000);
          reporte.AddImageToReport("Resumen de compra", OPG.SS());
          
          
          SelenElem.Click(By.id("pay"));
          OPG.cargaAjax2();
          new FlapErrorVariosCampos( reporte, OPG, SelenElem, WebDrive);
     }

      public void PospagoAdiciones(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException{
          
          String dn = CargaVariables.LeerCSV(0, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
          String rfc = CargaVariables.LeerCSV(1, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
          String correo = CargaVariables.LeerCSV(2, "POSPAGO CON NUEVO NUMERO FLAP", Caso);

          WebDrive.WebDriver.navigate().to(Url);
          reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
     
          WebDrive.WebDriver.navigate().to(Url + "adiciones");
          reporte.AddImageToReport("Ingresamos al flujo adiciones", OPG.SS());
          
          SelenElem.TypeText(By.id("dn_movistar"), dn);
          OPG.cargaAjax2();
          reporte.AddImageToReport("Ingresa DN", OPG.SS());
          
          
          SelenElem.Click(By.id("btn_additional"));
          SelenElem.TypeText(By.id("valida-rfc"), rfc);
          SelenElem.TypeText(By.id("valida-rfc"), "1");
          reporte.AddImageToReport("Ingresar RFC", OPG.SS());
          SelenElem.Click(By.id("btn_additional"));
          
          //SelenElem.TypeText(By.id("otp-input"), "123456");
          SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
          SelenElem.TypeText(By.id("firstCharacter"), "1");
          SelenElem.TypeText(By.id("secondCharacter"), "2");
          SelenElem.TypeText(By.id("thirdCharacter"), "3");
          SelenElem.TypeText(By.id("fourthCharacter"), "4");
          SelenElem.TypeText(By.id("fifthCharacter"), "5");
          SelenElem.TypeText(By.id("sixthCharacter"), "6");
          Thread.sleep(4000);
          da.hideKeyBoard();
          reporte.AddImageToReport("Validar identidad", OPG.SS());
          SelenElem.Click(By.id("continue-btn2"));
          
          SelenElem.Click(By.id("Plan-ilimitado-Plus"));
          reporte.AddImageToReport("Elegir plan", OPG.SS());
          SelenElem.focus(By.id("submitplanes"));
          reporte.AddImageToReport("Sin SVA", OPG.SS());
          SelenElem.Click(By.id("submitplanes"));
          da.slide("Up", 1);
          reporte.AddImageToReport("Resumen de compra", OPG.SS());
          SelenElem.Click(By.id("total-mobile"));
          Thread.sleep(1000);
          reporte.AddImageToReport("Resumen de compra", OPG.SS());
     
          SelenElem.Click(By.id("continueToCheckout"));
          SelenElem.waitForLoadPage();
          SelenElem.focus(By.id("valid-email"));
          SelenElem.Click(By.id("valid-email"));
          SelenElem.TypeText(By.id("valid-email"), correo);
          da.hideKeyBoard();
          da.slide("Up", 1);
          reporte.AddImageToReport("Sección tus datos del checkout", OPG.SS());
          
          SelenElem.Click(By.id("btn_continuar"));
          OPG.cargaAjax2();
          SelenElem.Click(By.id("inv-mail2"));
          Thread.sleep(2000);
          SelenElem.TypeText(By.id("js_codigo_postal"), "06600");
          SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
          OPG.cargaAjax2();   
          SelenElem.Click(By.id("99090073cac"));
          da.slide("Up", 2);
          reporte.AddImageToReport("Sección opciones de envio del checkout", OPG.SS());
          da.slide("Down", 2);
          reporte.AddImageToReport("Sección opciones de envio del checkout", OPG.SS());
          
          Thread.sleep(2000);
          SelenElem.Click(By.id("btn_continuar_2"));
          OPG.cargaAjax2();   
          SelenElem.Click(By.id("checkConsentimiento"));
          SelenElem.Click(By.id("checkTerms"));
          da.slide("Up", 1);
          reporte.AddImageToReport("Sección Contrato del checkout", OPG.SS());
          
          Thread.sleep(1000);
          SelenElem.Click(By.id("end-step"));
          Thread.sleep(1000);
          da.slide("Up", 2);
          reporte.AddImageToReport("Resumen de compra", OPG.SS());
          SelenElem.Click(By.id("total-mobile"));
          Thread.sleep(1000);
          reporte.AddImageToReport("Resumen de compra", OPG.SS());
          
          
          SelenElem.Click(By.id("pay"));
          OPG.cargaAjax2();
          new Flap( reporte, OPG, SelenElem, WebDrive);
          SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
          reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
          da.slide("Down", 2);
          reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
          da.slide("Down", 2);
          reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
          
     
      }
     public void PospagoLiferay(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException{
         
        String skuterminal = CargaVariables.LeerCSV(0, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String rfc = CargaVariables.LeerCSV(1, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String correo = CargaVariables.LeerCSV(5, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String tel = CargaVariables.LeerCSV(6, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String calle = CargaVariables.LeerCSV(8, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String cp = CargaVariables.LeerCSV(7, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String colonia = CargaVariables.LeerCSV(12, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String ine = CargaVariables.LeerCSV(14, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String rfcc = CargaVariables.LeerCSV(15, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String skuplan = CargaVariables.LeerCSV(16, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String dia = CargaVariables.LeerCSV(17, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String mes = CargaVariables.LeerCSV(18, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String año = CargaVariables.LeerCSV(19, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        
        WebDrive.WebDriver.navigate().to(Url + "liferay.php");
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        Thread.sleep(2000);
        SelenElem.SelectOption(By.id("flowCode"), "pospago");
        Thread.sleep(1000);
        SelenElem.Find(By.id("skuTerminal")).clear();
        //SelenElem.TypeText(By.id("skuTerminal"), skuterminal);
        SelenElem.Find(By.id("skuPlan")).clear();
        SelenElem.TypeText(By.id("skuPlan"), skuplan);
        SelenElem.Find(By.id("namePlan")).clear();
        SelenElem.Find(By.id("skuRecarga")).clear();
        SelenElem.Find(By.id("monto")).clear();
        SelenElem.Find(By.id("skuDn")).clear();
        SelenElem.Find(By.id("rfc")).clear();
        reporte.AddImageToReport("Ingresamos los datos y damos clic en enviar", OPG.SS());
        SelenElem.Click(By.name("Comprar"));
        /*
        SelenElem.WaitForLoad(By.id("total-mobile"), 40);
       
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElementInstant());
         SelenElem.Click(By.id("total-mobile"));
        SelenElem.WaitForLoad(By.id("continueToCheckout"), 10);
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("continueToCheckout"));
        */
        SelenElem.WaitForLoad(By.id("valid-email"), 20);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
         
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        da.hideKeyBoard();
        //da.slide("Up", 2);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        //da.slide("Down", 2);
        //reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), año);
        SelenElem.SelectOption(By.className("ui-datepicker-month"), mes);
        SelenElem.Click(By.linkText(dia));
        Thread.sleep(5000);
        //OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        //SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        SelenElem.Click(By.id("calle-domicilio"));
        Thread.sleep(5000);
        
        OPG.cargaAjax2();
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("numero-domicilio-interior"), numInterior);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.Click(By.id("btn_continuar"));
        Thread.sleep(5000);
        //OPG.cargaAjax2();
        /*SelenElem.WaitForLoad(By.id("cod"), 40);
        reporte.AddImageToReport("Sección consulta tu crédito del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("cod"), "1111");
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        SelenElem.Click(By.id("checkout-step2-radio-04"));
        SelenElem.Click(By.id("checkout-step2-radio-06"));
        SelenElem.Click(By.id("checkout-step2"));
        reporte.AddImageToReport("Sección consulta tu crédito del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("btn_continuar_2"));
        
        */
        Thread.sleep(3000);
        SelenElem.WaitForLoad(By.id("inv-mail2"), 15);
        SelenElem.Click(By.id("inv-mail2"));
        SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("js_codigo_postal"), "06600");
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección opciones de envio del checkout", OPG.SS());
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        SelenElem.WaitForLoad(By.id("99090073cac"), 10);
        SelenElem.Click(By.id("99090073cac"));
        reporte.AddImageToReport("Sección opciones de envio del checkout", OPG.SS());
        
        SelenElem.Click(By.id("btn_continuar_3"));
        Thread.sleep(5000);
        //SelenElem.Click(By.id("checkout-step4-check-01"));
        SelenElem.Click(By.id("checkTerms"));
        da.slide("Up", 1);
        reporte.AddImageToReport("Sección contrato del checkout", OPG.SS());
        Thread.sleep(2500);
        SelenElem.Click(By.id("end-step"));
        Thread.sleep(3500);
        reporte.AddImageToReport("Resumen de compra1", OPG.SS());
        Thread.sleep(2500);
        SelenElem.Click(By.id("sumary_button"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        SelenElem.Click(By.id("continuar-checkout-out"));
        
        SelenElem.WaitForLoad(By.id("imagen-cliente"),10);
        reporte.AddImageToReport("Ingresamos a la pagina principal de FLAP", OPG.ScreenShotElement());
        SelenElem.focus(By.id("1"));
        reporte.AddImageToReport("", OPG.ScreenShotElement());
        SelenElem.Click(By.id("1"));
        SelenElem.WaitForLoad(By.id("numTarjeta"), 10);
        SelenElem.TypeText(By.id("numTarjeta"), CargaVariables.LeerCSV(2,"Config","Tarjeta_Flap"));
        SelenElem.TypeText(By.id("cvv2"), CargaVariables.LeerCSV(4,"Config","Tarjeta_Flap"));
        SelenElem.SelectOption(By.id("vigenciames"), CargaVariables.LeerCSV(5,"Config","Tarjeta_Flap"));
        SelenElem.SelectOption(By.id("vigenciaanio"), CargaVariables.LeerCSV(6,"Config","Tarjeta_Flap"));
        reporte.AddImageToReport("Llenamos los datos de pago y damos click en pagar", OPG.ScreenShotElement());
        SelenElem.Click(By.id("continuar"));
        
        Thread.sleep(2000);
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.ScreenShotElement());
        
        SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.ScreenShotElementInstant());
        //*/

     }

     public void ErrorINEPospagoLiferay(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException{
         
        String skuterminal = CargaVariables.LeerCSV(0, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String rfc = CargaVariables.LeerCSV(1, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String correo = CargaVariables.LeerCSV(5, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String tel = CargaVariables.LeerCSV(6, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String calle = CargaVariables.LeerCSV(8, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String cp = CargaVariables.LeerCSV(7, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String colonia = CargaVariables.LeerCSV(12, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String ine = CargaVariables.LeerCSV(14, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String rfcc = CargaVariables.LeerCSV(15, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String skuplan = CargaVariables.LeerCSV(16, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String dia = CargaVariables.LeerCSV(17, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String mes = CargaVariables.LeerCSV(18, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String año = CargaVariables.LeerCSV(19, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        

        WebDrive.WebDriver.navigate().to(Url + "liferay.php");
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        Thread.sleep(2000);
        SelenElem.SelectOption(By.id("flowCode"), "pospago");
        Thread.sleep(1000);
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.TypeText(By.id("skuTerminal"), skuterminal);
        SelenElem.Find(By.id("skuPlan")).clear();
        SelenElem.TypeText(By.id("skuPlan"), skuplan);
        SelenElem.Find(By.id("skuRecarga")).clear();
        SelenElem.Find(By.id("monto")).clear();
        SelenElem.Find(By.id("skuDn")).clear();
        reporte.AddImageToReport("Ingresamos los datos y damos clic en enviar", OPG.SS());
        SelenElem.Click(By.name("Comprar"));
        
        SelenElem.WaitForLoad(By.id("total-mobile"), 40);
       
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElementInstant());
         SelenElem.Click(By.id("total-mobile"));
        SelenElem.WaitForLoad(By.id("continueToCheckout"), 10);
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.waitForLoadPage();
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
         
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        da.hideKeyBoard();
        da.slide("Up", 2);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
         SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), año);
        SelenElem.SelectOption(By.className("ui-datepicker-month"), mes);
        SelenElem.Click(By.linkText(dia));
        Thread.sleep(50000);
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        //SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        SelenElem.Click(By.id("calle-domicilio"));
        Thread.sleep(10000);
        
        OPG.cargaAjax2();
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("numero-domicilio-interior"), numInterior);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.Click(By.id("checkout-step4-check-02"));
        
        
        SelenElem.Click(By.id("btn_continuar"));
        Thread.sleep(500);
        reporte.AddImageToReport("Error INE 18 digitos", OPG.ScreenShotElementInstant());
     }

     public void ErrorCPPospagoLiferay(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException{
         
        String skuterminal = CargaVariables.LeerCSV(0, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String rfc = CargaVariables.LeerCSV(1, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String correo = CargaVariables.LeerCSV(5, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String tel = CargaVariables.LeerCSV(6, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String calle = CargaVariables.LeerCSV(8, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String cp = CargaVariables.LeerCSV(7, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String colonia = CargaVariables.LeerCSV(12, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String ine = CargaVariables.LeerCSV(14, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String rfcc = CargaVariables.LeerCSV(15, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String skuplan = CargaVariables.LeerCSV(16, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String dia = CargaVariables.LeerCSV(17, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String mes = CargaVariables.LeerCSV(18, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String año = CargaVariables.LeerCSV(19, "POSPAGO CON NUEVO NUMERO FLAP", Caso);

        WebDrive.WebDriver.navigate().to(Url + "liferay.php");
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        Thread.sleep(2000);
        SelenElem.SelectOption(By.id("flowCode"), "pospago");
        Thread.sleep(1000);
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.TypeText(By.id("skuTerminal"), skuterminal);
        SelenElem.Find(By.id("skuPlan")).clear();
        SelenElem.TypeText(By.id("skuPlan"), skuplan);
        SelenElem.Find(By.id("skuRecarga")).clear();
        SelenElem.Find(By.id("monto")).clear();
        SelenElem.Find(By.id("skuDn")).clear();
        reporte.AddImageToReport("Ingresamos los datos y damos clic en enviar", OPG.SS());
        SelenElem.Click(By.name("Comprar"));
        
        SelenElem.WaitForLoad(By.id("total-mobile"), 40);
       
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElementInstant());
         SelenElem.Click(By.id("total-mobile"));
        SelenElem.WaitForLoad(By.id("continueToCheckout"), 10);
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.waitForLoadPage();
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
         
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        da.hideKeyBoard();
        da.slide("Up", 2);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
         SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), año);
        SelenElem.SelectOption(By.className("ui-datepicker-month"), mes);
        SelenElem.Click(By.linkText(dia));
        Thread.sleep(50000);
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        //SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        SelenElem.Click(By.id("calle-domicilio"));
        Thread.sleep(10000);
        
        OPG.cargaAjax2();
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("numero-domicilio-interior"), numInterior);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.Click(By.id("btn_continuar"));
        Thread.sleep(50000);
        //OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("cod"), 40);
        reporte.AddImageToReport("Sección consulta tu crédito del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("cod"), "1111");
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        SelenElem.Click(By.id("checkout-step2-radio-04"));
        SelenElem.Click(By.id("checkout-step2-radio-06"));
        SelenElem.Click(By.id("checkout-step2"));
        reporte.AddImageToReport("Sección consulta tu crédito del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("btn_continuar_2"));
        Thread.sleep(150000);
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 2);
        SelenElem.Click(By.id("inv-mail2"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("js_codigo_postal"), "022");
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        Thread.sleep(500);
        reporte.AddImageToReport("Validacion Error en codigo postal", OPG.ScreenShotElementInstant());
        
     }

     public void ErrorPagoPospagoLiferay(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException{
         
        String skuterminal = CargaVariables.LeerCSV(0, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String rfc = CargaVariables.LeerCSV(1, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String correo = CargaVariables.LeerCSV(5, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String tel = CargaVariables.LeerCSV(6, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String calle = CargaVariables.LeerCSV(8, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String cp = CargaVariables.LeerCSV(7, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String colonia = CargaVariables.LeerCSV(12, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String ine = CargaVariables.LeerCSV(14, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String rfcc = CargaVariables.LeerCSV(15, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String skuplan = CargaVariables.LeerCSV(16, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String dia = CargaVariables.LeerCSV(17, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String mes = CargaVariables.LeerCSV(18, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String año = CargaVariables.LeerCSV(19, "POSPAGO CON NUEVO NUMERO FLAP", Caso);

        WebDrive.WebDriver.navigate().to(Url + "liferay.php");
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        Thread.sleep(2000);
        SelenElem.SelectOption(By.id("flowCode"), "pospago");
        Thread.sleep(1000);
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.TypeText(By.id("skuTerminal"), skuterminal);
        SelenElem.Find(By.id("skuPlan")).clear();
        SelenElem.TypeText(By.id("skuPlan"), skuplan);
        SelenElem.Find(By.id("skuRecarga")).clear();
        SelenElem.Find(By.id("monto")).clear();
        SelenElem.Find(By.id("skuDn")).clear();
        reporte.AddImageToReport("Ingresamos los datos y damos clic en enviar", OPG.SS());
        SelenElem.Click(By.name("Comprar"));
        
        SelenElem.WaitForLoad(By.id("total-mobile"), 40);
       
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElementInstant());
         SelenElem.Click(By.id("total-mobile"));
        SelenElem.WaitForLoad(By.id("continueToCheckout"), 10);
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.waitForLoadPage();
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
         
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        da.hideKeyBoard();
        da.slide("Up", 2);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
         SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), año);
        SelenElem.SelectOption(By.className("ui-datepicker-month"), mes);
        SelenElem.Click(By.linkText(dia));
        Thread.sleep(50000);
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        //SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        SelenElem.Click(By.id("calle-domicilio"));
        Thread.sleep(10000);
        
        OPG.cargaAjax2();
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("numero-domicilio-interior"), numInterior);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.Click(By.id("btn_continuar"));
        Thread.sleep(50000);
        //OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("cod"), 40);
        reporte.AddImageToReport("Sección consulta tu crédito del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("cod"), "1111");
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        SelenElem.Click(By.id("checkout-step2-radio-04"));
        SelenElem.Click(By.id("checkout-step2-radio-06"));
        SelenElem.Click(By.id("checkout-step2"));
        reporte.AddImageToReport("Sección consulta tu crédito del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("btn_continuar_2"));
        Thread.sleep(150000);
        
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        
        SelenElem.Click(By.id("btn_continuar_3"));
        OPG.cargaAjax2();
        SelenElem.Click(By.id("checkout-step4-check-01"));
        SelenElem.Click(By.id("checkTerms"));
        reporte.AddImageToReport("Sección contrato del checkout", OPG.SS());
        Thread.sleep(2500);
        SelenElem.Click(By.id("end-step"));
        Thread.sleep(3500);
        reporte.AddImageToReport("Resumen de compra1", OPG.SS());
        Thread.sleep(2500);
        SelenElem.Click(By.id("grandTotalMobile"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("continuar-checkout-out"));
        //SelenElem.ClickById(WebDrive.WebDriver, "continuar-checkout-out");
        //OPG.cargaAjax2();|ñ{
          
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("logo_client_mobile"),10);
        reporte.AddImageToReport("Ingresamos a la pagina principal de FLAP", OPG.ScreenShotElement());
        SelenElem.WaitForLoad(By.id("mp2-detail-title"),10);
        SelenElem.Click(By.id("mp2-detail-title"));
        SelenElem.loadElement(By.id("mp2-detail-table-last-tr"));
        SelenElem.focus(By.id("mp2-detail-table-last-tr"));   
        reporte.AddImageToReport("Vemos el detalle", OPG.ScreenShotElement());
        
        SelenElem.focus(By.id("numTarjeta"));          
        //SelenElem.TypeText(By.id("numTarjeta"), CargaVariables.LeerCSV(2,"Config","Tarjeta_Flap"));
        SelenElem.TypeText(By.id("numTarjeta"), "5673532672832");
        SelenElem.TypeText(By.id("cvv2"), CargaVariables.LeerCSV(4,"Config","Tarjeta_Flap"));  
        Thread.sleep(2000);
        da.hideKeyBoard();
        SelenElem.Click(By.id("continuar"));
        Thread.sleep(2000);
        da.slide("Down", 1);
        reporte.AddImageToReport("Verificacion de error en el metodo de pago Número de tarjeta incompleto", OPG.ScreenShotElementInstant());
        
        while(!SelenElem.Find(By.id("numTarjeta")).getAttribute("value").isEmpty())
            SelenElem.TypeText(By.id("numTarjeta"), Keys.BACK_SPACE);
        SelenElem.TypeText(By.id("numTarjeta"), CargaVariables.LeerCSV(2,"Config","Tarjeta_Flap"));
        
        SelenElem.SelectOption(By.id("vigenciames"), "1");
        SelenElem.TypeText(By.id("cvv2"), CargaVariables.LeerCSV(4,"Config","Tarjeta_Flap"));
        Thread.sleep(2000);
        da.hideKeyBoard();
        SelenElem.Click(By.id("continuar"));
        Thread.sleep(5000);
        da.slide("Down", 1);
        reporte.AddImageToReport("Verificacion de error en el metodo de pago Fecha anterior a la actual", OPG.ScreenShotElementInstant());
       
        SelenElem.TypeText(By.id("numTarjeta"), CargaVariables.LeerCSV(2,"Config","Tarjeta_Flap"));
        Thread.sleep(2000);
        da.hideKeyBoard();
        SelenElem.Click(By.id("continuar"));
        Thread.sleep(5000);
        da.slide("Down", 1);
        reporte.AddImageToReport("Verificacion de error en el metodo de pago Sin CVV", OPG.ScreenShotElementInstant());
       
        
        
     }

     public void PospagoSIMTerminal(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException{
        
        String dn = CargaVariables.LeerCSV(0, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String rfc = CargaVariables.LeerCSV(1, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String correo = CargaVariables.LeerCSV(5, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String tel = CargaVariables.LeerCSV(6, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String calle = CargaVariables.LeerCSV(8, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String cp = CargaVariables.LeerCSV(7, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String colonia = CargaVariables.LeerCSV(12, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String ine = CargaVariables.LeerCSV(14, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String rfcc = CargaVariables.LeerCSV(15, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        
        reporte.AddImageToReport("Iniciamos desde la pagina principal y seleccionamos telefonos", OPG.ScreenShotElement());
        SelenElem.Click(By.className("icon"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionamos el flujo de Telefonos", OPG.ScreenShotElement());
        SelenElem.Click(By.xpath("//input[@value='Smartphones']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionamos el flujo de Telefonos", OPG.ScreenShotElement());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        reporte.AddImageToReport("Seleccionamos la opción Telefonos", OPG.ScreenShotElement());
        
        Thread.sleep(2000);
        da.slide("Down", 2 );
        reporte.AddImageToReport("Vemos los telefonos  ", OPG.SS());
        da.slide("Down", 1 );
        reporte.AddImageToReport("Se selecciona el telefono", OPG.SS());
        
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + CargaVariables.LeerCSV(16,"POSPAGO CON NUEVO NUMERO FLAP",Caso) + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        Thread.sleep(2000);
        reporte.AddImageToReport("Ver detalle de Smartphone y seleccionar plan", OPG.SS());
        SelenElem.waitForLoadPage();
        
        /////
        SelenElem.Click(By.id("soy-nuevo"));
        OPG.cargaAjax2();
        Thread.sleep(4000);
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//*[@id=\"js_slider_mp_pospago\"]/div/div/div[5]/div/div/div/div"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//div[.='Quiero este plan']"));
        
        Thread.sleep(3000);
        
        SelenElem.Click(By.id("js_submit_miniparrilla_pospago"));
        SelenElem.WaitForLoad(By.id("total-mobile"), 10);
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElementInstant());
         SelenElem.Click(By.id("total-mobile"));
        SelenElem.WaitForLoad(By.id("continueToCheckout"), 10);
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.waitForLoadPage();
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
         
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        da.hideKeyBoard();
        da.slide("Up", 2);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), "1980");
        SelenElem.SelectOption(By.className("ui-datepicker-month"), "Mar");
        SelenElem.Click(By.linkText("12"));
        Thread.sleep(50000);
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        //SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        Thread.sleep(5000);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("numero-domicilio-interior"), numInterior);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.Click(By.id("checkout-step4-check-02"));
        //SelenElem.focus(By.id("couponCode"));
        //SelenElem.TypeText(By.id("couponCode"), "de43de4");
        SelenElem.Click(By.id("btn_continuar"));
        OPG.cargaAjax2();
        Thread.sleep(50000);
        SelenElem.WaitForLoad(By.id("cod"), 40);
        reporte.AddImageToReport("Sección consulta tu crédito del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("cod"), "1111");
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        SelenElem.Click(By.id("checkout-step2-radio-04"));
        SelenElem.Click(By.id("checkout-step2-radio-06"));
        SelenElem.Click(By.id("checkout-step2"));
        reporte.AddImageToReport("Sección consulta tu crédito del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("btn_continuar_2"));
        OPG.cargaAjax2();
        Thread.sleep(20000);
        
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        da.hideKeyBoard();
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        
        SelenElem.Click(By.id("btn_continuar_3"));
        OPG.cargaAjax2();
        Thread.sleep(10000);
        SelenElem.Click(By.id("checkout-step4-check-01"));
        SelenElem.Click(By.id("checkTerms"));
        da.slide("Up", 1);
        reporte.AddImageToReport("Sección contrato del checkout", OPG.SS());
        Thread.sleep(2500);
        SelenElem.Click(By.id("end-step"));
        Thread.sleep(3500);
        reporte.AddImageToReport("Resumen de compra1", OPG.SS());
        Thread.sleep(2500);
        SelenElem.Click(By.id("grandTotalMobile"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("continuar-checkout-out"));
        //SelenElem.ClickById(WebDrive.WebDriver, "continuar-checkout-out");
        //OPG.cargaAjax2();|ñ{
          
        new FlapPorta( reporte, OPG, SelenElem, WebDrive);
        
        SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.ScreenShotElementInstant());
        //*/

     }

     public void ErrorPagoPospagoSIMTerminal(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException{
        
        String dn = CargaVariables.LeerCSV(0, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String rfc = CargaVariables.LeerCSV(1, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String correo = CargaVariables.LeerCSV(5, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String tel = CargaVariables.LeerCSV(6, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String calle = CargaVariables.LeerCSV(8, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String cp = CargaVariables.LeerCSV(7, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String colonia = CargaVariables.LeerCSV(12, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String ine = CargaVariables.LeerCSV(14, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String rfcc = CargaVariables.LeerCSV(15, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Vemos los telefonos  ", OPG.SS());
        reporte.AddImageToReport("Se selecciona el telefono", OPG.ScreenShotElementFocus(By.xpath("//a[@href='" + Url + "iphone-xr-lte-amarillo-64gb-apple.html']")));
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + CargaVariables.LeerCSV(16,"POSPAGO CON NUEVO NUMERO FLAP",Caso) + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        Thread.sleep(2000);
        reporte.AddImageToReport("Ver detalle de Smartphone y seleccionar plan", OPG.SS());
        SelenElem.waitForLoadPage();
        
        /////
        SelenElem.Click(By.id("soy-nuevo"));
        OPG.cargaAjax2();
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//*[@id=\"js_slider_mp_pospago\"]/div/div/div[5]/div/div/div/div"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//div[.='Quiero este plan']"));
        
        Thread.sleep(3000);
        /*
        SelenElem.Click(By.id("btn-buy-one"));
        reporte.AddImageToReport("Elegir plan Movistar", OPG.SS());
        SelenElem.Click(By.id("Plan-Video"));
        reporte.AddImageToReport("Sin SVA ", OPG.SS());
        SelenElem.Click(By.id("submitplanes"));
        */
        
        
        SelenElem.Click(By.id("js_submit_miniparrilla_pospago"));
        SelenElem.WaitForLoad(By.id("total-mobile"), 10);
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElementInstant());
         SelenElem.Click(By.id("total-mobile"));
        SelenElem.WaitForLoad(By.id("continueToCheckout"), 10);
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.waitForLoadPage();
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
         
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        da.hideKeyBoard();
        da.slide("Up", 2);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), "1988");
        SelenElem.SelectOption(By.className("ui-datepicker-month"), "Mar");
        SelenElem.Click(By.linkText("3"));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("numero-domicilio-interior"), numInterior);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.Click(By.id("checkout-step4-check-02"));
        //SelenElem.focus(By.id("couponCode"));
        //SelenElem.TypeText(By.id("couponCode"), "de43de4");
        SelenElem.Click(By.id("btn_continuar"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("cod"), 4);
        reporte.AddImageToReport("Sección consulta tu crédito del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("cod"), "1111");
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        SelenElem.Click(By.id("checkout-step2-radio-04"));
        SelenElem.Click(By.id("checkout-step2-radio-06"));
        SelenElem.Click(By.id("checkout-step2"));
        reporte.AddImageToReport("Sección consulta tu crédito del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("btn_continuar_2"));
        OPG.cargaAjax2();
        
        /*SelenElem.Click(By.id("inv-mail2"));
        SelenElem.focus(By.id("cp"));
        SelenElem.WaitForLoad(By.id("cp"), 4);
        OPG.cargaAjax2();
        
        SelenElem.TypeText(By.id("cp"), "06600");
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        SelenElem.Click(By.id("longlat"));
        OPG.cargaAjax2(); 
        da.hideKeyBoard();
        SelenElem.Click(By.id("99090073cac"));
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());*/
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección opciones de envió del checkout", OPG.SS());
        
        SelenElem.Click(By.id("btn_continuar_3"));
        OPG.cargaAjax2();
        SelenElem.Click(By.id("checkout-step4-check-01"));
        SelenElem.Click(By.id("checkTerms"));
        reporte.AddImageToReport("Sección contrato del checkout", OPG.SS());
        Thread.sleep(2500);
        SelenElem.Click(By.id("end-step"));
        Thread.sleep(3500);
        reporte.AddImageToReport("Resumen de compra1", OPG.SS());
        Thread.sleep(2500);
        SelenElem.Click(By.id("grandTotalMobile"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("continuar-checkout-out"));
        //SelenElem.ClickById(WebDrive.WebDriver, "continuar-checkout-out");
        //OPG.cargaAjax2();|ñ{
          
        new FlapPortaVariosCampos( reporte, OPG, SelenElem, WebDrive);
        
        

     }

     public void ErrorCPPospagoSIMTerminal(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException{
       
        String dn = CargaVariables.LeerCSV(0, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String rfc = CargaVariables.LeerCSV(1, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String correo = CargaVariables.LeerCSV(5, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String tel = CargaVariables.LeerCSV(6, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String calle = CargaVariables.LeerCSV(8, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String cp = CargaVariables.LeerCSV(7, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String colonia = CargaVariables.LeerCSV(12, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String ine = CargaVariables.LeerCSV(14, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String rfcc = CargaVariables.LeerCSV(15, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        
        reporte.AddImageToReport("Iniciamos desde la pagina principal y seleccionamos telefonos", OPG.ScreenShotElement());
        SelenElem.Click(By.className("icon"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionamos el flujo de Telefonos", OPG.ScreenShotElement());
        SelenElem.Click(By.xpath("//input[@value='Smartphones']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionamos el flujo de Telefonos", OPG.ScreenShotElement());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        reporte.AddImageToReport("Seleccionamos la opción Telefonos", OPG.ScreenShotElement());
        
        Thread.sleep(2000);
        da.slide("Down", 2 );
        reporte.AddImageToReport("Vemos los telefonos  ", OPG.SS());
        da.slide("Down", 1 );
        reporte.AddImageToReport("Se selecciona el telefono", OPG.SS());
        
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + CargaVariables.LeerCSV(16,"POSPAGO CON NUEVO NUMERO FLAP",Caso) + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        Thread.sleep(2000);
        reporte.AddImageToReport("Ver detalle de Smartphone y seleccionar plan", OPG.SS());
        SelenElem.waitForLoadPage();
        
        /////
        SelenElem.Click(By.id("soy-nuevo"));
        OPG.cargaAjax2();
        Thread.sleep(4000);
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//*[@id=\"js_slider_mp_pospago\"]/div/div/div[5]/div/div/div/div"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//div[.='Quiero este plan']"));
        
        Thread.sleep(3000);
        /*
        SelenElem.Click(By.id("btn-buy-one"));
        reporte.AddImageToReport("Elegir plan Movistar", OPG.SS());
        SelenElem.Click(By.id("Plan-Video"));
        reporte.AddImageToReport("Sin SVA ", OPG.SS());
        SelenElem.Click(By.id("submitplanes"));
        */
        
        
        SelenElem.Click(By.id("js_submit_miniparrilla_pospago"));
        SelenElem.WaitForLoad(By.id("total-mobile"), 10);
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElementInstant());
         SelenElem.Click(By.id("total-mobile"));
        SelenElem.WaitForLoad(By.id("continueToCheckout"), 10);
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.waitForLoadPage();
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
         
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        da.hideKeyBoard();
        da.slide("Up", 2);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), "1980");
        SelenElem.SelectOption(By.className("ui-datepicker-month"), "Mar");
        SelenElem.Click(By.linkText("12"));
        Thread.sleep(50000);
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        //SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        Thread.sleep(5000);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("numero-domicilio-interior"), numInterior);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.Click(By.id("checkout-step4-check-02"));
        //SelenElem.focus(By.id("couponCode"));
        //SelenElem.TypeText(By.id("couponCode"), "de43de4");
        SelenElem.Click(By.id("btn_continuar"));
        OPG.cargaAjax2();
        Thread.sleep(50000);
        SelenElem.WaitForLoad(By.id("cod"), 40);
        reporte.AddImageToReport("Sección consulta tu crédito del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("cod"), "1111");
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        SelenElem.Click(By.id("checkout-step2-radio-04"));
        SelenElem.Click(By.id("checkout-step2-radio-06"));
        SelenElem.Click(By.id("checkout-step2"));
        reporte.AddImageToReport("Sección consulta tu crédito del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("btn_continuar_2"));
        OPG.cargaAjax2();
        Thread.sleep(20000);
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 15);
        
        SelenElem.Click(By.id("inv-mail2"));
        //OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("js_codigo_postal"), "022");
        da.hideKeyBoard();
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        Thread.sleep(500);
        reporte.AddImageToReport("Validacion Error en codigo postal", OPG.ScreenShotElementInstant());
     }

      public void ErrorINEPospagoSIMTerminal(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException{
         
        String dn = CargaVariables.LeerCSV(0, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String rfc = CargaVariables.LeerCSV(1, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String correo = CargaVariables.LeerCSV(5, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String tel = CargaVariables.LeerCSV(6, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String calle = CargaVariables.LeerCSV(8, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String cp = CargaVariables.LeerCSV(7, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String colonia = CargaVariables.LeerCSV(12, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String ine = CargaVariables.LeerCSV(14, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        String rfcc = CargaVariables.LeerCSV(15, "POSPAGO CON NUEVO NUMERO FLAP", Caso);
        
        
        reporte.AddImageToReport("Iniciamos desde la pagina principal y seleccionamos telefonos", OPG.ScreenShotElement());
        SelenElem.Click(By.className("icon"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionamos el flujo de Telefonos", OPG.ScreenShotElement());
        SelenElem.Click(By.xpath("//input[@value='Smartphones']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionamos el flujo de Telefonos", OPG.ScreenShotElement());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        reporte.AddImageToReport("Seleccionamos la opción Telefonos", OPG.ScreenShotElement());
        
        Thread.sleep(2000);
        da.slide("Down", 2 );
        reporte.AddImageToReport("Vemos los telefonos  ", OPG.SS());
        da.slide("Down", 1 );
        reporte.AddImageToReport("Se selecciona el telefono", OPG.SS());
        
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + CargaVariables.LeerCSV(16,"POSPAGO CON NUEVO NUMERO FLAP",Caso) + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        Thread.sleep(2000);
        reporte.AddImageToReport("Ver detalle de Smartphone y seleccionar plan", OPG.SS());
        SelenElem.waitForLoadPage();
        
        /////
        SelenElem.Click(By.id("soy-nuevo"));
        OPG.cargaAjax2();
        Thread.sleep(4000);
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//*[@id=\"js_slider_mp_pospago\"]/div/div/div[5]/div/div/div/div"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//div[.='Quiero este plan']"));
        
        Thread.sleep(3000);
        /*
        SelenElem.Click(By.id("btn-buy-one"));
        reporte.AddImageToReport("Elegir plan Movistar", OPG.SS());
        SelenElem.Click(By.id("Plan-Video"));
        reporte.AddImageToReport("Sin SVA ", OPG.SS());
        SelenElem.Click(By.id("submitplanes"));
        */
        
        
        SelenElem.Click(By.id("js_submit_miniparrilla_pospago"));
        SelenElem.WaitForLoad(By.id("total-mobile"), 10);
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElementInstant());
         SelenElem.Click(By.id("total-mobile"));
        SelenElem.WaitForLoad(By.id("continueToCheckout"), 10);
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.waitForLoadPage();
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
         
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        da.hideKeyBoard();
        da.slide("Up", 2);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), "1980");
        SelenElem.SelectOption(By.className("ui-datepicker-month"), "Mar");
        SelenElem.Click(By.linkText("12"));
        Thread.sleep(50000);
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        //SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        Thread.sleep(5000);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("numero-domicilio-interior"), numInterior);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.Click(By.id("checkout-step4-check-02"));
        //SelenElem.focus(By.id("couponCode"));
        //SelenElem.TypeText(By.id("couponCode"), "de43de4");
        SelenElem.Click(By.id("btn_continuar"));
        reporte.AddImageToReport("Error INE 18 digitos", OPG.SS());
      }
    
}
