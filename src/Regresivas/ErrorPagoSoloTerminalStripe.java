
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class ErrorPagoSoloTerminalStripe {

    public ErrorPagoSoloTerminalStripe (Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
        
       String dn = CargaVariables.LeerCSV(0, "Error en Forma de Pago Solo Terminal - Stripe", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Error en Forma de Pago Solo Terminal - Stripe", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Error en Forma de Pago Solo Terminal - Stripe", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Error en Forma de Pago Solo Terminal - Stripe", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Error en Forma de Pago Solo Terminal - Stripe", Caso);
        String correo = CargaVariables.LeerCSV(5, "Error en Forma de Pago Solo Terminal - Stripe", Caso);
        String tel = CargaVariables.LeerCSV(6, "Error en Forma de Pago Solo Terminal - Stripe", Caso);
        String calle = CargaVariables.LeerCSV(8, "Error en Forma de Pago Solo Terminal - Stripe", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Error en Forma de Pago Solo Terminal - Stripe", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Error en Forma de Pago Solo Terminal - Stripe", Caso);
        String cp = CargaVariables.LeerCSV(7, "Error en Forma de Pago Solo Terminal - Stripe", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Error en Forma de Pago Solo Terminal - Stripe", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Error en Forma de Pago Solo Terminal - Stripe", Caso);
        String ine = CargaVariables.LeerCSV(14, "Error en Forma de Pago Solo Terminal - Stripe", Caso);
        String rfcc = CargaVariables.LeerCSV(15, "Error en Forma de Pago Solo Terminal - Stripe", Caso);
        String tarjeta = CargaVariables.LeerCSV(17, "Error en Forma de Pago Solo Terminal - Stripe", Caso);
        String vencimiento = CargaVariables.LeerCSV(18, "Error en Forma de Pago Solo Terminal - Stripe", Caso);
        String cvv = CargaVariables.LeerCSV(19, "Error en Forma de Pago Solo Terminal - Stripe", Caso);
        String telefono = CargaVariables.LeerCSV(16,"Error en Forma de Pago Solo Terminal - Stripe",Caso) ;

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        reporte.AddImageToReport("Se selecciona el telefono", OPG.ScreenShotElementFocus(By.xpath("//a[@href='" + Url + "iphone-xr-lte-amarillo-64gb-apple.html']")));
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + telefono + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        Thread.sleep(3000);
        
        reporte.AddImageToReport("Ver detalle de Smartphone", OPG.SS());
        SelenElem.focus(By.id("solo-smartphone"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//p[.='Comprar']"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.xpath("//p[.='Comprar']"));
        
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra ",OPG.SS() );
        SelenElem.waitForLoadPage();
        SelenElem.focus(By.id("continueToCheckout"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 10);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        SelenElem.Click(By.id("checkPrivacyOne"));
        
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("recibeFactura"));
        
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Arrendamiento");
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.focus(By.id("btn_continuar"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("btn_continuar"));
        OPG.cargaAjax2();
        
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("card-name"), 20);
        SelenElem.Click(By.id("card-name"));
        SelenElem.TypeText(By.id("card-name"), "Vianey Macias Nieves");
        
       
        //usar iframes
        
        SelenElem.SwitchToFrameByTargetElement(By.xpath("//*[@id=\"root\"]/form/span[2]/div/div[2]/span/input"));        
        SelenElem.TypeText(By.xpath("//*[@id=\"root\"]/form/span[2]/div/div[2]/span/input"),"40755957164837");
        SelenElem.BacktoMainFrame();
        
        
        SelenElem.SwitchToFrame(By.xpath("//*[@id=\"stripe-payments-card-expiry\"]/div/iframe"));
        SelenElem.TypeText(By.xpath("//*[@id=\"root\"]/form/span[2]/span/input"), vencimiento);
        SelenElem.BacktoMainFrame();
        
        SelenElem.SwitchToFrame(By.xpath("//*[@id=\"stripe-payments-card-cvc\"]/div/iframe"));
        SelenElem.TypeText(By.xpath("//*[@id=\"root\"]/form/span[2]/span/input"), cvv);
        SelenElem.BacktoMainFrame();
        
        Thread.sleep(1000);
        reporte.AddImageToReport("Error forma de pago Tarjeta incompleta", OPG.SS());
        Thread.sleep(1000);
        
        SelenElem.SwitchToFrameByTargetElement(By.xpath("//*[@id=\"root\"]/form/span[2]/div/div[2]/span/input"));        
        SelenElem.FindElementByXPath("//*[@id=\"root\"]/form/span[2]/div/div[2]/span/input").clear();
        SelenElem.TypeText(By.xpath("//*[@id=\"root\"]/form/span[2]/div/div[2]/span/input"),tarjeta);
        SelenElem.BacktoMainFrame();
        
        Thread.sleep(1500);
        SelenElem.FindElementByID("card-name").clear();
        Thread.sleep(500);
        
        reporte.AddImageToReport("Error forma de pago sin nombre del titular", OPG.SS());
        
        //////////////////////// sin probar //////////////
        Thread.sleep(1000);
        SelenElem.Click(By.id("card-name"));
        SelenElem.TypeText(By.id("card-name"), "Vianey Macias Nieves");
        
        SelenElem.SwitchToFrame(By.xpath("//*[@id=\"stripe-payments-card-expiry\"]/div/iframe"));
        SelenElem.FindElementByXPath("//*[@id=\"root\"]/form/span[2]/span/input").clear();
        SelenElem.TypeText(By.xpath("//*[@id=\"root\"]/form/span[2]/span/input"), "0122");
        SelenElem.BacktoMainFrame();
        
        SelenElem.SwitchToFrame(By.xpath("//*[@id=\"stripe-payments-card-cvc\"]/div/iframe"));
        SelenElem.TypeText(By.xpath("//*[@id=\"root\"]/form/span[2]/span/input"), cvv);
        SelenElem.BacktoMainFrame();
        
        SelenElem.Click(By.id("card-name"));
        Thread.sleep(500);
        
        reporte.AddImageToReport("Error forma de pago Fecha de tarjeta expirada", OPG.SS());
        
        
        
        SelenElem.SwitchToFrame(By.xpath("//*[@id=\"stripe-payments-card-expiry\"]/div/iframe"));
        SelenElem.FindElementByXPath("//*[@id=\"root\"]/form/span[2]/span/input").clear();
        SelenElem.TypeText(By.xpath("//*[@id=\"root\"]/form/span[2]/span/input"), vencimiento);
        SelenElem.BacktoMainFrame();
        
        SelenElem.SwitchToFrame(By.xpath("//*[@id=\"stripe-payments-card-cvc\"]/div/iframe"));
        SelenElem.FindElementByXPath("//*[@id=\"root\"]/form/span[2]/span/input").clear();
        SelenElem.BacktoMainFrame();
        
        SelenElem.Click(By.id("card-name"));
        Thread.sleep(500);
        
        Thread.sleep(1000);
        reporte.AddImageToReport("Error forma de pago sin CVV", OPG.SS());
        Thread.sleep(1000);
        
     

     }
}
