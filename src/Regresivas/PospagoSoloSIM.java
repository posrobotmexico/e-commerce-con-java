
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.FlapError;
import Operaciones.OpcionesEnvio;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class PospagoSoloSIM {

    public PospagoSoloSIM(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
       
        String dn = CargaVariables.LeerCSV(0, "Pospago solo SIM", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Pospago solo SIM", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Pospago solo SIM", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Pospago solo SIM", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Pospago solo SIM", Caso);
        String correo = CargaVariables.LeerCSV(5, "Pospago solo SIM", Caso);
        String tel = CargaVariables.LeerCSV(6, "Pospago solo SIM", Caso);
        String calle = CargaVariables.LeerCSV(8, "Pospago solo SIM", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Pospago solo SIM", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Pospago solo SIM", Caso);
        String cp = CargaVariables.LeerCSV(7, "Pospago solo SIM", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Pospago solo SIM", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Pospago solo SIM", Caso);
        String ine = CargaVariables.LeerCSV(14, "Pospago solo SIM", Caso);
        String rfcc = CargaVariables.LeerCSV(15, "Pospago solo SIM", Caso);
        
        WebDrive.WebDriver.navigate().to(Url);
        Thread.sleep(500);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "parrillas");
        //SelenElem.Click(By.linkText("Planes"));
        SelenElem.WaitForLoad(By.xpath("//a[@href='/parrillas']"), 4);
        
        SelenElem.Click(By.xpath("//a[@href='/parrillas']"));
        Thread.sleep(500);
        reporte.AddImageToReport("Seleccionamos un plan", OPG.SS());
        SelenElem.Click(By.id("Plan-Video"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.id("spotify"));
        reporte.AddImageToReport("Sin SVA", OPG.SS());
        Thread.sleep(500);
        SelenElem.focus(By.xpath("//*[@id=\"dropdown-parrillas\"]/div/div/div/div"));
        SelenElem.focus(By.xpath("//*[@id=\"dropdown-parrillas\"]/div/div/div/div"));
        
        reporte.AddImageToReport("", OPG.SS(By.xpath("//*[@id=\"condiciones-generales-parrillas\"]/div/div/div/div/p")));
         
        
        SelenElem.Click(By.id("submitplanes"));
        SelenElem.WaitForLoad(By.id("continueToCheckout"), 30);
        Thread.sleep(2000);
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        SelenElem.focus(By.id("continueToCheckout"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.ClickById(WebDrive.WebDriver,"continueToCheckout");
                
        SelenElem.waitForLoadPage();
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.focus(By.id("telefono-contacto"));
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), "1993");
        SelenElem.SelectOption(By.className("ui-datepicker-month"), "Mar");
        SelenElem.Click(By.linkText("3"));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("numero-domicilio-interior"), numInterior);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.focus(By.id("couponCode"));
        SelenElem.TypeText(By.id("couponCode"), "de43de4");
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        
        SelenElem.Click(By.id("btn_continuar"));
        OPG.cargaAjax2();
        
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        /*
        SelenElem.Click(By.id("inv-mail2"));
        SelenElem.focus(By.id("cp"));
        SelenElem.WaitForLoad(By.id("cp"), 4);
        OPG.cargaAjax2();
        ///cambio
        SelenElem.TypeText(By.id("cp"), cp);
        SelenElem.Click(By.id("longlat"));
        OPG.cargaAjax2();      
                
        SelenElem.Click(By.id("21010006cac"));
        SelenElem.focus(By.id("99010012cac"));
        reporte.AddImageToReport("Seccion opciones de envio de checkout", OPG.SS());
        */
        SelenElem.Click(By.id("btn_continuar_3"));
        OPG.cargaAjax2();   
        SelenElem.Click(By.id("checkTerms"));
        SelenElem.Click(By.id("checkout-step4-check-01"));
        SelenElem.focus(By.xpath("//*[@id=\"titleStep-4\"]"));
        reporte.AddImageToReport("Sección Contrato del checkout", OPG.SS());
        
        SelenElem.Click(By.id("end-step"));
        OPG.cargaAjax2();
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
       
        SelenElem.Click(By.id("continuar-checkout-out")); 
        OPG.cargaAjax2();
        new FlapError( reporte, OPG, SelenElem, WebDrive);
        SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/section[2]/dl/dd/p"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/footer/a"));
        reporte.AddImageToReport("", OPG.SS());
        
        
    
    
    }
    
    
}
