
package Regresivas;

import Core.DeviceActions;
import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.OpcionesEnvio;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

/**
 *
 * @author LAPTOP-JORGE
 */
public class PortabilidadPrepagoFlap {
    
    public PortabilidadPrepagoFlap (){
    
    }
    
    public void PortabilidadPrepagoTerminalMasRecargaLiferay(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String caso) throws InterruptedException, IOException
    {
        String dn = CargaVariables.LeerCSV(0, "Portabilidad Prepago Flap", caso);
        String rfc = CargaVariables.LeerCSV(1, "Portabilidad Prepago Flap", caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Portabilidad Prepago Flap", caso);
        String apellidop = CargaVariables.LeerCSV(3, "Portabilidad Prepago Flap", caso);
        String apellidom = CargaVariables.LeerCSV(4, "Portabilidad Prepago Flap", caso);
        String correo = CargaVariables.LeerCSV(5, "Portabilidad Prepago Flap", caso);
        String tel = CargaVariables.LeerCSV(6, "Portabilidad Prepago Flap", caso);
        String calle = CargaVariables.LeerCSV(8, "Portabilidad Prepago Flap", caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Portabilidad Prepago Flap", caso);
        String numInterior = CargaVariables.LeerCSV(10, "Portabilidad Prepago Flap", caso);
        String cp = CargaVariables.LeerCSV(7, "Portabilidad Prepago Flap", caso);
        String ciudad = CargaVariables.LeerCSV(13, "Portabilidad Prepago Flap", caso);
        String colonia = CargaVariables.LeerCSV(12, "Portabilidad Prepago Flap", caso);
        String ine = CargaVariables.LeerCSV(14, "Portabilidad Prepago Flap", caso);
        String año = CargaVariables.LeerCSV(16, "Portabilidad Prepago Flap", caso);
        String mes = CargaVariables.LeerCSV(17, "Portabilidad Prepago Flap", caso);
        String  dia= CargaVariables.LeerCSV(18, "Portabilidad Prepago Flap", caso);
        String curp= CargaVariables.LeerCSV(19, "Portabilidad Prepago Flap", caso);
        String skuterminal= CargaVariables.LeerCSV(20, "Portabilidad Prepago Flap", caso);
        String skuRecarga= CargaVariables.LeerCSV(21, "Portabilidad Prepago Flap", caso);
        
        WebDrive.WebDriver.navigate().to(Url + "liferay.php");
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        Thread.sleep(2000);
        SelenElem.SelectOption(By.id("flowCode"), "portabilidad_prepago");
        Thread.sleep(1000);
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.TypeText(By.id("skuTerminal"), skuterminal);
        SelenElem.Find(By.id("skuPlan")).clear();
        SelenElem.Find(By.id("namePlan")).clear();
        SelenElem.Find(By.id("skuRecarga")).clear();
        SelenElem.TypeText(By.id("skuRecarga"), skuRecarga);
        SelenElem.Find(By.id("monto")).clear();
        SelenElem.Find(By.id("rfc")).clear();
        SelenElem.Find(By.id("skuDn")).clear();
        SelenElem.TypeText(By.id("skuDn"), dn);
        reporte.AddImageToReport("Ingresamos los datos y damos clic en enviar", OPG.SS());
        SelenElem.Click(By.name("Comprar"));
        
        /*WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        
        SelenElem.Click(By.xpath("//span[.='Portabilidad']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar Portabilidad > Prepago", OPG.SS());
        
        SelenElem.Click(By.xpath("//span[.='Prepago']"));
        Thread.sleep(4000);
        reporte.AddImageToReport("Seleccionar smartphone ", OPG.SS());
        
        //SelenElem.ClickByXpath(WebDrive.WebDriver,"//img[contains(@alt, '" + modeloTel + "')]"); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        
        reporte.AddImageToReport("Ver detalle del Smartphone ", OPG.SS());
        SelenElem.focus(By.className("rating-summary"));
        reporte.AddImageToReport("Ver detalle del Smartphone ", OPG.SS());
        SelenElem.focus(By.id("js_input_dn_button_portabilidad"));
        reporte.AddImageToReport(" ", OPG.SS());
        
        SelenElem.Click(By.id("js_dn_input_portabilidad"));
        SelenElem.TypeText(By.id("js_dn_input_portabilidad"), dn);
        reporte.AddImageToReport("Visualizar el detalle del smartphone e ingresar DN y dar clic en aceptar ", OPG.SS());
        
        SelenElem.Click(By.id("js_input_dn_button_portabilidad"));
        SelenElem.WaitForLoad(By.xpath("//span[.='Continuar']"), 20);
        reporte.AddImageToReport("", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.xpath("//span[.='Continuar']"));
        reporte.AddImageToReport("Elegir plan con recarga", OPG.SS());
        
        SelenElem.Click(By.id("js_submit_miniparrilla_portabilidad_prepago"));
        SelenElem.WaitForLoad(By.id("portability_number"), 20);
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        SelenElem.focus(By.id("eliminar-recarga"));
        reporte.AddImageToReport(" ", OPG.SS());
        SelenElem.focus(By.className("data-section_item"));
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/section/div/div[1]/div/button"));
        */
        SelenElem.waitForLoadPage();
        Thread.sleep(1500);
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        SelenElem.Click(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/section/div/div[1]/div/button"));
        Thread.sleep(1500);
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        
        
        da.hideKeyBoard();
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        SelenElem.Click(By.id("continueToCheckout"));
        
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 15);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-email"), correo);
        da.hideKeyBoard();
        da.slide("Up", 1);
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        da.hideKeyBoard();
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.Click(By.id("checkPrivacyOne"));
        da.hideKeyBoard();
        da.slide("Up", 1);
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("botonTusDatos"));
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 15);
        
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());
        
        SelenElem.focus(By.id("checkout-paso2-renovaciones"));
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("checkout-paso2-renovaciones"));
       
        SelenElem.WaitForLoad(By.id("valid-nip"), 10);
        SelenElem.TypeText(By.id("valid-nip"), "1234");
        SelenElem.TypeText(By.id("valid-curp"), curp);
        SelenElem.Click(By.id("dateToPorta"));
        da.hideKeyBoard();
        da.slide("Up", 1);
        reporte.AddImageToReport("Sección Cámbiate a Movistar", OPG.SS());
        SelenElem.focus(By.id("checkout-paso4-porta-pre"));
        reporte.AddImageToReport("", OPG.SS());
        
        Thread.sleep(2000);
        SelenElem.ClickById(WebDrive.WebDriver, "checkout-paso4-porta-pre");
        /*Thread.sleep(1000);
        OPG.cargaAjax2();
        Thread.sleep(5000);
        da.slide("Up", 1);
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        //SelenElem.Click(By.id("checkout-paso4-porta-pre"));
        //OPG.cargaAjax2();*/
        Thread.sleep(10000);
        new Flap(reporte,OPG,SelenElem,WebDrive);
        
        SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
    }
    
    public void PortabilidadPrepagoTerminalMasSIMConRecarga(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String caso) throws InterruptedException, IOException
    {
        String dn = CargaVariables.LeerCSV(0, "Portabilidad Prepago Flap", caso);
        String rfc = CargaVariables.LeerCSV(1, "Portabilidad Prepago Flap", caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Portabilidad Prepago Flap", caso);
        String apellidop = CargaVariables.LeerCSV(3, "Portabilidad Prepago Flap", caso);
        String apellidom = CargaVariables.LeerCSV(4, "Portabilidad Prepago Flap", caso);
        String correo = CargaVariables.LeerCSV(5, "Portabilidad Prepago Flap", caso);
        String tel = CargaVariables.LeerCSV(6, "Portabilidad Prepago Flap", caso);
        String calle = CargaVariables.LeerCSV(8, "Portabilidad Prepago Flap", caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Portabilidad Prepago Flap", caso);
        String numInterior = CargaVariables.LeerCSV(10, "Portabilidad Prepago Flap", caso);
        String cp = CargaVariables.LeerCSV(7, "Portabilidad Prepago Flap", caso);
        String ciudad = CargaVariables.LeerCSV(13, "Portabilidad Prepago Flap", caso);
        String colonia = CargaVariables.LeerCSV(12, "Portabilidad Prepago Flap", caso);
        String ine = CargaVariables.LeerCSV(14, "Portabilidad Prepago Flap", caso);
        String año = CargaVariables.LeerCSV(16, "Portabilidad Prepago Flap", caso);
        String mes = CargaVariables.LeerCSV(17, "Portabilidad Prepago Flap", caso);
        String  dia= CargaVariables.LeerCSV(18, "Portabilidad Prepago Flap", caso);
        String curp= CargaVariables.LeerCSV(19, "Portabilidad Prepago Flap", caso);
        String modeloTel= CargaVariables.LeerCSV(20, "Portabilidad Prepago Flap", caso);
        
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        
        SelenElem.Click(By.xpath("//span[.='Portabilidad']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar Portabilidad > Prepago", OPG.SS());
        
        SelenElem.Click(By.xpath("//span[.='Prepago']"));
        Thread.sleep(4000);
        reporte.AddImageToReport("Seleccionar smartphone ", OPG.SS());
        
        SelenElem.ClickByXpath(WebDrive.WebDriver,"//img[contains(@alt, '" + modeloTel + "')]"); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        
        reporte.AddImageToReport("Ver detalle del Smartphone ", OPG.SS());
        SelenElem.focus(By.className("rating-summary"));
        reporte.AddImageToReport("Ver detalle del Smartphone ", OPG.SS());
        SelenElem.focus(By.id("js_input_dn_button_portabilidad"));
        reporte.AddImageToReport(" ", OPG.SS());
        
        SelenElem.Click(By.id("js_dn_input_portabilidad"));
        SelenElem.TypeText(By.id("js_dn_input_portabilidad"), dn);
        reporte.AddImageToReport("Visualizar el detalle del smartphone e ingresar DN y dar clic en aceptar ", OPG.SS());
        
        SelenElem.Click(By.id("js_input_dn_button_portabilidad"));
        SelenElem.WaitForLoad(By.xpath("//span[.='Continuar']"), 20);
        reporte.AddImageToReport("", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.xpath("//span[.='Continuar']"));
        reporte.AddImageToReport("Elegir plan con recarga", OPG.SS());
        
        SelenElem.Click(By.id("js_submit_miniparrilla_portabilidad_prepago"));
        SelenElem.WaitForLoad(By.id("portability_number"), 20);
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        SelenElem.focus(By.id("eliminar-recarga"));
        reporte.AddImageToReport(" ", OPG.SS());
        SelenElem.focus(By.className("data-section_item"));
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/section/div/div[1]/div/button"));
        
        
        SelenElem.Click(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/section/div/div[1]/div/button"));
        Thread.sleep(500);
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        
        da.hideKeyBoard();
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 5);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-email"), correo);
        da.hideKeyBoard();
        da.slide("Up", 1);
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        da.hideKeyBoard();
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.Click(By.id("checkPrivacyOne"));
        da.hideKeyBoard();
        da.slide("Up", 1);
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("botonTusDatos"));
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 15);
        
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());
        
        SelenElem.focus(By.id("checkout-paso2-renovaciones"));
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("checkout-paso2-renovaciones"));
       
        SelenElem.WaitForLoad(By.id("valid-nip"), 10);
        SelenElem.TypeText(By.id("valid-nip"), "1234");
        SelenElem.TypeText(By.id("valid-curp"), curp);
        SelenElem.Click(By.id("dateToPorta"));
        da.hideKeyBoard();
        da.slide("Up", 1);
        reporte.AddImageToReport("Sección Cámbiate a Movistar", OPG.SS());
        SelenElem.focus(By.id("checkout-paso4-porta-pre"));
        reporte.AddImageToReport("", OPG.SS());
        
        Thread.sleep(2000);
        SelenElem.ClickById(WebDrive.WebDriver, "checkout-paso4-porta-pre");
        /*Thread.sleep(1000);
        OPG.cargaAjax2();
        Thread.sleep(5000);
        da.slide("Up", 1);
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        //SelenElem.Click(By.id("checkout-paso4-porta-pre"));
        //OPG.cargaAjax2();*/
        Thread.sleep(10000);
        new Flap(reporte,OPG,SelenElem,WebDrive);
        
        SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/h2"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/section[3]"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/footer/a"));
        reporte.AddImageToReport("", OPG.SS());
    }
    
    public void ErrorDNPortabilidadPrepago(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String caso) throws InterruptedException, IOException
    {
        String dn = CargaVariables.LeerCSV(0, "Portabilidad Prepago Flap", caso);
        String modeloTel = CargaVariables.LeerCSV(1, "Portabilidad Prepago Flap", caso);
       
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        
        SelenElem.Click(By.xpath("//span[.='Portabilidad']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar Portabilidad > Prepago", OPG.SS());
        
        SelenElem.Click(By.xpath("//span[.='Prepago']"));
        Thread.sleep(4000);
        reporte.AddImageToReport("Seleccionar smartphone ", OPG.SS());
        
        SelenElem.ClickByXpath(WebDrive.WebDriver,"//img[contains(@alt, '" + modeloTel + "')]"); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        
        reporte.AddImageToReport("Ver detalle del Smartphone ", OPG.SS());
        SelenElem.focus(By.className("rating-summary"));
        reporte.AddImageToReport("Ver detalle del Smartphone ", OPG.SS());
        SelenElem.focus(By.id("js_input_dn_button_portabilidad"));
        reporte.AddImageToReport(" ", OPG.SS());
        
        SelenElem.Click(By.id("js_dn_input_portabilidad"));
        SelenElem.TypeText(By.id("js_dn_input_portabilidad"), dn);
        reporte.AddImageToReport("Visualizar el detalle del smartphone e ingresar DN y dar clic en aceptar ", OPG.SS());
        
        SelenElem.Click(By.id("js_input_dn_button_portabilidad"));
        SelenElem.WaitForLoad(By.xpath("//span[.='Continuar']"), 20);
        
        reporte.AddImageToReport("Error DN", OPG.ScreenShotElementInstant());
    
    }
   public void ErrorRFCPortabilidadPrepago(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String caso) throws InterruptedException, IOException
    {
        String dn = CargaVariables.LeerCSV(0, "Portabilidad Prepago Flap", caso);
        String rfc = CargaVariables.LeerCSV(1, "Portabilidad Prepago Flap", caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Portabilidad Prepago Flap", caso);
        String apellidop = CargaVariables.LeerCSV(3, "Portabilidad Prepago Flap", caso);
        String apellidom = CargaVariables.LeerCSV(4, "Portabilidad Prepago Flap", caso);
        String correo = CargaVariables.LeerCSV(5, "Portabilidad Prepago Flap", caso);
        String tel = CargaVariables.LeerCSV(6, "Portabilidad Prepago Flap", caso);
        String modeloTel = CargaVariables.LeerCSV(20, "Portabilidad Prepago Flap", caso);
        
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        
        SelenElem.Click(By.xpath("//span[.='Portabilidad']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar Portabilidad > Prepago", OPG.SS());
        
        SelenElem.Click(By.xpath("//span[.='Prepago']"));
        Thread.sleep(4000);
        reporte.AddImageToReport("Seleccionar smartphone ", OPG.SS());
        
        SelenElem.ClickByXpath(WebDrive.WebDriver,"//img[contains(@alt, '" + modeloTel + "')]"); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        
        reporte.AddImageToReport("Ver detalle del Smartphone ", OPG.SS());
        SelenElem.focus(By.className("rating-summary"));
        reporte.AddImageToReport("Ver detalle del Smartphone ", OPG.SS());
        SelenElem.focus(By.id("js_input_dn_button_portabilidad"));
        reporte.AddImageToReport(" ", OPG.SS());
        
        SelenElem.Click(By.id("js_dn_input_portabilidad"));
        SelenElem.TypeText(By.id("js_dn_input_portabilidad"), dn);
        reporte.AddImageToReport("Visualizar el detalle del smartphone e ingresar DN y dar clic en aceptar ", OPG.SS());
        
        SelenElem.Click(By.id("js_input_dn_button_portabilidad"));
        SelenElem.WaitForLoad(By.xpath("//span[.='Continuar']"), 20);
        reporte.AddImageToReport("", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.xpath("//span[.='Continuar']"));
        reporte.AddImageToReport("Elegir plan con recarga", OPG.SS());
        
        SelenElem.Click(By.id("js_submit_miniparrilla_portabilidad_prepago"));
        SelenElem.WaitForLoad(By.id("portability_number"), 20);
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        SelenElem.focus(By.id("eliminar-recarga"));
        reporte.AddImageToReport(" ", OPG.SS());
        SelenElem.focus(By.className("data-section_item"));
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/section/div/div[1]/div/button"));
        
        
        SelenElem.Click(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/section/div/div[1]/div/button"));
        Thread.sleep(500);
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        
        da.hideKeyBoard();
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 5);
        Thread.sleep(1000);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-email"), correo);
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.Click(By.id("checkPrivacyOne"));
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        SelenElem.focus(By.id("botonTusDatos"));
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        SelenElem.Click(By.id("botonTusDatos"));
        //reporte.AddImageToReport("Error en RFC", OPG.SS());
        Thread.sleep(500);
        SelenElem.focus(By.id("valida-rfc"));
        reporte.AddImageToReport("Error en RFC", OPG.SS());
    } 
   
    public void ErrorCPPortabilidadPrepago(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String caso) throws InterruptedException, IOException
    {
        String dn = CargaVariables.LeerCSV(0, "Portabilidad Prepago Flap", caso);
        String rfc = CargaVariables.LeerCSV(1, "Portabilidad Prepago Flap", caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Portabilidad Prepago Flap", caso);
        String apellidop = CargaVariables.LeerCSV(3, "Portabilidad Prepago Flap", caso);
        String apellidom = CargaVariables.LeerCSV(4, "Portabilidad Prepago Flap", caso);
        String correo = CargaVariables.LeerCSV(5, "Portabilidad Prepago Flap", caso);
        String tel = CargaVariables.LeerCSV(6, "Portabilidad Prepago Flap", caso);
        String calle = CargaVariables.LeerCSV(8, "Portabilidad Prepago Flap", caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Portabilidad Prepago Flap", caso);
        String numInterior = CargaVariables.LeerCSV(10, "Portabilidad Prepago Flap", caso);
        String cp = CargaVariables.LeerCSV(7, "Portabilidad Prepago Flap", caso);
        String ciudad = CargaVariables.LeerCSV(13, "Portabilidad Prepago Flap", caso);
        String colonia = CargaVariables.LeerCSV(12, "Portabilidad Prepago Flap", caso);
        String ine = CargaVariables.LeerCSV(14, "Portabilidad Prepago Flap", caso);
        String año = CargaVariables.LeerCSV(16, "Portabilidad Prepago Flap", caso);
        String mes = CargaVariables.LeerCSV(17, "Portabilidad Prepago Flap", caso);
        String  dia= CargaVariables.LeerCSV(18, "Portabilidad Prepago Flap", caso);
        String curp= CargaVariables.LeerCSV(19, "Portabilidad Prepago Flap", caso);
        String modeloTel= CargaVariables.LeerCSV(20, "Portabilidad Prepago Flap", caso);
        
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        
        SelenElem.Click(By.xpath("//span[.='Portabilidad']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar Portabilidad > Prepago", OPG.SS());
        
        SelenElem.Click(By.xpath("//span[.='Prepago']"));
        Thread.sleep(4000);
        reporte.AddImageToReport("Seleccionar smartphone ", OPG.SS());
        
        SelenElem.ClickByXpath(WebDrive.WebDriver,"//img[contains(@alt, '" + modeloTel + "')]"); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        
        reporte.AddImageToReport("Ver detalle del Smartphone ", OPG.SS());
        SelenElem.focus(By.className("rating-summary"));
        reporte.AddImageToReport("Ver detalle del Smartphone ", OPG.SS());
        SelenElem.focus(By.id("js_input_dn_button_portabilidad"));
        reporte.AddImageToReport(" ", OPG.SS());
        
        SelenElem.Click(By.id("js_dn_input_portabilidad"));
        SelenElem.TypeText(By.id("js_dn_input_portabilidad"), dn);
        reporte.AddImageToReport("Visualizar el detalle del smartphone e ingresar DN y dar clic en aceptar ", OPG.SS());
        
        SelenElem.Click(By.id("js_input_dn_button_portabilidad"));
        SelenElem.WaitForLoad(By.xpath("//span[.='Continuar']"), 20);
        reporte.AddImageToReport("", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.xpath("//span[.='Continuar']"));
        reporte.AddImageToReport("Elegir plan con recarga", OPG.SS());
        
        SelenElem.Click(By.id("js_submit_miniparrilla_portabilidad_prepago"));
        SelenElem.WaitForLoad(By.id("portability_number"), 20);
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        SelenElem.focus(By.id("eliminar-recarga"));
        reporte.AddImageToReport(" ", OPG.SS());
        SelenElem.focus(By.className("data-section_item"));
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/section/div/div[1]/div/button"));
        
        
        SelenElem.Click(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/section/div/div[1]/div/button"));
        Thread.sleep(500);
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        
        da.hideKeyBoard();
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 5);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-email"), correo);
        da.hideKeyBoard();
        da.slide("Up", 1);
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        da.hideKeyBoard();
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.Click(By.id("checkPrivacyOne"));
        da.hideKeyBoard();
        da.slide("Up", 1);
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("botonTusDatos"));
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 15);
        
        SelenElem.Click(By.id("inv-mail2"));
        //OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("js_codigo_postal"), "066");
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        Thread.sleep(500);
        reporte.AddImageToReport("Validacion Error en codigo postal", OPG.ScreenShotElementInstant());
    }
    
     public void ErrorNIPPortabilidadPrepago(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String caso) throws InterruptedException, IOException
    {
        String dn = CargaVariables.LeerCSV(0, "Portabilidad Prepago Flap", caso);
        String rfc = CargaVariables.LeerCSV(1, "Portabilidad Prepago Flap", caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Portabilidad Prepago Flap", caso);
        String apellidop = CargaVariables.LeerCSV(3, "Portabilidad Prepago Flap", caso);
        String apellidom = CargaVariables.LeerCSV(4, "Portabilidad Prepago Flap", caso);
        String correo = CargaVariables.LeerCSV(5, "Portabilidad Prepago Flap", caso);
        String tel = CargaVariables.LeerCSV(6, "Portabilidad Prepago Flap", caso);
        String calle = CargaVariables.LeerCSV(8, "Portabilidad Prepago Flap", caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Portabilidad Prepago Flap", caso);
        String numInterior = CargaVariables.LeerCSV(10, "Portabilidad Prepago Flap", caso);
        String cp = CargaVariables.LeerCSV(7, "Portabilidad Prepago Flap", caso);
        String ciudad = CargaVariables.LeerCSV(13, "Portabilidad Prepago Flap", caso);
        String colonia = CargaVariables.LeerCSV(12, "Portabilidad Prepago Flap", caso);
        String ine = CargaVariables.LeerCSV(14, "Portabilidad Prepago Flap", caso);
        String año = CargaVariables.LeerCSV(16, "Portabilidad Prepago Flap", caso);
        String mes = CargaVariables.LeerCSV(17, "Portabilidad Prepago Flap", caso);
        String  dia= CargaVariables.LeerCSV(18, "Portabilidad Prepago Flap", caso);
        String curp= CargaVariables.LeerCSV(19, "Portabilidad Prepago Flap", caso);
        String modeloTel= CargaVariables.LeerCSV(20, "Portabilidad Prepago Flap", caso);
        
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        
        SelenElem.Click(By.xpath("//span[.='Portabilidad']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar Portabilidad > Prepago", OPG.SS());
        
        SelenElem.Click(By.xpath("//span[.='Prepago']"));
        Thread.sleep(4000);
        reporte.AddImageToReport("Seleccionar smartphone ", OPG.SS());
        
        SelenElem.ClickByXpath(WebDrive.WebDriver,"//img[contains(@alt, '" + modeloTel + "')]"); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        
        reporte.AddImageToReport("Ver detalle del Smartphone ", OPG.SS());
        SelenElem.focus(By.className("rating-summary"));
        reporte.AddImageToReport("Ver detalle del Smartphone ", OPG.SS());
        SelenElem.focus(By.id("js_input_dn_button_portabilidad"));
        reporte.AddImageToReport(" ", OPG.SS());
        
        SelenElem.Click(By.id("js_dn_input_portabilidad"));
        SelenElem.TypeText(By.id("js_dn_input_portabilidad"), dn);
        reporte.AddImageToReport("Visualizar el detalle del smartphone e ingresar DN y dar clic en aceptar ", OPG.SS());
        
        SelenElem.Click(By.id("js_input_dn_button_portabilidad"));
        SelenElem.WaitForLoad(By.xpath("//span[.='Continuar']"), 20);
        reporte.AddImageToReport("", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.xpath("//span[.='Continuar']"));
        reporte.AddImageToReport("Elegir plan con recarga", OPG.SS());
        
        SelenElem.Click(By.id("js_submit_miniparrilla_portabilidad_prepago"));
        SelenElem.WaitForLoad(By.id("portability_number"), 20);
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        SelenElem.focus(By.id("eliminar-recarga"));
        reporte.AddImageToReport(" ", OPG.SS());
        SelenElem.focus(By.className("data-section_item"));
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/section/div/div[1]/div/button"));
        
        
        SelenElem.Click(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/section/div/div[1]/div/button"));
        Thread.sleep(500);
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        
        da.hideKeyBoard();
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 5);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-email"), correo);
        da.hideKeyBoard();
        da.slide("Up", 1);
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        da.hideKeyBoard();
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.Click(By.id("checkPrivacyOne"));
        da.hideKeyBoard();
        da.slide("Up", 1);
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("botonTusDatos"));
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 15);
        
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        da.slide("Down", 2);
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());
        
        SelenElem.focus(By.id("checkout-paso2-renovaciones"));
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("checkout-paso2-renovaciones"));
       
        SelenElem.WaitForLoad(By.id("valid-nip"), 10);
        
        Thread.sleep(1000);
        SelenElem.Click(By.id("checkout-paso4-porta-pre"));
        Thread.sleep(300);
        reporte.AddImageToReport("Error en Error en NIP, CURP y Fecha Ventana de Cambio", OPG.ScreenShotElementInstant());
    }
     
     public void PortabilidadPrepagoSIMLiferay(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String caso) throws InterruptedException, IOException
    {
        String Dn = CargaVariables.LeerCSV(0, "Portabilidad Prepago Flap", caso);
        String SkuRecarga = CargaVariables.LeerCSV(1, "Portabilidad Prepago Flap", caso);
        String Monto = CargaVariables.LeerCSV(2, "Portabilidad Prepago Flap", caso);
        String Nombre = CargaVariables.LeerCSV(3, "Portabilidad Prepago Flap", caso);
        String ApellPa = CargaVariables.LeerCSV(4, "Portabilidad Prepago Flap", caso);
        String ApellMa = CargaVariables.LeerCSV(5, "Portabilidad Prepago Flap", caso);
        String Telefono = CargaVariables.LeerCSV(6, "Portabilidad Prepago Flap", caso);
        String TelefonoAdic = CargaVariables.LeerCSV(7, "Portabilidad Prepago Flap", caso);
        String Correo = CargaVariables.LeerCSV(8, "Portabilidad Prepago Flap", caso);
        String Rfc = CargaVariables.LeerCSV(9, "Portabilidad Prepago Flap", caso);
        String CP = CargaVariables.LeerCSV(10, "Portabilidad Prepago Flap", caso);
        String Nip = CargaVariables.LeerCSV(11, "Portabilidad Prepago Flap", caso);
        String Curp = CargaVariables.LeerCSV(12, "Portabilidad Prepago Flap", caso);
        String Dia = CargaVariables.LeerCSV(13, "Portabilidad Prepago Flap", caso);
        String NumTarjeta = CargaVariables.LeerCSV(14, "Portabilidad Prepago Flap", caso);
        String MesTarjeta = CargaVariables.LeerCSV(15, "Portabilidad Prepago Flap", caso);
        String AñoTarjeta = CargaVariables.LeerCSV(16, "Portabilidad Prepago Flap", caso);
        String Cvv = CargaVariables.LeerCSV(17, "Portabilidad Prepago Flap", caso);
        
        WebDrive.WebDriver.navigate().to(Url + "liferay.php");
        SelenElem.SelectOption(By.id("flowCode"), "portabilidad_prepago_login");
        SelenElem.Find(By.id("skuPlan")).clear();
        SelenElem.Find(By.id("namePlan")).clear();
        SelenElem.Find(By.id("skuRecarga")).clear();
        SelenElem.TypeText(By.id("skuRecarga"), SkuRecarga);
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.Find(By.id("skuDn")).clear();
        SelenElem.Find(By.id("rfc")).clear();
        reporte.AddImageToReport("Pagina Principal", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.name("Comprar"));
        
        SelenElem.WaitForLoad(By.id("mismo"), 10);
        SelenElem.Click(By.id("mismo"));
        SelenElem.TypeText(By.id("dnPorta"), Dn);
        da.hideKeyBoard();
        reporte.AddImageToReport("Ingresar Dn", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("btn-porta"));
        
        Thread.sleep(2000);
        SelenElem.SelectOption(By.id("monto_recarga"), Monto);
        reporte.AddImageToReport("Se ingresa monto a aplicar", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.xpath("//input[@value='Aplicar']"));
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 10);
        SelenElem.TypeText(By.id("valid-nombre"), Nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), ApellPa);
        SelenElem.TypeText(By.id("valid-appelido-m"), ApellMa);
        SelenElem.TypeText(By.id("telefono-contacto"), Telefono);
        SelenElem.TypeText(By.id("additionalContactPhone"), TelefonoAdic);
        SelenElem.TypeText(By.id("valid-email"), Correo);
        SelenElem.TypeText(By.id("valida-rfc"), Rfc);
        da.hideKeyBoard();
        SelenElem.Click(By.id("checkPrivacyOne"));
        reporte.AddImageToReport("Sección tus datos del checkout", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("botonTusDatos"));
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 10);
        SelenElem.Click(By.id("inv-mail2"));
        SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
        SelenElem.TypeText(By.id("js_codigo_postal"), CP);
        da.hideKeyBoard();
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        Thread.sleep(3000);
        SelenElem.Click(By.id("99090073cac"));
        reporte.AddImageToReport("Sección opciones de envío del checkout", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("checkout-paso2-renovaciones"));
        
        SelenElem.WaitForLoad(By.id("valid-nip"), 10);
        SelenElem.TypeText(By.id("valid-nip"), Nip);
        SelenElem.TypeText(By.id("valid-curp"), Curp);
        da.hideKeyBoard();
        SelenElem.Click(By.id("dateToPorta_datepicker"));
        SelenElem.Click(By.linkText(Dia));
        reporte.AddImageToReport("Sección cambiate a movistar", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("checkout-paso4-porta-pre"));
        
        SelenElem.WaitForLoad(By.id("ui-accordion-itemsSale-header-0"), 15);
        SelenElem.focus(By.id("1"));
        reporte.AddImageToReport("Medio de pago Flap", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("1"));
        
        SelenElem.WaitForLoad(By.id("numTarjeta"), 10);
        SelenElem.TypeText(By.id("numTarjeta"), NumTarjeta);
        SelenElem.SelectOption(By.id("vigenciames"), MesTarjeta);
        SelenElem.SelectOption(By.id("vigenciaanio"), AñoTarjeta);
        SelenElem.TypeText(By.id("cvv2"), Cvv);
        da.hideKeyBoard();
        reporte.AddImageToReport("", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("continuar"));
        
        SelenElem.WaitForLoad(By.id("btnFinalizar"), 10);
        da.slide("Down", 2);
        reporte.AddImageToReport("Información acerca del pago Flap", OPG.ScreenShotElementInstant());
        
        SelenElem.WaitForLoad(By.id("numero_portar"), 20);
        reporte.AddImageToReport("Página de pago realizada con exito ", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.ScreenShotElementInstant());
        
    }
}
