
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class ErrorCPSoloTerminalMercadoPago {

     public ErrorCPSoloTerminalMercadoPago (Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
       
       String dn = CargaVariables.LeerCSV(0, "Error codigo postal Solo Terminal - Mercado Pago", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Error codigo postal Solo Terminal - Mercado Pago", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Error codigo postal Solo Terminal - Mercado Pago", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Error codigo postal Solo Terminal - Mercado Pago", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Error codigo postal Solo Terminal - Mercado Pago", Caso);
        String correo = CargaVariables.LeerCSV(5, "Error codigo postal Solo Terminal - Mercado Pago", Caso);
        String tel = CargaVariables.LeerCSV(6, "Error codigo postal Solo Terminal - Mercado Pago", Caso);
        String calle = CargaVariables.LeerCSV(8, "Error codigo postal Solo Terminal - Mercado Pago", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Error codigo postal Solo Terminal - Mercado Pago", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Error codigo postal Solo Terminal - Mercado Pago", Caso);
        String cp = CargaVariables.LeerCSV(7, "Error codigo postal Solo Terminal - Mercado Pago", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Error codigo postal Solo Terminal - Mercado Pago", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Error codigo postal Solo Terminal - Mercado Pago", Caso);
        String ine = CargaVariables.LeerCSV(14, "Error codigo postal Solo Terminal - Mercado Pago", Caso);
        String rfcc = CargaVariables.LeerCSV(15, "Error codigo postal Solo Terminal - Mercado Pago", Caso);
        String tarjeta = CargaVariables.LeerCSV(17, "Error codigo postal Solo Terminal - Mercado Pago", Caso);
        String vencimiento = CargaVariables.LeerCSV(18, "Error codigo postal Solo Terminal - Mercado Pago", Caso);
        String cvv = CargaVariables.LeerCSV(19, "Error codigo postal Solo Terminal - Mercado Pago", Caso);

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        
        reporte.AddImageToReport("Se selecciona el telefono", OPG.ScreenShotElementFocus(By.xpath("//a[@href='" + Url + "iphone-xr-lte-amarillo-64gb-apple.html']")));
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + CargaVariables.LeerCSV(16,"Error codigo postal Solo Terminal - Mercado Pago",Caso) + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        
        Thread.sleep(3000);
        SelenElem.focus(By.xpath("//*[@id=\"solo-smartphone__content\"]/form/button/p"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//*[@id=\"solo-smartphone__content\"]/form/button/p"));
        reporte.AddImageToReport("Resumen de compra ",OPG.SS() );
        SelenElem.focus(By.xpath("//*[@id=\"section-equipo\"]/div[2]/div[3]/a"));
        reporte.AddImageToReport("",OPG.SS() );
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 5);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        SelenElem.Click(By.id("checkPrivacyOne"));
        
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("recibeFactura"));
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.focus(By.id("btn_continuar"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("btn_continuar"));
        OPG.cargaAjax2();
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 2);
        SelenElem.Click(By.id("inv-mail2"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("js_codigo_postal"), "0");
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        Thread.sleep(500);
        reporte.AddImageToReport("Validacion Error en codigo postal", OPG.ScreenShotElementInstant());
        
        SelenElem.TypeText(By.id("js_codigo_postal"), "0000");
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        Thread.sleep(500);
        OPG.cargaAjax2();
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        Thread.sleep(500);
        reporte.AddImageToReport("Validacion Error en codigo postal", OPG.ScreenShotElementInstant());
        //reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());
        
        
       

     }
    
}
