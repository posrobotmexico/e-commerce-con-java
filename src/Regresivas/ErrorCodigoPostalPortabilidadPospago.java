
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class ErrorCodigoPostalPortabilidadPospago {

     public ErrorCodigoPostalPortabilidadPospago(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
        
        String dn = CargaVariables.LeerCSV(0, "Error de Codigo Postal Portabilidad Pospago", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Error de Codigo Postal Portabilidad Pospago", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Error de Codigo Postal Portabilidad Pospago", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Error de Codigo Postal Portabilidad Pospago", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Error de Codigo Postal Portabilidad Pospago", Caso);
        String correo = CargaVariables.LeerCSV(5, "Error de Codigo Postal Portabilidad Pospago", Caso);
        String tel = CargaVariables.LeerCSV(6, "Error de Codigo Postal Portabilidad Pospago", Caso);
        String calle = CargaVariables.LeerCSV(8, "Error de Codigo Postal Portabilidad Pospago", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Error de Codigo Postal Portabilidad Pospago", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Error de Codigo Postal Portabilidad Pospago", Caso);
        String cp = CargaVariables.LeerCSV(7, "Error de Codigo Postal Portabilidad Pospago", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Error de Codigo Postal Portabilidad Pospago", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Error de Codigo Postal Portabilidad Pospago", Caso);
        String ine = CargaVariables.LeerCSV(14, "Error de Codigo Postal Portabilidad Pospago", Caso);
        String año = CargaVariables.LeerCSV(16, "Error de Codigo Postal Portabilidad Pospago", Caso);
        String mes = CargaVariables.LeerCSV(17, "Error de Codigo Postal Portabilidad Pospago", Caso);
        String  dia= CargaVariables.LeerCSV(18, "Error de Codigo Postal Portabilidad Pospago", Caso);
        String curp= CargaVariables.LeerCSV(19, "Error de Codigo Postal Portabilidad Pospago", Caso);
       

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        SelenElem.waitForLoadPage();
        Thread.sleep(3000);

        WebDrive.WebDriver.navigate().to(Url + "cambiate-a-movistar");
        reporte.AddImageToReport("Se ingresa a el flujo de Cámbiate a Movistar", OPG.SS());
        SelenElem.focus(By.xpath("//p[.='8 GB ']"));
        reporte.AddImageToReport("Se ingresa a el flujo de Cámbiate a Movistar", OPG.SS());
        SelenElem.Click(By.id("DN"));
        SelenElem.TypeText(By.id("DN"),dn);
        reporte.AddImageToReport(" Ingresar DN", OPG.SS());
        
        SelenElem.Click(By.id("portar_DN"));
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.ScreenShotElement());
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-nombre"), 6);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), año);
        SelenElem.SelectOption(By.className("ui-datepicker-month"), mes);
        SelenElem.Click(By.linkText(dia));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("calleNumeroInterior"), numInterior);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.Click(By.id("checkout-step4-check-02"));
        
        SelenElem.Click(By.id("paso1-pospago-porta"));
        
        OPG.cargaAjax2();
        
        OPG.cargaAjax2();
        Thread.sleep(500);
        OPG.cargaAjax2();
        
        Thread.sleep(40000);
        SelenElem.WaitForLoad(By.id("inv-mail2"), 15);
        
        SelenElem.Click(By.id("inv-mail2"));
        //OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("js_codigo_postal"), "066");
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        Thread.sleep(500);
        reporte.AddImageToReport("Validacion Error en codigo postal", OPG.ScreenShotElementInstant());
        
        
        
    
    }
    
}
