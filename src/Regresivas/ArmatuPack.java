/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.OperacionesArmatuPack;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class ArmatuPack {
    private String resultado;
    
    public ArmatuPack()
    {
        
    }
    
    public void MovistarPersonalizado5Gb(Report reporte,OperacionesGenerales OPG,OperacionesArmatuPack armatupack,String DN,String Url,WebAction SelenElem,WebDriv WebDrive) throws InterruptedException, IOException
    {
        resultado = "Prueba fallida";
        
        reporte.AddImageToReport("Iniciamos desde la pagina principal", OPG.ScreenShotElement());
        WebDrive.WebDriver.navigate().to(Url + "customplan/movistarlibre");
        reporte.AddImageToReport("Navegamos a la pagina" + Url + "customplan/movistarlibre y seleccionamos prepago (superior)", OPG.ScreenShotElementFocus(By.xpath("//form[@id='custom-prepaid']//descendant::button")));
        //armatupack.SeleccionarPrepago();
        SelenElem.WaitForLoad(By.id("menu_list"), 15);
        OPG.SelectMenu("Oferta Prepago");
        armatupack.IngresaNumero(DN);
        reporte.AddImageToReport("Ingresamos el numero y verificamos", OPG.ScreenShotElementFocus(By.className("c-button--green")));
        armatupack.VerificarNumero();
        armatupack.IngresaCodigo123();
        reporte.AddImageToReport("Ingresamos el código y damos click en verificar", OPG.ScreenShotElement());
        armatupack.VerificarCodigo123();
        reporte.AddImageToReport("Beneficios incluidos", OPG.ScreenShotElement());
        armatupack.EligeCaracteristicas();
        reporte.AddImageToReport("Elegimos las caracteristicas", OPG.ScreenShotElement());
        armatupack.ModulosAdicionales();
        reporte.AddImageToReport("Agregamos las caracteristicas adicionales", OPG.ScreenShotElement());
        armatupack.VerResumendePlanes();
        reporte.AddImageToReport("Vemos el resumen de la compra", OPG.ScreenShotElementFocus(By.className("table-customPlan-cart")));
        armatupack.VerResumendeCosto();
        reporte.AddImageToReport("Vemos el total a pagar y damos click en lo quiero!", OPG.ScreenShotElementFocus(By.id("terms")));
        armatupack.VerificaCuadroConfirmacion(OPG,reporte);        
        armatupack.CompletarCheckout();
        reporte.AddImageToReport("Llenamos el campo de datos", OPG.ScreenShotElement());
        armatupack.PushButtonContinuar();
        reporte.AddImageToReport("Damos click en pagar", OPG.ScreenShotElement());
        armatupack.Pagar();
        reporte.AddImageToReport("Success", OPG.ScreenShotElementFocus(By.className("content_info")));
                
        resultado = "Prueba exitosa";
    }

    public String getResultado() {
        return resultado;
    }
}