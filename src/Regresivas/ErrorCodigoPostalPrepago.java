/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class ErrorCodigoPostalPrepago {

     public ErrorCodigoPostalPrepago (Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
        
        String dn = CargaVariables.LeerCSV(0, "Error de Codigo Postal Prepago", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Error de Codigo Postal Prepago", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Error de Codigo Postal Prepago", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Error de Codigo Postal Prepago", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Error de Codigo Postal Prepago", Caso);
        String correo = CargaVariables.LeerCSV(5, "Error de Codigo Postal Prepago", Caso);
        String tel = CargaVariables.LeerCSV(6, "Error de Codigo Postal Prepago", Caso);
        String calle = CargaVariables.LeerCSV(8, "Error de Codigo Postal Prepago", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Error de Codigo Postal Prepago", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Error de Codigo Postal Prepago", Caso);
        String cp = CargaVariables.LeerCSV(7, "Error de Codigo Postal Prepago", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Error de Codigo Postal Prepago", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Error de Codigo Postal Prepago", Caso);
        String ine = CargaVariables.LeerCSV(14, "Error de Codigo Postal Prepago", Caso);
        String rfcc = CargaVariables.LeerCSV(15, "Error de Codigo Postal Prepago", Caso);

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "prepago/oferta");
        Thread.sleep(3000);
        reporte.AddImageToReport("Seleccionamos el flujo de Prepago", OPG.SS());
        
        SelenElem.Click(By.id("sin_numero"));
        reporte.AddImageToReport("Seleccionar quiero un número nuevo", OPG.SS());
        SelenElem.Click(By.id("btnArt"));
        
        SelenElem.Click(By.id("plan1"));
        reporte.AddImageToReport("Seleccionar sin Smartphone y continuar", OPG.SS());
        SelenElem.Click(By.id("continueWithoutSmartphone"));
        
        //OPG.cargaAjax2();
        reporte.AddImageToReport(" Elegir plan con recarga", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"parrilla_planes_form_sim\"]/div[1]/div/div[1]/div[6]/img"));
        reporte.AddImageToReport(" Elegir plan con recarga", OPG.SS());
        SelenElem.waitForLoadPage();
        SelenElem.ClickByXpath(WebDrive.WebDriver,"//button[.='Lo quiero']");
        //SelenElem.Click(By.xpath("//*[@id=\"parrilla_planes_form_sim\"]/div[1]/div/div[1]/div[10]/button"));
        
        //OPG.cargaAjax2();
        SelenElem.WaitFor(By.id("continueToCheckout"));
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElement());
        SelenElem.Click(By.id("continueToCheckout"));
        
        //WebDrive.WebDriver.navigate().to(Url + "flujo-ventas/prepago-sin-equipo/cart/");
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        SelenElem.ClickById(WebDrive.WebDriver,"continueToCheckout");
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 5);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        SelenElem.Click(By.id("checkPrivacyOne"));
        
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        SelenElem.Click(By.id("btn_continuar"));
        
        OPG.cargaAjax2();
        
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 2);
        SelenElem.Click(By.id("inv-mail2"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("js_codigo_postal"), "0660");
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        Thread.sleep(500);
        reporte.AddImageToReport("Error en codigo postal", OPG.ScreenShotElementInstant());
       
    
    }
}
