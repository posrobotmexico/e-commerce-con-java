
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.OpcionesEnvio;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class PrepagoTerminalFlap {

     public PrepagoTerminalFlap (Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
         
        String dn = CargaVariables.LeerCSV(0, "Prepago Terminal - Flap", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Prepago Terminal - Flap", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Prepago Terminal - Flap", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Prepago Terminal - Flap", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Prepago Terminal - Flap", Caso);
        String correo = CargaVariables.LeerCSV(5, "Prepago Terminal - Flap", Caso);
        String tel = CargaVariables.LeerCSV(6, "Prepago Terminal - Flap", Caso);
        String calle = CargaVariables.LeerCSV(8, "Prepago Terminal - Flap", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Prepago Terminal - Flap", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Prepago Terminal - Flap", Caso);
        String cp = CargaVariables.LeerCSV(7, "Prepago Terminal - Flap", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Prepago Terminal - Flap", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Prepago Terminal - Flap", Caso);
        String ine = CargaVariables.LeerCSV(14, "Prepago Terminal - Flap", Caso);
        String rfcc = CargaVariables.LeerCSV(15, "Prepago Terminal - Flap", Caso);

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        SelenElem.Click(By.id("boton_smartphone")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        Thread.sleep(1000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());

        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Seleccionar Smartphone", OPG.SS());
        
        reporte.AddImageToReport("Se selecciona el telefono", OPG.ScreenShotElementFocus(By.xpath("//a[@href='" + Url + "iphone-xr-lte-amarillo-64gb-apple.html?category=43']")));
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + CargaVariables.LeerCSV(16,"Prepago Terminal - Flap",Caso) + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        Thread.sleep(2000);
        reporte.AddImageToReport("Máxima Cantidad permitida por Terminal", OPG.SS());
        SelenElem.focus(By.xpath("//p[.=' Precio final del equipo']"));
        reporte.AddImageToReport("", OPG.SS());
        //SelenElem.Click(By.id("btn-buy-three"));
        
        SelenElem.Click(By.xpath("//p[.='Comprar']"));
        reporte.AddImageToReport("Resumen de compra ",OPG.SS() );
        SelenElem.focus(By.xpath("//p[.='¿Tienes una promoción?']"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 5);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        SelenElem.Click(By.id("checkPrivacyOne"));
        
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        SelenElem.Click(By.id("btn_continuar"));
        
        OPG.cargaAjax2();
        
        /*
        SelenElem.Click(By.id("inv-mail2"));
        OPG.cargaAjax2();
        SelenElem.Click(By.id("cp"));
        SelenElem.Click(By.id("cp"));
        SelenElem.TypeText(By.id("cp"), "06600");
        SelenElem.Click(By.xpath("//*[@id=\"longlat\"]"));
        OPG.cargaAjax2();
        SelenElem.Click(By.id("99090073cac"));
        reporte.AddImageToReport("Seleccionar opcion de envio", OPG.SS());*/
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        SelenElem.focus(By.id("js-continuar-metodos-envio"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        
        OPG.cargaAjax2();
        SelenElem.Click(By.id("continuar-checkout-out"));
        SelenElem.ClickById(WebDrive.WebDriver, "continuar-checkout-out");
        //OPG.cargaAjax2();
        
        SelenElem.WaitForLoad(By.id("imagen-cliente"),20);
        reporte.AddImageToReport("Ingresamos a la pagina principal de FLAP", OPG.ScreenShotElement());
        Thread.sleep(3000);
        SelenElem.focus(By.id("1"));
        reporte.AddImageToReport("Medio de pago Flap", OPG.ScreenShotElement());
        SelenElem.Click(By.id("1"));
        SelenElem.WaitForLoad(By.id("contrato_108553_1"), 15);
        SelenElem.focus(By.id("contrato_108553_1"));
        reporte.AddImageToReport("", OPG.ScreenShotElement());
        SelenElem.Click(By.id("contrato_108553_1"));
        SelenElem.WaitForLoad(By.id("numTarjeta"), 15);
        SelenElem.focus(By.id("numTarjeta"));          
        SelenElem.TypeText(By.id("numTarjeta"), CargaVariables.LeerCSV(2,"Config","Tarjeta_Flap"));
        SelenElem.TypeText(By.id("cvv2"), CargaVariables.LeerCSV(4,"Config","Tarjeta_Flap"));   
        SelenElem.TypeText(By.id("vigenciames"), CargaVariables.LeerCSV(5,"Config","Tarjeta_Flap"));
        SelenElem.TypeText(By.id("vigenciaanio"), CargaVariables.LeerCSV(6,"Config","Tarjeta_Flap"));
        reporte.AddImageToReport("Llenamos los datos de pago y damos click en pagar", OPG.ScreenShotElement());
        SelenElem.Click(By.id("continuar"));
        
        Thread.sleep(2000);
        reporte.AddImageToReport("", OPG.ScreenShotElement());
        
        SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        SelenElem.focus(By.xpath("//p[.='Datos de envío']"));
        reporte.AddImageToReport("", OPG.SS());
       

     }
}

