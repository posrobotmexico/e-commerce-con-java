
package Regresivas;

import Core.DeviceActions;
import Core.WebAction;
import Core.WebDriv;
import Core.Report;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

/**
 *
 * @author LAPTOP-JORGE
 */
public class SoloTerminalVitrina {
    
    public SoloTerminalVitrina(){
        
    } 
    
    public void SoloTerminalDescuentoCupon(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, DeviceActions da, String Caso) throws InterruptedException, IOException 
    {
        
        String ModTel = CargaVariables.LeerCSV(0, "Solo Terminal - Flap - Vitrina", Caso);
        String Cupon = CargaVariables.LeerCSV(1, "Solo Terminal - Flap - Vitrina", Caso);
        String Nombre = CargaVariables.LeerCSV(2, "Solo Terminal - Flap - Vitrina", Caso);
        String ApellidoP = CargaVariables.LeerCSV(3, "Solo Terminal - Flap - Vitrina", Caso);
        String ApellidoM = CargaVariables.LeerCSV(4, "Solo Terminal - Flap - Vitrina", Caso);
        String Correo = CargaVariables.LeerCSV(5, "Solo Terminal - Flap - Vitrina", Caso);
        String Telefono = CargaVariables.LeerCSV(6, "Solo Terminal - Flap - Vitrina", Caso);
        String TelefAdicional = CargaVariables.LeerCSV(7, "Solo Terminal - Flap - Vitrina", Caso);
        String RFC = CargaVariables.LeerCSV(8, "Solo Terminal - Flap - Vitrina", Caso);
        String CodPostal = CargaVariables.LeerCSV(9, "Solo Terminal - Flap - Vitrina", Caso);
        String Calle = CargaVariables.LeerCSV(10, "Solo Terminal - Flap - Vitrina", Caso);
        String NumExt = CargaVariables.LeerCSV(11, "Solo Terminal - Flap - Vitrina", Caso);
        String NumInt = CargaVariables.LeerCSV(12, "Solo Terminal - Flap - Vitrina", Caso);
        String Colonia = CargaVariables.LeerCSV(13, "Solo Terminal - Flap - Vitrina", Caso);
        String Tienda = CargaVariables.LeerCSV(14, "Solo Terminal - Flap - Vitrina", Caso);
        String Tarjeta = CargaVariables.LeerCSV(15, "Solo Terminal - Flap - Vitrina", Caso);
        String Cvv = CargaVariables.LeerCSV(16, "Solo Terminal - Flap - Vitrina", Caso);
        String Mes = CargaVariables.LeerCSV(17, "Solo Terminal - Flap - Vitrina", Caso);
        String Año = CargaVariables.LeerCSV(18, "Solo Terminal - Flap - Vitrina", Caso);

        WebDrive.WebDriver.navigate().to(Url + "smartphone-open-box.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Continuamos con la pagina principal", OPG.SS());
        SelenElem.focus(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        reporte.AddImageToReport("Se procede a seleccionar el Smartphone", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Se observan los detalles del Smartphone", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//button[@class='fondo-boton-comprar']"));
        
        SelenElem.Click(By.name("coupon_code"));
        SelenElem.TypeText(By.name("coupon_code"), Cupon);
        SelenElem.Click(By.xpath("//button[@value='Aplicar Descuento']"));
        Thread.sleep(2000);
        reporte.AddImageToReport("Resumen de compra despues de ingresar Cupón", OPG.SS());
         
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 10);
        SelenElem.TypeText(By.name("name"), Nombre);
        SelenElem.TypeText(By.name("lastName"), ApellidoP);
        SelenElem.TypeText(By.name("lastName2"), ApellidoM);
        SelenElem.TypeText(By.name("email"), Correo);
        SelenElem.TypeText(By.name("phone"), Telefono);
        SelenElem.TypeText(By.name("additionalContactPhone"), TelefAdicional);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.Click(By.name("invoiceRequested"));
        
        SelenElem.TypeText(By.name("RFC"), RFC);
        SelenElem.TypeText(By.name("billingPostalCode"), CodPostal);
        SelenElem.TypeText(By.name("billingStreet"), Calle);
        SelenElem.TypeText(By.name("billingNum"), NumExt);
        SelenElem.TypeText(By.name("billingInt"), NumInt);
        Thread.sleep(3000);
        SelenElem.Click(By.id("colonia"));
        SelenElem.TypeText(By.id("colonia"), Colonia);
        
        SelenElem.Click(By.name("privacyCheck"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("btn_continuar"));
        
        SelenElem.Click(By.id("inv-mail2"));
        SelenElem.Click(By.id("js_codigo_postal"));
        SelenElem.TypeText(By.id("js_codigo_postal"), CodPostal);
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        
        SelenElem.WaitForLoad(By.id("js_geo_option_cards"), 10);
        SelenElem.ClickButton(WebDrive.WebDriver, SelenElem, "ClassName", "geo-cac-elocker-option__place-title");
        reporte.AddImageToReport("Sección opciones de envío", OPG.SS());
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        
        reporte.AddImageToReport("Resumen de Compra", OPG.SS());
        
        SelenElem.Click(By.id("continuar-checkout-out"));
        
        SelenElem.WaitForLoad(By.id("logo_client_mobile"), 10);
        reporte.AddImageToReport("Medio de pago FLAP", OPG.SS());
        SelenElem.Click(By.id("mp2-detail-icon"));
        
        SelenElem.Click(By.id("1"));
        SelenElem.Click(By.id("contrato_108553_1"));
        
        SelenElem.Click(By.id("numTarjeta"));
        SelenElem.TypeText(By.id("numTarjeta"), Tarjeta);
        SelenElem.Click(By.id("vigenciames"));
        SelenElem.TypeText(By.id("vigenciames"), Mes);
        SelenElem.Click(By.id("vigenciaanio"));
        SelenElem.TypeText(By.id("vigenciaanio"), Año);
        SelenElem.Click(By.id("cvv2"));
        SelenElem.TypeText(By.id("cvv2"), Cvv);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.id("continuar"));
        Thread.sleep(2000);
        reporte.AddImageToReport("", OPG.SS());
        
        Thread.sleep(10000);
        reporte.AddImageToReport("Página de pago realizada con éxito", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
       
        
    }  

    public void SoloTerminalCupon(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, DeviceActions da,String Caso) throws InterruptedException, IOException 
    {
        
        String ModTel = CargaVariables.LeerCSV(0, "Solo Terminal - Flap - Vitrina", Caso);
        String Cupon = CargaVariables.LeerCSV(1, "Solo Terminal - Flap - Vitrina", Caso);
        String Nombre = CargaVariables.LeerCSV(2, "Solo Terminal - Flap - Vitrina", Caso);
        String ApellidoP = CargaVariables.LeerCSV(3, "Solo Terminal - Flap - Vitrina", Caso);
        String ApellidoM = CargaVariables.LeerCSV(4, "Solo Terminal - Flap - Vitrina", Caso);
        String Correo = CargaVariables.LeerCSV(5, "Solo Terminal - Flap - Vitrina", Caso);
        String Telefono = CargaVariables.LeerCSV(6, "Solo Terminal - Flap - Vitrina", Caso);
        String TelefAdicional = CargaVariables.LeerCSV(7, "Solo Terminal - Flap - Vitrina", Caso);
        String CodPostal = CargaVariables.LeerCSV(8, "Solo Terminal - Flap - Vitrina", Caso);
        String Tienda = CargaVariables.LeerCSV(9, "Solo Terminal - Flap - Vitrina", Caso);
        String Tarjeta = CargaVariables.LeerCSV(10, "Solo Terminal - Flap - Vitrina", Caso);
        String Cvv = CargaVariables.LeerCSV(11, "Solo Terminal - Flap - Vitrina", Caso);
        String Mes = CargaVariables.LeerCSV(12, "Solo Terminal - Flap - Vitrina", Caso);
        String Año = CargaVariables.LeerCSV(13, "Solo Terminal - Flap - Vitrina", Caso);

        WebDrive.WebDriver.navigate().to(Url + "smartphone-open-box.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Continuamos con la pagina principal", OPG.SS());
        SelenElem.focus(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        reporte.AddImageToReport("Se procede a seleccionar el Smartphone", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        reporte.AddImageToReport("Se observan los detalles del Smartphone", OPG.SS());
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Detalle del smartphone", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//button[@class='fondo-boton-comprar']"));

        SelenElem.Click(By.name("coupon_code"));
        SelenElem.TypeText(By.name("coupon_code"), Cupon);
        SelenElem.Click(By.xpath("//button[@value='Aplicar Descuento']"));
        Thread.sleep(2000);
        reporte.AddImageToReport("Resumen de compra despues de ingresar Cupón", OPG.SS());
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 10);
        SelenElem.TypeText(By.name("name"), Nombre);
        SelenElem.TypeText(By.name("lastName"), ApellidoP);
        SelenElem.TypeText(By.name("lastName2"), ApellidoM);
        SelenElem.TypeText(By.name("email"), Correo);
        SelenElem.TypeText(By.name("phone"), Telefono);
        SelenElem.TypeText(By.name("additionalContactPhone"), TelefAdicional);
        da.hideKeyBoard();
        
        SelenElem.Click(By.name("privacyCheck"));
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.Click(By.id("btn_continuar"));
        
        SelenElem.Click(By.id("inv-mail2"));
        SelenElem.Click(By.id("js_codigo_postal"));
        SelenElem.TypeText(By.id("js_codigo_postal"), CodPostal);
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        
        SelenElem.WaitForLoad(By.id("js_geo_option_cards"), 10);
        SelenElem.ClickButton(WebDrive.WebDriver, SelenElem, "ClassName", "geo-cac-elocker-option__place-title");
        reporte.AddImageToReport("Sección opciones de envío", OPG.SS());
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        
        reporte.AddImageToReport("Resumen de Compra", OPG.SS());
        SelenElem.Click(By.id("continuar-checkout-out"));
           
        SelenElem.WaitForLoad(By.id("logo_client_mobile"), 10);
        reporte.AddImageToReport("Medio de pago FLAP", OPG.SS());
        SelenElem.Click(By.id("mp2-detail-icon"));
        
        SelenElem.Click(By.id("1"));
        SelenElem.Click(By.id("contrato_108553_1"));
        
        SelenElem.Click(By.id("numTarjeta"));
        SelenElem.TypeText(By.id("numTarjeta"), Tarjeta);
        SelenElem.Click(By.id("vigenciames"));
        SelenElem.TypeText(By.id("vigenciames"), Mes);
        SelenElem.Click(By.id("vigenciaanio"));
        SelenElem.TypeText(By.id("vigenciaanio"), Año);
        SelenElem.Click(By.id("cvv2"));
        SelenElem.TypeText(By.id("cvv2"), Cvv);
        reporte.AddImageToReport("Medios de pago FLAP", OPG.SS());
        SelenElem.Click(By.id("continuar"));
        Thread.sleep(2000);
        reporte.AddImageToReport("", OPG.SS());
        
        Thread.sleep(10000);
        reporte.AddImageToReport("Página de pago realizada con éxito", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        

    }
    
    public void SoloTerminalDescuento(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, DeviceActions da,String Caso) throws InterruptedException, IOException 
    {
        
        String ModTel = CargaVariables.LeerCSV(0, "Solo Terminal - Flap - Vitrina", Caso);
        String Nombre = CargaVariables.LeerCSV(1, "Solo Terminal - Flap - Vitrina", Caso);
        String ApellidoP = CargaVariables.LeerCSV(2, "Solo Terminal - Flap - Vitrina", Caso);
        String ApellidoM = CargaVariables.LeerCSV(3, "Solo Terminal - Flap - Vitrina", Caso);
        String Correo = CargaVariables.LeerCSV(4, "Solo Terminal - Flap - Vitrina", Caso);
        String Telefono = CargaVariables.LeerCSV(5, "Solo Terminal - Flap - Vitrina", Caso);
        String TelefAdicional = CargaVariables.LeerCSV(6, "Solo Terminal - Flap - Vitrina", Caso);
        String CodPostal = CargaVariables.LeerCSV(7, "Solo Terminal - Flap - Vitrina", Caso);
        String Tarjeta = CargaVariables.LeerCSV(8, "Solo Terminal - Flap - Vitrina", Caso);
        String Cvv = CargaVariables.LeerCSV(9, "Solo Terminal - Flap - Vitrina", Caso);
        String Mes = CargaVariables.LeerCSV(10, "Solo Terminal - Flap - Vitrina", Caso);
        String Año = CargaVariables.LeerCSV(11, "Solo Terminal - Flap - Vitrina", Caso);

        WebDrive.WebDriver.navigate().to(Url + "smartphone-open-box.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Continuamos con la pagina principal", OPG.SS());
        SelenElem.focus(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        reporte.AddImageToReport("Se procede a seleccionar el Smartphone", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        reporte.AddImageToReport("Se observan los detalles del Smartphone", OPG.SS());
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Detalle del smartphone", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//button[@class='fondo-boton-comprar']"));
        
        SelenElem.Click(By.id("continueToCheckout"));;
           
        SelenElem.WaitForLoad(By.id("valid-nombre"), 10);
        SelenElem.TypeText(By.name("name"), Nombre);
        SelenElem.TypeText(By.name("lastName"), ApellidoP);
        SelenElem.TypeText(By.name("lastName2"), ApellidoM);
        SelenElem.TypeText(By.name("email"), Correo);
        SelenElem.TypeText(By.name("phone"), Telefono);
        SelenElem.TypeText(By.name("additionalContactPhone"), TelefAdicional);
        da.hideKeyBoard();
        
        SelenElem.Click(By.name("privacyCheck"));
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.Click(By.id("btn_continuar"));
         
        SelenElem.Click(By.id("inv-mail2"));
        SelenElem.Click(By.id("js_codigo_postal"));
        SelenElem.TypeText(By.id("js_codigo_postal"), CodPostal);
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        
        SelenElem.WaitForLoad(By.id("js_geo_option_cards"), 10);
        SelenElem.ClickButton(WebDrive.WebDriver, SelenElem, "ClassName", "geo-cac-elocker-option__place-title");
        reporte.AddImageToReport("Sección opciones de envío", OPG.SS());
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        
        reporte.AddImageToReport("Resumen de Compra", OPG.SS());
        SelenElem.Click(By.id("continuar-checkout-out"));
          
        SelenElem.WaitForLoad(By.id("logo_client_mobile"), 10);
        reporte.AddImageToReport("Medio de pago FLAP", OPG.SS());
        SelenElem.Click(By.id("mp2-detail-icon"));
        
        SelenElem.Click(By.id("1"));
        SelenElem.Click(By.id("contrato_108553_1"));
        
        SelenElem.Click(By.id("numTarjeta"));
        SelenElem.TypeText(By.id("numTarjeta"), Tarjeta);
        SelenElem.Click(By.id("vigenciames"));
        SelenElem.TypeText(By.id("vigenciames"), Mes);
        SelenElem.Click(By.id("vigenciaanio"));
        SelenElem.TypeText(By.id("vigenciaanio"), Año);
        SelenElem.Click(By.id("cvv2"));
        SelenElem.TypeText(By.id("cvv2"), Cvv);
        reporte.AddImageToReport("Medios de pago FLAP", OPG.SS());
        SelenElem.Click(By.id("continuar"));
        Thread.sleep(2000);
        reporte.AddImageToReport("", OPG.SS());
        
        Thread.sleep(10000);
        reporte.AddImageToReport("Página de pago realizada con éxito", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        
    }

    public void SoloTerminalPrecioCupon(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, DeviceActions da,String Caso) throws InterruptedException, IOException 
    {
        String ModTel = CargaVariables.LeerCSV(0, "Solo Terminal - Flap - Vitrina", Caso);
        String Cupon = CargaVariables.LeerCSV(1, "Solo Terminal - Flap - Vitrina", Caso);

        WebDrive.WebDriver.navigate().to(Url + "smartphone-open-box.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Continuamos con la pagina principal", OPG.SS());
        SelenElem.focus(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        reporte.AddImageToReport("Se procede a seleccionar el Smartphone", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));   
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Detalle del smartphone", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//button[@class='fondo-boton-comprar']"));
        
        SelenElem.Click(By.name("coupon_code"));
        SelenElem.TypeText(By.name("coupon_code"), Cupon);
        SelenElem.Click(By.xpath("//button[@value='Aplicar Descuento']"));
        reporte.AddImageToReport("Resumen de compra despues de ingresar Cupón", OPG.SS());
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        Thread.sleep(5000);
        reporte.AddImageToReport("Malla de terminales", OPG.SS());
        
    }

    public void SoloTerminalPrecioDescuento(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, DeviceActions da,String Caso) throws InterruptedException, IOException 
    {
        
        String ModTel = CargaVariables.LeerCSV(0, "Solo Terminal - Flap - Vitrina", Caso);

        WebDrive.WebDriver.navigate().to(Url + "smartphone-open-box.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Continuamos con la pagina principal", OPG.SS());
        SelenElem.focus(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        reporte.AddImageToReport("Se procede a seleccionar el Smartphone", OPG.SS());
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + ModTel + "')]"));
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Detalle del smartphone", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//button[@class='fondo-boton-comprar']"));
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        Thread.sleep(5000);
        reporte.AddImageToReport("Malla de terminales", OPG.SS());
        
    }
}
