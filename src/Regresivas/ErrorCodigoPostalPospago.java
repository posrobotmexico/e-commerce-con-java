
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.OperacionesGenerales;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class ErrorCodigoPostalPospago {

    public ErrorCodigoPostalPospago(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url,String Caso) throws InterruptedException
    {
        String dn = CargaVariables.LeerCSV(0, "Error de Codigo Postal Pospago", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Error de Codigo Postal Pospago", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Error de Codigo Postal Pospago", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Error de Codigo Postal Pospago", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Error de Codigo Postal Pospago", Caso);
        String correo = CargaVariables.LeerCSV(5, "Error de Codigo Postal Pospago", Caso);
        String tel = CargaVariables.LeerCSV(6, "Error de Codigo Postal Pospago", Caso);
        String calle = CargaVariables.LeerCSV(8, "Error de Codigo Postal Pospago", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Error de Codigo Postal Pospago", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Error de Codigo Postal Pospago", Caso);
        String cp = CargaVariables.LeerCSV(7, "Error de Codigo Postal Pospago", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Error de Codigo Postal Pospago", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Error de Codigo Postal Pospago", Caso);
        String ine = CargaVariables.LeerCSV(14, "Error de Codigo Postal Pospago", Caso);
        String rfcc = CargaVariables.LeerCSV(15, "Error de Codigo Postal Pospago", Caso);

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Se selecciona el flujo de telefonos", OPG.SS());
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Vemos los telefonos  ", OPG.SS());
        reporte.AddImageToReport("Se selecciona el telefono", OPG.ScreenShotElementFocus(By.xpath("//a[@href='" + Url + "iphone-xr-lte-amarillo-64gb-apple.html']")));
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + CargaVariables.LeerCSV(16,"Error de Codigo Postal Pospago",Caso) + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        reporte.AddImageToReport("Ver detalle de Smartphone y seleccionar plan", OPG.SS());
        SelenElem.waitForLoadPage();
        
        /////
        SelenElem.Click(By.id("soy-nuevo"));
        OPG.cargaAjax2();
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//*[@id=\"js_slider_mp_pospago\"]/div/div/div[5]/div/div/div/div"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.Click(By.xpath("//div[.='Quiero este plan']"));
        
        Thread.sleep(2000);
        SelenElem.focus(By.id("js_submit_miniparrilla_pospago"));
        Thread.sleep(1000);
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("js_submit_miniparrilla_pospago"));
        SelenElem.WaitForLoad(By.id("continueToCheckout"), 10);
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        
        reporte.AddImageToReport("Resumen de compra ", OPG.ScreenShotElementFocus(By.id("continueToCheckout")));
        SelenElem.WaitForLoad(By.id("continueToCheckout"), 4);
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.waitForLoadPage();
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        
         reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.focus(By.id("telefono-contacto"));
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), "1993");
        SelenElem.SelectOption(By.className("ui-datepicker-month"), "Mar");
        SelenElem.Click(By.linkText("3"));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), "006");
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        reporte.AddImageToReport("Error de codigo postal", OPG.SS());
        
    
    
    }
    
}
