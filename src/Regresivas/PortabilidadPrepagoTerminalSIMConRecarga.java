
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.OpcionesEnvio;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class PortabilidadPrepagoTerminalSIMConRecarga {

    public PortabilidadPrepagoTerminalSIMConRecarga(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException{
        
        String dn = CargaVariables.LeerCSV(0, "Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String correo = CargaVariables.LeerCSV(5, "Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String tel = CargaVariables.LeerCSV(6, "Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String calle = CargaVariables.LeerCSV(8, "Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String cp = CargaVariables.LeerCSV(7, "Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String ine = CargaVariables.LeerCSV(14, "Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String año = CargaVariables.LeerCSV(16, "Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String mes = CargaVariables.LeerCSV(17, "Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String  dia= CargaVariables.LeerCSV(18, "Portabilidad Prepago Terminal + SIM Con Recarga", Caso);
        String curp= CargaVariables.LeerCSV(19, "Portabilidad Prepago Terminal + SIM Con Recarga", Caso);

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        //SelenElem.Click(By.linkText("Cámbiate a Movistar"));
        SelenElem.WaitForLoad(By.id("menu_list"), 15);
        OPG.SelectMenu("Cámbiate a Movistar");
        
        SelenElem.WaitFor(By.id("sc2-dn"));
       
        SelenElem.Click(By.id("sc2-dn"));
        SelenElem.TypeText(By.id("sc2-dn"),dn);
        reporte.AddImageToReport("Seleccionamos el flujo de Cámbiate a Movistar", OPG.SS());
        
        SelenElem.Click(By.id("sc2-goPortaStep2"));
        SelenElem.waitForLoadPage();
       
        SelenElem.WaitForLoad(By.id("option_prepaid"),10);
        SelenElem.Click(By.id("option_prepaid"));
       
        Thread.sleep(350);
        reporte.AddImageToReport("Seleccionamos Quiero comprar un prepago", OPG.ScreenShotElement());
        SelenElem.Click(By.id("sc2-goToPortability"));
       
        SelenElem.focus(By.id("plan2"));
        reporte.AddImageToReport("Seleccionar con Smartphone y continuar", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[5]/div/div[3]/a"));
        reporte.AddImageToReport("Dar clic en ver mas equipos", OPG.SS());

        WebDrive.WebDriver.navigate().to(Url + "telefonos.html/");
        
        reporte.AddImageToReport("Seleccionar un smartphone", OPG.ScreenShotElementFocus(By.xpath("//a[@href='" + Url + "iphone-xr-lte-amarillo-64gb-apple.html']")));
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + CargaVariables.LeerCSV(20,"Portabilidad Prepago Terminal + SIM Con Recarga",Caso) + "')]")); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[2]/div/div[3]/div/div[4]/div[2]/div/div[2]/div[2]/div/a"));
        reporte.AddImageToReport("Ver detalle del Smartphone ", OPG.SS());
        SelenElem.focus(By.id("portabilidad"));
        reporte.AddImageToReport(" ", OPG.SS());
        SelenElem.focus(By.id("js_submit_miniparrilla_portabilidad_prepago"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("js_submit_miniparrilla_portabilidad_prepago"));
        SelenElem.WaitForLoad(By.id("portability_number"), 20);
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        SelenElem.focus(By.id("coupon_code"));
        reporte.AddImageToReport(" ", OPG.SS());
        SelenElem.focus(By.id("continueToCheckout"));
        reporte.AddImageToReport(" ", OPG.SS());
        
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 5);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.Click(By.id("checkPrivacyOne"));
        
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        SelenElem.focus(By.id("botonTusDatos"));
        reporte.AddImageToReport(" Sección tus datos del Checkout", OPG.SS());
        SelenElem.Click(By.id("botonTusDatos"));
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 15);
        
        /*SelenElem.Click(By.id("inv-mail2"));
        SelenElem.WaitForLoad(By.id("cp"), 10);
        SelenElem.Click(By.id("cp"));
        SelenElem.TypeText(By.id("cp"), "22000");
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());
        SelenElem.Click(By.id("longlat"));
        OPG.cargaAjax2(); 
        
        SelenElem.Click(By.id("21010006cac"));
        SelenElem.focus(By.id("99010012cac"));
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());
        
        SelenElem.focus(By.id("99010012cac"));
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());*/
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        SelenElem.focus(By.id("checkout-paso2-renovaciones"));
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("checkout-paso2-renovaciones"));
       
        SelenElem.WaitForLoad(By.id("valid-nip"), 10);
        SelenElem.TypeText(By.id("valid-nip"), "1234");
        SelenElem.TypeText(By.id("valid-curp"), curp);
        SelenElem.Click(By.id("dateToPorta"));
        reporte.AddImageToReport("Sección Cámbiate a Movistar", OPG.SS());
        SelenElem.focus(By.id("checkout-paso4-porta-pre"));
        reporte.AddImageToReport("", OPG.SS());
        
        Thread.sleep(1000);
        SelenElem.Click(By.id("checkout-paso4-porta-pre"));
        //OPG.cargaAjax2();
        
        new Flap(reporte,OPG,SelenElem,WebDrive);
        
        SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/h2"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/section[3]"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/footer/a"));
        reporte.AddImageToReport("", OPG.SS());

        
        
        //*/
        
    }
    
    
}
