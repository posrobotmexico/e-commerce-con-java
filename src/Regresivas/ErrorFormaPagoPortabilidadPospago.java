
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.FlapError;
import Operaciones.OpcionesEnvio;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class ErrorFormaPagoPortabilidadPospago {

     public ErrorFormaPagoPortabilidadPospago(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url,String Caso) throws InterruptedException, IOException{
         
        String dn = CargaVariables.LeerCSV(0, "Error en la forma de Pago Portabilidad Pospago", Caso);
        String rfc = CargaVariables.LeerCSV(1, "Error en la forma de Pago Portabilidad Pospago", Caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Error en la forma de Pago Portabilidad Pospago", Caso);
        String apellidop = CargaVariables.LeerCSV(3, "Error en la forma de Pago Portabilidad Pospago", Caso);
        String apellidom = CargaVariables.LeerCSV(4, "Error en la forma de Pago Portabilidad Pospago", Caso);
        String correo = CargaVariables.LeerCSV(5, "Error en la forma de Pago Portabilidad Pospago", Caso);
        String tel = CargaVariables.LeerCSV(6, "Error en la forma de Pago Portabilidad Pospago", Caso);
        String calle = CargaVariables.LeerCSV(8, "Error en la forma de Pago Portabilidad Pospago", Caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Error en la forma de Pago Portabilidad Pospago", Caso);
        String numInterior = CargaVariables.LeerCSV(10, "Error en la forma de Pago Portabilidad Pospago", Caso);
        String cp = CargaVariables.LeerCSV(7, "Error en la forma de Pago Portabilidad Pospago", Caso);
        String ciudad = CargaVariables.LeerCSV(13, "Error en la forma de Pago Portabilidad Pospago", Caso);
        String colonia = CargaVariables.LeerCSV(12, "Error en la forma de Pago Portabilidad Pospago", Caso);
        String ine = CargaVariables.LeerCSV(14, "Error en la forma de Pago Portabilidad Pospago", Caso);
        String año = CargaVariables.LeerCSV(16, "Error en la forma de Pago Portabilidad Pospago", Caso);
        String mes = CargaVariables.LeerCSV(17, "Error en la forma de Pago Portabilidad Pospago", Caso);
        String  dia= CargaVariables.LeerCSV(18, "Error en la forma de Pago Portabilidad Pospago", Caso);
        String curp= CargaVariables.LeerCSV(19, "Error en la forma de Pago Portabilidad Pospago", Caso);
       

        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        SelenElem.waitForLoadPage();
        Thread.sleep(3000);
        //SelenElem.Click(By.linkText("Cámbiate a Movistar"));
        //SelenElem.Click(By.xpath("//a[@title='Cámbiate a Movistar']"));
        SelenElem.WaitForLoad(By.id("menu_list"), 15);
        OPG.SelectMenu("Cámbiate a Movistar");
        
        SelenElem.WaitFor(By.id("sc2-dn"));
        SelenElem.Click(By.id("sc2-dn"));
        SelenElem.TypeText(By.id("sc2-dn"),dn);
        reporte.AddImageToReport("Se selecciona el flujo de Cámbiate a Movistar", OPG.SS());
        
        SelenElem.Click(By.id("sc2-goPortaStep2"));
        SelenElem.Click(By.id("option_plan"));
        reporte.AddImageToReport("Seleccionamos quiero contratar un plan", OPG.ScreenShotElement());
        SelenElem.Click(By.id("sc2-goToPortability"));
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("plan1"), 15);
        SelenElem.focus(By.id("plan1"));
        SelenElem.Click(By.id("plan1"));
        reporte.AddImageToReport("Seleccionamos sin smartphone y damos click en continuar", OPG.ScreenShotElement());
        SelenElem.Click(By.id("gtm-plans-continue-button"));
        
        SelenElem.Click(By.xpath("//button[.='Lo quiero']"));
        reporte.AddImageToReport("Elegimos el plan Movistar", OPG.ScreenShotElement()); 
        
        SelenElem.WaitForLoad(By.id("check-spotify"),5);
        SelenElem.focus(By.id("check-spotify"));
        SelenElem.focus(By.xpath("//*[@id=\"summary-parrillas\"]/div/ul/li[2]/div/a"));
        reporte.AddImageToReport("Sin SVA", OPG.ScreenShotElement());
        SelenElem.Click(By.id("submitplanes"));
        
        SelenElem.WaitForLoad(By.linkText("Cambiar plan"), 5);
        reporte.AddImageToReport("Resumen de compra",OPG.SS() );
        SelenElem.Click(By.xpath("//button[@title='Da clic aquí para ir al carrito de compra']"));
        SelenElem.WaitFor(By.id("continueToCheckout"));
        SelenElem.focus(By.id("continueToCheckout"));
        reporte.AddImageToReport("",OPG.SS() );
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.WaitForLoad(By.id("valid-nombre"), 6);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), año);
        SelenElem.SelectOption(By.className("ui-datepicker-month"), mes);
        SelenElem.Click(By.linkText(dia));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.SelectOption(By.id("regimen_fiscal"), "Sueldos y Salarios e Ingresos Asimilados a Salarios");
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("calleNumeroInterior"), numInterior);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.Click(By.id("checkout-step4-check-02"));
        
        SelenElem.Click(By.id("paso1-pospago-porta"));
        
        OPG.cargaAjax2();
       /* SelenElem.WaitForLoad(By.id("inv-mail2"), 6);
        SelenElem.Click(By.id("inv-mail2"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("cp"), 15);
        SelenElem.Click(By.id("cp"));
        SelenElem.TypeText(By.id("cp"), cp);
        SelenElem.Click(By.id("longlat"));
        OPG.cargaAjax2();
        
        SelenElem.WaitForLoad(By.id("99090073cac"),6);
        SelenElem.Click(By.id("99090073cac"));
        reporte.AddImageToReport("Sección opciones de envío del Checkout", OPG.ScreenShotElement()); */  
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        SelenElem.focus(By.id("checkout-paso2-renovaciones"));
        reporte.AddImageToReport("Sección opciones de envío del Checkout", OPG.ScreenShotElementInstant()); 
        SelenElem.Click(By.id("checkout-paso2-renovaciones"));
        
        OPG.cargaAjax2();
        
        SelenElem.TypeText(By.id("valid-nip"), "1234");
        SelenElem.TypeText(By.id("valid-curp"), curp);
        SelenElem.Click(By.id("dateToPorta_datepicker"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("ui-datepicker-div"),10);
        SelenElem.Click(By.className("ui-datepicker-days-cell-over"));
        reporte.AddImageToReport("Sección Cámbiate a Movistar del Checkout", OPG.ScreenShotElement());        
        SelenElem.Click(By.id("checkout-paso4-porta-pre"));
        
        SelenElem.Click(By.id("checkTeminosCondiciones"));
        reporte.AddImageToReport("Sección Contrato del Checkout", OPG.ScreenShotElement());
        SelenElem.Click(By.id("realizar-pago-2"));
        
        new FlapError(reporte,OPG,SelenElem,WebDrive);
        
        
        
    
    }
    
}
