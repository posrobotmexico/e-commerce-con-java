
package Regresivas;

import Core.DeviceActions;
import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;
import Operaciones.OperacionesRecargas;

/**
 *
 * @author LAPTOP-JORGE
 */
public class RecargaPublicaMercadoPP {
    
    public RecargaPublicaMercadoPP(){
    
    }

     public void MigracionNBA(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String caso) throws InterruptedException, IOException{
    
        String dn = CargaVariables.LeerCSV(0, "Recargas mercado pago pro", caso);
        
        String nombre = CargaVariables.LeerCSV(1, "Recargas mercado pago pro", caso);
        String apellidop = CargaVariables.LeerCSV(2, "Recargas mercado pago pro", caso);
        String apellidom = CargaVariables.LeerCSV(3, "Recargas mercado pago pro", caso);
        String correo = CargaVariables.LeerCSV(4, "Recargas mercado pago pro", caso);
        String tel = CargaVariables.LeerCSV(5, "Recargas mercado pago pro", caso);
        String calle = CargaVariables.LeerCSV(7, "Recargas mercado pago pro", caso);
        String numdomicilio = CargaVariables.LeerCSV(8, "Recargas mercado pago pro", caso);
        String cp = CargaVariables.LeerCSV(6, "Recargas mercado pago pro", caso);
        String ciudad = CargaVariables.LeerCSV(10, "Recargas mercado pago pro", caso);
        String colonia = CargaVariables.LeerCSV(9, "Recargas mercado pago pro", caso);
        String ine = CargaVariables.LeerCSV(11, "Recargas mercado pago pro", caso);
        String rfc = CargaVariables.LeerCSV(12, "Recargas mercado pago pro", caso);
        String delegacion = CargaVariables.LeerCSV(13, "Recargas mercado pago pro", caso);
        String  dia= CargaVariables.LeerCSV(14, "Recargas mercado pago pro", caso);

       WebDrive.WebDriver.navigate().to(Url);

        SelenElem.WaitForLoad(By.id("menu_list"), 15);
        reporte.AddImageToReport("Iniciamos desde la pagina principal", OPG.SS());
        //SelenElem.Click(By.linkText("Recargas"));
        WebDrive.WebDriver.navigate().to(Url + "recarga-en-linea");
        SelenElem.WaitForLoad(By.id("codeMov"), 20);
        reporte.AddImageToReport("Seleccionar el flujo de Recargas  ", OPG.ScreenShotElementInstant());
        
        SelenElem.waitForLoadPage();
        Thread.sleep(5000);
        da.slide("Down", 1);
        Thread.sleep(15000);
        SelenElem.TypeText(By.id("codeMov"), dn); // DN
        SelenElem.Click(By.id("codeMov"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección 1: ingresa numero", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("btn_fiststep"));
        Thread.sleep(5000);
        
        SelenElem.WaitForLoad(By.id("nba_recarga_si_quiero"), 35);
        reporte.AddImageToReport("Aceptar oferta NBA ", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("nba_recarga_si_quiero"));
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        da.hideKeyBoard();
        reporte.AddImageToReport("Valida tu identidad", OPG.ScreenShotElementInstant());
        Thread.sleep(4000);
        
        SelenElem.ClickById(WebDrive.WebDriver, "continue-btn");
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("total-mobile"), 35);
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        SelenElem.Click(By.id("total-mobile"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        
        
        SelenElem.Click(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/section/div/div[1]/div/div/div[1]/div[2]/button"));
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-nombre"), 15);
        
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        da.hideKeyBoard();
        da.slide("Up", 1);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.ScreenShotElementInstant()); 
        
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.ScreenShotElementInstant());
        SelenElem.SelectOption(By.id("colonia"), colonia);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.ScreenShotElementInstant());
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        SelenElem.TypeText(By.id("ciudad-valida"), delegacion);
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        da.hideKeyBoard();
        da.slide("Up", 1);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("paso1-migracaribu"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("checkTeminosCondiciones"), 90);
        Thread.sleep(2000);
        SelenElem.Click(By.id("checkTeminosCondiciones"));
        SelenElem.Click(By.id("checkConsentimiento"));
        SelenElem.Click(By.id("checkConsentimientoRecarga"));
        da.hideKeyBoard();
        da.slide("Up", 2);
        reporte.AddImageToReport("Sección contrato de Continuar con la compra", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("paso2-migracaribu"));
        
        
        SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
        Thread.sleep(900);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.ScreenShotElementInstant());
        
        
     
     }
     public void recargamontoLiferay(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException{
     
        String dn = CargaVariables.LeerCSV(0, "Recargas mercado pago pro", Caso);
        String monto = CargaVariables.LeerCSV(1, "Recargas mercado pago pro", Caso);
        String nombre = CargaVariables.LeerCSV(2, "Recargas mercado pago pro", Caso);
        String numero = CargaVariables.LeerCSV(3, "Recargas mercado pago pro", Caso);
        String exp = CargaVariables.LeerCSV(4, "Recargas mercado pago pro", Caso);
        String cvv = CargaVariables.LeerCSV(5, "Recargas mercado pago pro", Caso);

        WebDrive.WebDriver.navigate().to(Url + "recargas.php");
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        Thread.sleep(2000);
        SelenElem.Find(By.id("skuDn")).clear();
        SelenElem.TypeText(By.id("skuDn"), dn);
        SelenElem.Find(By.id("monto")).clear();
        SelenElem.TypeText(By.id("monto"), monto);
        reporte.AddImageToReport("Ingresamos los datos y damos clic en enviar", OPG.SS());
        SelenElem.Click(By.name("Comprar"));
        //reporte.AddImageToReport("Ingresamos los datos y damos clic en enviar", OPG.ScreenShotElementInstant());
        
        Thread.sleep(5000);
        SelenElem.WaitForLoad(By.id("UserCredit"), 50);
        Thread.sleep(5000);
        SelenElem.Click(By.id("UserCredit"));
        SelenElem.TypeText(By.id("UserCredit"), nombre);
        SelenElem.Click(By.id("NumCredit2"));
        SelenElem.TypeText(By.id("NumCredit2"), numero);
        SelenElem.Click(By.id("expiration"));
        SelenElem.TypeText(By.id("expiration"), exp);
        SelenElem.Click(By.id("ccv"));
        SelenElem.TypeText(By.id("ccv"), cvv);
        SelenElem.Click(By.id("terms"));
        da.hideKeyBoard();
        da.slide("Up", 1);
        reporte.AddImageToReport("Ingresamos la tarjeta", OPG.SS());
     
        SelenElem.Click(By.id("btn_recargar"));
        reporte.AddImageToReport("Confirma tus datos para recargar ", OPG.SS());
        SelenElem.Click(By.id("recharge"));
        
        
        SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
        Thread.sleep(2000);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.ScreenShotElementInstant());
        
        
        
        
     }
     

     public void recargapaquetesLiferay(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException{
     
        String dn = CargaVariables.LeerCSV(0, "Recargas mercado pago pro", Caso);
        String monto = CargaVariables.LeerCSV(1, "Recargas mercado pago pro", Caso);
        String nombre = CargaVariables.LeerCSV(2, "Recargas mercado pago pro", Caso);
        String numero = CargaVariables.LeerCSV(3, "Recargas mercado pago pro", Caso);
        String exp = CargaVariables.LeerCSV(4, "Recargas mercado pago pro", Caso);
        String cvv = CargaVariables.LeerCSV(5, "Recargas mercado pago pro", Caso);

        WebDrive.WebDriver.navigate().to(Url + "recargas.php");
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        Thread.sleep(2000);
        SelenElem.Find(By.id("skuDn")).clear();
        SelenElem.TypeText(By.id("skuDn"), dn);
        SelenElem.Find(By.id("monto")).clear();
        SelenElem.TypeText(By.id("monto"), monto);
        reporte.AddImageToReport("Ingresamos los datos y damos clic en enviar", OPG.SS());
        SelenElem.Click(By.name("Comprar"));
        //reporte.AddImageToReport("Ingresamos los datos y damos clic en enviar", OPG.ScreenShotElementInstant());
        
        Thread.sleep(5000);
        SelenElem.WaitForLoad(By.id("UserCredit"), 50);
        Thread.sleep(5000);
        SelenElem.Click(By.id("UserCredit"));
        SelenElem.TypeText(By.id("UserCredit"), nombre);
        SelenElem.Click(By.id("NumCredit2"));
        SelenElem.TypeText(By.id("NumCredit2"), numero);
        SelenElem.Click(By.id("expiration"));
        SelenElem.TypeText(By.id("expiration"), exp);
        SelenElem.Click(By.id("ccv"));
        SelenElem.TypeText(By.id("ccv"), cvv);
        SelenElem.Click(By.id("terms"));
        da.hideKeyBoard();
        da.slide("Up", 1);
        reporte.AddImageToReport("Ingresamos la tarjeta", OPG.SS());
     
        SelenElem.Click(By.id("btn_recargar"));
        reporte.AddImageToReport("Confirma tus datos para recargar ", OPG.SS());
        SelenElem.Click(By.id("recharge"));
        
        SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
        Thread.sleep(2000);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.ScreenShotElementInstant());
        
        
        
     }

     public void recargamontos(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String caso) throws InterruptedException, IOException{
    
        String dn = CargaVariables.LeerCSV(0, "Recargas mercado pago pro", caso);
        String monto = CargaVariables.LeerCSV(1, "Recargas mercado pago pro", caso);
        String nombre = CargaVariables.LeerCSV(2, "Recargas mercado pago pro", caso);
        String numero = CargaVariables.LeerCSV(3, "Recargas mercado pago pro", caso);
        String exp = CargaVariables.LeerCSV(4, "Recargas mercado pago pro", caso);
        String cvv = CargaVariables.LeerCSV(5, "Recargas mercado pago pro", caso);
        String correo = CargaVariables.LeerCSV(6, "Recargas mercado pago pro", caso);
        
        //SelenElem.WaitForLoad(By.linkText("Recargas"), 15);
        WebDrive.WebDriver.navigate().to(Url);

        SelenElem.WaitForLoad(By.id("menu_list"), 15);
        reporte.AddImageToReport("Iniciamos desde la pagina principal", OPG.SS());
        //SelenElem.Click(By.linkText("Recargas"));
        WebDrive.WebDriver.navigate().to(Url + "recarga-en-linea");
        SelenElem.WaitForLoad(By.id("codeMov"), 20);
        reporte.AddImageToReport("Seleccionar el flujo de Recargas  ", OPG.ScreenShotElementInstant());
        
        SelenElem.waitForLoadPage();
        Thread.sleep(5000);
        da.slide("Down", 1);
        Thread.sleep(20000);
        SelenElem.TypeText(By.id("codeMov"), dn); // DN
        SelenElem.Click(By.id("codeMov"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección 1: ingresa numero", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("btn_fiststep"));
        Thread.sleep(5000);
        
        SelenElem.WaitForLoad(By.xpath("//span[@data-price='"+monto+"']"), 12); // Ambos tienen de 200
        //reporte.AddImageToReport("Sección 2: selección del monto", OPG.SS());
        
        reporte.AddImageToReport("Seleccionamos el monto de recarga de $"+ monto, OPG.SS());
        
        
        SelenElem.Click(By.xpath("//span[@data-price='"+ monto +"']")); // Aquí poner el monto
        SelenElem.Click(By.id("redirect-mp"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionamos un metodo de pago", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.xpath("//span[@title='Da clic aquí para ir al sitio de mercado pago para realizar tu pago']"));
        
        SelenElem.WaitForLoad(By.id("new_card_row"), 20);
        reporte.AddImageToReport("Mercado Pago 1 Método de pago",OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("new_card_row"));
        
        SelenElem.WaitForLoad(By.id("card_number"), 30);
        SelenElem.Click(By.id("fullname"));
        SelenElem.TypeText(By.id("fullname"), nombre);
        SelenElem.Click(By.id("card_number"));
        SelenElem.TypeText(By.id("card_number"), numero);
        SelenElem.Click(By.id("input_expiration_date"));
        SelenElem.TypeText(By.id("input_expiration_date"), exp);
        SelenElem.Click(By.id("cvv"));
        SelenElem.TypeText(By.id("cvv"), cvv);
        da.hideKeyBoard();
        reporte.AddImageToReport("Mercado Pago 2 Datos de la tarjeta", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("submit"));
        
        SelenElem.WaitForLoad(By.id("email"), 12);
        SelenElem.Click(By.id("email"));
        SelenElem.TypeText(By.id("email"), correo);
        da.hideKeyBoard();
        reporte.AddImageToReport("Mercado Pago 3 Correo electrónico  ", OPG.SS());
        SelenElem.Click(By.xpath("//*[@id=\"pay\"]/span"));
        
        SelenElem.WaitForLoad(By.xpath("//span[contains(text(),'¡Listo! Se acreditó tu pago')]"), 20);
        
        reporte.AddImageToReport("Vemos el detalle de la compra", OPG.ScreenShotElementInstant());
       
    }

     public void recargapaquetes(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String caso) throws InterruptedException, IOException{
    
        String dn = CargaVariables.LeerCSV(0, "Recargas mercado pago pro", caso);
        String monto = CargaVariables.LeerCSV(1, "Recargas mercado pago pro", caso);
        String nombre = CargaVariables.LeerCSV(2, "Recargas mercado pago pro", caso);
        String numero = CargaVariables.LeerCSV(3, "Recargas mercado pago pro", caso);
        String exp = CargaVariables.LeerCSV(4, "Recargas mercado pago pro", caso);
        String cvv = CargaVariables.LeerCSV(5, "Recargas mercado pago pro", caso);
        String correo = CargaVariables.LeerCSV(6, "Recargas mercado pago pro", caso);
        
        //SelenElem.WaitForLoad(By.linkText("Recargas"), 15);
        WebDrive.WebDriver.navigate().to(Url);

        SelenElem.WaitForLoad(By.id("menu_list"), 15);
        reporte.AddImageToReport("Iniciamos desde la pagina principal", OPG.SS());
        //SelenElem.Click(By.linkText("Recargas"));
        WebDrive.WebDriver.navigate().to(Url + "recarga-en-linea");
        SelenElem.WaitForLoad(By.id("codeMov"), 20);
        reporte.AddImageToReport("Seleccionar el flujo de Recargas  ", OPG.ScreenShotElementInstant());
        
        SelenElem.waitForLoadPage();
        Thread.sleep(5000);
        da.slide("Down", 1);
        Thread.sleep(20000);
        SelenElem.TypeText(By.id("codeMov"), dn); // DN
        SelenElem.Click(By.id("codeMov"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección 1: ingresa numero", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("btn_fiststep"));
        Thread.sleep(5000);
        
        SelenElem.WaitForLoad(By.xpath("//span[@data-price='"+monto+"']"), 12); // Ambos tienen de 200
        //reporte.AddImageToReport("Sección 2: selección del monto", OPG.SS());
        
        reporte.AddImageToReport("Seleccionamos el paquete de $"+ monto, OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("Seleccionamos el paquete de $"+ monto, OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.xpath("//span[@data-price='"+ monto +"']")); // Aquí poner el monto
        SelenElem.Click(By.id("redirect-mp"));
        Thread.sleep(2000);
        da.slide("Up", 2);
        reporte.AddImageToReport("Seleccionamos un metodo de pago", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.xpath("//span[@title='Da clic aquí para ir al sitio de mercado pago para realizar tu pago']"));
        
        SelenElem.WaitForLoad(By.id("new_card_row"), 20);
        reporte.AddImageToReport("Mercado Pago 1 Método de pago",OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("new_card_row"));
        
        SelenElem.WaitForLoad(By.id("card_number"), 30);
        SelenElem.Click(By.id("fullname"));
        SelenElem.TypeText(By.id("fullname"), nombre);
        SelenElem.Click(By.id("card_number"));
        SelenElem.TypeText(By.id("card_number"), numero);
        SelenElem.Click(By.id("input_expiration_date"));
        SelenElem.TypeText(By.id("input_expiration_date"), exp);
        SelenElem.Click(By.id("cvv"));
        SelenElem.TypeText(By.id("cvv"), cvv);
        da.hideKeyBoard();
        reporte.AddImageToReport("Mercado Pago 2 Datos de la tarjeta", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("submit"));
        
        SelenElem.WaitForLoad(By.id("email"), 12);
        SelenElem.Click(By.id("email"));
        SelenElem.TypeText(By.id("email"), correo);
        da.hideKeyBoard();
        reporte.AddImageToReport("Mercado Pago 3 Correo electrónico  ", OPG.SS());
        SelenElem.Click(By.xpath("//*[@id=\"pay\"]/span"));
        
        SelenElem.WaitForLoad(By.xpath("//span[contains(text(),'¡Listo! Se acreditó tu pago')]"), 20);
        
        reporte.AddImageToReport("Vemos el detalle de la compra", OPG.ScreenShotElementInstant());
       
    }
    
    
}
