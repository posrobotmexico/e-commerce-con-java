
package Regresivas;

import Core.DeviceActions;
import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.FlapPorta;
import Operaciones.OpcionesEnvio;
import Operaciones.OperacionesGenerales;
import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidElement;
import java.io.File;
import java.io.IOException;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author LAPTOP-JORGE
 */
public class MigracionesFlap {
    
    public MigracionesFlap(){
    
    }
    
    public void MigracionesSIMTerminal (Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String caso) throws InterruptedException, IOException
    {
        String dn = CargaVariables.LeerCSV(0, "Migraciones Flap", caso);
        String rfc = CargaVariables.LeerCSV(1, "Migraciones Flap", caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Migraciones Flap", caso);
        String apellidop = CargaVariables.LeerCSV(3, "Migraciones Flap", caso);
        String apellidom = CargaVariables.LeerCSV(4, "Migraciones Flap", caso);
        String correo = CargaVariables.LeerCSV(5, "Migraciones Flap", caso);
        String tel = CargaVariables.LeerCSV(6, "Migraciones Flap", caso);
        String calle = CargaVariables.LeerCSV(8, "Migraciones Flap", caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Migraciones Flap", caso);
        String numInterior = CargaVariables.LeerCSV(10, "Migraciones Flap", caso);
        String cp = CargaVariables.LeerCSV(7, "Migraciones Flap", caso);
        String ciudad = CargaVariables.LeerCSV(13, "Migraciones Flap", caso);
        String colonia = CargaVariables.LeerCSV(12, "Migraciones Flap", caso);
        String ine = CargaVariables.LeerCSV(14, "Migraciones Flap", caso);
        String año = CargaVariables.LeerCSV(16, "Migraciones Flap", caso);
        String mes = CargaVariables.LeerCSV(17, "Migraciones Flap", caso);
        String  dia= CargaVariables.LeerCSV(18, "Migraciones Flap", caso);
        String  nombreTel= CargaVariables.LeerCSV(19, "Migraciones Flap", caso);
        
        
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        
        Thread.sleep(3000);
        SelenElem.Click(By.xpath("//*[@id=\"checkbox\"]"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar la Opción Smartphones > Todas las marcas", OPG.SS());
        SelenElem.Click(By.xpath("//input[@value='Smartphones']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar la Opción Smartphones > Todas las marcas", OPG.SS());
        
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        Thread.sleep(3000);
        reporte.AddImageToReport("Seleccionar la Categoría Renovación y seleccionar Smartphone", OPG.SS());
        
        
        SelenElem.Click(By.xpath("//span[.='Renovación']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionar la Categoría Renovación y seleccionar Smartphone", OPG.SS());
        
        
        da.slide("Down", 4);
        reporte.AddImageToReport("Seleccionar smartphone ", OPG.SS());
        SelenElem.ClickByXpath(WebDrive.WebDriver,"//img[contains(@alt, '" + nombreTel + "')]"); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        //SelenElem.ClickByXpath(WebDrive.WebDriver,"//img[contains(@alt, '" + modeloTel + "')]"); // CargaVariables.LeerCSV(1,"PrepagoTerminal",caso)
        SelenElem.waitForLoadPage();
        Thread.sleep(1000);
        
        SelenElem.WaitForLoad(By.xpath("//*[@id=\"solo-smartphone__content\"]/form/button"),10);
        reporte.AddImageToReport("Visualizar el detalle del smartphone e ingresar DN y dar click en “Usar este número”", OPG.SS());
        
        Thread.sleep(1000);
        SelenElem.Click(By.id("soy-cliente"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_dn_input_soy_cliente"), 10);
        SelenElem.TypeText(By.id("js_dn_input_soy_cliente"),dn);
        reporte.AddImageToReport(" ", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport(" ", OPG.SS());
        
        SelenElem.Click(By.id("js_input_dn_button_soy_cliente"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.className("temm-2022-modal"), 10);
        Thread.sleep(2000);
        reporte.AddImageToReport(" ", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.xpath("//span[.='Continuar']"));
        
        
        da.slide("Down", 1);
        reporte.AddImageToReport(" ", OPG.ScreenShotElementInstant());
        
        Thread.sleep(2000);
        SelenElem.Click(By.id("js_submit_miniparrilla_migracion"));
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("continueToCheckout"), 10);
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        SelenElem.focus(By.id("firstCharacter"));
        reporte.AddImageToReport("Valida tu identidad", OPG.ScreenShotElement());
        Thread.sleep(15000);
        da.slide("Down", 1);
        da.slide("Up", 2);
        da.hideKeyBoard();
        Thread.sleep(1000);
        SelenElem.ClickById(WebDrive.WebDriver, "continue-btn");
        

        Thread.sleep(1000);
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        SelenElem.Click(By.className("data__btn"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/section/div/div[1]/section/div[4]/div[3]/a"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("total-mobile"));
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        
        SelenElem.Click(By.id("btn_continue_check"));
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-nombre"), 6);
        
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), año);
        SelenElem.SelectOption(By.className("ui-datepicker-month"), mes);
        SelenElem.Click(By.linkText(dia));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("calleNumeroInterior"), numInterior);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.focus(By.id("paso1-pospago-porta"));
        reporte.AddImageToReport("", OPG.SS()); 
        
        da.hideKeyBoard();
        SelenElem.Click(By.id("paso1-pospago-porta"));
        OPG.cargaAjax2();
        
        reporte.AddImageToReport("Sección de consulta tu crédito del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("cod"), "1111");
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        SelenElem.Click(By.id("checkout-step2-radio-04"));
        SelenElem.Click(By.id("checkout-step2-radio-06"));
        SelenElem.Click(By.id("checkout-step2"));
        
        reporte.AddImageToReport("Sección de consulta tu crédito del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("creditCheck"));
        
        OPG.cargaAjax2();
        
       /* SelenElem.Click(By.id("inv-mail2"));
        SelenElem.WaitForLoad(By.id("cp"), 4);
        SelenElem.Click(By.id("cp"));
        SelenElem.TypeText(By.id("cp"), "06600");
        SelenElem.Click(By.id("longlat"));
        OPG.cargaAjax2(); 
        
        SelenElem.Click(By.id("99090073cac"));
        SelenElem.focus(By.id("99090005cac"));
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());
        
        SelenElem.focus(By.id("99090003cac"));
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());*/
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        OPG.cargaAjax2();
        
        SelenElem.focus(By.id("pasarAlPaso3"));
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("pasarAlPaso3"));
        OPG.cargaAjax2(); 
        
        SelenElem.WaitForLoad(By.id("checkTeminosCondiciones"), 10);
        SelenElem.Click(By.id("checkTeminosCondiciones"));
        SelenElem.Click(By.id("checkConsentimiento"));
        SelenElem.Click(By.id("checkConsentimientoRecarga"));
        reporte.AddImageToReport("Sección Contrato del checkout", OPG.SS());
        SelenElem.Click(By.id("realizar-pago-2"));
        
        new FlapPorta(reporte,OPG,SelenElem,WebDrive);
        
        SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"numero_migrar\"]"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/section[2]/section[2]/div[1]/dl[1]/dt"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/section[2]/section[2]/div[1]/dl[1]/dt"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/section[2]/footer/a[1]"));
        reporte.AddImageToReport("", OPG.SS());
        
    
    }
    
    public void MigracionesSIMTerminalLiferay (Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String caso) throws InterruptedException, IOException
    {
        String dn = CargaVariables.LeerCSV(0, "Migraciones Flap", caso);
        String rfc = CargaVariables.LeerCSV(1, "Migraciones Flap", caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Migraciones Flap", caso);
        String apellidop = CargaVariables.LeerCSV(3, "Migraciones Flap", caso);
        String apellidom = CargaVariables.LeerCSV(4, "Migraciones Flap", caso);
        String correo = CargaVariables.LeerCSV(5, "Migraciones Flap", caso);
        String tel = CargaVariables.LeerCSV(6, "Migraciones Flap", caso);
        String calle = CargaVariables.LeerCSV(8, "Migraciones Flap", caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Migraciones Flap", caso);
        String numInterior = CargaVariables.LeerCSV(10, "Migraciones Flap", caso);
        String cp = CargaVariables.LeerCSV(7, "Migraciones Flap", caso);
        String ciudad = CargaVariables.LeerCSV(13, "Migraciones Flap", caso);
        String colonia = CargaVariables.LeerCSV(12, "Migraciones Flap", caso);
        String ine = CargaVariables.LeerCSV(14, "Migraciones Flap", caso);
        String año = CargaVariables.LeerCSV(16, "Migraciones Flap", caso);
        String mes = CargaVariables.LeerCSV(17, "Migraciones Flap", caso);
        String  dia= CargaVariables.LeerCSV(18, "Migraciones Flap", caso);
        String  nombreTel= CargaVariables.LeerCSV(19, "Migraciones Flap", caso);
        String  skuterminal= CargaVariables.LeerCSV(20, "Migraciones Flap", caso);
        String  skuplan= CargaVariables.LeerCSV(21, "Migraciones Flap", caso);
        String  skurecarga= CargaVariables.LeerCSV(22, "Migraciones Flap", caso);
        String  monto= CargaVariables.LeerCSV(23, "Migraciones Flap", caso);
        
        WebDrive.WebDriver.navigate().to(Url + "liferay.php");
        
        SelenElem.SelectOption(By.id("flowCode"), "migracion");
        
        SelenElem.Find(By.id("skuPlan")).clear();
        SelenElem.TypeText(By.id("skuPlan"), skuplan);
        SelenElem.Find(By.id("skuRecarga")).clear();
        //SelenElem.TypeText(By.id("skuRecarga"), skurecarga);
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.TypeText(By.id("skuTerminal"), skuterminal);
        SelenElem.Find(By.id("skuDn")).clear();
        SelenElem.TypeText(By.id("skuDn"), dn);
        SelenElem.Find(By.id("monto")).clear();
        //SelenElem.TypeText(By.id("monto"), monto);
        Thread.sleep(2000);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        SelenElem.Click(By.name("Comprar"));
        
        SelenElem.waitForLoadPage();
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        SelenElem.focus(By.id("firstCharacter"));
        reporte.AddImageToReport("Valida tu identidad", OPG.ScreenShotElement());
        Thread.sleep(15000);
        
        SelenElem.ClickById(WebDrive.WebDriver, "continue-btn");
        
        Thread.sleep(1000);
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        SelenElem.Click(By.className("data__btn"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/section/div/div[1]/section/div[4]/div[3]/a"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("total-mobile"));
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        
        SelenElem.Click(By.id("btn_continue_check"));
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-nombre"), 6);
        
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), año);
        SelenElem.SelectOption(By.className("ui-datepicker-month"), mes);
        SelenElem.Click(By.linkText(dia));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("calleNumeroInterior"), numInterior);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.focus(By.id("paso1-pospago-porta"));
        reporte.AddImageToReport("", OPG.SS()); 
        
        SelenElem.Click(By.id("paso1-pospago-porta"));
        OPG.cargaAjax2();
        
        reporte.AddImageToReport("Sección de consulta tu crédito del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("cod"), "1111");
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        SelenElem.Click(By.id("checkout-step2-radio-04"));
        SelenElem.Click(By.id("checkout-step2-radio-06"));
        SelenElem.Click(By.id("checkout-step2"));
        
        reporte.AddImageToReport("Sección de consulta tu crédito del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("creditCheck"));
        
        OPG.cargaAjax2();
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 2);
        SelenElem.Click(By.id("inv-mail2"));
        //OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("js_codigo_postal"), "06600");
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección opciones de envio del checkout", OPG.SS());
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        SelenElem.ClickById(WebDrive.WebDriver,"js_search_cacs_by_postal_code");
        OPG.cargaAjax2();   
        SelenElem.Click(By.id("99090073cac"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección opciones de envio del checkout", OPG.SS());
        //new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        OPG.cargaAjax2();
        
        SelenElem.focus(By.id("pasarAlPaso3"));
        reporte.AddImageToReport("Sección opciones envío del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("pasarAlPaso3"));
        OPG.cargaAjax2(); 
        
        SelenElem.WaitForLoad(By.id("checkTeminosCondiciones"), 10);
        SelenElem.Click(By.id("checkTeminosCondiciones"));
        SelenElem.Click(By.id("checkConsentimiento"));
        SelenElem.Click(By.id("checkConsentimientoRecarga"));
        
        SelenElem.Click(By.id("realizar-pago-2"));
        
        new Flap(reporte,OPG,SelenElem,WebDrive);
        
        SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"numero_migrar\"]"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/section[2]/section[2]/div[1]/dl[1]/dt"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/section[2]/section[2]/div[1]/dl[1]/dt"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/div[3]/main/section/article/div/section[2]/footer/a[1]"));
        reporte.AddImageToReport("", OPG.SS());
        
    
    }
    
    /*
    public void ErrorDN (Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String caso) throws InterruptedException, IOException
    {
        String dn = CargaVariables.LeerCSV(0, "Migraciones Flap", caso);
        WebDrive.WebDriver.navigate().to(Url + "liferay.php");
        
        WebElement boton = null;
        
        SelenElem.SelectOption(By.id("flowCode"), "migracion");
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.TypeText(By.id("skuTerminal"), "TMLMXSANOTE9AZ00");
        SelenElem.Find(By.id("skuDn")).clear();
        SelenElem.TypeText(By.id("skuDn"), dn);
        Thread.sleep(2000);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        SelenElem.Click(By.name("Comprar"));
        Thread.sleep(3000);
        //reporte.AddImageToReport("Error en el DN",  ((TakesScreenshot)WebDrive.WebDriver).getScreenshotAs(OutputType.BYTES));
        //((AndroidElement) WebDrive.WebDriver.findElements(MobileBy.id("positive_btn")).get(0)).click();
        //reporte.AddImageToReport("Error en el DN",  OPG.SS());
        //((AndroidElement)  WebDrive.WebDriver.findElements(MobileBy.id("com.wallet.client:id/positive_btn")).get(0)).click();
        //WebDrive.WebDriver.switchTo().alert();
        
        Alert alert = WebDrive.WebDriver.switchTo().alert();
        //alert.accept();
        //WebDrive.WebDriver.switchTo().alert();
        //WebDrive.AssingNewDriver((WebDriver) alert);
        WebDrive.WebDriver.switchTo().alert();
        //da.getScreenShot();
        WebDriver frame = WebDrive.WebDriver.switchTo().defaultContent();
        WebDrive.AssingNewDriver(frame);
        
        reporte.AddImageToReport("Error en el DN", OPG.SS());
        alert.accept();
        WebDrive.WebDriver.switchTo().alert();
        alert.accept();
        WebDrive.WebDriver.switchTo().alert();
        //reporte.AddImageToReport("Error en el DN", OPG.SS());
        alert.accept();
        //reporte.AddImageToReport("Error en DN", OPG.SS());
        //reporte.AddImageToReport("Error en el DN", OPG.ScreenShotElement());
        
    }*/
    
    public void ErrorINE (Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String caso) throws InterruptedException, IOException
    {
        String dn = CargaVariables.LeerCSV(0, "Migraciones Flap", caso);
        String rfc = CargaVariables.LeerCSV(1, "Migraciones Flap", caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Migraciones Flap", caso);
        String apellidop = CargaVariables.LeerCSV(3, "Migraciones Flap", caso);
        String apellidom = CargaVariables.LeerCSV(4, "Migraciones Flap", caso);
        String correo = CargaVariables.LeerCSV(5, "Migraciones Flap", caso);
        String tel = CargaVariables.LeerCSV(6, "Migraciones Flap", caso);
        String calle = CargaVariables.LeerCSV(8, "Migraciones Flap", caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Migraciones Flap", caso);
        String numInterior = CargaVariables.LeerCSV(10, "Migraciones Flap", caso);
        String cp = CargaVariables.LeerCSV(7, "Migraciones Flap", caso);
        String ciudad = CargaVariables.LeerCSV(13, "Migraciones Flap", caso);
        String colonia = CargaVariables.LeerCSV(12, "Migraciones Flap", caso);
        String ine = CargaVariables.LeerCSV(14, "Migraciones Flap", caso);
        String año = CargaVariables.LeerCSV(16, "Migraciones Flap", caso);
        String mes = CargaVariables.LeerCSV(17, "Migraciones Flap", caso);
        String  dia= CargaVariables.LeerCSV(18, "Migraciones Flap", caso);
        String  nombreTel= CargaVariables.LeerCSV(19, "Migraciones Flap", caso);
        String  skuterminal= CargaVariables.LeerCSV(20, "Migraciones Flap", caso);
        String  skuplan= CargaVariables.LeerCSV(21, "Migraciones Flap", caso);
        String  skurecarga= CargaVariables.LeerCSV(22, "Migraciones Flap", caso);
        String  monto= CargaVariables.LeerCSV(23, "Migraciones Flap", caso);
        
        WebDrive.WebDriver.navigate().to(Url + "liferay.php");
        
        SelenElem.SelectOption(By.id("flowCode"), "migracion");
        
        SelenElem.Find(By.id("skuPlan")).clear();
        SelenElem.TypeText(By.id("skuPlan"), skuplan);
        SelenElem.Find(By.id("skuRecarga")).clear();
        //SelenElem.TypeText(By.id("skuRecarga"), skurecarga);
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.TypeText(By.id("skuTerminal"), skuterminal);
        SelenElem.Find(By.id("skuDn")).clear();
        SelenElem.TypeText(By.id("skuDn"), dn);
        SelenElem.Find(By.id("monto")).clear();
        //SelenElem.TypeText(By.id("monto"), monto);
        Thread.sleep(2000);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        SelenElem.Click(By.name("Comprar"));
        
        SelenElem.waitForLoadPage();
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        SelenElem.focus(By.id("firstCharacter"));
        reporte.AddImageToReport("Valida tu identidad", OPG.ScreenShotElement());
        Thread.sleep(15000);
        
        SelenElem.ClickById(WebDrive.WebDriver, "continue-btn");
        
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        SelenElem.Click(By.className("data__btn"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/section/div/div[1]/section/div[4]/div[3]/a"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("total-mobile"));
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        
        SelenElem.Click(By.id("btn_continue_check"));
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-nombre"), 6);
        
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), año);
        SelenElem.SelectOption(By.className("ui-datepicker-month"), mes);
        SelenElem.Click(By.linkText(dia));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("calleNumeroInterior"), numInterior);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        //SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        
        SelenElem.Click(By.id("paso1-pospago-porta"));
        //SelenElem.ClickById(WebDrive.WebDriver,"paso1-pospago-porta");
        try{
                Thread.sleep(700);
            }catch(Exception ex){
                
            }
        reporte.AddImageToReport("Error en Clave de Elector", OPG.ScreenShotElementInstant()); 
        Thread.sleep(5000);
        SelenElem.TypeText(By.id("INE-IFE"), "1243675252");
        Thread.sleep(5000);
        SelenElem.Click(By.id("paso1-pospago-porta"));
        try{
                Thread.sleep(700);
            }catch(Exception ex){
                
            }
        //SelenElem.focus(By.id("INE-IFE"));
        reporte.AddImageToReport("Error en Clave de Elector", OPG.ScreenShotElementInstant()); 
        //OPG.cargaAjax2();
    
    }
    
    public void ErrorCP (Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String caso) throws InterruptedException, IOException
    {
        String dn = CargaVariables.LeerCSV(0, "Migraciones Flap", caso);
        String rfc = CargaVariables.LeerCSV(1, "Migraciones Flap", caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Migraciones Flap", caso);
        String apellidop = CargaVariables.LeerCSV(3, "Migraciones Flap", caso);
        String apellidom = CargaVariables.LeerCSV(4, "Migraciones Flap", caso);
        String correo = CargaVariables.LeerCSV(5, "Migraciones Flap", caso);
        String tel = CargaVariables.LeerCSV(6, "Migraciones Flap", caso);
        String calle = CargaVariables.LeerCSV(8, "Migraciones Flap", caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Migraciones Flap", caso);
        String numInterior = CargaVariables.LeerCSV(10, "Migraciones Flap", caso);
        String cp = CargaVariables.LeerCSV(7, "Migraciones Flap", caso);
        String ciudad = CargaVariables.LeerCSV(13, "Migraciones Flap", caso);
        String colonia = CargaVariables.LeerCSV(12, "Migraciones Flap", caso);
        String ine = CargaVariables.LeerCSV(14, "Migraciones Flap", caso);
        String año = CargaVariables.LeerCSV(16, "Migraciones Flap", caso);
        String mes = CargaVariables.LeerCSV(17, "Migraciones Flap", caso);
        String  dia= CargaVariables.LeerCSV(18, "Migraciones Flap", caso);
        String  nombreTel= CargaVariables.LeerCSV(19, "Migraciones Flap", caso);
        String  skuterminal= CargaVariables.LeerCSV(20, "Migraciones Flap", caso);
        String  skuplan= CargaVariables.LeerCSV(21, "Migraciones Flap", caso);
        String  skurecarga= CargaVariables.LeerCSV(22, "Migraciones Flap", caso);
        String  monto= CargaVariables.LeerCSV(23, "Migraciones Flap", caso);
        
        WebDrive.WebDriver.navigate().to(Url + "liferay.php");
        
        SelenElem.SelectOption(By.id("flowCode"), "migracion");
        
        SelenElem.Find(By.id("skuPlan")).clear();
        SelenElem.TypeText(By.id("skuPlan"), skuplan);
        SelenElem.Find(By.id("skuRecarga")).clear();
        //SelenElem.TypeText(By.id("skuRecarga"), skurecarga);
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.TypeText(By.id("skuTerminal"), skuterminal);
        SelenElem.Find(By.id("skuDn")).clear();
        SelenElem.TypeText(By.id("skuDn"), dn);
        SelenElem.Find(By.id("monto")).clear();
        //SelenElem.TypeText(By.id("monto"), monto);
        Thread.sleep(2000);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        SelenElem.Click(By.name("Comprar"));
        
        SelenElem.waitForLoadPage();
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        SelenElem.focus(By.id("firstCharacter"));
        reporte.AddImageToReport("Valida tu identidad", OPG.ScreenShotElement());
        Thread.sleep(15000);
        
        SelenElem.ClickById(WebDrive.WebDriver, "continue-btn");
        
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        SelenElem.Click(By.className("data__btn"));
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"maincontent\"]/div[3]/div/section/div/div[1]/section/div[4]/div[3]/a"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("total-mobile"));
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        
        SelenElem.Click(By.id("btn_continue_check"));
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-nombre"), 6);
        
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        
        SelenElem.Click(By.id("dateToRFC_datepicker"));
        SelenElem.SelectOption(By.className("ui-datepicker-year"), año);
        SelenElem.SelectOption(By.className("ui-datepicker-month"), mes);
        SelenElem.Click(By.linkText(dia));
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.Click(By.id("calle-domicilio"));
        OPG.cargaAjax2();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("calleNumeroInterior"), numInterior);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        reporte.AddImageToReport("", OPG.SS());
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.Click(By.id("checkout-step4-check-02"));
        SelenElem.focus(By.id("paso1-pospago-porta"));
        reporte.AddImageToReport("", OPG.SS()); 
        
        SelenElem.Click(By.id("paso1-pospago-porta"));
        OPG.cargaAjax2();
        
        reporte.AddImageToReport("Sección de consulta tu crédito del Checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("cod"), "1111");
        SelenElem.SelectOption(By.id("checkout-step2-select-01"), "BBVA Bancomer");
        SelenElem.Click(By.id("checkout-step2-radio-04"));
        SelenElem.Click(By.id("checkout-step2-radio-06"));
        SelenElem.Click(By.id("checkout-step2"));
        
        reporte.AddImageToReport("Sección de consulta tu crédito del Checkout", OPG.SS()); 
        SelenElem.Click(By.id("creditCheck"));
        
        OPG.cargaAjax2();
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 2);
        SelenElem.Click(By.id("inv-mail2"));
        OPG.cargaAjax2();
        SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("js_codigo_postal"), "0");
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        Thread.sleep(500);
        reporte.AddImageToReport("Validacion Error en codigo postal", OPG.ScreenShotElementInstant());
        
        
        /*SelenElem.Click(By.id("inv-mail2"));
        SelenElem.WaitForLoad(By.id("cp"), 4);
        SelenElem.Click(By.id("cp"));
        SelenElem.TypeText(By.id("cp"), "0");
        SelenElem.Click(By.id("longlat"));
        Thread.sleep(700);
        reporte.AddImageToReport("Error en código postal", OPG.ScreenShotElementInstant());*/
    
    }
    
    public void MigracionCaribuLiferay(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String caso) throws InterruptedException, IOException
    {
        String dn = CargaVariables.LeerCSV(0, "Migraciones Flap", caso);
        String rfc = CargaVariables.LeerCSV(1, "Migraciones Flap", caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Migraciones Flap", caso);
        String apellidop = CargaVariables.LeerCSV(3, "Migraciones Flap", caso);
        String apellidom = CargaVariables.LeerCSV(4, "Migraciones Flap", caso);
        String correo = CargaVariables.LeerCSV(5, "Migraciones Flap", caso);
        String tel = CargaVariables.LeerCSV(6, "Migraciones Flap", caso);
        String calle = CargaVariables.LeerCSV(8, "Migraciones Flap", caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Migraciones Flap", caso);
        String numInterior = CargaVariables.LeerCSV(10, "Migraciones Flap", caso);
        String cp = CargaVariables.LeerCSV(7, "Migraciones Flap", caso);
        String ciudad = CargaVariables.LeerCSV(13, "Migraciones Flap", caso);
        String colonia = CargaVariables.LeerCSV(12, "Migraciones Flap", caso);
        String ine = CargaVariables.LeerCSV(14, "Migraciones Flap", caso);
        String año = CargaVariables.LeerCSV(16, "Migraciones Flap", caso);
        String mes = CargaVariables.LeerCSV(17, "Migraciones Flap", caso);
        String  dia= CargaVariables.LeerCSV(18, "Migraciones Flap", caso);
        String  nombreTel= CargaVariables.LeerCSV(19, "Migraciones Flap", caso);
        String  skuterminal= CargaVariables.LeerCSV(20, "Migraciones Flap", caso);
        String  skuplan= CargaVariables.LeerCSV(21, "Migraciones Flap", caso);
        String  skurecarga= CargaVariables.LeerCSV(22, "Migraciones Flap", caso);
        String  monto= CargaVariables.LeerCSV(23, "Migraciones Flap", caso);
        
        WebDrive.WebDriver.navigate().to(Url + "liferay.php");
        
        SelenElem.SelectOption(By.id("flowCode"), "migracion_caribu");
        
        SelenElem.Find(By.id("skuPlan")).clear();
        SelenElem.TypeText(By.id("skuPlan"), skuplan);
        SelenElem.Find(By.id("skuRecarga")).clear();
        //SelenElem.TypeText(By.id("skuRecarga"), skurecarga);
        SelenElem.Find(By.id("skuTerminal")).clear();
        SelenElem.Find(By.id("skuDn")).clear();
        SelenElem.TypeText(By.id("skuDn"), dn);
        SelenElem.Find(By.id("monto")).clear();
        //SelenElem.TypeText(By.id("monto"), monto);
        Thread.sleep(2000);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        SelenElem.Click(By.name("Comprar"));
        SelenElem.waitForLoadPage();
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        SelenElem.focus(By.id("firstCharacter"));
        reporte.AddImageToReport("Valida tu identidad", OPG.ScreenShotElement());
        Thread.sleep(15000);
        da.slide("Down", 1);
        SelenElem.ClickById(WebDrive.WebDriver, "continue-btn");
        
        Thread.sleep(1000);
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        SelenElem.Click(By.className("data__btn"));
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("total-mobile"));
        reporte.AddImageToReport("Resumen de compra ", OPG.SS());
        SelenElem.Click(By.xpath("//button[.='Continuar con mi compra']"));
        
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-nombre"), 6);
        
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.focus(By.id("valid-nombre"));
        
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        da.hideKeyBoard();
        da.slide("Up", 1);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.ScreenShotElementInstant()); 
        
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        
        
        OPG.cargaAjax2();
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("numero-domicilio-interior"), numInterior);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        da.slide("Up", 1);
        da.hideKeyBoard();
        reporte.AddImageToReport("", OPG.ScreenShotElementInstant());
        
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        
        
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        da.hideKeyBoard();
        da.slide("Up", 1);
        reporte.AddImageToReport("", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("paso1-migracaribu"));
        SelenElem.WaitForLoad(By.id("checkTeminosCondiciones"), 10);
        Thread.sleep(120000);
        da.slide("Up", 1);
        reporte.AddImageToReport("Sección Contrato del Checkout", OPG.ScreenShotElementInstant());
        
         OPG.cargaAjax2();
        
        SelenElem.WaitForLoad(By.id("checkTeminosCondiciones"), 10);
        SelenElem.Click(By.id("checkTeminosCondiciones"));
        SelenElem.Click(By.id("checkConsentimiento"));
        SelenElem.Click(By.id("checkConsentimientoRecarga"));
        da.slide("Down", 1);
        reporte.AddImageToReport("Sección Contrato del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("paso2-migracaribu"));
        
        
        
        SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.SS());
        
       
        
    }
    public void MigracionCaribu(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String caso) throws InterruptedException, IOException
    {
        String dn = CargaVariables.LeerCSV(0, "Migraciones Flap", caso);
        String rfc = CargaVariables.LeerCSV(1, "Migraciones Flap", caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Migraciones Flap", caso);
        String apellidop = CargaVariables.LeerCSV(3, "Migraciones Flap", caso);
        String apellidom = CargaVariables.LeerCSV(4, "Migraciones Flap", caso);
        String correo = CargaVariables.LeerCSV(5, "Migraciones Flap", caso);
        String tel = CargaVariables.LeerCSV(6, "Migraciones Flap", caso);
        String calle = CargaVariables.LeerCSV(8, "Migraciones Flap", caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Migraciones Flap", caso);
        String numInterior = CargaVariables.LeerCSV(10, "Migraciones Flap", caso);
        String cp = CargaVariables.LeerCSV(7, "Migraciones Flap", caso);
        String ciudad = CargaVariables.LeerCSV(13, "Migraciones Flap", caso);
        String colonia = CargaVariables.LeerCSV(12, "Migraciones Flap", caso);
        String ine = CargaVariables.LeerCSV(14, "Migraciones Flap", caso);
        String año = CargaVariables.LeerCSV(16, "Migraciones Flap", caso);
        String mes = CargaVariables.LeerCSV(17, "Migraciones Flap", caso);
        String  dia= CargaVariables.LeerCSV(18, "Migraciones Flap", caso);
        String  nombreTel= CargaVariables.LeerCSV(19, "Migraciones Flap", caso);
        
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        SelenElem.waitForLoadPage();
        Thread.sleep(3000);
        
        SelenElem.WaitForLoad(By.id("menu_list"), 15);
        OPG.SelectMenu("Cámbiate a un plan");
        
        SelenElem.WaitFor(By.id("sc3-dn"));
       
        SelenElem.Click(By.id("sc3-dn"));
        SelenElem.TypeText(By.id("sc3-dn"),dn);
        da.hideKeyBoard();
         reporte.AddImageToReport("Se selecciona el flujo de Cámbiate a un plan", OPG.SS());
        Thread.sleep(3000);
        SelenElem.Click(By.id("sc3-goMigraStep2"));
        Thread.sleep(1000);
        //SelenElem.Click(By.id("sc3-goMigraStep2"));
        SelenElem.waitForLoadPage();
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        SelenElem.focus(By.id("firstCharacter"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Valida tu identidad", OPG.ScreenShotElement());
        
        Thread.sleep(4000);
        
        SelenElem.ClickById(WebDrive.WebDriver, "continue-btn");
        
        SelenElem.WaitForLoad(By.id("Plan-Video"), 10);
        reporte.AddImageToReport("Elegir plan movistar", OPG.ScreenShotElementInstant());
        SelenElem.focus(By.className("redes-sociales-logos"));
        reporte.AddImageToReport("Elegir plan movistar", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("Plan-Video"));
        SelenElem.focus(By.id("spotify"));
        reporte.AddImageToReport("Sin SVA", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("submitplanes"));
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.xpath("//button[.='Ver detalle de plan']"));
        SelenElem.focus(By.xpath("//button[.='Ver detalle de plan']"));
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("total-mobile"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.xpath("//button[.='Continuar con mi compra']"));
        
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-nombre"), 6);
        
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.focus(By.id("valid-nombre"));
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.ScreenShotElementInstant()); 
        
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        
        
        OPG.cargaAjax2();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.ScreenShotElementInstant()); 
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("numero-domicilio-interior"), numInterior);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        reporte.AddImageToReport("", OPG.ScreenShotElementInstant());
        
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        
        
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        reporte.AddImageToReport("", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("paso1-migracaribu"));
        SelenElem.WaitForLoad(By.id("checkTeminosCondiciones"), 10);
        reporte.AddImageToReport("Sección Contrato del Checkout", OPG.ScreenShotElementInstant());
        
         OPG.cargaAjax2();
        
        SelenElem.WaitForLoad(By.id("checkTeminosCondiciones"), 10);
        SelenElem.Click(By.id("checkTeminosCondiciones"));
        SelenElem.Click(By.id("checkConsentimiento"));
        SelenElem.Click(By.id("checkConsentimientoRecarga"));
        
        reporte.AddImageToReport("Sección Contrato del Checkout", OPG.SS());
        
        SelenElem.Click(By.id("paso2-migracaribu"));
        
        
        
        SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
        SelenElem.focus(By.xpath("//*[@id=\"numero_migrar\"]"));
        reporte.AddImageToReport("", OPG.SS());
        
       
        
    }
    
    public void ErrorDNMigracionCaribu(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String caso) throws InterruptedException, IOException
    {
        String dn = CargaVariables.LeerCSV(0, "Migraciones Flap", caso);
        
        
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        SelenElem.waitForLoadPage();
        Thread.sleep(3000);
        
        SelenElem.WaitForLoad(By.id("menu_list"), 15);
        OPG.SelectMenu("Cámbiate a un plan");
        
        SelenElem.WaitFor(By.id("sc3-dn"));
       
        SelenElem.Click(By.id("sc3-dn"));
        SelenElem.TypeText(By.id("sc3-dn"),dn);
        da.hideKeyBoard();
        reporte.AddImageToReport("Se selecciona el flujo de Cámbiate a un plan", OPG.ScreenShotElementInstant());
        Thread.sleep(3000);
        SelenElem.Click(By.id("sc3-goMigraStep2"));
        Thread.sleep(500);
        reporte.AddImageToReport("Error en DN", OPG.ScreenShotElementInstant());
        
        
    }
    
    public void ErrorRFCMigracionCaribu(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String caso) throws InterruptedException, IOException
    {
        String dn = CargaVariables.LeerCSV(0, "Migraciones Flap", caso);
        String rfc = CargaVariables.LeerCSV(1, "Migraciones Flap", caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Migraciones Flap", caso);
        String apellidop = CargaVariables.LeerCSV(3, "Migraciones Flap", caso);
        String apellidom = CargaVariables.LeerCSV(4, "Migraciones Flap", caso);
        String correo = CargaVariables.LeerCSV(5, "Migraciones Flap", caso);
        String tel = CargaVariables.LeerCSV(6, "Migraciones Flap", caso);
        String calle = CargaVariables.LeerCSV(8, "Migraciones Flap", caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Migraciones Flap", caso);
        String numInterior = CargaVariables.LeerCSV(10, "Migraciones Flap", caso);
        String cp = CargaVariables.LeerCSV(7, "Migraciones Flap", caso);
        String ciudad = CargaVariables.LeerCSV(13, "Migraciones Flap", caso);
        String colonia = CargaVariables.LeerCSV(12, "Migraciones Flap", caso);
        String ine = CargaVariables.LeerCSV(14, "Migraciones Flap", caso);
        
        
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        SelenElem.waitForLoadPage();
        Thread.sleep(3000);
        
        SelenElem.WaitForLoad(By.id("menu_list"), 15);
        OPG.SelectMenu("Cámbiate a un plan");
        
        SelenElem.WaitFor(By.id("sc3-dn"));
       
        SelenElem.Click(By.id("sc3-dn"));
        SelenElem.TypeText(By.id("sc3-dn"),dn);
        da.hideKeyBoard();
         reporte.AddImageToReport("Se selecciona el flujo de Cámbiate a un plan", OPG.SS());
        Thread.sleep(3000);
        SelenElem.Click(By.id("sc3-goMigraStep2"));
        Thread.sleep(1000);
        //SelenElem.Click(By.id("sc3-goMigraStep2"));
        SelenElem.waitForLoadPage();
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
        Thread.sleep(10000);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        SelenElem.focus(By.id("firstCharacter"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Valida tu identidad", OPG.ScreenShotElement());
        
        Thread.sleep(4000);
        
        SelenElem.ClickById(WebDrive.WebDriver, "continue-btn");
        
        SelenElem.WaitForLoad(By.id("Plan-Video"), 10);
        reporte.AddImageToReport("Elegir plan movistar", OPG.ScreenShotElementInstant());
        SelenElem.focus(By.className("redes-sociales-logos"));
        reporte.AddImageToReport("Elegir plan movistar", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("Plan-Video"));
        SelenElem.focus(By.id("spotify"));
        reporte.AddImageToReport("Sin SVA", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("submitplanes"));
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.xpath("//button[.='Ver detalle de plan']"));
        SelenElem.focus(By.xpath("//button[.='Ver detalle de plan']"));
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("total-mobile"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.xpath("//button[.='Continuar con mi compra']"));
        
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-nombre"), 6);
        
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.focus(By.id("valid-nombre"));
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.ScreenShotElementInstant()); 
        
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        
        
        OPG.cargaAjax2();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.ScreenShotElementInstant()); 
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("numero-domicilio-interior"), numInterior);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        reporte.AddImageToReport("", OPG.ScreenShotElementInstant());
        
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        
        
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        
        SelenElem.Click(By.id("paso1-migracaribu"));
        SelenElem.focus(By.id("valida-rfc"));
        reporte.AddImageToReport("Error en RFC", OPG.ScreenShotElementInstant());
    }
    
    public void ErrorINEMigracionCaribu(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String caso) throws InterruptedException, IOException
    {
        String dn = CargaVariables.LeerCSV(0, "Migraciones Flap", caso);
        String rfc = CargaVariables.LeerCSV(1, "Migraciones Flap", caso);
        
        String nombre = CargaVariables.LeerCSV(2, "Migraciones Flap", caso);
        String apellidop = CargaVariables.LeerCSV(3, "Migraciones Flap", caso);
        String apellidom = CargaVariables.LeerCSV(4, "Migraciones Flap", caso);
        String correo = CargaVariables.LeerCSV(5, "Migraciones Flap", caso);
        String tel = CargaVariables.LeerCSV(6, "Migraciones Flap", caso);
        String calle = CargaVariables.LeerCSV(8, "Migraciones Flap", caso);
        String numdomicilio = CargaVariables.LeerCSV(9, "Migraciones Flap", caso);
        String numInterior = CargaVariables.LeerCSV(10, "Migraciones Flap", caso);
        String cp = CargaVariables.LeerCSV(7, "Migraciones Flap", caso);
        String ciudad = CargaVariables.LeerCSV(13, "Migraciones Flap", caso);
        String colonia = CargaVariables.LeerCSV(12, "Migraciones Flap", caso);
        String ine = CargaVariables.LeerCSV(14, "Migraciones Flap", caso);
        String año = CargaVariables.LeerCSV(16, "Migraciones Flap", caso);
        String mes = CargaVariables.LeerCSV(17, "Migraciones Flap", caso);
        String  dia= CargaVariables.LeerCSV(18, "Migraciones Flap", caso);
        String  nombreTel= CargaVariables.LeerCSV(19, "Migraciones Flap", caso);
        
        WebDrive.WebDriver.navigate().to(Url);
        reporte.AddImageToReport("Iniciamos con la pagina principal  ", OPG.SS());
        SelenElem.waitForLoadPage();
        Thread.sleep(3000);
        
        SelenElem.WaitForLoad(By.id("menu_list"), 15);
        OPG.SelectMenu("Cámbiate a un plan");
        
        SelenElem.WaitFor(By.id("sc3-dn"));
       
        SelenElem.Click(By.id("sc3-dn"));
        SelenElem.TypeText(By.id("sc3-dn"),dn);
        da.hideKeyBoard();
         reporte.AddImageToReport("Se selecciona el flujo de Cámbiate a un plan", OPG.SS());
        Thread.sleep(3000);
        SelenElem.Click(By.id("sc3-goMigraStep2"));
        Thread.sleep(1000);
        //SelenElem.Click(By.id("sc3-goMigraStep2"));
        SelenElem.waitForLoadPage();
        
        SelenElem.WaitForLoad(By.id("firstCharacter"), 15);
        SelenElem.TypeText(By.id("firstCharacter"), "1");
        SelenElem.TypeText(By.id("secondCharacter"), "2");
        SelenElem.TypeText(By.id("thirdCharacter"), "3");
        SelenElem.TypeText(By.id("fourthCharacter"), "4");
        SelenElem.TypeText(By.id("fifthCharacter"), "5");
        SelenElem.TypeText(By.id("sixthCharacter"), "6");
        SelenElem.focus(By.id("firstCharacter"));
        da.hideKeyBoard();
        reporte.AddImageToReport("Valida tu identidad", OPG.ScreenShotElement());
        
        Thread.sleep(4000);
        
        SelenElem.ClickById(WebDrive.WebDriver, "continue-btn");
        
        SelenElem.WaitForLoad(By.id("Plan-Video"), 10);
        reporte.AddImageToReport("Elegir plan movistar", OPG.ScreenShotElementInstant());
        SelenElem.focus(By.className("redes-sociales-logos"));
        reporte.AddImageToReport("Elegir plan movistar", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("Plan-Video"));
        SelenElem.focus(By.id("spotify"));
        reporte.AddImageToReport("Sin SVA", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("submitplanes"));
        SelenElem.waitForLoadPage();
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.xpath("//button[.='Ver detalle de plan']"));
        SelenElem.focus(By.xpath("//button[.='Ver detalle de plan']"));
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("total-mobile"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Resumen de compra", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.xpath("//button[.='Continuar con mi compra']"));
        
        
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("valid-nombre"), 6);
        
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.focus(By.id("valid-nombre"));
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.ScreenShotElementInstant()); 
        
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        
        
        OPG.cargaAjax2();
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.ScreenShotElementInstant()); 
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("numero-domicilio-interior"), numInterior);
        SelenElem.SelectOption(By.id("colonia"), colonia);
        reporte.AddImageToReport("", OPG.ScreenShotElementInstant());
        
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        
        
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.Click(By.id("checkPrivacyOne"));
        reporte.AddImageToReport("", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("paso1-migracaribu"));
        Thread.sleep(400);
        reporte.AddImageToReport("Error en INE menos de 18 caracteres ", OPG.ScreenShotElementInstant());
    }
    
}
