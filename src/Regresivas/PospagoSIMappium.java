
package Regresivas;

import Core.DeviceActions;
import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.FlapError;
import Operaciones.OpcionesEnvio;
import Operaciones.OperacionesGenerales;
import io.appium.java_client.AppiumDriver;
import java.io.File;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class PospagoSIMappium {
    
    public PospagoSIMappium(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String Caso) throws InterruptedException, IOException
    {
        String nombre = CargaVariables.LeerCSV(0, "Pospago solo SIM (SOHO) - appium", Caso);
        String apellidop = CargaVariables.LeerCSV(1, "Pospago solo SIM (SOHO) - appium", Caso);
        String apellidom = CargaVariables.LeerCSV(2, "Pospago solo SIM (SOHO) - appium", Caso);
        String correo = CargaVariables.LeerCSV(3, "Pospago solo SIM (SOHO) - appium", Caso);
        String tel = CargaVariables.LeerCSV(4, "Pospago solo SIM (SOHO) - appium", Caso);
        String calle = CargaVariables.LeerCSV(6, "Pospago solo SIM (SOHO) - appium", Caso);
        String numdomicilio = CargaVariables.LeerCSV(7, "Pospago solo SIM (SOHO) - appium", Caso);
        String numInterior = CargaVariables.LeerCSV(8, "Pospago solo SIM (SOHO) - appium", Caso);
        String cp = CargaVariables.LeerCSV(5, "Pospago solo SIM (SOHO) - appium", Caso);
        String ciudad = CargaVariables.LeerCSV(11, "Pospago solo SIM (SOHO) - appium", Caso);
        String colonia = CargaVariables.LeerCSV(10, "Pospago solo SIM (SOHO) - appium", Caso);
        String cfdi = CargaVariables.LeerCSV(15, "Pospago solo SIM (SOHO) - appium", Caso);
        String ine = CargaVariables.LeerCSV(13, "Pospago solo SIM (SOHO) - appium", Caso);
        String rfc = CargaVariables.LeerCSV(16, "Pospago solo SIM (SOHO) - appium", Caso);
        
        WebDrive.WebDriver.navigate().to(Url + "negocios/");
        reporte.AddImageToReport("Iniciamos con la pagina principal", OPG.SS());
        SelenElem.Click(By.xpath("//a[@href='/negocios/parrillas']"));
        reporte.AddImageToReport("Elegir plan movistar", OPG.SS());
        SelenElem.Click(By.xpath("//button[.='Lo quiero']"));
        //SelenElem.focus(By.id("submitplanes"));
        //reporte.AddImageToReport("Sin SVA", OPG.SS());
        //SelenElem.Click(By.id("submitplanes"));
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        da.slide("Down", 2);
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        SelenElem.Click(By.xpath("//button[@title='Da clic aquí para ir al carrito de compras']"));
        Thread.sleep(500);
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 40);
        SelenElem.waitForLoadPage();
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout ", OPG.SS());
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("numero-domicilio-interior"), numInterior);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        SelenElem.Click(By.id("valid-ciudad"));
        Thread.sleep(30000);
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        
        SelenElem.SelectOption(By.id("colonia"), colonia);
        da.hideKeyBoard();
        reporte.AddImageToReport("Sección tus datos del Checkout ", OPG.SS());
        SelenElem.SelectOption(By.id("cfdi"), cfdi);
        SelenElem.SelectOption(By.id("typeIdentification"), "INE/IFE");
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        
        SelenElem.Click(By.id("INE-FILE"));
        AppiumDriver ad = (AppiumDriver) WebDrive.WebDriver;
        String Chrome = ad.getContext();
        ad.context("NATIVE_APP");
        Thread.sleep(3000);        
        ad.findElement(By.xpath("//*[@text='INE.pdf']")).click();
        Thread.sleep(3000);        
        ad.context(Chrome);
        
        SelenElem.Click(By.id("checkPrivacyOne"));
        SelenElem.focus(By.id("checkout-step4-check-02"));
        SelenElem.Click(By.id("checkout-step4-check-02"));
       
        SelenElem.Click(By.id("archivoCEDULA"));
        OPG.cargaAjax2();
        SelenElem.Click(By.id("CEDULA-FILE"));
        ad.context("NATIVE_APP");
        Thread.sleep(3000);        
        ad.findElement(By.xpath("//*[@text='cedula.pdf']")).click();
        Thread.sleep(3000);        
        ad.context(Chrome);
        
        da.slide("Up", 1);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.Click(By.xpath("//button[@title='Da clic aquí para continuar a opciones de envío']"));
        SelenElem.ClickByXpath(WebDrive.WebDriver,"//button[@title='Da clic aquí para continuar a opciones de envío']");
        OPG.cargaAjax2();
        Thread.sleep(100000);   
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 20);
        reporte.AddImageToReport("Sección opciones de envio del checkout", OPG.SS());
          SelenElem.Click(By.id("inv-mail2"));
          //OPG.cargaAjax2();
          SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
          Thread.sleep(2000);
          SelenElem.TypeText(By.id("js_codigo_postal"), "06600");
          da.hideKeyBoard();
          reporte.AddImageToReport("Sección opciones de envio del checkout", OPG.SS());
          SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
          Thread.sleep(20000);  
          SelenElem.ClickById(WebDrive.WebDriver,"js_search_cacs_by_postal_code");
          OPG.cargaAjax2();   
          SelenElem.Click(By.id("99090073cac"));
          da.hideKeyBoard();
          reporte.AddImageToReport("Sección opciones de envio del checkout", OPG.SS());
        
        SelenElem.Click(By.xpath("//button[@title='Da clic aquí para continuar']"));
        OPG.cargaAjax2(); 
        Thread.sleep(20000);  
        
        da.slide("Up", 2);
        SelenElem.Click(By.id("checkTerms"));
        SelenElem.focus(By.id("viewDelivery"));
        reporte.AddImageToReport("Sección Contrato del checkout", OPG.SS());
        SelenElem.Click(By.id("end-step"));
        OPG.cargaAjax2();
        Thread.sleep(10000);
        
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        SelenElem.Click(By.id("continuar-checkout-out"));
        new FlapError( reporte, OPG, SelenElem, WebDrive);
        SelenElem.WaitForLoad(By.id("numeroOrden"), 30);
        reporte.AddImageToReport("Pagina de pago realizado con exito", OPG.SS());
    }
}
