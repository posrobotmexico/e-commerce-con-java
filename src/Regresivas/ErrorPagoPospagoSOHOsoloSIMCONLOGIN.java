
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.FlapError;
import Operaciones.OpcionesEnvio;
import Operaciones.OperacionesGenerales;
import java.io.File;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class ErrorPagoPospagoSOHOsoloSIMCONLOGIN {
    
     public ErrorPagoPospagoSOHOsoloSIMCONLOGIN(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, String Caso) throws InterruptedException, IOException {
       
        String nombre = CargaVariables.LeerCSV(0, "Error forma de pago Pospago SOHO solo SIM CON LOGIN", Caso);
        String apellidop = CargaVariables.LeerCSV(1, "Error forma de pago Pospago SOHO solo SIM CON LOGIN", Caso);
        String apellidom = CargaVariables.LeerCSV(2, "Error forma de pago Pospago SOHO solo SIM CON LOGIN", Caso);
        String correo = CargaVariables.LeerCSV(3, "Error forma de pago Pospago SOHO solo SIM CON LOGIN", Caso);
        String tel = CargaVariables.LeerCSV(4, "Error forma de pago Pospago SOHO solo SIM CON LOGIN", Caso);
        String calle = CargaVariables.LeerCSV(6, "Error forma de pago Pospago SOHO solo SIM CON LOGIN", Caso);
        String numdomicilio = CargaVariables.LeerCSV(7, "Error forma de pago Pospago SOHO solo SIM CON LOGIN", Caso);
        String numInterior = CargaVariables.LeerCSV(8, "Error forma de pago Pospago SOHO solo SIM CON LOGIN", Caso);
        String cp = CargaVariables.LeerCSV(5, "Error forma de pago Pospago SOHO solo SIM CON LOGIN", Caso);
        String ciudad = CargaVariables.LeerCSV(11, "Error forma de pago Pospago SOHO solo SIM CON LOGIN", Caso);
        String colonia = CargaVariables.LeerCSV(10, "Error forma de pago Pospago SOHO solo SIM CON LOGIN", Caso);
        String cfdi = CargaVariables.LeerCSV(15, "Error forma de pago Pospago SOHO solo SIM CON LOGIN", Caso);
        String ine = CargaVariables.LeerCSV(13, "Error forma de pago Pospago SOHO solo SIM CON LOGIN", Caso);
        String rfc = CargaVariables.LeerCSV(16, "Error forma de pago Pospago SOHO solo SIM CON LOGIN", Caso);

        WebDrive.WebDriver.navigate().to(Url + "negocios/");
        SelenElem.WaitForLoad(By.id("banner-session"), 6);
        reporte.AddImageToReport("Continuamos con la pagina principal ", OPG.SS());
        SelenElem.Click(By.xpath("//a[@href='/negocios/parrillas']"));
        reporte.AddImageToReport("Elegir plan movistar", OPG.SS());
        SelenElem.Click(By.xpath("//button[.='Lo quiero']"));
        reporte.AddImageToReport("Elegir plan movistar", OPG.SS());
        SelenElem.focus(By.id("submitplanes"));
        reporte.AddImageToReport("Sin SVA", OPG.SS());
        SelenElem.Click(By.id("submitplanes"));
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        SelenElem.focus(By.id("continueToCheckout"));
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        SelenElem.Click(By.id("continueToCheckout"));
        SelenElem.WaitForLoad(By.id("valid-nombre"), 10);
        SelenElem.Click(By.id("valid-nombre"));
        SelenElem.TypeText(By.id("valid-nombre"), nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), apellidop);
        SelenElem.TypeText(By.id("valid-appelido-m"), apellidom);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("valid-email"), correo);
        SelenElem.TypeText(By.id("telefono-contacto"), tel);
        SelenElem.TypeText(By.id("calle-domicilio"), calle);
        SelenElem.TypeText(By.id("numero-domicilio"), numdomicilio);
        SelenElem.TypeText(By.id("numero-domicilio-interior"), numInterior);
        SelenElem.TypeText(By.id("codigo-postal-valida"), cp);
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("valid-ciudad"), ciudad);
        OPG.cargaAjax2();
        SelenElem.SelectOption(By.id("colonia"), colonia);
        SelenElem.SelectOption(By.id("cfdi"), cfdi);
        SelenElem.SelectOption(By.id("typeIdentification"), "INE/IFE");
        reporte.AddImageToReport("Sección tus datos del Checkout", OPG.SS());
        SelenElem.TypeText(By.id("INE-IFE"), ine);
        SelenElem.TypeText(By.id("valida-rfc"), rfc);
        
        

        File file = new File("A:\\DataEcommerce\\Error forma de pago Pospago SOHO solo SIM CON LOGIN\\INE.pdf");
        String path = file.getAbsolutePath();
        SelenElem.Find(By.id("INE-FILE")).sendKeys(path);

        File file2 = new File("A:\\DataEcommerce\\Error forma de pago Pospago SOHO solo SIM CON LOGIN\\cedula.pdf");
        String path2 = file2.getAbsolutePath();
        SelenElem.Find(By.id("CEDULA-FILE")).sendKeys(path2);
        
        SelenElem.WaitForLoad(By.xpath("//*[@id=\"msmx-pospago\"]/div[1]/fieldset[1]/div[2]/div/div/div[15]/div[2]/small"), 5);
        SelenElem.Click(By.id("INE-IFE"));
        reporte.AddImageToReport("", OPG.SS());  
        
               
        SelenElem.Click(By.id("checkPrivacyOne"));
        SelenElem.Click(By.xpath("//button[@title='Da clic aquí para continuar a opciones de envío']"));
        OPG.cargaAjax2();
        SelenElem.Click(By.xpath("//button[@title='Da clic aquí para continuar a opciones de envío']"));
        OPG.cargaAjax2();
        
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        /*SelenElem.WaitForLoad(By.id("inv-mail2"), 6);
        
        SelenElem.Click(By.id("inv-mail2"));
        reporte.AddImageToReport("Seccion opciones de envio de checkout", OPG.SS());
        SelenElem.focus(By.id("cp"));
        
        SelenElem.TypeText(By.id("cp"), "22000");
        OPG.cargaAjax2();
        
        SelenElem.Click(By.id("longlat"));
        OPG.cargaAjax2();      
                
        SelenElem.Click(By.id("21010006cac"));
        SelenElem.focus(By.id("99010012cac"));
        reporte.AddImageToReport("Seccion opciones de envio de checkout", OPG.SS());*/
        
        SelenElem.Click(By.xpath("//button[@title='Da clic aquí para continuar']"));
        OPG.cargaAjax2();  
        SelenElem.Click(By.id("checkTerms"));
        SelenElem.focus(By.id("viewDelivery"));
        reporte.AddImageToReport("Sección Contrato del checkout", OPG.SS());
        SelenElem.Click(By.id("end-step"));
        OPG.cargaAjax2();
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        SelenElem.Click(By.id("continuar-checkout-out"));
        new FlapError( reporte, OPG, SelenElem, WebDrive);
        
        
        

    }
    
}
