
package Regresivas;

import Core.DeviceActions;
import Core.WebAction;
import Core.WebDriv;
import Core.Report;
import Operaciones.CargaVariables;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class CajaDañada {
    
    public CajaDañada(){
        
    }
    
    public void CajaDañadaTarjetaDebito(Report reporte, WebAction SelenElem, OperacionesGenerales OPG, WebDriv WebDrive, String Url, DeviceActions da,String Caso) throws InterruptedException, IOException 
    {
        String ModTel = CargaVariables.LeerCSV(0, "Caja Dañada", Caso);
        String Nombre = CargaVariables.LeerCSV(1, "Caja Dañada", Caso);
        String ApellPa = CargaVariables.LeerCSV(2, "Caja Dañada", Caso);
        String ApellMa = CargaVariables.LeerCSV(3, "Caja Dañada", Caso);
        String Correo = CargaVariables.LeerCSV(4, "Caja Dañada", Caso);
        String Telefono = CargaVariables.LeerCSV(5, "Caja Dañada", Caso);
        String TelefonoAdi = CargaVariables.LeerCSV(6, "Caja Dañada", Caso);
        String CP = CargaVariables.LeerCSV(7, "Caja Dañada", Caso);
        String NumTarjeta = CargaVariables.LeerCSV(8, "Caja Dañada", Caso);
        String Cvv = CargaVariables.LeerCSV(9, "Caja Dañada", Caso);
        String MesTarjeta = CargaVariables.LeerCSV(10, "Caja Dañada", Caso);
        String AñoTarjeta = CargaVariables.LeerCSV(11, "Caja Dañada", Caso);
        
        WebDrive.WebDriver.navigate().to(Url + "smartphone-open-box.html");
        reporte.AddImageToReport("Se ingresa a la pagina principal", OPG.SS());
        SelenElem.focus(By.xpath("//img[@alt='" + ModTel + "']"));
        reporte.AddImageToReport("Se selecciona el telefono", OPG.SS());
        SelenElem.Click(By.xpath("//img[@alt='" + ModTel + "']"));
        
        Thread.sleep(2000);
        reporte.AddImageToReport("Ver detalle del telefono", OPG.SS());
        da.slide("Down", 3);
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.xpath("//p[.='Comprar']"));
        
        SelenElem.WaitForLoad(By.id("coupon_code"), 15);
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        SelenElem.Click(By.id("total-mobile"));
        reporte.AddImageToReport("", OPG.SS());
        
        SelenElem.Click(By.id("continueToCheckout"));
        
        SelenElem.WaitForLoad(By.id("valid-nombre"), 15);
        SelenElem.TypeText(By.id("valid-nombre"), Nombre);
        SelenElem.TypeText(By.id("valid-appelido-p"), ApellPa);
        SelenElem.TypeText(By.id("valid-appelido-m"), ApellMa);
        SelenElem.TypeText(By.id("valid-email"), Correo);
        SelenElem.TypeText(By.id("telefono-contacto"), Telefono);
        SelenElem.TypeText(By.id("additionalContactPhone"), TelefonoAdi);
        da.hideKeyBoard();
        SelenElem.Click(By.id("checkPrivacyOne"));
        reporte.AddImageToReport("Sección tus datos del checkout", OPG.SS());
        
        SelenElem.Click(By.id("btn_continuar"));
        
        SelenElem.WaitForLoad(By.id("inv-mail2"), 15);
        SelenElem.Click(By.id("inv-mail2"));
        Thread.sleep(2000);
        SelenElem.TypeText(By.id("js_codigo_postal"), CP);
        da.hideKeyBoard();
        SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
        SelenElem.WaitForLoad(By.id("99090073cac"), 15);
        SelenElem.Click(By.id("99090073cac"));
        reporte.AddImageToReport("Sección opciones envío del checkout", OPG.SS());
        
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        
        reporte.AddImageToReport("Resumen de compra", OPG.SS());
        SelenElem.Click(By.id("continuar-checkout-out"));
        
        SelenElem.WaitForLoad(By.id("ui-accordion-itemsSale-header-0"), 15);
        SelenElem.focus(By.id("1"));
        reporte.AddImageToReport("Medio de pago Flap", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("1"));
        SelenElem.focus(By.id("contrato_108553_1"));
        reporte.AddImageToReport("", OPG.ScreenShotElementInstant());
        SelenElem.Click(By.id("contrato_108553_1"));
        SelenElem.WaitForLoad(By.id("numTarjeta"), 10);
        SelenElem.TypeText(By.id("numTarjeta"), NumTarjeta);
        SelenElem.SelectOption(By.id("vigenciames"), MesTarjeta);
        SelenElem.SelectOption(By.id("vigenciaanio"), AñoTarjeta);
        SelenElem.TypeText(By.id("cvv2"), Cvv);
        da.hideKeyBoard();
        reporte.AddImageToReport("", OPG.ScreenShotElementInstant());
        
        SelenElem.Click(By.id("continuar"));
        
        SelenElem.WaitForLoad(By.id("btnFinalizar"), 10);
        da.slide("Down", 2);
        reporte.AddImageToReport("Información acerca del pago Flap", OPG.ScreenShotElementInstant());
        
        SelenElem.WaitForLoad(By.id("numero_portar"), 20);
        reporte.AddImageToReport("Página de pago realizada con exito ", OPG.ScreenShotElementInstant());
        da.slide("Down", 2);
        reporte.AddImageToReport("", OPG.ScreenShotElementInstant());
        
        
    }
    
}
