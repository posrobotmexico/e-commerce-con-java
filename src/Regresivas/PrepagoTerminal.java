/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Regresivas;

import Core.Appium;
import Core.DeviceActions;
import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.Flap;
import Operaciones.OpcionesEnvio;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

/**
 *
 * @author LAPTOP-JORGE
 */
public class PrepagoTerminal {
    
    private String resultado;

    public String getResultado() {
        return resultado;
    }
    
    public PrepagoTerminal(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive,String Url,DeviceActions da,String caso) throws InterruptedException, IOException
    {
        resultado = "Prueba fallida";
        reporte.AddImageToReport("Iniciamos desde la pagina principal y seleccionamos telefonos", OPG.ScreenShotElement());
        SelenElem.Click(By.className("icon"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionamos el flujo de Telefonos", OPG.ScreenShotElement());
        SelenElem.Click(By.xpath("//input[@value='Smartphones']"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionamos el flujo de Telefonos", OPG.ScreenShotElement());
        WebDrive.WebDriver.navigate().to(Url + "telefonos.html");
        reporte.AddImageToReport("Seleccionamos la opción Telefonos", OPG.ScreenShotElement());
        
        SelenElem.waitForLoadPage();
        SelenElem.Click(By.xpath("//img[contains(@alt, '" + CargaVariables.LeerCSV(0,"PrepagoTerminal",caso) + "')]"));
        SelenElem.waitForLoadPage();
        Thread.sleep(2000);
      
        reporte.AddImageToReport("Vemos el detalle del equipo", OPG.ScreenShotElement());
        da.slide("Down", 4);
        reporte.AddImageToReport("Vemos mas detalle del equipo", OPG.ScreenShotElement()); 
        //SelenElem.focus(By.id("portaTerminalDetailBox"));
        //reporte.AddImageToReport("Vemos mas detalle del equipo", OPG.ScreenShotElement()); 
        //SelenElem.Click(By.id("btn-buy-three"));
        SelenElem.Click(By.xpath("//*[@id=\"solo-smartphone__content\"]/form/button/p"));
        SelenElem.WaitForLoad(By.linkText("Cambiar equipo"),6);
        reporte.AddImageToReport("Damos click en pagar y vemos las caracteristicas", OPG.ScreenShotElement()); 
        SelenElem.Click(By.id("total-mobile"));
        Thread.sleep(1000);
        reporte.AddImageToReport("Vemos el monto total", OPG.SS()); 
        SelenElem.Click(By.id("continueToCheckout"));
        SelenElem.WaitForLoad(By.id("valid-nombre"), 6);
        Thread.sleep(2000);
        SelenElem.Click(By.id("valid-nombre"));
        SelenElem.TypeText(By.id("valid-nombre"), CargaVariables.LeerCSV(1,"PrepagoTerminal",caso));
        
        SelenElem.TypeText(By.id("valid-appelido-p"), CargaVariables.LeerCSV(2,"PrepagoTerminal",caso));
        SelenElem.Click(By.id("js_ocultar_mostrar_flecha"));
        da.slide("Up", 1);
        reporte.AddImageToReport("Sección de tus datos del checkout", OPG.SS()); 
        SelenElem.TypeText(By.id("valid-appelido-m"), CargaVariables.LeerCSV(3,"PrepagoTerminal",caso));
        SelenElem.TypeText(By.id("valid-email"), CargaVariables.LeerCSV(4,"PrepagoTerminal",caso));
        SelenElem.TypeText(By.id("telefono-contacto"), CargaVariables.LeerCSV(5,"PrepagoTerminal",caso));
        SelenElem.Click(By.id("checkPrivacyOne"));
        
        da.hideKeyBoard();
        
        reporte.AddImageToReport("Sección de tus datos del checkout", OPG.ScreenShotElement()); 
        SelenElem.Click(By.id("btn_continuar"));
        OPG.cargaAjax2();
        
        /*
        SelenElem.WaitForLoad(By.id("inv-mail2"), 6);
        SelenElem.Click(By.id("inv-mail2"));
        SelenElem.TypeText(By.id("cp"), "22000");
        OPG.cargaAjax2();
        SelenElem.Click(By.id("longlat"));
        
        SelenElem.WaitForLoad(By.className("geo-cac-elocker-option__place-title"),6);
        SelenElem.focus(By.className("geo-cac-elocker-option__place-title"));
        SelenElem.Click(By.className("geo-cac-elocker-option__place-title"));
        reporte.AddImageToReport("Seleccionamos el CAC", OPG.ScreenShotElement());*/
        new OpcionesEnvio( reporte, OPG, SelenElem, WebDrive);
        
        SelenElem.Click(By.id("js-continuar-metodos-envio"));
        SelenElem.WaitForLoad(By.id("continuar-checkout-out"),6);
        Thread.sleep(2000);
        SelenElem.Click(By.id("js_ocultar_mostrar_flecha"));
        Thread.sleep(500);
        reporte.AddImageToReport("Sección de tus datos del checkout", OPG.ScreenShotElementInstant()); 
        SelenElem.Click(By.id("continuar-checkout-out"));
        
        new Flap(reporte,OPG,SelenElem,WebDrive);

        SelenElem.WaitForLoad(By.className("descripcion-orden-terminal-card-texto"),30);
        SelenElem.waitForLoadPage();                
        //da.slide("Up",1);        
        reporte.AddImageToReport("Vemos el detalle de la compra", OPG.ScreenShotElement());
        da.slide("Down",2);        
        reporte.AddImageToReport("Vemos el detalle de datos personales", OPG.ScreenShotElement());
        da.slide("Down",2);
        reporte.AddImageToReport("Vemos el detalle del envio", OPG.ScreenShotElement());
        
        resultado = "Prueba exitosa";
    }
    
}
