/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package Vistas;

import Core.Report;
import Core.ReportEjec;
import Core.SystemA;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.OperacionesGenerales;
import Regresivas.ErrorCPPortabilidadPrepagoTerminalSIMConRecarga;
import Regresivas.ErrorDNPortabilidadPrepagoTerminalSIMConRecarga;
import Regresivas.PortabilidadPrepagoTerminalSIMConRecarga;
import Regresivas.PortabilidadPrepagoSoloSIMRecarga;
import java.util.logging.Level;
import java.util.logging.Logger;
import ecommerce.Menu2;

/**
 *
 * @author LAPTOP-JORGE
 */
public class PortaPrepago extends javax.swing.JPanel {
    private WebDriv WebDrive;

    /**
     * Creates new form PortaPrepago
     */
    public PortaPrepago() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Caso1 = new javax.swing.JRadioButton();
        Caso2 = new javax.swing.JRadioButton();
        Caso3 = new javax.swing.JRadioButton();
        Caso4 = new javax.swing.JRadioButton();
        SelecCasos = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        BtnEjecutar1 = new javax.swing.JButton();
        BtnArchivo = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        Caso1.setText("Portabilidad Prepago Terminal + SIM Con Recarga");

        Caso2.setText("Error DN Portabilidad Prepago Terminal + SIM Con Recarga");
        Caso2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Caso2ActionPerformed(evt);
            }
        });

        Caso3.setText("Error CP Portabilidad Prepago Terminal + SIM Con Recarga");

        Caso4.setText("Portabilidad Prepago SIM + Recarga - Modal Porta Pre");

        SelecCasos.setText("Seleccionar todos los casos ");
        SelecCasos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SelecCasosActionPerformed(evt);
            }
        });

        jButton1.setText("Eliminar seleccion ");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        BtnEjecutar1.setText("Ejecutar");
        BtnEjecutar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEjecutar1ActionPerformed(evt);
            }
        });

        BtnArchivo.setText("Archivo");
        BtnArchivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnArchivoActionPerformed(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        jButton2.setText("Carpeta Reporte");
        jButton2.setToolTipText("");
        jButton2.setActionCommand("Caroeta Reporte");
        jButton2.setMaximumSize(new java.awt.Dimension(79, 23));
        jButton2.setMinimumSize(new java.awt.Dimension(79, 23));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Caso2)
                            .addComponent(Caso4)
                            .addComponent(Caso3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(SelecCasos)
                            .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Caso1)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(39, 39, 39)
                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(27, 27, 27)
                                .addComponent(BtnEjecutar1, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(29, 29, 29)
                                .addComponent(BtnArchivo, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap(109, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(Caso1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Caso2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Caso3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Caso4))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(32, 32, 32)
                                .addComponent(jButton1))
                            .addComponent(SelecCasos))))
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BtnEjecutar1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BtnArchivo, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(219, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void Caso2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Caso2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Caso2ActionPerformed

    private void SelecCasosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SelecCasosActionPerformed
        // TODO add your handling code here:
        Caso1.setSelected(true);
        Caso2.setSelected(true);
        Caso3.setSelected(true);
        Caso4.setSelected(true);

    }//GEN-LAST:event_SelecCasosActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        Caso1.setSelected(false);
        Caso2.setSelected(false);
        Caso3.setSelected(false);
        Caso4.setSelected(false);

    }//GEN-LAST:event_jButton1ActionPerformed

    private void BtnEjecutar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEjecutar1ActionPerformed
        // TODO add your handling code here:
        Report reporte = null;
        ReportEjec reporte2 = null;
        try {
            String Explorador,Or,Error;
            String Url="https://certi-246-tienda-movistar.canalesdigitales.com.mx/";
            //String Url="http://google.com";
            (new OperacionesGenerales()).CierraNavegadores("Chrome");
            Thread.sleep(1000);
            reporte2 = new ReportEjec();

            WebDrive = new WebDriv(Url, "Chrome");

            WebAction SelenElem=new WebAction(WebDrive.WebDriver);//Objeto para ocupar funciones WebAction
            OperacionesGenerales OPG = new OperacionesGenerales(WebDrive,SelenElem);
            //Variables
            String Caso = null,FechaIn,FechaFin,Operador,CodigoOferta,IMEI,RFC,Dia,Mes,Anio,Fecha,Edo,Ope, Recarga, Monto,User,Contra,Plan,Importe,SKU,BusinessType;
            if(Caso1.isSelected())
            {
                try
                {
                    Caso = "Portabilidad Prepago Terminal + SIM Con Recarga";
                    reporte = new Report(Caso); //Se inicializa el reporte

                    new PortabilidadPrepagoTerminalSIMConRecarga(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                    reporte.Export();
                    reporte2.writeExecution(Caso);
                    OPG.CierraNavegadores("Chrome");
                    Thread.sleep(1000);
                } catch (Exception e) {
                    reporte.writeError("La prueba FALLO favor de reintentar, si el problema persiste comunicarse con los desarrolladores");
                    reporte.Export();
                    reporte2.writeErrorExecution(Caso);
                    OPG.CierraNavegadores("Chrome");
                    Thread.sleep(1000);
                }
            }
            if(Caso2.isSelected())
            {
                try
                {
                    Caso = "Error DN Portabilidad Prepago Terminal + SIM Con Recarga";
                    reporte = new Report(Caso); //Se inicializa el reporte

                    new ErrorDNPortabilidadPrepagoTerminalSIMConRecarga(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                    reporte.Export();
                    reporte2.writeExecution(Caso);
                    OPG.CierraNavegadores("Chrome");
                    Thread.sleep(1000);
                } catch (Exception e) {
                    reporte.writeError("La prueba FALLO favor de reintentar, si el problema persiste comunicarse con los desarrolladores");
                    reporte.Export();
                    reporte2.writeErrorExecution(Caso);
                    OPG.CierraNavegadores("Chrome");
                    Thread.sleep(1000);
                }
            }
            if(Caso3.isSelected())
            {
                try
                {
                    Caso = "Error CP Portabilidad Prepago Terminal + SIM Con Recarga";
                    reporte = new Report(Caso); //Se inicializa el reporte

                    new ErrorCPPortabilidadPrepagoTerminalSIMConRecarga(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                    reporte.Export();
                    reporte2.writeExecution(Caso);
                    OPG.CierraNavegadores("Chrome");
                    Thread.sleep(1000);
                } catch (Exception e) {
                    reporte.writeError("La prueba FALLO favor de reintentar, si el problema persiste comunicarse con los desarrolladores");
                    reporte.Export();
                    reporte2.writeErrorExecution(Caso);
                    OPG.CierraNavegadores("Chrome");
                    Thread.sleep(1000);
                }
            }
            if(Caso4.isSelected())
            {
                try
                {
                    Caso = "Portabilidad Prepago SIM + Recarga - Modal Porta Pre";
                    reporte = new Report(Caso); //Se inicializa el reporte

                    new PortabilidadPrepagoSoloSIMRecarga().PortaPrepagoSIMRecargaModalPorta(reporte, SelenElem, OPG, WebDrive, Url, Caso);
                    reporte.Export();
                    reporte2.writeExecution(Caso);
                    OPG.CierraNavegadores("Chrome");
                    Thread.sleep(1000);
                } catch (Exception e) {
                    reporte.writeError("La prueba FALLO favor de reintentar, si el problema persiste comunicarse con los desarrolladores");
                    reporte.Export();
                    reporte2.writeErrorExecution(Caso);
                    OPG.CierraNavegadores("Chrome");
                    Thread.sleep(1000);
                }
            }

        } catch (Exception ex) {
            Logger.getLogger(Menu2.class.getName()).log(Level.SEVERE, null, ex);
            reporte.writeError("La prueba FALLO favor de reintentar, si el problema persiste comunicarse con los desarrolladores");
            reporte.Export();
        } finally {
            reporte2.Export();
            if(reporte2.getNamefile() != null)
            (new SystemA()).OpenFile(reporte2.getNamefile());
        }
    }//GEN-LAST:event_BtnEjecutar1ActionPerformed

    private void BtnArchivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnArchivoActionPerformed
        // TODO add your handling code here:
        String ruta = null;
        if(Caso1.isSelected())
        {
            ruta = "A:\\DataEcommerce\\Portabilidad Prepago Terminal + SIM Con Recarga\\Portabilidad Prepago Terminal + SIM Con Recarga.csv";
            (new SystemA()).OpenFile(ruta);
        }
        if(Caso2.isSelected())
        {
            ruta = "A:\\DataEcommerce\\Error DN Portabilidad Prepago Terminal + SIM Con Recarga\\Error DN Portabilidad Prepago Terminal + SIM Con Recarga.csv";
            (new SystemA()).OpenFile(ruta);
        }
        if(Caso3.isSelected())
        {
            ruta = "A:\\DataEcommerce\\Error CP Portabilidad Prepago Terminal + SIM Con Recarga\\Error CP Portabilidad Prepago Terminal + SIM Con Recarga.csv";
            (new SystemA()).OpenFile(ruta);
        }
        if(Caso4.isSelected())
        {
            ruta = "A:\\DataEcommerce\\Portabilidad Prepago Solo SIM + Recarga\\Portabilidad Prepago SIM + Recarga - Modal Porta Pre.csv";
            (new SystemA()).OpenFile(ruta);
        }
        

    }//GEN-LAST:event_BtnArchivoActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

        //(new SystemA()).OpenFile(ruta);
        (new SystemA()).OpenFolder("A:\\Reportes");
    }//GEN-LAST:event_jButton2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnArchivo;
    private javax.swing.JButton BtnEjecutar1;
    private javax.swing.JRadioButton Caso1;
    private javax.swing.JRadioButton Caso2;
    private javax.swing.JRadioButton Caso3;
    private javax.swing.JRadioButton Caso4;
    private javax.swing.JButton SelecCasos;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    // End of variables declaration//GEN-END:variables
}
