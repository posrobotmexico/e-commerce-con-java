
package ecommerce;


import Core.*;
import Operaciones.*;
import Regresivas.Adiciones;
import Regresivas.ArmatuPack;
import Regresivas.ArmatuPlan;
import Regresivas.AñadeMasLineasPospago;
import Regresivas.ErrorCPPortabilidadPospagoSIMmasTerminal;
import Regresivas.ErrorCPPortabilidadPrepagoTerminalSIMConRecarga;
import Regresivas.ErrorCPPospagoSIMmasTerminal;
import Regresivas.ErrorCPPospagoSOHOsoloSIMCONLOGIN;
import Regresivas.ErrorCPSoloTerminalMercadoPago;
import Regresivas.ErrorCPSoloTerminalStripe;
import Regresivas.ErrorCodigoPostalPortabilidadPospago;
import Regresivas.ErrorCodigoPostalPospago;
import Regresivas.ErrorCodigoPostalPrepago;
import Regresivas.ErrorDNMigración;
import Regresivas.ErrorDNPortabilidadPospago;
import Regresivas.ErrorDNPortabilidadPospagoSIMmasTerminal;
import Regresivas.ErrorDNPortabilidadPrepagoTerminalSIMConRecarga;
import Regresivas.ErrorElectorPospagoSIMmasTerminal;
import Regresivas.ErrorFormaPagoMigración;
import Regresivas.ErrorFormaPagoPortabilidadPospago;
import Regresivas.ErrorINEPortabilidadPospago;
import Regresivas.ErrorINEPortabilidadPospagoSIMmasTerminal;
import Regresivas.ErrorPagoPortabilidadPospagoSIMmasTerminal;
import Regresivas.ErrorPagoPospagoSIMmasTerminal;
import Regresivas.ErrorPagoPospagoSOHOsoloSIMCONLOGIN;
import Regresivas.ErrorPagoPrepago;
import Regresivas.ErrorPagoSoloTerminalMercadoPago;
import Regresivas.ErrorPagoSoloTerminalStripe;
import Regresivas.ErrorRFCPospagoSOHOsoloSIMCONLOGIN;
import Regresivas.ErrorRFCPrepago;
import Regresivas.ErrorformaDePagoPospago;
import Regresivas.MigracionSoloSIM;
import Regresivas.MigracionSoloSIMTerminal;
import Regresivas.Migraciones;
import Regresivas.MigracionesFlap;
import Regresivas.PortaPospagoSim;
import Regresivas.PortabilidadPospago;
import Regresivas.PortabilidadPospagoSIMmasTerminal;
import Regresivas.PortabilidadPospagoSoloSIM;
import Regresivas.PortabilidadPrepagoFlap;
import Regresivas.PortabilidadPrepagoSoloSIMRecarga;
import Regresivas.PortabilidadPrepagoTerminalSIMConRecarga;
import Regresivas.PospagoNuevoNumeroFlap;
import Regresivas.PospagoSIMAdiciones;
import Regresivas.PospagoSIMTerminal;
import Regresivas.PospagoSIMappium;
import Regresivas.PospagoSIMmasTerminal;
import Regresivas.PospagoSOHOsoloSIM;
import Regresivas.PospagoSOHOsoloSIMCONLOGIN;
import Regresivas.PospagoSOHOsoloSIMLiferay;
import Regresivas.PospagoSoloSIM;
import Regresivas.PrepagoFlapOXXO;
import Regresivas.PrepagoFlapSTRIPE;
import Regresivas.PrepagoNuevoNumeroFlap;
import Regresivas.PrepagoSoloSIMRecarga;
import Regresivas.PrepagoSoloSIMTerminalRecarga;
import Regresivas.PrepagoTerminal;
import Regresivas.PrepagoTerminalFlap;
import Regresivas.RecargaFlap;
import Regresivas.RecargaPublicaMercadoPP;
import Regresivas.Recargas;
import Regresivas.RenovacionTotalPagarCero;
import Regresivas.Renovaciones;
import Regresivas.SoloTerminalMercadoPago;
import Regresivas.SoloTerminalStripe;
import Regresivas.SoloTerminalVitrina;
import Regresivas.SoloTerminalFlap;
import io.appium.java_client.AppiumDriver;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/*
    Es necesario instalar NodeJS
*/

public class Menu extends javax.swing.JFrame {
    private WebDriv WebDrive;
    
    public Menu() {
        initComponents();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jLabel2 = new javax.swing.JLabel();
        ComboBox1 = new javax.swing.JComboBox<>();
        Aceptar = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        Resultado = new javax.swing.JLabel();
        Orden = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jCheckBox2 = new javax.swing.JCheckBox();
        jButton3 = new javax.swing.JButton();
        jCheckBox1 = new javax.swing.JCheckBox();

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Practia - eCommerce Automatización");
        setBackground(new java.awt.Color(255, 255, 255));
        setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        setForeground(new java.awt.Color(255, 255, 255));
        setLocationByPlatform(true);
        setResizable(false);

        jLabel2.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        jLabel2.setText("Selecciona el caso a ejecutar:");

        ComboBox1.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        ComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "------------------------  REGRESIVAS WEB -------------------------", "Recarga pública (Montos) - Mercado Pago Pro", "Recarga pública (Paquetes) - Mercado Pago Pro", "Arma tu plan MOVISTAR PERSONALIZADO", "Arma tu plan Personalizacion (12 Gbs)", "Arma tu plan Salida de Oferta Cambio de plan a Plan Datos Ilimitados", "Arma tu pack Movistar personalizado 5 Gbs", "Prepago Flap (OXXO) Solo Terminal", "Pospago SOHO solo SIM", "Pospago solo SIM Adiciones", "Añade + lineas Pospago", "------------------------  POSPAGO  -------------------------", "Pospago solo SIM", "Error de Codigo Postal Pospago", "Error en la forma de pago Pospago", "------------------------  PREPAGO_FLAP  -------------------------", "Prepago Solo SIM + Recarga", "Prepago Solo SIM + Terminal + Recarga", "Prepago Terminal - Flap", "Error de Codigo Postal Prepago", "Error de RFC Prepago", "Error en la forma de pago Prepago", "------------------------   MIGRACIONES  -------------------------", "Migración Solo SIM", "Error en el DN Migración", "Error en la forma de pago Migración", "------------------------   PORTABLIDAD POSPAGO FLAP  -------------------------", "Portabilidad Pospago Solo SIM", "Error en el DN Portabilidad Pospago", "Error de Codigo Postal Portabilidad Pospago", "Error en los 18 digitos de INE Portabilidad Pospago", "Portabilidad Pospago SIM + Terminal", "Error DN Portabilidad Pospago SIM + Terminal", "Error INE Portabilidad Pospago SIM + Terminal", "Error CP Portabilidad Pospago SIM + Terminal", "Error Pago Portabilidad Pospago SIM + Terminal", "------------------  PREPAGO CON NUEVO NÚMERO - FLAP  ----------------------", "Solo Terminal - Mercado Pago", "Error codigo postal Solo Terminal - Mercado Pago", "Error en Forma de Pago Solo Terminal - Mercado Pago", "Solo Terminal – Stripe", "Error codigo postal Solo Terminal - Stripe", "Error en Forma de Pago Solo Terminal - Stripe", "Solo Terminal - Mercado Pago - Cupon - appium", "Solo Terminal - Mercado Pago - Descuento directo - appium", "Solo Terminal - Mercado Pago - Descuento directo + Cupon - appium", "Solo Terminal - Mercado Pago - Precio Costo - Cupon - appium", "Solo Terminal - Mercado Pago - Precio Costo - Descuento directo - appium", "------------------------ PORTABILIDAD PREPAGO - FLAP  ----------------------", "Portabilidad Prepago Terminal + SIM Con Recarga", "Error DN Portabilidad Prepago Terminal + SIM Con Recarga", "Error CP Portabilidad Prepago Terminal + SIM Con Recarga", "--------------------- POSPAGO CON NUEVO NUMERO - FLAP --------------", "Pospago SIM + Terminal", "Error en Clave de Elector Pospago SIM + Terminal", "Error en Código Postal Pospago SIM + Terminal", "Error en Forma de Pago Pospago SIM + Terminal", "Pospago SOHO solo SIM CON LOGIN", "Error RFC Pospago SOHO solo SIM CON LOGIN", "Error CP Pospago SOHO solo SIM CON LOGIN", "Error forma de pago Pospago SOHO solo SIM CON LOGIN", "Adiciones con login", "Error CP Adiciones con login", "Error Pago Adiciones con login", "Pospago SOHO solo SIM – Fianza 200 – Liferay", "Adiciones Liferay - Fianza $200 - appium", "Adiciones Liferay - Fianza $0 - appium", "--------------------- MIGRACIONES - FLAP --------------", "Migraciones SIM + Terminal", "Error DN Migraciones SIM + Terminal", "Error INE Migraciones SIM + Terminal", "Error CP Migraciones SIM + Terminal", "Error forma de Pago Migraciones SIM + Terminal", "--------------------RECARGA PUBLICA - FLAP --------------", "Recarga con montos", "Recarga con paquetes", "--------------------- RENOVACION - FLAP --------------", "Renovacion", "Error en DN Renovacion", "Error en INE Renovacion", "Renovación Total a Pagar $0", "--------------------- RENOVACION - R3 --------------", "Renovaciones R3 Efectivo", "Renovaciones R3 Tarjeta", "Renovacion R3 Tarjeta Asociada - appium", "Renovacion R3 Tokenizacion Liferay - appium", "------------------------  REGRESIVAS MOVILES  -------------------------", "Pospago solo SIM (SOHO) - appium", "Prepago Terminal - Flap (Solo terminal) - appium", "Portabilidad pospago Sim - appium", "Migraciones FLAP - appium", "Renovaciones FLAP - appium", "Recarga pública (Paquetes) Flap - appium", "Recarga pública (Montos) Flap - appium", "--------------------  PREPAGO CON NUEVO NUMERO - FLAP --------------------", "Prepago Solo SIM + Recarga + Terminal - appium", "Error CP Prepago Solo SIM + Recarga + Terminal - appium", "Error Pago Prepago Solo SIM + Recarga + Terminal - appium", "Prepago Solo Terminal Oxxo - appium", "Error CP Prepago Solo Terminal Oxxo - appium", "Prepago Solo Terminal Mercado Pago - appium", "Error CP Prepago Solo Terminal Mercado Pago - appium", "Error Pago Prepago Solo Terminal Mercado Pago - appium", "Prepago Solo Terminal STRIPE - appium", "Error CP Prepago Solo Terminal STRIPE - appium", "Error Forma de Pago Prepago Solo Terminal STRIPE - appium", "Prepago Stripe+mpp+mpe- appium", "Error Datos Prepago Stripe+mpp+mpe- appium", "Error CP Prepago Stripe+mpp+mpe- appium", "Error Pago Prepago Stripe+mpp+mpe- appium", "Prepago Solo Terminal Mercado Pago Pro - appium", "-------------------- PORTABILIDAD PREPAGO - FLAP  --------------------", "Portabilidad Prepago Terminal + Recarga Liferay - appium", "Portabilidad Prepago Terminal + SIM Con Recarga - appium", "Error DN Portabilidad Prepago Terminal + SIM Con Recarga - appium", "Error RFC Portabilidad Prepago Terminal + SIM Con Recarga - appium", "Error CP Portabilidad Prepago Terminal + SIM Con Recarga - appium", "Error NIP Portabilidad Prepago Terminal + SIM Con Recarga - appium", "-------------------- PORTABILIDAD POSPAGO - FLAP  --------------------", "Portabilidad Pospago solo SIM + terminal Liferay - appium", "Portabilidad Pospago solo SIM + terminal - appium", "Error en DN Portabilidad Pospago solo SIM + terminal - appium", "Error INE Portabilidad Pospago solo SIM + terminal - appium", "Error CP Portabilidad Pospago solo SIM + terminal - appium", "Error NIP CURP y fecha de ventana - appium", "Portabilidad Pospago Solo SIM - Liferay", "Portabilidad Pospago Solo SIM - Liferay Fianza $200", "Portabilidad Pospago con Terminal - Fianza $200 - appium", "Portabilidad refactor Liferay - Fianza $0 - appium", "Portabilidad Pospago Solo SIM Liferay - Fianza $0 - appium", "Portabilidad Pospago Solo SIM Liferay - Fianza $200 - appium", "-------------------- MIGRACIONES - FLAP  --------------------", "Migraciones SIM + Terminal - Liferay - appium", "Error INE Migraciones SIM + Terminal - Liferay - appium", "Error CP Migraciones SIM + Terminal - Liferay - appium", "Migracion Caribu - appium", "Error DN Migracion Caribu - appium", "Error RFC Migracion Caribu - appium", "Error INE Migracion Caribu - appium", "Migraciones SIM + Terminal - appium", "Migración Caribu - Liferay - appium", "-------------------- RENOVACION - FLAP  --------------------", "Renovación Total a Pagar $0 - appium", "Renovación Total a Pagar $0 Liferay - appium", "Renovación Liferay - appium", "Error INE Renovación Liferay - appium", "Error CP Renovación Liferay - appium", "--------------------- POSPAGO CON NUEVO NUMERO - FLAP --------------", "Pospago Liferay - appium", "Error INE Pospago Liferay - appium", "Error CP Pospago Liferay - appium", "Error Pago Pospago Liferay - appium", "Pospago SIM + Terminal - appium", "Error INE Pospago SIM + Terminal - appium", "Error CP Pospago SIM + Terminal - appium", "Error Forma de Pago Pospago SIM + Terminal - appium", "Pospago solo SIM Adiciones - appium", "Error CP Pospago solo SIM Adiciones - appium", "Error Pago Pospago solo SIM Adiciones - appium", "Pospago SOHO Fianza $0 Envio a CAC - appium", "Pospago SOHO Fianza 0 Liferay - appium", "--------------------- RECARGA PUBLICA - MERCADO PAGO PRO --------------", "Recarga con montos - Liferay - appium", "Recarga con paquetes - Liferay - appium", "Recarga con montos Mercado Pago - appium", "Recarga con paquetes Mercado Pago - appium", "--------------------- RECARGA PUBLICA - NBA --------------", "Migración Caribu NBA montos - appium", "Migración Caribu NBA paquetes - appium", "---------------------Solo Terminal - FLAP - Vitrina----------------------", "Solo Terminal FLAP Vitrina - Descuento directo + Cupon - appium", "Solo Terminal FLAP Vitrina - Cupon - appium", "Solo Terminal FLAP Vitrina - Descuento directo - appium", "Solo Terminal FLAP Vitrina - Precio Costo - Cupon - appium", "---------------------Solo Terminal - FLAP-----------------------------", "Solo Terminal Flap - Cupon - appium", "Solo Terminal Flap - Descuento directo - appium", "Solo Terminal Flap - Descuento directo + Cupon - appium", "Solo Terminal Flap - Precio Costo - Cupon - appium", "Solo Terminal Flap - Precio Costo - Descuento directo - appium" }));
        ComboBox1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ComboBox1ItemStateChanged(evt);
            }
        });
        ComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ComboBox1ActionPerformed(evt);
            }
        });

        Aceptar.setText("Ejecutar");
        Aceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AceptarActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Times New Roman", 0, 10)); // NOI18N
        jLabel3.setText("Practia © México ");

        Resultado.setBorder(javax.swing.BorderFactory.createTitledBorder("Resultados"));

        Orden.setBorder(javax.swing.BorderFactory.createTitledBorder("Número de orden"));

        jButton1.setText("Salir");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Practia - Logo.png"))); // NOI18N

        jLabel6.setFont(new java.awt.Font("Verdana", 0, 36)); // NOI18N
        jLabel6.setText("e-Commerce");

        jLabel9.setFont(new java.awt.Font("Verdana", 0, 18)); // NOI18N
        jLabel9.setText("Automatización");

        jButton2.setText("Carpeta reportes");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jCheckBox2.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        jCheckBox2.setSelected(true);
        jCheckBox2.setText("Abrir reporte al finalizar la prueba");
        jCheckBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox2ActionPerformed(evt);
            }
        });

        jButton3.setText("Editar archivo de configuración");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jCheckBox1.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        jCheckBox1.setText("Usar login para la prueba");
        jCheckBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jLabel9)
                                        .addGap(63, 63, 63)))
                                .addGap(82, 82, 82)
                                .addComponent(jLabel1)
                                .addGap(31, 31, 31))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Resultado, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 426, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Orden, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 426, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
                        .addComponent(Aceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addComponent(jButton2)
                        .addGap(40, 40, 40)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(139, 139, 139))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(31, 31, 31)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jCheckBox1)
                                    .addComponent(jCheckBox2)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(42, 42, 42)
                                .addComponent(ComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(181, 181, 181)
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel9)
                        .addGap(18, 18, 18))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addGap(12, 12, 12)
                .addComponent(ComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 50, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jCheckBox2)
                        .addGap(18, 18, 18)
                        .addComponent(jCheckBox1)
                        .addGap(25, 25, 25)
                        .addComponent(Resultado, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Orden, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Aceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jLabel2.getAccessibleContext().setAccessibleName("jLabel");

        getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ComboBox1ActionPerformed
        
    }//GEN-LAST:event_ComboBox1ActionPerformed

    private void ComboBox1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ComboBox1ItemStateChanged

    }//GEN-LAST:event_ComboBox1ItemStateChanged

    private void AceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AceptarActionPerformed
        String Url = "https://certi-246-tienda-movistar.canalesdigitales.com.mx/";
        //String Url = "https://prepro246-tienda-movistar.canalesdigitales.com.mx/";
        String Caso = ComboBox1.getSelectedItem().toString();  //Se obtiene el texto del combo 
        Resultado.setText("Exito.");
        int sizeofimageReport = 40;
                
        Appium ap = null;
        AppiumDriver device = null;
        
        (new OperacionesGenerales()).CierraNavegadores("Chrome");
        /*Thread thread = new Thread() { // Use pipes for 
            public void run() {
                JOptionPane.showMessageDialog(null,"Se ha actualizado el webDriver, inicie nuevamente la aplicación");
            }
        };

        thread.start();*/
        
        if(WebDrive != null)
            WebDrive.CloseWebDriver();  
        
        if(Caso.contains("appium"))
        {
            ap = new Appium();
            ap.startServer();
            device = ap.connectDevice();
            
            WebDrive = new WebDriv(Url,ap.getDriver()); 
            sizeofimageReport = 26;
        }
        else
            WebDrive = new WebDriv(Url, "Chrome");   
        
        Report reporte = new Report(Caso,sizeofimageReport);
        WebAction SelenElem = new WebAction(WebDrive.WebDriver);//Objeto para ocupar funciones WebAction
        OperacionesGenerales OPG = new OperacionesGenerales(WebDrive,SelenElem);
        
        String DN, Monto;
        try {
            if(Caso.contains("Con login") || jCheckBox1.isSelected())
                OPG.Login(SelenElem,reporte);
            
             switch (Caso) 
            {
                case "Recarga pública (Paquetes) - Mercado Pago Pro":
                case "Recarga pública (Montos) - Mercado Pago Pro":
                    DN = CargaVariables.LeerCSV(0,"Recargas",Caso);
                    Monto = CargaVariables.LeerCSV(1,"Recargas",Caso);
                    
                    OperacionesRecargas OpRecargas = new OperacionesRecargas(WebDrive,SelenElem);
                    Recargas Recarga = new Recargas(reporte,OPG,OpRecargas,DN,Monto,SelenElem,WebDrive);
                    Resultado.setText(Recarga.getResultado());
                break;
                
                case "Arma tu plan MOVISTAR PERSONALIZADO":
                case "Arma tu plan Personalizacion (12 Gbs)":
                case "Arma tu plan Salida de Oferta Cambio de plan a Plan Datos Ilimitados":
                    OperacionesArmatuPlan OpArmatuPlan = new OperacionesArmatuPlan(WebDrive,SelenElem);  
                    DN = CargaVariables.LeerCSV(0,"Arma tu plan",Caso);
                    ArmatuPlan armatuplan = new ArmatuPlan();
                    
                    if(Caso.contains("MOVISTAR PERSONALIZADO"))
                        armatuplan.MovistarPersonalizado(reporte,OPG,OpArmatuPlan,DN,Url,SelenElem,WebDrive);
                    else if(Caso.contains("Personalizacion (12 Gbs)"))
                        armatuplan.MovistarPersonalizado2(reporte,OPG,OpArmatuPlan,DN,Url,SelenElem,WebDrive);
                    else if(Caso.contains("Plan Datos Ilimitados"))
                        armatuplan.CambioPlanDatosIlimitados(reporte,OPG,OpArmatuPlan,DN,Url,SelenElem,WebDrive);
                    
                    Resultado.setText(armatuplan.getResultado());
                break;
                
                case "Arma tu pack Movistar personalizado 5 Gbs":
                    OperacionesArmatuPack OpArmatuPack = new OperacionesArmatuPack(WebDrive,SelenElem);  
                    DN = CargaVariables.LeerCSV(0,"Arma tu pack",Caso);
                    ArmatuPack armatupack = new ArmatuPack();
                    
                    if(Caso.contains("Movistar personalizado 5 Gbs"))
                        armatupack.MovistarPersonalizado5Gb(reporte,OPG,OpArmatuPack,DN,Url,SelenElem,WebDrive);
                    
                    Resultado.setText(armatupack.getResultado());
                break;
                
                   
                case "Prepago Terminal - Flap (Solo terminal) - appium":
                    new PrepagoTerminal(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Portabilidad pospago Sim - appium":
                    new PortaPospagoSim(reporte,OPG,SelenElem,WebDrive,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Migraciones FLAP - appium":
                    new Migraciones(reporte,OPG,SelenElem,WebDrive,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Renovaciones FLAP - appium":
                    new Renovaciones().Renovaciones1(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Recarga pública (Paquetes) Flap - appium":
                    new RecargaFlap().Paquetes(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Recarga pública (Montos) Flap - appium":
                    new RecargaFlap().Montos(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Pospago solo SIM (SOHO) - appium":
                    new PospagoSIMappium(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Portabilidad Prepago Terminal + Recarga Liferay - appium":
                    new PortabilidadPrepagoFlap().PortabilidadPrepagoTerminalMasRecargaLiferay(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Portabilidad Prepago Terminal + SIM Con Recarga - appium":
                    new PortabilidadPrepagoFlap().PortabilidadPrepagoTerminalMasSIMConRecarga(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error DN Portabilidad Prepago Terminal + SIM Con Recarga - appium":
                    new PortabilidadPrepagoFlap().ErrorDNPortabilidadPrepago(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error RFC Portabilidad Prepago Terminal + SIM Con Recarga - appium":
                    new PortabilidadPrepagoFlap().ErrorRFCPortabilidadPrepago(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error CP Portabilidad Prepago Terminal + SIM Con Recarga - appium":
                    new PortabilidadPrepagoFlap().ErrorCPPortabilidadPrepago(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error NIP Portabilidad Prepago Terminal + SIM Con Recarga - appium":
                    new PortabilidadPrepagoFlap().ErrorNIPPortabilidadPrepago(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Migraciones SIM + Terminal - Liferay - appium":
                    new MigracionesFlap().MigracionesSIMTerminalLiferay(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Migraciones SIM + Terminal - appium":
                    new MigracionesFlap().MigracionesSIMTerminal(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                /*
                case "Error DN Migraciones SIM + Terminal - Liferay - appium":
                    new MigracionesFlap().ErrorDN(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;*/
                
                case "Error INE Migraciones SIM + Terminal - Liferay - appium":
                    new MigracionesFlap().ErrorINE(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error CP Migraciones SIM + Terminal - Liferay - appium":
                    new MigracionesFlap().ErrorCP(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Migración Caribu - Liferay - appium":
                    new MigracionesFlap().MigracionCaribuLiferay(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Migracion Caribu - appium":
                    new MigracionesFlap().MigracionCaribu(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error DN Migracion Caribu - appium":
                    new MigracionesFlap().ErrorDNMigracionCaribu(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error RFC Migracion Caribu - appium":
                    new MigracionesFlap().ErrorRFCMigracionCaribu(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error INE Migracion Caribu - appium":
                    new MigracionesFlap().ErrorINEMigracionCaribu(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Prepago Solo Terminal STRIPE - appium":
                    new PrepagoNuevoNumeroFlap().PrepagoFlapSTRIPE(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error CP Prepago Solo Terminal STRIPE - appium":
                    new PrepagoNuevoNumeroFlap().ErrorCPPrepagoFlapSTRIPE(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error Forma de Pago Prepago Solo Terminal STRIPE - appium":
                    new PrepagoNuevoNumeroFlap().ErrorPagoPrepagoFlapSTRIPE(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                
                case "Prepago Solo Terminal Mercado Pago - appium":
                    new PrepagoNuevoNumeroFlap().PrepagoMercadoPago(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error CP Prepago Solo Terminal Mercado Pago - appium":
                    new PrepagoNuevoNumeroFlap().ErrorCPPrepagoMercadoPago(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error Pago Prepago Solo Terminal Mercado Pago - appium":
                    new PrepagoNuevoNumeroFlap().ErrorPagoPrepagoMercadoPago(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Prepago Solo Terminal Oxxo - appium":
                    new PrepagoNuevoNumeroFlap().PrepagoSoloTerminalOxxo(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Prepago Solo Terminal Mercado Pago Pro - appium":
                    new PrepagoNuevoNumeroFlap().PrepagoSoloTerminalMPP(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error CP Prepago Solo Terminal Oxxo - appium":
                    new PrepagoNuevoNumeroFlap().ErrorCPPrepagoSoloTerminalOxxo(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Prepago Solo SIM + Recarga + Terminal - appium":
                    new PrepagoNuevoNumeroFlap().PrepagoSoloSIMTerminalRecarga(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error CP Prepago Solo SIM + Recarga + Terminal - appium":
                    new PrepagoNuevoNumeroFlap().ErrorCPPrepagoSoloSIMTerminalRecarga(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error Pago Prepago Solo SIM + Recarga + Terminal - appium":
                    new PrepagoNuevoNumeroFlap().ErrorPagoPrepagoSoloSIMTerminalRecarga(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Prepago Stripe+mpp+mpe - appium":
                    new PrepagoNuevoNumeroFlap().StripMppMpe(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error Datos Prepago Stripe+mpp+mpe - appium":
                    new PrepagoNuevoNumeroFlap().ErrorDatosStripMppMpe(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error CP Prepago Stripe+mpp+mpe - appium":
                    new PrepagoNuevoNumeroFlap().ErrorCPStripMppMpe(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error Pago Prepago Stripe+mpp+mpe - appium":
                    new PrepagoNuevoNumeroFlap().ErrorPagoStripMppMpe(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Portabilidad Pospago solo SIM + terminal Liferay - appium":
                    new PortabilidadPospago().PortabilidadPospagosoloSIMterminalLiferay(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Portabilidad Pospago solo SIM + terminal - appium":
                    new PortabilidadPospago().PortabilidadPospagosoloSIMterminal(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error en DN Portabilidad Pospago solo SIM + terminal - appium":
                    new PortabilidadPospago().ErrorDNPortabilidadPospagosoloSIMterminal(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error INE Portabilidad Pospago solo SIM + terminal - appium":
                    new PortabilidadPospago().ErrorINEPortabilidadPospagosoloSIMterminal(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error CP Portabilidad Pospago solo SIM + terminal - appium":
                    new PortabilidadPospago().ErrorCPPortabilidadPospagosoloSIMterminal(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error NIP CURP y fecha de ventana - appium":
                    new PortabilidadPospago().ErroresNIPPortabilidadPospagosoloSIMterminal(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Renovación Total a Pagar $0 - appium":
                    new Renovaciones().RenovaciónTotalPagarCero(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Pospago SIM + Terminal - appium":
                    new PospagoNuevoNumeroFlap().PospagoSIMTerminal(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error INE Pospago SIM + Terminal - appium":
                    new PospagoNuevoNumeroFlap().ErrorINEPospagoSIMTerminal(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error CP Pospago SIM + Terminal - appium":
                    new PospagoNuevoNumeroFlap().ErrorCPPospagoSIMTerminal(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error Forma de Pago Pospago SIM + Terminal - appium":
                    new PospagoNuevoNumeroFlap().ErrorPagoPospagoSIMTerminal(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Pospago Liferay - appium":
                    new PospagoNuevoNumeroFlap().PospagoLiferay(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error INE Pospago Liferay - appium":
                    new PospagoNuevoNumeroFlap().ErrorINEPospagoLiferay(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error CP Pospago Liferay - appium":
                    new PospagoNuevoNumeroFlap().ErrorCPPospagoLiferay(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error Pago Pospago Liferay - appium":
                    new PospagoNuevoNumeroFlap().ErrorPagoPospagoLiferay(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Pospago solo SIM Adiciones - appium":
                    new PospagoNuevoNumeroFlap().PospagoAdiciones(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error CP Pospago solo SIM Adiciones - appium":
                    new PospagoNuevoNumeroFlap().ErrorCPPospagoAdiciones(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error Pago Pospago solo SIM Adiciones - appium":
                    new PospagoNuevoNumeroFlap().ErrorPagoPospagoAdiciones(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Recarga con montos - Liferay - appium":
                    new RecargaPublicaMercadoPP().recargamontoLiferay(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Recarga con paquetes - Liferay - appium":
                    new RecargaPublicaMercadoPP().recargapaquetesLiferay(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Recarga con montos Mercado Pago - appium":
                    new RecargaPublicaMercadoPP().recargamontos(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Recarga con paquetes Mercado Pago - appium":
                    new RecargaPublicaMercadoPP().recargapaquetes(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Migración Caribu NBA montos - appium":
                    new RecargaPublicaMercadoPP().MigracionNBA(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Migración Caribu NBA paquetes - appium":
                    new RecargaPublicaMercadoPP().MigracionNBA(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Renovación Liferay - appium":
                    new Renovaciones().RenovacionLiferay(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Renovación Total a Pagar $0 Liferay - appium":
                    new Renovaciones().RenovacionCeroLiferay(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error INE Renovación Liferay - appium":
                    new Renovaciones().ErrorINERenovacionLiferay(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Error CP Renovación Liferay - appium":
                    new Renovaciones().ErrorCPRenovacionLiferay(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Pospago SOHO Fianza $0 Envio a CAC - appium":
                    new PospagoNuevoNumeroFlap().PospagoSOHOFianza0(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Prepago Flap (STRIPE) Solo Terminal":
                    new PrepagoFlapSTRIPE(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Prepago Flap (OXXO) Solo Terminal":
                    new PrepagoFlapOXXO(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Pospago SOHO solo SIM":
                    new PospagoSOHOsoloSIM(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Añade + lineas Pospago":
                    new AñadeMasLineasPospago(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                /* no funciona boton validar identidad
                case "Renovación Total a Pagar $0":
                    new RenovacionTotalPagarCero(reporte,SelenElem,OPG,WebDrive,Caso);
                break;
                */
                case "Renovación Total a Pagar $0":
                    new RenovacionTotalPagarCero(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Pospago solo SIM":
                    new PospagoSoloSIM(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Pospago solo SIM Adiciones":
                    new PospagoSIMAdiciones(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Pospago solo SIM + Terminal":
                    new PospagoSIMTerminal(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error de Codigo Postal Pospago":
                    new ErrorCodigoPostalPospago(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error en la forma de pago Pospago":
                    new ErrorformaDePagoPospago(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Prepago Solo SIM + Recarga":
                    new PrepagoSoloSIMRecarga(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Prepago Solo SIM + Terminal + Recarga":
                    new PrepagoSoloSIMTerminalRecarga(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Prepago Terminal - Flap":
                    new PrepagoTerminalFlap(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error de Codigo Postal Prepago":
                    new ErrorCodigoPostalPrepago(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error de RFC Prepago":
                    new ErrorRFCPrepago(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error en la forma de pago Prepago":
                    new ErrorPagoPrepago(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Migración Solo SIM":
                    new MigracionSoloSIM(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                /* no finalizado por cambio de plan 
                case "Migración Solo SIM + Terminal":
                    new MigracionSoloSIMTerminal(reporte,SelenElem,OPG,WebDrive,Caso);
                break;
                */
                
                case "Error en el DN Migración":
                    new ErrorDNMigración(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                
                case "Error en la forma de pago Migración":
                    new ErrorFormaPagoMigración(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Portabilidad Pospago Solo SIM":
                    new PortabilidadPospagoSoloSIM (reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                
                case "Error en el DN Portabilidad Pospago":
                    new ErrorDNPortabilidadPospago(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                
                case "Error en la forma de Pago Portabilidad Pospago":
                    new ErrorFormaPagoPortabilidadPospago(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error de Codigo Postal Portabilidad Pospago":
                    new ErrorCodigoPostalPortabilidadPospago(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error en los 18 digitos de INE Portabilidad Pospago":
                    new ErrorINEPortabilidadPospago(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Portabilidad Pospago SIM + Terminal":
                    new PortabilidadPospagoSIMmasTerminal(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error DN Portabilidad Pospago SIM + Terminal":
                    new ErrorDNPortabilidadPospagoSIMmasTerminal(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error INE Portabilidad Pospago SIM + Terminal":
                    new ErrorINEPortabilidadPospagoSIMmasTerminal(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error CP Portabilidad Pospago SIM + Terminal":
                    new ErrorCPPortabilidadPospagoSIMmasTerminal(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error Pago Portabilidad Pospago SIM + Terminal":
                    new ErrorPagoPortabilidadPospagoSIMmasTerminal(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                /* Se teduvo el plan y quedo incncluso
                case "Portabilidad Prepago Solo SIM + Recarga":
                    new PortabilidadPrepagoSoloSIMRecarga(reporte,SelenElem,OPG,WebDrive,Caso);
                break;
                */
                
                //////////// PLAN JUNIO  //////////////////
                
                case "Solo Terminal - Mercado Pago":
                    new SoloTerminalMercadoPago(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error codigo postal Solo Terminal - Mercado Pago":
                    new ErrorCPSoloTerminalMercadoPago(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error en Forma de Pago Solo Terminal - Mercado Pago":
                    new ErrorPagoSoloTerminalMercadoPago(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Solo Terminal – Stripe":
                    new SoloTerminalStripe(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error codigo postal Solo Terminal - Stripe":
                    new ErrorCPSoloTerminalStripe(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error en Forma de Pago Solo Terminal - Stripe":
                    new ErrorPagoSoloTerminalStripe(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                
                case "Portabilidad Prepago Terminal + SIM Con Recarga":
                    new PortabilidadPrepagoTerminalSIMConRecarga(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error DN Portabilidad Prepago Terminal + SIM Con Recarga":
                    new ErrorDNPortabilidadPrepagoTerminalSIMConRecarga(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error CP Portabilidad Prepago Terminal + SIM Con Recarga":
                    new ErrorCPPortabilidadPrepagoTerminalSIMConRecarga(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Pospago SIM + Terminal":
                    new PospagoSIMmasTerminal(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error en Clave de Elector Pospago SIM + Terminal":
                    new ErrorElectorPospagoSIMmasTerminal(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error en Código Postal Pospago SIM + Terminal":
                    new ErrorCPPospagoSIMmasTerminal(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error en Forma de Pago Pospago SIM + Terminal":
                    new ErrorPagoPospagoSIMmasTerminal(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Pospago SOHO solo SIM CON LOGIN":
                    new PospagoSOHOsoloSIMCONLOGIN(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error RFC Pospago SOHO solo SIM CON LOGIN":
                    new ErrorRFCPospagoSOHOsoloSIMCONLOGIN(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error CP Pospago SOHO solo SIM CON LOGIN":
                    new ErrorCPPospagoSOHOsoloSIMCONLOGIN(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error forma de pago Pospago SOHO solo SIM CON LOGIN":
                    new ErrorPagoPospagoSOHOsoloSIMCONLOGIN(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Adiciones con login":
                    new Adiciones().AdicionesLogin(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error CP Adiciones con login":
                    new Adiciones().ErrorCP(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error Pago Adiciones con login":
                    new Adiciones().ErrorPago(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                
                case "Migraciones SIM + Terminal":
                    new MigracionSoloSIMTerminal().Migracion(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error DN Migraciones SIM + Terminal":
                    new MigracionSoloSIMTerminal().ErroDN(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error INE Migraciones SIM + Terminal":
                    new MigracionSoloSIMTerminal().ErrorINE(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error CP Migraciones SIM + Terminal":
                    new MigracionSoloSIMTerminal().ErrorCP(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error forma de Pago Migraciones SIM + Terminal":
                    new MigracionSoloSIMTerminal().ErrorPago(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Recarga con montos":
                    new RecargaFlap().montosWeb(reporte,SelenElem,OPG,WebDrive,Caso);
                break;
                
                case "Recarga con paquetes":
                    new RecargaFlap().paquetesWeb(reporte,SelenElem,OPG,WebDrive,Caso);
                break;
                
                case "Renovacion":
                    new Renovaciones().RenovacionWeb(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error en DN Renovacion":
                    new Renovaciones().ErrorDN(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Error en INE Renovacion":
                    new Renovaciones().ErrorINE(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Pospago SOHO solo SIM – Fianza 200 – Liferay":
                    new PospagoSOHOsoloSIMLiferay().PospagoSOHOsoloSIMFianza200Liferay(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Pospago SOHO Fianza 0 Liferay - appium":
                    new PospagoSOHOsoloSIMLiferay().PospagoSOHOsoloSIMFianza0Liferay(reporte,OPG,SelenElem,WebDrive,Url,(new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Renovaciones R3 Efectivo":
                    new Renovaciones().RenoR3Efectivo(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Renovaciones R3 Tarjeta":
                    new Renovaciones().RenoR3Tarjeta(reporte,SelenElem,OPG,WebDrive,Url,Caso);
                break;
                
                case "Solo Terminal FLAP Vitrina - Descuento directo + Cupon - appium":
                    new SoloTerminalVitrina().SoloTerminalDescuentoCupon(reporte, SelenElem, OPG, WebDrive,Url, (new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Solo Terminal FLAP Vitrina - Cupon - appium":
                    new SoloTerminalVitrina().SoloTerminalCupon(reporte, SelenElem, OPG, WebDrive,Url, (new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Solo Terminal FLAP Vitrina - Descuento directo - appium":
                    new SoloTerminalVitrina().SoloTerminalDescuento(reporte, SelenElem, OPG, WebDrive,Url, (new DeviceActions(ap.getDriver())), Caso);
                break;
                
                case "Solo Terminal FLAP Vitrina - Precio Costo - Cupon - appium":
                    new SoloTerminalVitrina().SoloTerminalPrecioCupon(reporte, SelenElem, OPG, WebDrive,Url, (new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Solo Terminal FLAP Vitrina - Precio Costo - Descuento directo - appium":
                    new SoloTerminalVitrina().SoloTerminalPrecioDescuento(reporte, SelenElem, OPG, WebDrive,Url, (new DeviceActions(ap.getDriver())), Caso);
                break;
                
                case "Renovacion R3 Tarjeta Asociada - appium":
                    new Renovaciones().RenovacionR3TarjetaMovil(reporte, SelenElem, OPG, WebDrive, Url, (new DeviceActions(ap.getDriver())), Caso);
                break;
                
                case "Renovacion R3 Tokenizacion Liferay - appium":
                    new Renovaciones().RenovacionR3TokenMovil(reporte, SelenElem, OPG, WebDrive,Url, (new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Adiciones Liferay - Fianza $200 - appium":
                    new Adiciones().AdicionesFianza200(reporte, SelenElem, OPG, WebDrive,Url, (new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Adiciones Liferay - Fianza $0 - appium":
                    new Adiciones().AdicionesFianza0(reporte, SelenElem, OPG, WebDrive,Url, (new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Solo Terminal Flap - Cupon - appium":
                    new SoloTerminalFlap().SoloTerminalCupon(reporte, SelenElem, OPG, WebDrive,Url, (new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Solo Terminal Flap - Descuento directo - appium":
                    new SoloTerminalFlap().SoloTerminalDescuento(reporte, SelenElem, OPG, WebDrive,Url, (new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Solo Terminal Flap - Descuento directo + Cupon - appium":
                    new SoloTerminalFlap().SoloTerminalDescuentoCupon(reporte, SelenElem, OPG, WebDrive,Url, (new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Solo Terminal Flap - Precio Costo - Cupon - appium":
                    new SoloTerminalFlap().SoloTerminalPrecioCupon(reporte, SelenElem, OPG, WebDrive,Url, (new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Solo Terminal Flap - Precio Costo - Descuento directo - appium":
                    new SoloTerminalFlap().SoloTerminalPrecioDescuento(reporte, SelenElem, OPG, WebDrive,Url, (new DeviceActions(ap.getDriver())),Caso);
                break;
                
                case "Solo Terminal - Mercado Pago - Cupon - appium":
                    new SoloTerminalMercadoPago().SoloTermialMercadoCupon(reporte, SelenElem, OPG, WebDrive,Url, (new DeviceActions(ap.getDriver())), Caso);
                break;
                
                case "Solo Terminal - Mercado Pago - Descuento directo - appium":
                    new SoloTerminalMercadoPago().SoloTerminalMercadoDescuento(reporte, SelenElem, OPG, WebDrive,Url, (new DeviceActions(ap.getDriver())),Caso);
                break;
  
                case "Solo Terminal - Mercado Pago - Descuento directo + Cupon - appium":
                    new SoloTerminalMercadoPago().SoloTerminalMercadoDescuentoCupon(reporte, SelenElem, OPG, WebDrive, Url,(new DeviceActions(ap.getDriver())), Caso);
                break;
                
                case "Solo Terminal - Mercado Pago - Precio Costo - Cupon - appium":
                    new SoloTerminalMercadoPago().SoloTerminalMercadoPrecioCupon(reporte, SelenElem, OPG, WebDrive, Url, (new DeviceActions(ap.getDriver())), Caso);
                break;    
                
                case "Solo Terminal - Mercado Pago - Precio Costo - Descuento directo - appium":
                    new SoloTerminalMercadoPago().SoloTerminalMercadoPrecioDescuento(reporte, SelenElem, OPG, WebDrive, Url, (new DeviceActions(ap.getDriver())), Caso);
                    
                case "Portabilidad Pospago con Terminal - Fianza $200 - appium":
                    new PortabilidadPospago().PortabilidadPospagoSIMterminal200(reporte, SelenElem, OPG, WebDrive, Url, (new DeviceActions(ap.getDriver())), Caso);
                break;
                
                case "Portabilidad refactor Liferay - Fianza $0 - appium":
                    new PortabilidadPospago().PortabilidadPospagosoloSIMRefactor(reporte, SelenElem, OPG, WebDrive, Url, (new DeviceActions(ap.getDriver())), Caso);
               break;
               
                case "Portabilidad Pospago Solo SIM Liferay - Fianza $0 - appium":
                    new PortabilidadPospago().PortabilidadPospagosoloSIMterminalLiferay0(reporte, SelenElem, OPG, WebDrive, Url, (new DeviceActions(ap.getDriver())), Caso);
                break;
                
                case "Portabilidad Pospago Solo SIM Liferay - Fianza $200 - appium":
                    new PortabilidadPospago().PortabilidadPospagosoloSIMterminalLiferay200(reporte, SelenElem, OPG, WebDrive, Url, (new DeviceActions(ap.getDriver())), Caso);
                break;
                
                case "Portabilidad Pospago Solo SIM - Liferay":
                    new PortabilidadPospago().PortaPosSoloSIMLiferay(reporte, SelenElem, OPG, WebDrive, Url, Caso);
                break;
                
                case "Portabilidad Pospago Solo SIM - Liferay Fianza $200":
                    new PortabilidadPospago().PortaPosLPSIMLiferay200(reporte, SelenElem, OPG, WebDrive, Url, Caso);
                break;
                
                default:
                    Resultado.setText("Caso no definido");
            }
            
            
        } catch (Exception ex) {
            //Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
            //System.out.println(ex.getLocalizedMessage());
            //reporte.writeError(ex.getMessage());            
            
            // Error generico
            String error = "Ejecute nuevamente, si el problema persiste contacte al area de desarrollo";
            // Lista de errores localizados
            if((ex.getLocalizedMessage().contains("no such element: Unable to locate element: {\"method\":\"css selector\",\"selector\":\"#redirect\\-mp\"}")) || (ex.getLocalizedMessage().contains("no such element: Unable to locate element: {\"method\":\"xpath\",\"selector\":\"//label[@for='redirect-mp']\"}")))
                error = "Error: La opción de Mercado Pago no esta activada";
            else if(ex.getLocalizedMessage().contains("no such element: Unable to locate element"))
                error = "El flujo o un identificador en la pagina han cambiado";
            
            Resultado.setText(error);
            
            reporte.writeError(error,OPG.ScreenShotElement()); 
            //reporte.writeError(error);
            
        } finally {
            reporte.Export();
        }

        if(jCheckBox2.isSelected())
        {
            if(reporte.getNamefile() != null)
                (new SystemA()).OpenFile(reporte.getNamefile());
        }
        
        /*if(Caso.contains("appium"))
        {
            if(device != null)
                device.quit();
    
            
            
            if(ap != null)
                ap.stopServer();
        }*/
    }//GEN-LAST:event_AceptarActionPerformed
    
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if(WebDrive != null)
                WebDrive.CloseWebDriver();  

            (new SystemA()).KillProccess("node.exe");
            //(new SystemA()).KillProccess("chromedriver.exe");
        System.exit(0);
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
       (new SystemA()).OpenFolder("A:\\Reportes");
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jCheckBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        String ruta = null;
        String Caso = ComboBox1.getSelectedItem().toString();
        if(Caso.contains("Recarga pública"))
            ruta = "A:\\DataEcommerce\\Recargas\\"+Caso+".csv"; 
        else if(Caso.equals("Arma tu plan"))
            ruta = "A:\\DataEcommerce\\Arma tu plan\\"+Caso+".csv";
        else if(Caso.equals("Arma tu pack"))
            ruta = "A:\\DataEcommerce\\Arma tu pack\\"+Caso+".csv";
        else if(Caso.equals("Prepago Terminal"))
            ruta = "A:\\DataEcommerce\\PrepagoTerminal\\"+Caso+".csv";
        else if(Caso.equals("Portabilidad pospago Sim"))
            ruta = "A:\\DataEcommerce\\Portabilidad Pospago Sim\\"+Caso+".csv";
        else if(Caso.equals("Migraciones FLAP"))
            ruta = "A:\\DataEcommerce\\Migraciones\\"+Caso+".csv";
        else if(Caso.equals("Renovaciones FLAP"))
            ruta = "A:\\DataEcommerce\\Renovaciones\\"+Caso+".csv";
        else if(Caso.equals("Prepago Flap (STRIPE) Solo Termina"))
            ruta = "A:\\DataEcommerce\\Prepago Flap (Stripe)\\"+Caso+".csv";
        else if(Caso.equals("Prepago Flap (OXXO) Solo Termina"))
            ruta = "A:\\DataEcommerce\\Prepago Flap (Oxxo)\\"+Caso+".csv";
        else if(Caso.equals("Pospago SOHO solo SIM"))
            ruta = "A:\\DataEcommerce\\Pospago SOHO solo SIM\\"+Caso+".csv";
        else if(Caso.equals("Añade + lineas Pospago"))
            ruta = "A:\\DataEcommerce\\Añade Mas Lineas Pospago\\"+Caso+".csv";
        else if(Caso.equals("Renovación Total a Pagar $0"))
            ruta = "A:\\DataEcommerce\\Renovación Total a Pagar $0\\"+Caso+".csv";
        else if(Caso.equals("Pospago solo SIM"))
            ruta = "A:\\DataEcommerce\\Pospago solo SIM\\"+Caso+".csv";
        else if(Caso.equals("Pospago solo SIM (SOHO) - appium"))
            ruta = "A:\\DataEcommerce\\Pospago solo SIM (SOHO) - appium\\"+Caso+".csv";
        else if(Caso.equals("Pospago solo SIM Adiciones"))
            ruta = "A:\\DataEcommerce\\Pospago solo SIM Adiciones\\"+Caso+".csv";
        else if(Caso.equals("Pospago solo SIM + Terminal"))
            ruta = "A:\\DataEcommerce\\Pospago solo SIM + Terminal\\"+Caso+".csv";
        else if(Caso.equals("Error de Codigo Postal Pospago"))
            ruta = "A:\\DataEcommerce\\Error de Codigo Postal Pospago\\"+Caso+".csv";
        else if(Caso.equals("Error en la forma de pago Pospago"))
            ruta = "A:\\DataEcommerce\\Error en la forma de pago Pospago\\"+Caso+".csv";
        else if(Caso.equals("Prepago Solo SIM + Recarga"))
            ruta = "A:\\DataEcommerce\\Prepago Solo SIM + Recarga\\"+Caso+".csv";
        else if(Caso.equals("Prepago Solo SIM + Terminal + Recarga"))
            ruta = "A:\\DataEcommerce\\Prepago Solo SIM + Terminal + Recarga\\"+Caso+".csv";
        else if(Caso.equals("Prepago Terminal - Flap"))
            ruta = "A:\\DataEcommerce\\Prepago Terminal - Flap\\"+Caso+".csv";
        else if(Caso.equals("Error de Codigo Postal Prepago"))
            ruta = "A:\\DataEcommerce\\Error de Codigo Postal Prepago\\"+Caso+".csv";
        else if(Caso.equals("Error de RFC Prepago"))
            ruta = "A:\\DataEcommerce\\Error de RFC Prepago\\"+Caso+".csv";
        else if(Caso.equals("Portabilidad Pospago Solo SIM"))
            ruta = "A:\\DataEcommerce\\Portabilidad Pospago Solo SIM\\"+Caso+".csv";
        else if(Caso.equals("Error en el DN Portabilidad Pospago"))
            ruta = "A:\\DataEcommerce\\Error en el DN Portabilidad Pospago\\"+Caso+".csv";
        else if(Caso.equals("Migración Solo SIM"))
            ruta = "A:\\DataEcommerce\\Migración Solo SIM\\"+Caso+".csv";
        else if(Caso.equals("Error en la forma de Pago Portabilidad Pospago"))
            ruta = "A:\\DataEcommerce\\Error en la forma de Pago Portabilidad Pospago\\"+Caso+".csv";
        else if(Caso.equals("Error de Codigo Postal Portabilidad Pospago"))
            ruta = "A:\\DataEcommerce\\Error de Codigo Postal Portabilidad Pospago\\"+Caso+".csv";
        else if(Caso.equals("Error en los 18 digitos de INE Portabilidad Pospago"))
            ruta = "A:\\DataEcommerce\\Error en los 18 digitos de INE Portabilidad Pospago\\"+Caso+".csv";
        else if(Caso.equals("Portabilidad Prepago Solo SIM + Recarga"))
            ruta = "A:\\DataEcommerce\\Portabilidad Prepago Solo SIM + Recarga\\"+Caso+".csv";
        else if(Caso.equals("Solo Terminal - Mercado Pago"))
            ruta = "A:\\DataEcommerce\\Solo Terminal - Mercado Pago\\"+Caso+".csv";
        else if(Caso.equals("Error codigo postal Solo Terminal - Mercado Pago"))
            ruta = "A:\\DataEcommerce\\Error codigo postal Solo Terminal - Mercado Pago\\"+Caso+".csv";
        else if(Caso.equals("Error en Forma de Pago Solo Terminal - Mercado Pago"))
            ruta = "A:\\DataEcommerce\\Error en Forma de Pago Solo Terminal - Mercado Pago\\"+Caso+".csv";
        else if(Caso.equals("Solo Terminal – Stripe"))
            ruta = "A:\\DataEcommerce\\Solo Terminal – Stripe\\"+Caso+".csv";
        else if(Caso.equals("Error codigo postal Solo Terminal - Stripe"))
            ruta = "A:\\DataEcommerce\\Error codigo postal Solo Terminal - Stripe\\"+Caso+".csv";
        else if(Caso.equals("Error codigo postal Solo Terminal - Stripe"))
            ruta = "A:\\DataEcommerce\\Error en Forma de Pago Solo Terminal - Stripe\\"+Caso+".csv";
        else if(Caso.equals("Portabilidad Prepago Terminal + SIM Con Recarga"))
            ruta = "A:\\DataEcommerce\\Portabilidad Prepago Terminal + SIM Con Recarga\\"+Caso+".csv";
        else if(Caso.equals("Pospago SIM + Terminal"))
            ruta = "A:\\DataEcommerce\\Pospago SIM + Terminal\\"+Caso+".csv";
        else if(Caso.equals("Error en Clave de Elector Pospago SIM + Terminal"))
            ruta = "A:\\DataEcommerce\\Error en Clave de Elector Pospago SIM + Terminal\\"+Caso+".csv";
        else if(Caso.equals("Error en Código Postal Pospago SIM + Terminal"))
            ruta = "A:\\DataEcommerce\\Error en Código Postal Pospago SIM + Terminal\\"+Caso+".csv";
        else if(Caso.equals("Error en Forma de Pago Pospago SIM + Terminal"))
            ruta = "A:\\DataEcommerce\\Error en Forma de Pago Pospago SIM + Terminal\\"+Caso+".csv";
        else if(Caso.equals("Pospago SOHO solo SIM CON LOGIN"))
            ruta = "A:\\DataEcommerce\\Pospago SOHO solo SIM CON LOGIN\\"+Caso+".csv";
        else if(Caso.equals("Error RFC Pospago SOHO solo SIM CON LOGIN"))
            ruta = "A:\\DataEcommerce\\Error RFC Pospago SOHO solo SIM CON LOGIN\\"+Caso+".csv";
        else if(Caso.equals("Error CP Pospago SOHO solo SIM CON LOGIN"))
            ruta = "A:\\DataEcommerce\\Error CP Pospago SOHO solo SIM CON LOGIN\\"+Caso+".csv";
        else if(Caso.equals("Error forma de pago Pospago SOHO solo SIM CON LOGIN"))
            ruta = "A:\\DataEcommerce\\Error forma de pago Pospago SOHO solo SIM CON LOGIN\\"+Caso+".csv";
        else if(Caso.equals("Error DN Portabilidad Prepago Terminal + SIM Con Recarga"))
            ruta = "A:\\DataEcommerce\\Error DN Portabilidad Prepago Terminal + SIM Con Recarga\\"+Caso+".csv";
        else if(Caso.equals("Error CP Portabilidad Prepago Terminal + SIM Con Recarga"))
            ruta = "A:\\DataEcommerce\\Error CP Portabilidad Prepago Terminal + SIM Con Recarga\\"+Caso+".csv";
        else if(Caso.equals("Portabilidad Pospago SIM + Terminal"))
            ruta = "A:\\DataEcommerce\\Portabilidad Pospago SIM + Terminal\\"+Caso+".csv";
        else if(Caso.equals("Error DN Portabilidad Pospago SIM + Terminal"))
            ruta = "A:\\DataEcommerce\\Error DN Portabilidad Pospago SIM + Terminal\\"+Caso+".csv";
        else if(Caso.equals("Error INE Portabilidad Pospago SIM + Terminal"))
            ruta = "A:\\DataEcommerce\\Error INE Portabilidad Pospago SIM + Terminal\\"+Caso+".csv";
        else if(Caso.equals("Error CP Portabilidad Pospago SIM + Terminal"))
            ruta = "A:\\DataEcommerce\\Error CP Portabilidad Pospago SIM + Terminal\\"+Caso+".csv";
        else if(Caso.equals("Error Pago Portabilidad Pospago SIM + Terminal"))
            ruta = "A:\\DataEcommerce\\Error Pago Portabilidad Pospago SIM + Terminal\\"+Caso+".csv";
        else if(Caso.equals("Migraciones SIM + Terminal"))
            ruta = "A:\\DataEcommerce\\Migraciones SIM + Terminal\\"+Caso+".csv";
        else if(Caso.equals("Error DN Migraciones SIM + Terminal"))
            ruta = "A:\\DataEcommerce\\Migraciones SIM + Terminal\\"+Caso+".csv";
        else if(Caso.equals("Error INE Migraciones SIM + Terminal"))
            ruta = "A:\\DataEcommerce\\Migraciones SIM + Terminal\\"+Caso+".csv";
        else if(Caso.equals("Error CP Migraciones SIM + Terminal"))
            ruta = "A:\\DataEcommerce\\Migraciones SIM + Terminal\\"+Caso+".csv";
        else if(Caso.equals("Error forma de Pago Migraciones SIM + Terminal"))
            ruta = "A:\\DataEcommerce\\Migraciones SIM + Terminal\\"+Caso+".csv";
        else if(Caso.equals("Recarga con montos"))
            ruta = "A:\\DataEcommerce\\Recargas\\"+Caso+".csv";
        else if(Caso.equals("Recarga con paquetes"))
            ruta = "A:\\DataEcommerce\\Recargas\\"+Caso+".csv";
        else if(Caso.equals("Renovacion"))
            ruta = "A:\\DataEcommerce\\Renovaciones\\"+Caso+".csv";
        else if(Caso.equals("Error en DN Renovacion"))
            ruta = "A:\\DataEcommerce\\Renovaciones\\"+Caso+".csv";
        else if(Caso.equals("Error en INE Renovacion"))
            ruta = "A:\\DataEcommerce\\Renovaciones\\"+Caso+".csv";
        else if(Caso.equals("Adiciones con login"))
            ruta = "A:\\DataEcommerce\\Adiciones\\"+Caso+".csv";
        else if(Caso.equals("Error CP Adiciones con login"))
            ruta = "A:\\DataEcommerce\\Adiciones\\"+Caso+".csv";
        else if(Caso.equals("Error Pago Adiciones con login"))
            ruta = "A:\\DataEcommerce\\Adiciones\\"+Caso+".csv";
        else if(Caso.equals("Portabilidad Prepago Terminal + Recarga Liferay - appium"))
            ruta = "A:\\DataEcommerce\\Portabilidad Prepago Flap\\"+Caso+".csv";
        else if(Caso.equals("Portabilidad Prepago Terminal + SIM Con Recarga - appium"))
            ruta = "A:\\DataEcommerce\\Portabilidad Prepago Flap\\"+Caso+".csv";
        else if(Caso.equals("Error DN Portabilidad Prepago Terminal + SIM Con Recarga - appium"))
            ruta = "A:\\DataEcommerce\\Portabilidad Prepago Flap\\"+Caso+".csv";
        else if(Caso.equals("Error RFC Portabilidad Prepago Terminal + SIM Con Recarga - appium"))
            ruta = "A:\\DataEcommerce\\Portabilidad Prepago Flap\\"+Caso+".csv";
        else if(Caso.equals("Error CP Portabilidad Prepago Terminal + SIM Con Recarga - appium"))
            ruta = "A:\\DataEcommerce\\Portabilidad Prepago Flap\\"+Caso+".csv";
        else if(Caso.equals("Error NIP Portabilidad Prepago Terminal + SIM Con Recarga - appium"))
            ruta = "A:\\DataEcommerce\\Portabilidad Prepago Flap\\"+Caso+".csv";
        else if(Caso.equals("Migraciones SIM + Terminal - appium"))
            ruta = "A:\\DataEcommerce\\Migraciones Flap\\"+Caso+".csv";
        else if(Caso.equals("Migraciones SIM + Terminal - Liferay - appium"))
            ruta = "A:\\DataEcommerce\\Migraciones Flap\\"+Caso+".csv";
        else if(Caso.equals("Error DN Migraciones SIM + Terminal - Liferay - appium"))
            ruta = "A:\\DataEcommerce\\Migraciones Flap\\"+Caso+".csv";
        else if(Caso.equals("Error INE Migraciones SIM + Terminal - Liferay - appium"))
            ruta = "A:\\DataEcommerce\\Migraciones Flap\\"+Caso+".csv";
        else if(Caso.equals("Error CP Migraciones SIM + Terminal - Liferay - appium"))
            ruta = "A:\\DataEcommerce\\Migraciones Flap\\"+Caso+".csv";
        else if(Caso.equals("Migración Caribu - Liferay - appium"))
            ruta = "A:\\DataEcommerce\\Migraciones Flap\\"+Caso+".csv";
        else if(Caso.equals("Migracion Caribu - appium"))
            ruta = "A:\\DataEcommerce\\Migraciones Flap\\"+Caso+".csv";
        else if(Caso.equals("Error DN Migracion Caribu - appium"))
            ruta = "A:\\DataEcommerce\\Migraciones Flap\\"+Caso+".csv";
        else if(Caso.equals("Error RFC Migracion Caribu - appium"))
            ruta = "A:\\DataEcommerce\\Migraciones Flap\\"+Caso+".csv";
        else if(Caso.equals("Error INE Migracion Caribu - appium"))
            ruta = "A:\\DataEcommerce\\Migraciones Flap\\"+Caso+".csv";
        else if(Caso.equals("Prepago Solo Terminal STRIPE - appium"))
            ruta = "A:\\DataEcommerce\\Prepago con nuevo numero – Flap\\"+Caso+".csv";
        else if(Caso.equals("Error CP Prepago Solo Terminal STRIPE - appium"))
            ruta = "A:\\DataEcommerce\\Prepago con nuevo numero – Flap\\"+Caso+".csv";
        else if(Caso.equals("Error Forma de Pago Prepago Solo Terminal STRIPE - appium"))
            ruta = "A:\\DataEcommerce\\Prepago con nuevo numero – Flap\\"+Caso+".csv";
        else if(Caso.equals("Prepago Solo Terminal Mercado Pago - appium"))
            ruta = "A:\\DataEcommerce\\Prepago con nuevo numero – Flap\\"+Caso+".csv";
        else if(Caso.equals("Error CP Prepago Solo Terminal Mercado Pago - appium"))
            ruta = "A:\\DataEcommerce\\Prepago con nuevo numero – Flap\\"+Caso+".csv";
        else if(Caso.equals("Error Pago Prepago Solo Terminal Mercado Pago - appium"))
            ruta = "A:\\DataEcommerce\\Prepago con nuevo numero – Flap\\"+Caso+".csv";
        else if(Caso.equals("Prepago Solo Terminal Oxxo - appium"))
            ruta = "A:\\DataEcommerce\\Prepago con nuevo numero – Flap\\"+Caso+".csv";
        else if(Caso.equals("Error CP Prepago Solo Terminal Oxxo - appium"))
            ruta = "A:\\DataEcommerce\\Prepago con nuevo numero – Flap\\"+Caso+".csv";
        else if(Caso.equals("Prepago Solo SIM + Recarga + Terminal - appium"))
            ruta = "A:\\DataEcommerce\\Prepago con nuevo numero – Flap\\"+Caso+".csv";
        else if(Caso.equals("Error CP Prepago Solo SIM + Recarga + Terminal - appium"))
            ruta = "A:\\DataEcommerce\\Prepago con nuevo numero – Flap\\"+Caso+".csv";
        else if(Caso.equals("Error Pago Prepago Solo SIM + Recarga + Terminal - appium"))
            ruta = "A:\\DataEcommerce\\Prepago con nuevo numero – Flap\\"+Caso+".csv";
        else if(Caso.equals("Prepago Stripe+mpp+mpe- appium"))
            ruta = "A:\\DataEcommerce\\Prepago con nuevo numero – Flap\\"+Caso+".csv";
        else if(Caso.equals("Error Datos Prepago Stripe+mpp+mpe- appium"))
            ruta = "A:\\DataEcommerce\\Prepago con nuevo numero – Flap\\"+Caso+".csv";
        else if(Caso.equals("Error CP Prepago Stripe+mpp+mpe- appium"))
            ruta = "A:\\DataEcommerce\\Prepago con nuevo numero – Flap\\"+Caso+".csv";
        else if(Caso.equals("Error Pago Prepago Stripe+mpp+mpe- appium"))
            ruta = "A:\\DataEcommerce\\Prepago con nuevo numero – Flap\\"+Caso+".csv";
        else if(Caso.equals("Portabilidad Pospago solo SIM + terminal - appium"))
            ruta = "A:\\DataEcommerce\\Portabilidad Pospago\\"+Caso+".csv";
        else if(Caso.equals("Portabilidad Pospago solo SIM + terminal Liferay - appium"))
            ruta = "A:\\DataEcommerce\\Portabilidad Pospago\\"+Caso+".csv";
        else if(Caso.equals("Error en DN Portabilidad Pospago solo SIM + terminal - appium"))
            ruta = "A:\\DataEcommerce\\Portabilidad Pospago\\"+Caso+".csv";
        else if(Caso.equals("Error INE Portabilidad Pospago solo SIM + terminal - appium"))
            ruta = "A:\\DataEcommerce\\Portabilidad Pospago\\"+Caso+".csv";
        else if(Caso.equals("Error CP Portabilidad Pospago solo SIM + terminal - appium"))
            ruta = "A:\\DataEcommerce\\Portabilidad Pospago\\"+Caso+".csv";
        else if(Caso.equals("Error CP Portabilidad Pospago solo SIM + terminal - appium"))
            ruta = "A:\\DataEcommerce\\Portabilidad Pospago\\"+Caso+".csv";
        else if(Caso.equals("Error NIP CURP y fecha de ventana - appium"))
            ruta = "A:\\DataEcommerce\\Portabilidad Pospago\\"+Caso+".csv";
        else if(Caso.equals("Renovación Total a Pagar $0 - appium"))
            ruta = "A:\\DataEcommerce\\Renovaciones\\"+Caso+".csv";
        else if(Caso.equals("Pospago SIM + Terminal - appium"))
            ruta = "A:\\DataEcommerce\\POSPAGO CON NUEVO NUMERO FLAP\\"+Caso+".csv";
        else if(Caso.equals("Error INE Pospago SIM + Terminal - appium"))
            ruta = "A:\\DataEcommerce\\POSPAGO CON NUEVO NUMERO FLAP\\"+Caso+".csv";
        else if(Caso.equals("Error CP Pospago SIM + Terminal - appium"))
            ruta = "A:\\DataEcommerce\\POSPAGO CON NUEVO NUMERO FLAP\\"+Caso+".csv";
        else if(Caso.equals("Error Forma de Pago Pospago SIM + Terminal - appium"))
            ruta = "A:\\DataEcommerce\\POSPAGO CON NUEVO NUMERO FLAP\\"+Caso+".csv";
        else if(Caso.equals("Pospago Liferay - appium"))
            ruta = "A:\\DataEcommerce\\POSPAGO CON NUEVO NUMERO FLAP\\"+Caso+".csv";
        else if(Caso.equals("Error INE Pospago Liferay - appium"))
            ruta = "A:\\DataEcommerce\\POSPAGO CON NUEVO NUMERO FLAP\\"+Caso+".csv";
        else if(Caso.equals("Error CP Pospago Liferay - appium"))
            ruta = "A:\\DataEcommerce\\POSPAGO CON NUEVO NUMERO FLAP\\"+Caso+".csv";
        else if(Caso.equals("Error Pago Pospago Liferay - appium"))
            ruta = "A:\\DataEcommerce\\POSPAGO CON NUEVO NUMERO FLAP\\"+Caso+".csv";
        else if(Caso.equals("Recarga con montos - Liferay - appium"))
            ruta = "A:\\DataEcommerce\\Recargas mercado pago pro\\"+Caso+".csv";
        else if(Caso.equals("Recarga con paquetes - Liferay - appium"))
            ruta = "A:\\DataEcommerce\\Recargas mercado pago pro\\"+Caso+".csv";
        else if(Caso.equals("Recarga con montos Mercado Pago - appium"))
            ruta = "A:\\DataEcommerce\\Recargas mercado pago pro\\"+Caso+".csv";
        else if(Caso.equals("Recarga con paquetes Mercado Pago - appium"))
            ruta = "A:\\DataEcommerce\\Recargas mercado pago pro\\"+Caso+".csv";
        else if(Caso.equals("Renovación Liferay - appium"))
            ruta = "A:\\DataEcommerce\\Renovaciones\\"+Caso+".csv";
        else if(Caso.equals("Renovación Total a Pagar $0 Liferay - appium"))
            ruta = "A:\\DataEcommerce\\Renovaciones\\"+Caso+".csv";
        else if(Caso.equals("Error INE Renovación Liferay - appium"))
            ruta = "A:\\DataEcommerce\\Renovaciones\\"+Caso+".csv";
        else if(Caso.equals("Error CP Renovación Liferay - appium"))
            ruta = "A:\\DataEcommerce\\Renovaciones\\"+Caso+".csv";
        else if(Caso.equals("Migración Caribu NBA montos - appium"))
            ruta = "A:\\DataEcommerce\\Recargas mercado pago pro\\"+Caso+".csv";
        else if(Caso.equals("Migración Caribu NBA paquetes - appium"))
            ruta = "A:\\DataEcommerce\\Recargas mercado pago pro\\"+Caso+".csv";
        else if(Caso.equals("Pospago solo SIM Adiciones - appium"))
            ruta = "A:\\DataEcommerce\\POSPAGO CON NUEVO NUMERO FLAP\\"+Caso+".csv";
        else if(Caso.equals("Error CP Pospago solo SIM Adiciones - appium"))
            ruta = "A:\\DataEcommerce\\POSPAGO CON NUEVO NUMERO FLAP\\"+Caso+".csv";
        else if(Caso.equals("Error Pago Pospago solo SIM Adiciones - appium"))
            ruta = "A:\\DataEcommerce\\POSPAGO CON NUEVO NUMERO FLAP\\"+Caso+".csv";
        else if(Caso.equals("Pospago SOHO solo SIM – Fianza 200 – Liferay"))
            ruta = "A:\\DataEcommerce\\Pospago SOHO Liferay\\"+Caso+".csv";
        else if(Caso.equals("Renovaciones R3 Efectivo"))
            ruta = "A:\\DataEcommerce\\Renovaciones R3\\"+Caso+".csv";
        else if(Caso.equals("Renovaciones R3 Tarjeta"))
            ruta = "A:\\DataEcommerce\\Renovaciones R3\\"+Caso+".csv";
        else if(Caso.equals("Solo Terminal FLAP Vitrina - Descuento directo + Cupon - appium"))
            ruta = "A:\\DataEcommerce\\Solo Terminal - Flap - Vitrina\\"+Caso+".csv";
        else if(Caso.equals("Solo Terminal FLAP Vitrina - Cupon - appium"))
            ruta = "A:\\DataEcommerce\\Solo Terminal - Flap - Vitrina\\"+Caso+".csv";
        else if(Caso.equals("Solo Terminal FLAP Vitrina - Descuento directo - - appium"))
            ruta = "A:\\DataEcommerce\\Solo Terminal - Flap - Vitrina\\"+Caso+".csv";
        else if(Caso.equals("Solo Terminal FLAP Vitrina - Precio Costo - Cupon - appium"))
            ruta = "A:\\DataEcommerce\\Solo Terminal - Flap - Vitrina\\"+Caso+".csv";
        else if(Caso.equals("Renovacion R3 Tarjeta Asociada - appium"))
            ruta = "A:\\DataEcommerce\\Renovaciones R3\\"+Caso+".csv";
        else if(Caso.equals("Renovacion R3 Tokenizacion Liferay - appium"))
            ruta = "A:\\DataEcommerce\\Renovaciones R3\\"+Caso+".csv";
        else if(Caso.equals("Adiciones Liferay - Fianza $200 - appium"))
            ruta = "A:\\DataEcommerce\\Adiciones\\"+Caso+".csv";
        else if(Caso.equals("Adiciones Liferay - Fianza $0 - appium"))
            ruta = "A:\\DataEcommerce\\Adiciones\\"+Caso+".csv";
        else if(Caso.equals("Solo Terminal Flap - Cupon - appium"))
            ruta = "A:\\DataEcommerce\\Solo Terminal - Flap\\"+Caso+".csv";
        else if(Caso.equals("Solo Terminal Flap - Descuento directo - appium"))
            ruta = "A:\\DataEcommerce\\Solo Terminal - Flap\\"+Caso+".csv";
        else if(Caso.equals("Solo Terminal Flap - Descuento directo + Cupon - appium"))
            ruta = "A:\\DataEcommerce\\Solo Terminal - Flap\\"+Caso+".csv";
        else if(Caso.equals("Solo Terminal Flap - Precio Costo - Cupon - appium"))
            ruta = "A:\\DataEcommerce\\Solo Terminal - Flap\\"+Caso+".csv";
        else if(Caso.equals("Solo Terminal Flap - Precio Costo - Descuento directo - appium"))
            ruta = "A:\\DataEcommerce\\Solo Terminal - Flap\\"+Caso+".csv";
        else if(Caso.equals("Solo Terminal - Mercado Pago - Cupon - appium"))
            ruta = "A:\\DataEcommerce\\Solo Terminal - Mercado Pago\\"+Caso+".csv";
        else if(Caso.equals("Solo Terminal - Mercado Pago - Descuento directo - appium"))
            ruta = "A:\\DataEcommerce\\Solo Terminal - Mercado Pago\\"+Caso+".csv";
        else if(Caso.equals("Solo Terminal - Mercado Pago - Descuento directo + Cupon - appium"))
            ruta = "A:\\DataEcommerce\\Solo Terminal - Mercado Pago\\"+Caso+".csv";
        else if(Caso.equals("Solo Terminal - Mercado Pago - Precio Costo - Cupon - appium"))
            ruta = "A:\\DataEcommerce\\Solo Terminal - Mercado Pago\\"+Caso+".csv";
        else if(Caso.equals("Solo Terminal - Mercado Pago - Precio Costo - Descuento directo - appium"))
            ruta = "A:\\DataEcommerce\\Solo Terminal - Mercado Pago\\"+Caso+".csv";
        else if(Caso.equals("Portabilidad Pospago con Terminal - Fianza $200 - appium"))
            ruta = "A:\\DataEcommerce\\Portabilidad Pospago\\"+Caso+".csv";
        else if(Caso.equals("Portabilidad refactor Liferay - Fianza $0 - appium"))
            ruta = "A:\\DataEcommerce\\Portabilidad Pospago\\"+Caso+".csv";
        else if(Caso.equals("Portabilidad Pospago Solo SIM Liferay - Fianza $0 - appium"))
            ruta = "A:\\DataEcommerce\\Portabilidad Pospago\\"+Caso+".csv";
        else if(Caso.equals("Portabilidad Pospago Solo SIM Liferay - Fianza $200 - appium"))
            ruta = "A:\\DataEcommerce\\Portabilidad Pospago\\"+Caso+".csv";
        else if(Caso.equals("Portabilidad Pospago Solo SIM - Liferay"))
            ruta = "A:\\DataEcommerce\\Portabilidad Pospago\\"+Caso+".csv";
        else if(Caso.equals("Portabilidad Pospago Solo SIM - Liferay Fianza $200"))
            ruta = "A:\\DataEcommerce\\Portabilidad Pospago\\"+Caso+".csv";
     
        (new SystemA()).OpenFile(ruta); 
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jCheckBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox1ActionPerformed

    
    
    public static void main(String args[]) {
        String val = System.getenv("ANDROID_HOME");
        System.out.println("Variable ANDROID_HOME: "+val); // C:\Users\Marco\AppData\Local\Android\Sdk
        //val = System.getenv("JAVA_HOME");
        //System.out.println("Variable JAVA_HOME: "+val);
        //Map<String, String> map = new HashMap<String, String>();
        //map.put("ANDROID_HOME", "A:\\Perfiles\\Android"); // C:\Users\Marco\AppData\Local\Android\Sdk
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Menu().setVisible(false);
                new Menu2().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Aceptar;
    private javax.swing.JComboBox<String> ComboBox1;
    private javax.swing.JLabel Orden;
    private javax.swing.JLabel Resultado;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JSeparator jSeparator1;
    // End of variables declaration//GEN-END:variables


}
