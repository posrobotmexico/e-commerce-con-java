/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Core;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author John-117
 */
public class WebAction {
    WebDriver Driver;
    
    public WebAction(WebDriver Driver)
    {
        this.Driver = Driver;
    }
    
    public WebElement FindElementByID(WebDriver drive, String NameElement)//Busca el elemento por ID regresa un objeto
    {
        WebElement Element;
        Element=null;
        try
        {
            WaitForLoad(drive,By.id(NameElement),5);
            Element=drive.findElement(By.id(NameElement));
        }catch(Exception e)
        {
            System.out.print("Error Elemento "+NameElement+" no encontrado Detalle de error: \n"+e);
            //System.exit(0);
        }
        return Element;
    }
    
    public void Over(WebDriver driver, String ID)
    {
        Actions action = new Actions(driver);
        WebElement element = driver.findElement(By.id(ID));
        action.moveToElement(element).perform();
        
    }
    
    public WebElement FindElementByID(String NameElement)
    {
        return FindElementByID(Driver,NameElement);
    }
    
    public WebElement FindElementByXPath(WebDriver drive, String NameElement)//Busca el elemento por ID regresa un objeto
    {
        WebElement Element;
        Element=null;
        try
        {
            Element=drive.findElement(By.xpath(NameElement));
        }catch(Exception e)
        {
            System.out.print("Error Elemento "+NameElement+" no encontrado Detalle de error: \n"+e);
            //System.exit(0);
        }
        return Element;
    }
    
    public WebElement FindElementByXPath( String NameElement)
    {
        return FindElementByXPath(Driver,NameElement);
    }
    
    public WebElement FindElementByLinkText(WebDriver drive, String NameElement)//Busca el elemento por ID regresa un objeto
    {
        WebElement Element;
        Element = null;
        try
        {
            Element=drive.findElement(By.linkText(NameElement));
        }catch(Exception e)
        {
            System.out.print("Error Elemento "+NameElement+" no encontrado Detalle de error: \n"+e);
            //System.exit(0);
        }
        return Element;
    }
    
    public WebElement FindElementByClassName(WebDriver drive, String NameElement)//Busca el elemento por ID regresa un objeto
    {
        WebElement Element;
        Element=null;
        try
        {
            Element=drive.findElement(By.className(NameElement));
        }catch(Exception e)
        {
            System.out.print("Error Elemento "+NameElement+" no encontrado Detalle de error: \n"+e);
            //System.exit(0);
        }
        return Element;
    }
    
    public WebElement FindElementByClassName(String NameElement)
    {
        return FindElementByClassName(Driver,NameElement);
    }
    
    public List<WebElement> FindElementsByClassName(WebDriver drive, String NameElement)//Busca el elemento por ID regresa un objeto
    {
        List<WebElement> Element;
        Element=null;
        try
        {
            Element=drive.findElements(By.className(NameElement));
        }catch(Exception e)
        {
            System.out.print("Error Elemento "+NameElement+" no encontrado Detalle de error: \n"+e);
            //System.exit(0);
        }
        return Element;
    }
    
    public List<WebElement> FindElementsByClassName(String NameElement)//Busca el elemento por ID regresa un objeto
    {
        return FindElementsByClassName(Driver,NameElement);
    }
    
    
    public void TypeText(WebElement Element,String Text) throws InterruptedException//Hace el type text de un elemento 
    {
        try
        {
            Element.sendKeys(Text);
            Thread.sleep(500);
        }catch(Exception e)
        {
            System.out.print("Error al hacer TypeText Sobre elemento Detalle de error: \n"+e);
            //System.exit(0);
        }
    }
    
    public void TypeText(By by,String Text)
    {
        Driver.findElement(by).sendKeys(Text);
    }
    
    public void TypeText(By by,Keys Text)
    {
        try
        {
            Driver.findElement(by).sendKeys(Text);
        }catch(Exception e)
        {
            System.out.print("Error al hacer TypeText Sobre elemento Detalle de error: \n"+e);
            //System.exit(0);
        }
    }
    
    public void SelenSelect(WebElement Element,String Type,String Value)
    {
        try
        {
            switch(Type)
            {
                case "Value"://Seleccion por Valor
                    Select selectObj=new Select(Element);
                    selectObj.selectByValue(Value);
                    break;

                default:
                    break;
            }
        }catch(Exception e)
        {
            System.out.print("Error al escoger el valor del select Detalle de error: \n"+e);
            //System.exit(0);
        }
      
    }
    
    public void ClickById(WebDriver driver,String NameElement) throws InterruptedException
    {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        try
        {
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            List <WebElement> Button=driver.findElements(By.id(NameElement));
            int count = Button.size();
            if(count==1)
                Button.get(0).click();
            else if(count>1)
            {
                int c=0;
                Boolean ban=false;
                while(c<count)
                {
                    try
                    {
                        Button.get(c).click();
                        ban=true;
                        break;
                    }catch(Exception e)
                    {
                        c++;
                    }
                }
                if(!ban)
                {
                    System.out.print("No se dio click al boton");
                    //System.exit(0);
                }
            }
            else
            {
                System.out.print("Boton no encontrado");
                //System.exit(0);
            }
        }
        catch(Exception e)
        {
            System.out.print("Error al dar Click al boton: \n"+e);
            //System.exit(0);
        }      
    }
    public void ClickByXpath(WebDriver driver,String NameElement) throws InterruptedException
    {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        try
        {
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            List <WebElement> Button=driver.findElements(By.xpath(NameElement));
            int count = Button.size();
            if(count==1)
                Button.get(0).click();
            else if(count>1)
            {
                int c=0;
                Boolean ban=false;
                while(c<count)
                {
                    try
                    {
                        Button.get(c).click();
                        ban=true;
                        break;
                    }catch(Exception e)
                    {
                        c++;
                    }
                }
                if(!ban)
                {
                    System.out.print("No se dio click al boton");
                    //System.exit(0);
                }
            }
            else
            {
                System.out.print("Boton no encontrado");
                //System.exit(0);
            }
        }
        catch(Exception e)
        {
            System.out.print("Error al dar Click al boton: \n"+e);
            //System.exit(0);
        }      
    }
     
     public void ClickButtonName(WebDriver driver,WebAction WebAction,String TipBus,String NameElement,String TextElement) throws InterruptedException
    {   //Siempre se buscara dar click a span por lo cual el metodo sirve con DIV Teniendo de hijos a SPAN
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        try
        {
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            List <WebElement> Button=null;
            switch(TipBus)
            {
                case "Id":
                    Button=driver.findElements(By.id(NameElement));
                    break;
                
                case "ClassName":
                    Button=driver.findElements(By.className(NameElement));
                    break;
                case "LinkText":
                    Button = driver.findElements(By.linkText(NameElement));
                    break;
                    
                default:
                    System.out.println("No esta definido el tipo de busqueda");
                    //System.exit(0);
                    break;
            }
           
            if(Button==null)
            {
                System.out.println("No se detecto elemento devolvio lista vacia");
                //System.exit(0);
            }            
            int count = Button.size();
            if(count==1)
            {
                List <WebElement> Div=WebAction.GetChildren(Button.get(0));
                Boolean ban=false;
                for(int i=0;i<Div.size();i++)
                {
                    String val=Button.get(0).getText();
                    if(val.contentEquals(TextElement))
                    {
                        Button.get(0).click();
                        ban=true;
                        break;
                    }
                }
                if(!ban)
                {
                    System.out.println("No se detecto el boton con el nombre "+TextElement);
                    //System.exit(0);
                }
            }
            else if(count>1)
            {
                
            }
            else
            {
                System.out.print("Boton no encontrado");
                //System.exit(0);
            }
        }
        catch(Exception e)
        {
            System.out.print("Error al dar Click al boton: \n"+e);
            //System.exit(0);
        }      
    }
    public List<WebElement> GetChildren(WebElement Element)
    {
        List<WebElement> childrenElements = Element.findElements(By.xpath("*"));
        return childrenElements;
    }

public void ClickButton(WebDriver driver,WebAction SelenElem,String TipBus,String NameElement) throws InterruptedException
    {   //Siempre se buscara dar click a span por lo cual el metodo sirve con DIV Teniendo de hijos a SPAN
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        try
        {
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            switch(TipBus)
            {
                case "Id":
                    driver.findElement(By.id(NameElement)).click();
                    break;
                
                case "ClassName":
                    driver.findElement(By.className(NameElement)).click();
                    break;
                    
                case "LinkText":
                    driver.findElement(By.linkText(NameElement)).click();
                    break;
                    
                case "CSS":
                    driver.findElement(By.cssSelector(NameElement)).click();
                    break;
                    
                case "Name":
                driver.findElement(By.name(NameElement)).click();
                break;
                
                case "PartialLinkText":
                driver.findElement(By.partialLinkText(NameElement)).click();
                break;
              
                case "TagName":
                driver.findElement(By.tagName(NameElement)).click();
                break;
                    
                case "Xpath":
                driver.findElement(By.xpath(NameElement)).click();
                break;
                
                default:
                    System.out.println("No esta definido el tipo de busqueda");
                    //System.exit(0);
                    break;
            }
        }
        catch(Exception e)
        {
            System.out.print("Error al dar Click al boton: \n"+e);
            //System.exit(0);
        }      
    }
    
    public void CargaAjax(WebDriver drive) 
    {
        int tiempo = 10 ; // tiempo en minutos maximo de espera, para que no se cicle si pasa algo raro
        try{
            tiempo = tiempo * 6000;
            WebElement Element = drive.findElement(By.id("carga-categorias-loader"));
            
            while(Element.isDisplayed() && tiempo > 0)
            {
                Thread.sleep(100); // duerme 100 ms para no sobre cargar el procesador
                tiempo -= 1;
            }
        }
        catch (InterruptedException ex) {
            Logger.getLogger(WebAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
    
    public void CargaAjax2(WebDriver drive) 
    {
        int tiempo = 15 ; // tiempo en minutos maximo de espera, para que no se cicle si pasa algo raro
        try{
            tiempo = tiempo * 6000;
            WebElement Element = drive.findElement(By.className("showbox"));
            
            while(Element.isEnabled()&& tiempo > 0)
            {
                Thread.sleep(100); // duerme 100 ms para no sobre cargar el procesador
                tiempo -= 1;
            }
        }
        catch (InterruptedException ex) {
            Logger.getLogger(WebAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 

    public WebElement Find(By by)
    {
        WebElement elemento = null;
        try{
            elemento = Driver.findElement(by);
        }catch(Exception ex)
        {
            
        }
        return elemento;
    }

public WebElement FindElement(WebDriver driver,WebAction WebAction,String TipBus,String NameElement) throws InterruptedException
    {   //Buscar elemento
        WebElement Element;
        Element = null;
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        try
        {
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

            switch(TipBus)
            {
                case "Id":
                    Element = driver.findElement(By.id(NameElement));
                    break;
                
                case "ClassName":
                    Element = driver.findElement(By.className(NameElement));
                    break;
                    
                case "LinkText":
                    Element = driver.findElement(By.linkText(NameElement));
                    break;
                    
                case "CCS":
                    Element = driver.findElement(By.cssSelector(NameElement));
                    break;
                    
                case "Name":
                    Element = driver.findElement(By.name(NameElement));
                    break;
                
                case "PartialLinkText":
                    Element = driver.findElement(By.partialLinkText(NameElement));
                    break;
              
                case "TagName":
                    Element = driver.findElement(By.tagName(NameElement));
                    break;
                    
                    
                case "Xpath":
                    Element = driver.findElement(By.xpath(NameElement));
                    break;
                
                default:
                    System.out.println("No esta definido el tipo de busqueda");
                    //System.exit(0);
                    break;
            }
        }
        catch(Exception e)
        {
            System.out.print("Error al dar Click al boton: \n"+e);
            
        }
        return Element;
    }
public String GetText(WebDriver driver,WebAction WebAction,String TipBus,String NameElement) throws InterruptedException
    {   //Obtiene text del elemento
        String Element;
        Element = null;
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        try
        {
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

            switch(TipBus)
            {
                case "Id":
                    Element = driver.findElement(By.id(NameElement)).getText();
                    break;
                
                case "ClassName":
                    Element = driver.findElement(By.className(NameElement)).getText();
                    break;
                    
                case "LinkText":
                    Element = driver.findElement(By.linkText(NameElement)).getText();
                    break;
                    
                case "CCS":
                    Element = driver.findElement(By.cssSelector(NameElement)).getText();
                    break;
                    
                case "Name":
                    Element = driver.findElement(By.name(NameElement)).getText();
                    break;
                
                case "PartialLinkText":
                    Element = driver.findElement(By.partialLinkText(NameElement)).getText();
                    break;
              
                case "TagName":
                    Element = driver.findElement(By.tagName(NameElement)).getText();
                    break;
                    
                    
                case "Xpath":
                    Element = driver.findElement(By.xpath(NameElement)).getText();
                    break;
                
                default:
                    System.out.println("No esta definido el tipo de busqueda");
                    //System.exit(0);
                    break;
            }
        }
        catch(Exception e)
        {

        }
        return Element;
    } 
    
    public void focus(WebElement elemento)
    {
        ((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true); window.scrollBy(0, -window.innerHeight / 4);",elemento);
        new Actions(Driver).moveToElement(elemento).perform(); 
    }
    
    public void focus(By by)
    {
        //try{
            
            
            ((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true); window.scrollBy(0, -window.innerHeight / 4);",Driver.findElement(by));
            new Actions(Driver).moveToElement(Driver.findElement(by)).perform(); 
        //}
    }
    
    public void loadElement(WebDriver Driver,WebElement elemento)
    {
        WebDriverWait wait = new WebDriverWait(Driver, /*Duration.ofSeconds(60)*/60); // 1 minuto de espera de carga
        wait.until(ExpectedConditions.elementToBeClickable(elemento));
    }
    
    public void loadElement(By by)
    {
        WebDriverWait wait = new WebDriverWait(Driver, /*Duration.ofSeconds(60)*/60); // 1 minuto de espera de carga
        WebElement elemento = Driver.findElement(by); 
        wait.until(ExpectedConditions.elementToBeClickable(elemento));
    }
    
    public void clickJs(WebDriver Driver,WebElement elemento)
    {
        JavascriptExecutor js = (JavascriptExecutor)Driver; 
        js.executeScript("arguments[0].click();", elemento);
    }
    
    public void clickJs(WebDriver Driver,By by)
    {
        JavascriptExecutor js = (JavascriptExecutor)Driver; 
        js.executeScript("arguments[0].click();", Driver.findElement(by));
    }
    
    public void clickJs(By by)
    {
        JavascriptExecutor js = (JavascriptExecutor)Driver; 
        js.executeScript("arguments[0].click();", Driver.findElement(by));
    }
    
    public void WaitFor(WebDriver Driver,WebElement elemento)
    {
        WebDriverWait wait = new WebDriverWait(Driver, 60); // 1 minuto de espera de carga
        //wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("about_me")));
        wait.until(ExpectedConditions.visibilityOf(elemento));
    }
    
    public void WaitForLoad(WebDriver Driver,By by,int timeinseconds)
    {
        WebDriverWait wait = new WebDriverWait(Driver, timeinseconds); 
        wait.until(ExpectedConditions.presenceOfElementLocated(by));
    }
    
    public void WaitFor(WebElement elemento)
    {
        WebDriverWait wait = new WebDriverWait(Driver, 60); // 1 minuto de espera de carga
        //wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("about_me")));
        wait.until(ExpectedConditions.visibilityOf(elemento));
    }
    
    public void WaitFor(By by)
    {
        WebDriverWait wait = new WebDriverWait(Driver, 60); // 1 minuto de espera de carga
        //wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("about_me")));
        wait.until(ExpectedConditions.visibilityOf(Driver.findElement(by)));
    }
    
    public void WaitForHidden(By by) throws InterruptedException
    {
        while(Driver.findElement(by).isDisplayed()) {
                Thread.sleep(100);
            }
    }
    
    public void WaitForLoad(By by,int timeinseconds)
    {
        try {
            WebDriverWait wait = new WebDriverWait(Driver, (timeinseconds));
            wait.until(ExpectedConditions.presenceOfElementLocated(by));
        } catch (Exception ex) {

        }
    }
    
    public boolean IsShowed(By by)
    {
        try{
            if(Driver.findElement(by) != null)
                if(Driver.findElement(by).isDisplayed())
                    return true;
        }
        catch(Exception ex)
        {
            System.out.println("El elemento no se encontro: " + by.toString());
        }
        return false;
    }
    
    public boolean IsinScreen(By by)
    {
        try{
            WebDriverWait wait = new WebDriverWait(Driver, /*Duration.ofSeconds(60)*/3); // 1 minuto de espera de carga
            WebElement elemento = Driver.findElement(by); 
            wait.until(ExpectedConditions.elementToBeClickable(elemento));
            return true;
        }
        catch(Exception ex)
        {
            System.out.println("El elemento no se encontro: " + by.toString());
        }
        return false;
    }
    
    public void SelectOption(By by,String option)
    {
        try{
            Select objSelect = new Select(Driver.findElement(by));
            objSelect.selectByVisibleText(option);
        }catch(Exception ex)
        {
            System.out.println("Error: "+ex);
            //System.out.println("El elemento no se encontro: " + by.toString());
        }
    }
    
    public void SelectOptionByValue(By by,String option)
    {
        try{
            Select objSelect = new Select(Driver.findElement(by));
            objSelect.selectByValue(option);
        }catch(Exception ex)
        {
            System.out.println("Error: "+ex);
            //System.out.println("El elemento no se encontro: " + by.toString());
        }
    }
    
    public void Click(By by)
    {
        try{
            Driver.findElement(by).click();
        }catch(Exception ex)
        {
            clickJs(by);
        }
    }
    
    public void SetAttribute(By by,String attName,String attValue)
    {
        JavascriptExecutor driver = (JavascriptExecutor) Driver;
        WebElement element = Driver.findElement(by);
        
        driver.executeScript("arguments[0].setAttribute(arguments[1], arguments[2]);",element , attName, attValue);
    }
    
    public void removeAttribute(By by,String attName)
    {
        JavascriptExecutor driver = (JavascriptExecutor) Driver;
        WebElement element = Driver.findElement(by);
        
        driver.executeScript("arguments[0].removeAttribute(arguments[1]);",element , attName);
    }
    
    public void eraseDataJs(String element)
    {
        JavascriptExecutor driver = (JavascriptExecutor) Driver;        
        driver.executeScript("document.getElementById(arguments[0]).value = '';",element);
    }
    
    public void HideElement(By by)
    {
        JavascriptExecutor driver = (JavascriptExecutor) Driver;       
        WebElement element = Driver.findElement(by);
        driver.executeScript("arguments[0].display = 'hidden';",element);
    }
    
    public By xpathContainsText(String xpath,String text)
    {
        return By.xpath("//"+xpath+"[contains(text(),'"+ text +"')]");  
    }
    
    public By xpathEquals(String xpath,String text)
    {
        return By.xpath("//"+xpath+"[contains(text(),'"+ text +"']");  
    }
    
    public void waitForLoadPage()
    {
        WebDriverWait wait = new WebDriverWait(Driver, 3000);

        wait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver wdriver) {
                return ((JavascriptExecutor) Driver).executeScript(
                    "return document.readyState"
                ).equals("complete");
            }
        });
    }
    
    public Point coordinates(By by)
    {
        WebElement location = Driver.findElement(by);
        return location.getLocation();
    }
    
    public void DeslizarAppiumElement(By by,Appium ap,String to)
    {/*
        int x = coordinates(By.className("menu-b2c")).getX();
        int y = coordinates(By.className("menu-b2c")).getY();
        System.out.println("Dimension: x: "+x+" y: "+y);
        int SizeX = ap.SizeScreen().getWidth() / 2;
        int SizeY = ap.SizeScreen().getHeight() / 2;
        System.out.println("Dimension: x: "+x+" y: "+y); // [63,867][992,1030]
        switch(to)
        {
            case "Right":
                ap.slide(SizeX, 900, SizeX-300, 900);
                break;
        }
        
        
        
        Dimension d = SelenElem.Find(By.className("menu-b2c")).getSize();
        System.out.println("Tamaño: x: "+d.width+" y: "+d.height);
        Rectangle r = SelenElem.Find(By.className("menu-b2c")).getRect();
        System.out.println("Tamaño: x: "+r.getX()+" y: "+r.getY());
        
        
        for(int i=0;i<10;i++)
            ap.slide(400, 900, 100, 900); //	[63,867][992,1030]*/
    }
    
    public void dragDrop(By by,int x,int y)
    {
        Actions ac = new Actions(Driver);
        ac.dragAndDropBy(Driver.findElement(by), x, y);
    }
    
    public void BacktoMainFrame()
    {
        Driver.switchTo().defaultContent();
    }
    
    public void SwitchToFrameByTargetElement(By by)
    {
        int s = Driver.findElements(By.tagName("iframe")).size();
        for(int i=0;i<=s;i++)
        {
            Driver.switchTo().frame(i);
            if(Driver.findElements(by).size() > 0)
                break;
        }
    }
    
    public void SwitchToFrame(By by)
    {
        WebElement frame = Driver.findElement(by);
        Driver.switchTo().frame(frame);
    }
    
   public void uploadFile(String ubi){
       File file = new File("A:\\DataEcommerce\\Pospago SOHO solo SIM\\INE.pdf"); 
       
       Driver.findElement(By.id("INE.pdf")).sendKeys(file.getAbsolutePath());

   }
   
   public WebElement highLight(By by) {
		WebElement element = Find(by);

		JavascriptExecutor js = (JavascriptExecutor) Driver;
		js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
				"color: red; border: 4px solid red;");
		

		return element;
	}
    /** Metodo para quitar el high light a un web element
     * 
     * @param by
     * @return
     */
	public WebElement unhighLight(By by) {
		WebElement element = Find(by);

		JavascriptExecutor js = (JavascriptExecutor) Driver;
		js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "");
		

		return element;
	}
        
        public void Back()
    {
        Driver.navigate().back();
    }
}

