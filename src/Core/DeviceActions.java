/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Core;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import static io.appium.java_client.touch.LongPressOptions.longPressOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;
import io.appium.java_client.touch.offset.PointOption;
import java.time.Duration;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

/**
 *
 * @author LAPTOP-JORGE
 */
public class DeviceActions {
    AppiumDriver driver;
    
    public DeviceActions(AppiumDriver driver)
    {
        this.driver = driver;
    }
    public void hideKeyBoard()
    {
        driver.hideKeyboard();
    }
    
    public void slide(int oldx,int oldy,int newx,int newy)
    {
        new TouchAction(driver).press(PointOption.point(oldx, oldy)).waitAction().moveTo(PointOption.point(newx, newy)).release().perform();
    }
    
     public void press(int tiempo,By by)
    {
        //WebElement dibujo = driver.findElement(by);
        //new TouchAction(driver).longPress(longPressOptions().withElement(element(dibujo)).withDuration(Duration.ofMillis(tiempo))).release().perform();
        //new TouchAction(driver).press(PointOption.point(getMiddleElement(by))).waitAction().release().perform();
        TouchAction t = new TouchAction(driver);
        t.longPress(PointOption.point(getMiddleElement(by)));
        t.perform();
        try{
            Thread.sleep(tiempo);
        }catch(Exception ex){
            
        }
        t.release();
        t.perform();
        
    }
    
    public void slide(String to)
    {
        slide(to,1);
    }
    
    public void slide(String to,int multiplier)
    {
        Dimension windowSize = driver.manage().window().getSize();
        int newx = windowSize.width / 2;
        int newy = windowSize.height / 2;
        
        switch(to)
        {
            case "Up":
                newy += 300;
                break;
            case "Down":
                newy -= 300;
                break;
            case "Left":
                newx += 150;
                break;
            case "Right":
                newx -= 150;
                break;
            default:
                break;
        }
        for(int i=0;i<multiplier;i++)
            new TouchAction(driver).press(PointOption.point(windowSize.width / 2, windowSize.height / 2)).waitAction().moveTo(PointOption.point(newx, newy)).release().perform();
    }
    
    public Dimension SizeScreen()
    {
        String context = driver.getContext();
        driver.context("NATIVE_APP");
        Dimension dim = driver.manage().window().getSize();
        driver.context(context);        
        return dim;
    }
    
    public Point getMiddleElement(By by)
    {
        String context = driver.getContext();
        driver.context("NATIVE_APP");
        Point p = driver.findElement(by).getLocation();
        Dimension d = driver.findElement(by).getSize();
        p.x = p.x + (d.width / 2);
        p.y = p.y + (d.height / 2);
        driver.context(context);
        return p;
    }
    
    public byte[] getScreenShot()
    {
        return driver.getScreenshotAs(OutputType.BYTES);
    }
    
    public void clickCoordenadas(int x, int y)
    {
       new TouchAction(driver).tap(PointOption.point(x, y)).perform();
        
    }
}
