/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Core;

/*

    Super importante.
    Librerias deben estar completas y adecuadas a la versión correcta tanto de appium como selenium
    meter al path las variables requeridas, ANDROID_HOME y JAVA_HOME
    Instalar NodeJS y luego
    npm install -g appium


*/

import com.google.common.collect.ImmutableMap;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 *
 * @author LAPTOP-JORGE
 */
public class Appium {
    AppiumDriver driver;
    AppiumDriverLocalService service;
    
    public AppiumDriver getDriver()
    {
        return driver;
    }
    
    public void setDriver(AppiumDriver driver) {
        this.driver = driver;
    }
    
    public AppiumDriver connectDevice() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        //capabilities.setCapability("deviceName", "moto g(8) power");
        capabilities.setCapability(CapabilityType.PLATFORM_NAME, "Android");
        //capabilities.setCapability("platformVersion", "11.0");
        //capabilities.setCapability("udid", "ZY22BJX2BV");
        //capabilities.setCapability("appPackage", "com.movistarmx.mx.app");
        //capabilities.setCapability("appActivity", ".MainActivity");
        capabilities.setCapability(CapabilityType.BROWSER_NAME, "Chrome"); 
        capabilities.setCapability("newCommandTimeout", 60 * 15);
        capabilities.setCapability("chromeOptions", ImmutableMap.of("w3c", false));
        capabilities.setCapability("chromedriverExecutable","A:\\Perfiles\\WebDrivers\\Appium\\chromedriver.exe");
        
        try {
            //File file = new File("apk folder", "apk name");
            //capabilities.setCapability("app", file.getAbsolutePath());            
            driver = new AppiumDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
            //driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        } catch (MalformedURLException ex) {
            Logger.getLogger(Appium.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch(SessionNotCreatedException ex)
        {
            if(ex.getLocalizedMessage().contains("environment variable was exported"))
                System.out.println("Es necesario ajustar las variables del sistema, se requiere agregar al path la ruta de ANDROID_HOME y/o ANDROID_SDK_ROOT");    
            
            Logger.getLogger(Appium.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return driver;
    }
    
    public void startServer() {
        //Set Capabilities
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability("noReset", "false");

        //Build the Appium service
        AppiumServiceBuilder builder = new AppiumServiceBuilder();
        builder.withIPAddress("127.0.0.1");
        builder.usingPort(4723);
        builder.withCapabilities(cap);
        //builder.withArgument(GeneralServerFlag.SESSION_OVERRIDE);
        builder.withArgument(GeneralServerFlag.LOG_LEVEL, "error");

        //Start the server with the builder
        service = AppiumDriverLocalService.buildService(builder.withArgument(() -> "--allow-insecure","chromedriver_autodownload"));
        service.start();
    }

    public void stopServer() {
        service.stop();
    }
    
    /*public void startServer() { // With nodeJs in terminal
        Runtime runtime = Runtime.getRuntime();
        try {
            runtime.exec("cmd.exe /c start cmd.exe /k \"appium -a 127.0.0.1 -p 4723 --session-override -dc \"{\"\"noReset\"\": \"\"false\"\"}\"\"");
            Thread.sleep(10000);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void stopServer() {
        Runtime runtime = Runtime.getRuntime();
        try {
            runtime.exec("taskkill /F /IM node.exe");
            runtime.exec("taskkill /F /IM cmd.exe");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/
    
    public void example()
    {
        Appium ap = new Appium();
        ap.startServer();
        AppiumDriver device = ap.connectDevice();
        
        device.get("https://certi245-tienda-movistar.canalesdigitales.com.mx/");
        
        try {
            Thread.sleep(20000); // you know
        } catch (InterruptedException ex) {
            Logger.getLogger(Appium.class.getName()).log(Level.SEVERE, null, ex);
        }

        device.quit();
        ap.stopServer();
    }    
}
