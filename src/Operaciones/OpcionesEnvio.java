/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Operaciones;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class OpcionesEnvio {
    
    public OpcionesEnvio(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive) throws InterruptedException, IOException
    {
          SelenElem.WaitForLoad(By.id("inv-mail2"), 15);
          SelenElem.Click(By.id("inv-mail2"));
          //OPG.cargaAjax2();
          SelenElem.WaitForLoad(By.id("js_codigo_postal"), 10);
          Thread.sleep(2000);
          SelenElem.TypeText(By.id("js_codigo_postal"), "06600");
          reporte.AddImageToReport("Sección opciones de envio del checkout", OPG.SS());
          SelenElem.Click(By.id("js_search_cacs_by_postal_code"));
          Thread.sleep(2000);
          SelenElem.ClickById(WebDrive.WebDriver,"js_search_cacs_by_postal_code");
          //OPG.cargaAjax2();
          SelenElem.WaitForLoad(By.id("99090073cac"), 10);
          SelenElem.Click(By.id("99090073cac"));
          reporte.AddImageToReport("Sección opciones de envio del checkout", OPG.SS());
    }
    
    
}
