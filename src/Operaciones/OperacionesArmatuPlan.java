/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operaciones;

import Core.WebAction;
import Core.WebDriv;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

/**
 *
 * @author root
 */
public class OperacionesArmatuPlan {
    
    private WebDriv Driver;
    private WebAction SelenElem;
    
    public OperacionesArmatuPlan(WebDriv Driver,WebAction SelenElem)
    {
        this.Driver = Driver;
        this.SelenElem = SelenElem;
    }
    
    public void SeleccionarPlanes()
    {
        SelenElem.FindElementByXPath("//form[@id='custom-postpaid']//descendant::button").click();
    }
    
    public void IngresaNumero(String DN) throws InterruptedException
    {
        SelenElem.TypeText(By.id("validate_dn"), DN);
    }
    
    public void VerificarNumero()
    {
        SelenElem.FindElementByClassName("c-button--green").click();
    }
    
    public void IngresaCodigo123() throws InterruptedException
    {
        SelenElem.TypeText(By.id("otp-input"), "123456");
    }
    
    public void VerificarCodigo123()
    {
        SelenElem.clickJs(By.id("continue-btn2"));
    }
    
    public void EligeCaracteristicas()
    {
        SelenElem.focus(By.id("navigation_products"));
        List<WebElement> despl = SelenElem.FindElementsByClassName("draggable_slider");
        (new Actions(Driver.WebDriver)).dragAndDrop(despl.get(0), SelenElem.FindElementByXPath("//li[@data-name='7 GB']")).perform();
        (new Actions(Driver.WebDriver)).dragAndDrop(despl.get(1), SelenElem.FindElementByXPath("//li[@data-name='3 GB']")).perform();
    }
    
    public void EligeCaracteristicas2()
    {
        SelenElem.focus(By.id("navigation_products"));
        List<WebElement> despl = SelenElem.FindElementsByClassName("draggable_slider");
        (new Actions(Driver.WebDriver)).dragAndDrop(despl.get(0), SelenElem.FindElementByXPath("//li[@data-name='12 GB']")).perform();
        (new Actions(Driver.WebDriver)).dragAndDrop(despl.get(1), SelenElem.FindElementByXPath("//li[@data-name='5 GB']")).perform();
    }
    
    public void ModulosAdicionales()
    {
        SelenElem.focus(By.id("section-additional-services"));
        SelenElem.clickJs(By.className("i-a-arrow-down"));
        SelenElem.clickJs(By.id("2897"));
        SelenElem.clickJs(By.id("2898"));
    }
    
    public void VerResumendePlanes() throws InterruptedException
    {
        SelenElem.clickJs(By.id("pay-today"));
        SelenElem.focus(By.xpath("//p[text()='Navegación']"));
        
    }
    
    public void VerResumendeCosto() throws InterruptedException
    {
        SelenElem.focus(By.className("summary"));
        SelenElem.clickJs(By.id("terms"));
    }
    
    public void GenerarCompraPlan() throws InterruptedException
    {
        SelenElem.clickJs(By.id("btn_customplan_validate"));
    }
    
    public void CompletarCheckout() throws InterruptedException
    {
        SelenElem.focus(By.id("valid-email"));
        SelenElem.TypeText(By.id("valid-email"), "automatizacion@practia.global");
        SelenElem.clickJs(By.id("manifest"));
    }
    
    public void PushButtonContinuar()
    {
        SelenElem.clickJs(By.id("stepBenefits"));
        SelenElem.clickJs(By.id("checkContract"));
    }
    
    public void PushButtonContinuar2() throws InterruptedException
    {
        SelenElem.clickJs(By.id("stepTwo"));
        SelenElem.clickJs(By.id("change-payment-button"));        
        
        SelenElem.TypeText(By.id("flap_token_cc_number"), CargaVariables.LeerCSV(2,"Config","Tarjeta_Flap"));
        SelenElem.TypeText(By.id("flap_token_expiration"), CargaVariables.LeerCSV(3,"Config","Tarjeta_Flap"));
        SelenElem.TypeText(By.id("flap_token_cc_cid"), CargaVariables.LeerCSV(4,"Config","Tarjeta_Flap"));
        SelenElem.TypeText(By.id("flap_token_cc_owner"), CargaVariables.LeerCSV(0,"Config","Tarjeta_Flap"));
        
        SelenElem.focus(By.id("flap_token_cc_owner"));
        
    }
    
    public void ProcesarOrden()
    {
        
        SelenElem.clickJs(By.id("btn-place-order"));
    }
    
    
    
    // ARMA TU PLAN / Salida de Oferta / Cambio de plan a Plan Datos Ilimitados - Sin Login
    
    public void SeleccionarPlanesInferior()
    {
        SelenElem.clickJs(By.xpath("//form[@id='change-planpost']//descendant::button"));
    }
    
    public void SeleccionaPlanGigasIlimitados()
    {
        SelenElem.clickJs(By.xpath("//article[@data-product='2142']//descendant::button"));
    }
    
    public void AceptarTerminosyCondiciones()
    {
        SelenElem.clickJs(By.id("terms"));
    }   
}
