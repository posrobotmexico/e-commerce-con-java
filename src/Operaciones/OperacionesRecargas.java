/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operaciones;

import Core.WebAction;
import Core.WebDriv;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


/**
 *
 * @author root
 */
public class OperacionesRecargas {
    private WebDriv Driver;
    private WebAction SelenElem;
    
    public OperacionesRecargas(WebDriv Driver,WebAction SelenElem)
    {
        this.Driver = Driver;
        this.SelenElem = SelenElem;
    }
    
    public void SeleccionaFlujoRecargas()
    {
        WebElement BotonRecarga = SelenElem.FindElementByLinkText(Driver.WebDriver,"Recargas");
        BotonRecarga.click();
    }
    
    public void IngresaNumeroRecarga(String DN) throws InterruptedException, IOException
    {
        WebElement NumeroCampo = SelenElem.FindElementByID(Driver.WebDriver,"codeMov");
        SelenElem.TypeText(NumeroCampo, DN); // numero de telefono hay que reemplazar por el del archivo
        SelenElem.focus(By.id("codeMov"));
    }
    
    public void ContinuaAlMontodeRecarga() throws InterruptedException, IOException
    {
        SelenElem.loadElement(Driver.WebDriver, Driver.WebDriver.findElement(By.id("btn_fiststep")));
        SelenElem.ClickButton(Driver.WebDriver, SelenElem, "Id", "btn_fiststep");
    }
    
    public void SeleccionaPaqueteRecarga(String amount) throws InterruptedException
    {
        Thread.sleep(2000);
        List<WebElement> precios = SelenElem.FindElementsByClassName(Driver.WebDriver, "radio-box__input");
        
        for(WebElement i : precios)
        {
            if(i.getAttribute("data-price").contentEquals(amount)) // precio del paquete, reemplazar por el del archivo
            {
                SelenElem.focus(i);
                SelenElem.clickJs(Driver.WebDriver,i);
                break;
            }
        }
    }
    
    public void enfocaPaqueteRecarga(String amount)
    {
        List<WebElement> precios = SelenElem.FindElementsByClassName(Driver.WebDriver, "radio-box__input");
        for(WebElement i : precios)
        {
            if(i.getAttribute("data-price").contentEquals(amount)) // precio del paquete, reemplazar por el del archivo
            {
                SelenElem.focus(i);
                break;
            }
        }
    }
    
    public void LlenarDatosRecargaFlap() throws InterruptedException // No usada aún, puede ser para Flap
    {
        if(SelenElem.FindElementByID(Driver.WebDriver,"UserCredit") != null)
        {
            SelenElem.TypeText(SelenElem.FindElementByID(Driver.WebDriver, "UserCredit"), CargaVariables.LeerCSV(0,"Config","Tarjeta_Flap"));
            SelenElem.TypeText(SelenElem.FindElementByID(Driver.WebDriver, "emailFlap"), CargaVariables.LeerCSV(1,"Config","Tarjeta_Flap"));
            SelenElem.TypeText(SelenElem.FindElementByID(Driver.WebDriver, "NumCredit2"), CargaVariables.LeerCSV(2,"Config","Tarjeta_Flap"));
            SelenElem.TypeText(SelenElem.FindElementByID(Driver.WebDriver, "expiration"), CargaVariables.LeerCSV(3,"Config","Tarjeta_Flap"));
            SelenElem.TypeText(SelenElem.FindElementByID(Driver.WebDriver, "ccv"), CargaVariables.LeerCSV(4,"Config","Tarjeta_Flap"));
            
            SelenElem.clickJs(Driver.WebDriver,SelenElem.FindElementByID(Driver.WebDriver, "terms"));
        }
        else
        {
            SelenElem.TypeText(SelenElem.FindElementByID(Driver.WebDriver, "cardholderName"), CargaVariables.LeerCSV(0,"Config","Tarjeta_Flap"));
            SelenElem.TypeText(SelenElem.FindElementByID(Driver.WebDriver, "email"), CargaVariables.LeerCSV(1,"Config","Tarjeta_Flap"));
            SelenElem.TypeText(SelenElem.FindElementByID(Driver.WebDriver, "cardNoSafe"), CargaVariables.LeerCSV(2,"Config","Tarjeta_Flap"));
            SelenElem.TypeText(SelenElem.FindElementByID(Driver.WebDriver, "cardExpiration"), CargaVariables.LeerCSV(3,"Config","Tarjeta_Flap"));
            SelenElem.TypeText(SelenElem.FindElementByID(Driver.WebDriver, "securityCode"), CargaVariables.LeerCSV(4,"Config","Tarjeta_Flap"));
        }
    }
    
    public void EnviaRecarga()
    {
        SelenElem.FindElementByID(Driver.WebDriver, "btn_recargar").click();
    }
    
    public void ConfirmacionDeCompraRecarga()
    {
        WebElement buttonRecarga = SelenElem.FindElementByID(Driver.WebDriver, "recarga");
        buttonRecarga.sendKeys(Keys.PAGE_DOWN);
        SelenElem.clickJs(Driver.WebDriver,buttonRecarga);
        SelenElem.focus(By.className("detail-board__subtitle"));
    }
    
    public void EnfocaBotonMP()
    {
        SelenElem.focus(By.id("redirect-mp"));
    }
    
    public void SeleccionaPagoMP()
    {
        WebElement boton = Driver.WebDriver.findElement(By.xpath("//span[@title='Da clic aquí para ir al sitio de mercado pago para realizar tu pago']"));
        SelenElem.clickJs(Driver.WebDriver,SelenElem.FindElementByID(Driver.WebDriver, "redirect-mp"));
        SelenElem.clickJs(Driver.WebDriver,boton);
    }
    
    public void SeleccionaNuevaTarjeta()
    {
        SelenElem.clickJs(Driver.WebDriver,SelenElem.FindElementByID(Driver.WebDriver,"new_card_row"));
    }
    
    public void RellenaDatosRecargaMP() throws InterruptedException
    {
        SelenElem.TypeText(SelenElem.FindElementByID(Driver.WebDriver, "fullname"), CargaVariables.LeerCSV(0,"Config","Tarjeta_MP"));
        SelenElem.TypeText(SelenElem.FindElementByID(Driver.WebDriver, "card_number"), CargaVariables.LeerCSV(2,"Config","Tarjeta_MP"));
        SelenElem.TypeText(SelenElem.FindElementByID(Driver.WebDriver, "input_expiration_date"), CargaVariables.LeerCSV(3,"Config","Tarjeta_MP"));
        SelenElem.TypeText(SelenElem.FindElementByID(Driver.WebDriver, "cvv"), CargaVariables.LeerCSV(4,"Config","Tarjeta_MP"));
    }
    
    public void EnviarRecargaMP()
    {
        SelenElem.clickJs(Driver.WebDriver,SelenElem.FindElementByID(Driver.WebDriver,"submit"));
    }
    
    public void RellenaDatosCorreoRecargaMP() throws InterruptedException
    {
        SelenElem.TypeText(SelenElem.FindElementByID(Driver.WebDriver, "email"), CargaVariables.LeerCSV(1,"Config","Tarjeta_MP"));
    }
    
    public void PagarRecargaMP()
    {
        SelenElem.clickJs(Driver.WebDriver,SelenElem.FindElementByID(Driver.WebDriver,"pay"));
    }
    
    // Flujo recargas movistar
    
    public void IngresaNumeroRecargaMovistar(String DN) throws InterruptedException, IOException
    {
        WebElement NumeroCampo = SelenElem.FindElementByID(Driver.WebDriver,"numeroMovistar");
        SelenElem.TypeText(NumeroCampo, DN);
        SelenElem.focus(NumeroCampo);
    }
    
    public void SeleccionaPaqueteRecargaMovistar(String amount)
    {
        WebElement Monto = SelenElem.FindElementByID(Driver.WebDriver,amount);
        SelenElem.clickJs(Driver.WebDriver, Monto);
    }
    
    public void ConfirmarRecargaMovistarPaquete()
    {
        WebElement botonConfirmar = SelenElem.FindElementByID(Driver.WebDriver,"confirmar-paquetes");
        SelenElem.WaitFor(Driver.WebDriver,botonConfirmar);
        SelenElem.clickJs(Driver.WebDriver, botonConfirmar);
    }
    
    public void ConfirmarRecargaMovistarMonto()
    {
        WebElement botonConfirmar = SelenElem.FindElementByID(Driver.WebDriver,"confirmar-hibrido");
        SelenElem.WaitFor(Driver.WebDriver,botonConfirmar);
        SelenElem.clickJs(Driver.WebDriver, botonConfirmar);
    }
    
    public void SeleccionaPagoMPMovistar()
    {
        WebElement boton = Driver.WebDriver.findElement(By.xpath("//label[@for='redirect-mp']"));
        SelenElem.clickJs(Driver.WebDriver,boton);
        
    }
    
    public void DireccionaPagoMPMovistar()
    {
        SelenElem.clickJs(Driver.WebDriver,SelenElem.FindElementByID(Driver.WebDriver, "payment-mppro"));
    }
}
