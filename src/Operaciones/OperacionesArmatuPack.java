/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operaciones;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

/**
 *
 * @author LAPTOP-JORGE
 */
public class OperacionesArmatuPack {
    
    private WebDriv Driver;
    private WebAction SelenElem;
    
    public OperacionesArmatuPack(WebDriv Driver,WebAction SelenElem)
    {
        this.Driver = Driver;
        this.SelenElem = SelenElem;
    }
    
    public void SeleccionarPrepago()
    {
        SelenElem.FindElementByXPath("//form[@id='custom-prepaid']//descendant::button").click();
    }
    
    public void IngresaNumero(String DN) throws InterruptedException
    {
        SelenElem.TypeText(By.id("validate_dn"), DN);
    }    
    
    public void VerificarNumero()
    {
        SelenElem.FindElementByClassName("c-button--green").click();
    }
    
    public void IngresaCodigo123() throws InterruptedException
    {
        SelenElem.TypeText(By.id("otp-input"), "123456");
    }
    
    public void VerificarCodigo123()
    {
        SelenElem.clickJs(By.id("continue-btn2"));
    }
    
    public void EligeCaracteristicas()
    {
        SelenElem.focus(By.id("navigation_products"));
        List<WebElement> despl = SelenElem.FindElementsByClassName("draggable_slider");
        (new Actions(Driver.WebDriver)).dragAndDrop(despl.get(0), SelenElem.FindElementByXPath("//li[@data-name='15 días']")).perform();
        (new Actions(Driver.WebDriver)).dragAndDrop(despl.get(1), SelenElem.FindElementByXPath("//li[@data-name='5 GB']")).perform();
    }
    
    //Basss
    
    public void ModulosAdicionales()
    {
        SelenElem.focus(By.id("section-additional-services"));
        SelenElem.clickJs(By.className("i-a-arrow-down"));
        SelenElem.clickJs(By.id("2883"));
    }
    
    public void VerResumendePlanes() throws InterruptedException
    {
        SelenElem.focus(By.id("pay-today"));
        Thread.sleep(1000);
        SelenElem.clickJs(By.id("total_price"));        
        SelenElem.clickJs(By.id("pay-today"));          
    }
    
    public void VerResumendeCosto() throws InterruptedException
    {
        SelenElem.focus(By.className("summary"));
        SelenElem.clickJs(By.id("invoice"));
        SelenElem.clickJs(By.id("terms"));
    }
        
    public void CompletarCheckout() throws InterruptedException
    {
        SelenElem.focus(By.id("valid-email"));
        SelenElem.TypeText(By.id("valid-nombre"), "Bot");
        SelenElem.TypeText(By.id("valid-appelido-p"), "Bot");
        SelenElem.TypeText(By.id("valid-appelido-m"), "Bot");
        SelenElem.TypeText(By.id("telefono-contacto"), "5512345678");
        SelenElem.TypeText(By.id("valid-email"), "automatizacion@practia.global");
        SelenElem.clickJs(By.id("checkPrivacyOne"));
    }
    
    public void PushButtonContinuar()
    {
        SelenElem.clickJs(By.id("buildPack-btn"));
        SelenElem.WaitForLoad(By.id("continuar-checkout-out"), 120);
    }
    
    public void VerificaCuadroConfirmacion(OperacionesGenerales OPG,Report reporte)
    {
        if(SelenElem.FindElementByID("btn_customplan_benefits") != null)
        {
            reporte.AddImageToReport("Damos click en aceptar", OPG.ScreenShotElement());
            SelenElem.clickJs(By.id("btn_customplan_benefits"));
        }
    }
    
    public void Pagar()
    {
        SelenElem.clickJs(By.id("continuar-checkout-out"));
    }
}

