
package Operaciones;

import Core.Report;
import Core.SystemA;
import Core.WebAction;
import Core.WebDriv;
import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;

/**
 *
 * @author John-117
 */
public class OperacionesGenerales {
    private WebDriv Driver;
    private WebAction SelenElem;
    
    public OperacionesGenerales()
    {
        
    }
            
    public OperacionesGenerales(WebDriv Driver,WebAction SelenElem)
    {
        this.SelenElem = SelenElem;
        this.Driver = Driver;
    }
    
    public void cargaAjax() throws InterruptedException {
        if(Driver.WebDriver.getCurrentUrl().contains("movistar"))
        {
            while(Driver.WebDriver.findElement(By.className("loading-spiner")).isDisplayed() || Driver.WebDriver.findElement(By.className("loader")).isDisplayed()) {
                Thread.sleep(100);
            }
        }
    }
    
    public void cargaAjax2() throws InterruptedException {
        if(Driver.WebDriver.getCurrentUrl().contains("movistar"))
        {
            while(Driver.WebDriver.findElement(By.className("loader")).isDisplayed()) {
                Thread.sleep(100);
            }
        }
    }
    

    public void Login(WebAction SelenElem,Report report) throws InterruptedException
    {
        String user = CargaVariables.LeerCSV(0,"Config","Users");
        String pass = CargaVariables.LeerCSV(1,"Config","Users");
        String url  = CargaVariables.LeerCSV(2,"Config","Users");
        
        Driver.WebDriver.navigate().to(url);
        
        SelenElem.TypeText(SelenElem.FindElementByID(Driver.WebDriver, "userCac"),user);
        SelenElem.TypeText(SelenElem.FindElementByID(Driver.WebDriver, "passCac"),pass);
        report.AddImageToReport("Pagina principal para login", ScreenShotElement());
        SelenElem.clickJs(Driver.WebDriver, SelenElem.FindElementByID(Driver.WebDriver, "submitCreateAccount"));
        
        SelenElem.TypeText(SelenElem.FindElementByID(Driver.WebDriver, "valid-nombre"),"Automatización Practia");
        SelenElem.TypeText(SelenElem.FindElementByID(Driver.WebDriver, "valid-email"),"automatizacion@practia.global");
        SelenElem.TypeText(SelenElem.FindElementByID(Driver.WebDriver, "telefono-contacto"),"5512345678");
        SelenElem.TypeText(SelenElem.FindElementByID(Driver.WebDriver, "codigo-postal-valida"),"54190");
        SelenElem.TypeText(SelenElem.FindElementByID(Driver.WebDriver, "address"),"Polanco México");
        report.AddImageToReport("Datos para login", ScreenShotElement());
        SelenElem.clickJs(Driver.WebDriver, SelenElem.FindElementByXPath(Driver.WebDriver, "//button[@title='continuarNC']"));        
    }
    
    public void clickInChildrenAnyLevel(WebDriv Driver,String grandfather,String target)
    {
        if(grandfather == null)
            return;
        
        WebElement t = null;
        try
        {
            t = Driver.WebDriver.findElement(By.id(grandfather));
        }
        catch(Exception ex)
        {
            t = Driver.WebDriver.findElement(By.className(grandfather));
        }
        
        List<WebElement> ele = t.findElements(By.xpath("./child::*"));
        for(WebElement el : ele){
            String type = "id";
            if(el.getAttribute("id").equals(""))
                type = "class";
            
            System.out.println("Atributo encontrado: "+el.getAttribute(type));
            if(el.getAttribute(type).contains(target))
                el.click();
            else
                clickInChildrenAnyLevel(Driver,el.getAttribute(type),target);
        }
    }
    
    public void CierraNavegadores(String Navegador)
    {
        SystemA sistema = new SystemA();
        switch(Navegador)
        {
            case "Chrome":
                sistema.KillProccess("chrome.exe");
                sistema.KillProccess("chromedriver.exe");                
            break;
            
            case "Edge":
                sistema.KillProccess("msedge.exe");
                sistema.KillProccess("msedgedriver.exe");
            break;
            
            case "Firefox":
                sistema.KillProccess("firefox.exe");
                sistema.KillProccess("geckodriver.exe");
            break;
        }
        
        /*try {
            Process pro = Runtime.getRuntime().exec("tasklist");
            BufferedReader reader = new BufferedReader(new InputStreamReader(pro.getInputStream()));
            String line;
            
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("chrome") && Navegador.contentEquals("Chrome")) {
                    Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe");
                    Runtime.getRuntime().exec("taskkill /F /IM chrome.exe");                    
                }
                else if(line.startsWith("msedge") && Navegador.contentEquals("Edge")) {
                    Runtime.getRuntime().exec("taskkill /F /IM msedge.exe");
                }
                else if(line.startsWith("firefox") && Navegador.contentEquals("Firefox")) {
                    Runtime.getRuntime().exec("taskkill /F /IM firefox.exe");
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }
    
    public byte[] ScreenShotElementFocus(By by)
    {
        try {
            cargaAjax();
        } catch (Exception ex) {
            //Logger.getLogger(OperacionesGenerales.class.getName()).log(Level.SEVERE, null, ex);
        }
        SelenElem.focus(by);
        return ((TakesScreenshot) Driver.WebDriver).getScreenshotAs(OutputType.BYTES);
    }
    
    public byte[] ScreenShotElement()
    {
        try {
            cargaAjax();
        } catch (Exception ex) {
            //Logger.getLogger(OperacionesGenerales.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ((TakesScreenshot) Driver.WebDriver).getScreenshotAs(OutputType.BYTES);
    }
    public byte[] ScreenShotElementInstant()
    {
       
        return ((TakesScreenshot) Driver.WebDriver).getScreenshotAs(OutputType.BYTES);
    }
    
    public byte[] ScreenShotElementAlert()
    {
        try {
            cargaAjax();
        } catch (Exception ex) {
            //Logger.getLogger(OperacionesGenerales.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ((TakesScreenshot) Driver.WebDriver.switchTo().frame("FrameLayou")).getScreenshotAs(OutputType.BYTES);
    }
    
    public byte[] ScreenShotElement(By by)
    {
        return ScreenShotElementFocus(by);
    }
    
    public byte[] SS(By by)
    {
        return ScreenShotElementFocus(by);
    }
    
    public byte[] SS()
    {
        return ScreenShotElement();
    }
    
    /*public byte[] ScreenShotElement(By by)
    {
        WebElement element = Driver.WebDriver.findElement(by);
        return element.getScreenshotAs(OutputType.BYTES);
    }*/
    
    public byte[] ScreenShotAll() 
    {
        Rectangle rectangleTam = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
        Robot robot;
        try {
            robot = new Robot();

            BufferedImage bufferedImage = robot.createScreenCapture(rectangleTam);
            try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                ImageIO.write(bufferedImage, "png", baos);
                return baos.toByteArray();
            } catch (IOException ex) {
                Logger.getLogger(Report.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (AWTException ex) {
            Logger.getLogger(Report.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }   
    
    public void SelectMenu(String nombre) throws InterruptedException{
        
        SelenElem.Click(By.xpath("//*[contains(text(),'"+ nombre +"')]"));
        //SelenElem.ClickByXpath(Driver.WebDriver, "//*[contains(text(),'"+ nombre +"')]");
        //SelenElem.ClickByXpath(Driver.WebDriver, "//*[contains(text(),'"+ nombre +"')]");
    }
}
