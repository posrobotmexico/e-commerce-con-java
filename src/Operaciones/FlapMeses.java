/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Operaciones;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import java.io.IOException;
import org.openqa.selenium.By;

/**
 *
 * @author LAPTOP-JORGE
 */
public class FlapMeses {
    
    public FlapMeses(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive) throws InterruptedException, IOException
    {
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("logo_client_mobile"),10);
        //reporte.AddImageToReport("Ingresamos a la pagina principal de FLAP", OPG.ScreenShotElement());
        SelenElem.WaitForLoad(By.id("mp2-detail-title"),10);
        reporte.AddImageToReport("Ingresamos a la pagina principal de FLAP", OPG.ScreenShotElement());
        Thread.sleep(3000);
        SelenElem.Click(By.id("mp2-detail-title"));
        SelenElem.loadElement(By.id("mp2-detail-table-last-tr"));
        SelenElem.focus(By.id("mp2-detail-table-last-tr"));   
        reporte.AddImageToReport("Vemos el detalle", OPG.ScreenShotElement());
        SelenElem.focus(By.id("command"));   
        reporte.AddImageToReport("Seleccionamos tarjeta de credito o debito", OPG.ScreenShotElement());
        
        SelenElem.WaitForLoad(By.id("mp2-container-dynamic"),5);
        reporte.AddImageToReport("Seleccionamos tarjeta de debito o credito visa/mastercard", OPG.ScreenShotElement());
        SelenElem.Click(By.id("mp2-button-logo-visa-master-card"));
        
        SelenElem.WaitForLoad(By.id("mp2-detail-title"),5);
        SelenElem.Click(By.id("mp2-detail-title"));
        SelenElem.focus(By.id("mp2-detail-title"));   
        reporte.AddImageToReport("Visualizamos nuevamente el detalle", OPG.ScreenShotElement());
        
        SelenElem.Click(By.id("contrato_108553_1"));
        
        SelenElem.focus(By.id("numTarjeta"));          
        SelenElem.TypeText(By.id("numTarjeta"), CargaVariables.LeerCSV(2,"Config","Tarjeta_Flap"));
        //SelenElem.TypeText(By.id("flap_token_expiration"), CargaVariables.LeerCSV(3,"Config","Tarjeta_Flap"));
        SelenElem.TypeText(By.id("cvv2"), CargaVariables.LeerCSV(4,"Config","Tarjeta_Flap"));     
        reporte.AddImageToReport("Llenamos los datos de pago y damos click en pagar", OPG.ScreenShotElement());
        SelenElem.Click(By.id("continuar"));
        SelenElem.WaitForLoad(By.id("mp2-summary-first-message"),5);
        reporte.AddImageToReport("Vemos el mensaje de pago de FLAP", OPG.ScreenShotElement());
        SelenElem.Click(By.id("mp2-summary-first-message"));
      
    }
    
}
