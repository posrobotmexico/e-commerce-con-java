
package Operaciones;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class CargaVariables {
    
    public static void EscribeCSV(int a,String text,String Ruta, String Archivo) 
    {
        FileWriter csvWriter;
        try {
            String toWrite = "";
            for(int i = 0; i < a-1; i++)
            {
                toWrite = toWrite + ",";
            }
            toWrite = toWrite + text;
            csvWriter = new FileWriter("A:\\DataEcommerce\\"+Ruta+"\\"+Archivo+".csv");
            csvWriter.append(toWrite);
            
            csvWriter.flush();
            csvWriter.close();
        
        } catch (IOException ex) {
            Logger.getLogger(CargaVariables.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static String LeerCSV(int a,String Ruta, String Archivo) 
    {   
        String User = null;
        try
        {
        // Abro el .csv en buffer de lectura
		BufferedReader bufferLectura = new BufferedReader(new FileReader("A:\\DataEcommerce\\"+Ruta+"\\"+Archivo+".csv"));
		
		// Leo una línea del archivo
		String linea = bufferLectura.readLine();
		
		while (linea != null) 
                {
			// Separa la línea leída con el separador definido previamente
			String[] campos = linea.split(String.valueOf(","));
                        User = campos[a];
			
			// Vuelvo a leer del fichero
			linea = bufferLectura.readLine();
		}
		
		// CIerro el buffer de lectura
		if (bufferLectura != null) 
                {
			bufferLectura.close();
		}
        }
        catch(Exception e)
        {
            
        }
        return User;
    }
    
}
