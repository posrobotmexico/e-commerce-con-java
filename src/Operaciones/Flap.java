/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operaciones;

import Core.Appium;
import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import io.appium.java_client.AppiumDriver;
import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

/**
 *
 * @author LAPTOP-JORGE
 */
public class Flap {
    
    public Flap(Report reporte,OperacionesGenerales OPG,WebAction SelenElem,WebDriv WebDrive) throws InterruptedException, IOException
    {
        SelenElem.waitForLoadPage();
        SelenElem.WaitForLoad(By.id("imagen-cliente"),10);
        //reporte.AddImageToReport("Ingresamos a la pagina principal de FLAP", OPG.ScreenShotElement());
        reporte.AddImageToReport("Ingresamos a la pagina principal de FLAP", OPG.ScreenShotElement());
        Thread.sleep(3000);
        SelenElem.focus(By.id("1"));
        reporte.AddImageToReport("Medio de pago Flap", OPG.ScreenShotElement());
        SelenElem.Click(By.id("1"));
        SelenElem.WaitForLoad(By.id("numTarjeta"), 15);
        //SelenElem.focus(By.id("contrato_108553_1"));
        //reporte.AddImageToReport("", OPG.ScreenShotElement());
        //SelenElem.Click(By.id("contrato_108553_1"));
        //SelenElem.loadElement(By.id("numTarjeta"));
        /*SelenElem.focus(By.id("numTarjeta"));   
        reporte.AddImageToReport("Vemos el detalle", OPG.ScreenShotElement());
        reporte.AddImageToReport("Seleccionamos tarjeta de credito o debito", OPG.ScreenShotElement());
        
        SelenElem.WaitForLoad(By.id("mp2-container-dynamic"),5);
        reporte.AddImageToReport("Seleccionamos tarjeta de debito o credito visa/mastercard", OPG.ScreenShotElement());
        SelenElem.Click(By.id("mp2-button-logo-visa-master-card"));
        
        SelenElem.WaitForLoad(By.id("mp2-detail-title"),5);
        SelenElem.Click(By.id("mp2-detail-title"));
        SelenElem.focus(By.id("mp2-detail-title"));   
        reporte.AddImageToReport("Visualizamos nuevamente el detalle", OPG.ScreenShotElement());
        */
        
        
        SelenElem.focus(By.id("numTarjeta"));          
        SelenElem.TypeText(By.id("numTarjeta"), CargaVariables.LeerCSV(2,"Config","Tarjeta_Flap"));
        //SelenElem.TypeText(By.id("flap_token_expiration"), CargaVariables.LeerCSV(3,"Config","Tarjeta_Flap"));
        SelenElem.TypeText(By.id("cvv2"), CargaVariables.LeerCSV(4,"Config","Tarjeta_Flap"));   
        SelenElem.TypeText(By.id("vigenciames"), CargaVariables.LeerCSV(5,"Config","Tarjeta_Flap"));
        SelenElem.TypeText(By.id("vigenciaanio"), CargaVariables.LeerCSV(6,"Config","Tarjeta_Flap"));
        reporte.AddImageToReport("Llenamos los datos de pago y damos click en pagar", OPG.ScreenShotElement());
        SelenElem.Click(By.id("continuar"));
        SelenElem.WaitForLoad(By.id("mp2-summary-first-message"),5);
        reporte.AddImageToReport("Vemos el mensaje de pago de FLAP", OPG.ScreenShotElement());
        SelenElem.Click(By.id("mp2-summary-first-message"));
      
    }
}
